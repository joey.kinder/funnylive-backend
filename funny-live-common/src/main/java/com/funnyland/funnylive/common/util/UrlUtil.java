package com.funnyland.funnylive.common.util;

public class UrlUtil {

    private static final String CDN_BASE_URL = "https://cdn.funnylive.co.kr";

    public static String convertToCdnUrl(String s3Key) {
        return String.format("%s/%s", CDN_BASE_URL, s3Key);
    }
}
