package com.funnyland.funnylive.common.exception;

public class ErrorResponse {
    private final int errorCode;
    private final String errorMessage;

    public ErrorResponse(ErrorCode errorCode) {
        this.errorCode = errorCode.getCode();
        this.errorMessage = errorCode.getMessage();
    }

    public ErrorResponse(int code, String message) {
        this.errorCode = code;
        this.errorMessage = message;
    }

    // Getters
    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
