package com.funnyland.funnylive.common.exception;

public enum ErrorCode {
    //시스템
    DOMAIN_NOT_FOUND(0000, "해당 항목을 찾을 수 없습니다"),
    JWT_EXPIRED(0001, "접근 만료입니다"),
    NOT_AUTHORIZED(0002, "해당 권한이 없습니다"),
    DUPLICATED_DOMAIN(0003, "서버 에러입니다"),

    //사용자
    USER_NOT_FOUND(1000, "해당 사용자를 찾을 수 없습니다"),
    USER_PASSWORD_NOT_EQUAL(1001, "비밀 번호가 일치하지 않습니다"),

    //크레인게임
    CRANE_GAME_NOT_FOUND(2000, "해당 크레인게임을 찾을 수 없습니다"),

    //결제
    PAYMENT_CONSUME_PRODUCT(3000, "결제 시스템에 문제가 있습니다"),
    PAYMENT_ALREADY_CANCELED(3001, "이미 취소된 결제건 입니다"),
    PAYMENT_FAILED_ACKNOWLEDGE(3002, "구매 승인 실패입니다"),
    PAYMENT_ALREADY_CONSUMED(3003, "이미 소비된 결제건입니다"),

    //포인트
    POINT_NOT_ENOUGH(3100, "포인트가 부족합니다"),

    //배송
    DELIVERY_NOT_FOUND(4000, "해당 배송을 찾을 수 없습니다"),

    //보관함
    STORAGE_BOX_NOT_FOUND(4100, "해당 보관함을 찾을 수 없습니다"),

    //문의
    INQUIRY_TYPE_NOT_FOUND(4200, "해당 문의 타입을 찾을 수 없습니다"),
    INQUIRY_NOT_FOUND(4201, "해당 문의를 찾을 수 없습니다"),
    INQUIRY_FILE_SIZE_EXCEED(4202, "파일 사이즈 10MB 초과입니다"),

    //업로드
    S3_CLIENT_ERROR(4300, "서버 업로드 에러입니다"),
    FILE_UPLOAD_ERROR(4301, "업로드 중 에러입니다");





    private final int code;
    private final String message;


    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
