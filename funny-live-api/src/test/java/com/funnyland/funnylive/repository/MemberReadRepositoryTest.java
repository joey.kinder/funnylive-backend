package com.funnyland.funnylive.repository;


//@ActiveProfiles("test")
//@ExtendWith(SpringExtension.class)
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@TestPropertySource(locations = "classpath:application-test.yml")
//class MemberReadRepositoryTest {
//
//    @Autowired
//    private MemberReadRepository memberReadRepository;
//
//    @Test
//    void 유저_저장_성공() {
//        Member member = Member.builder()
//                .name("퍼니1")
//                .email("funny@gmail.com")
//                .point(0).build();
//
//        Member savedMember = memberReadRepository.save(member);
//
//        Assertions.assertEquals(member.getId(), savedMember.getId());
//        Assertions.assertEquals(member.getName(), savedMember.getName());
//    }
//
//    @Test
//    void 유저_조회_실패() {
//        Member member = Member.builder()
//                .name("퍼니1")
//                .email("funny@gmail.com")
//                .point(0).build();
//
//        Member savedMember = memberReadRepository.save(member);
//
//        Optional<Member> findUser = memberReadRepository.findById(2L);
//
//        Assertions.assertNotEquals(savedMember.getId(), findUser.map(Member::getId)
//                .orElse(null));
//        Assertions.assertNull(findUser.map(Member::getId)
//                .orElse(null));
//    }
//
//    @Test
//    void 유저_조회_성공() {
//        Member member = Member.builder()
//                .name("퍼니1")
//                .email("funny@gmail.com")
//                .point(0).build();
//
//        Member savedMember = memberReadRepository.save(member);
//
//        Optional<Member> findUser = memberReadRepository.findById(member.getId());
//
//        Assertions.assertEquals(savedMember.getId(), findUser.map(Member::getId)
//                .orElse(null));
//    }
//}