package com.funnyland.funnylive.api.jwt;

import java.util.Base64;

import com.funnyland.funnylive.api.config.jwt.JwtTokenProvider;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;


//@ExtendWith(MockitoExtension.class)
//class JwtTokenProviderTest {
//
//    @InjectMocks
//    private JwtTokenProvider jwtTokenProvider;
//
//    @BeforeEach
//    void setUp() {
//        String secret = "abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcd";
//        byte[] keyBytes = Base64.getDecoder()
//                .decode(secret);
//        ReflectionTestUtils.setField(jwtTokenProvider, "secretKey", Keys.hmacShaKeyFor(keyBytes));
//    }
//
//    @Test
//    void jwt_토큰_검증_테스트() {
//
//        String accessToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJya2FhaHMyMDAwQG5hdmVyLmNvbSIsInJvbGUiOiJVU0VSIiwiaWF0IjoxNzAyMjIyMTE0LCJleHAiOjE3MDIyMjM5MTR9.Xy0wMdijTs7NIfL188nzj-9CrEsOMyLFSoR_IfeV6AE";
//        String uid = jwtTokenProvider.getUid(accessToken);
//
//        Assertions.assertEquals(uid, "aa");
//
//    }
//}