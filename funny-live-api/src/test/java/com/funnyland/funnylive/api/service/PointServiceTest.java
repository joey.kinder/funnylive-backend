package com.funnyland.funnylive.api.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.funnyland.funnylive.api.repository.PointTransactionReadRepository;
import com.funnyland.funnylive.api.repository.PointTransactionRepository;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


//class PointServiceTest {
//
//    private PointService pointService;
//
//    @Mock
//    private PointTransactionRepository pointTransactionRepository;
//
//    @Mock
//    private PointTransactionReadRepository pointTransactionReadRepository;
//
//    @Mock
//    private MemberDomainService memberDomainService;
//
//    @Mock
//    private ReceiptService receiptService;
//
//    @Mock
//    private PaymentService paymentService;
//
//    private Member member;
//
//    @BeforeEach
//    void setUp() {
//        MockitoAnnotations.openMocks(this);
//        pointService = new PointService(memberDomainService, receiptService, pointTransactionRepository, pointTransactionReadRepository, paymentService);
//        member = Member.builder().id(1L).email("test@example.com").name("Test User").point(300).build();
//    }
//
//    @Test
//    void 가지고있는_금액보다_적게_사용() {
//        // Given: 멤버가 유효기간이 다른 여러 포인트 트랜잭션을 가지고 있는 상태
//        PointTransaction transaction1 = new PointTransaction(100, 0, LocalDate.now(), LocalDate.now().plusDays(1), member, PointTransactionType.CHARGE);
//        PointTransaction transaction2 = new PointTransaction(200, 0, LocalDate.now(), LocalDate.now().plusDays(3), member, PointTransactionType.CHARGE);
//
//        List<PointTransaction> transactions = Arrays.asList(transaction1, transaction2);
//        given(pointTransactionReadRepository.findTransactionsOrderedByExpiration(member.getId())).willReturn(transactions);
//
//        // When: 150 포인트 사용
//        pointService.usePointsV2(member, PointTransactionType.GAME_PLAY, 150);
//
//        // Then: 첫 번째 트랜잭션(유효기간이 짧은)이 전부 사용되고, 두 번째 트랜잭션에서 50 포인트 사용
//        assertThat(transaction1.getUsedAmount()).isEqualTo(100);
//        assertThat(transaction2.getUsedAmount()).isEqualTo(50);
//    }
//
//    @Test
//    void 가지고있는_금액_정확히_사용() {
//        // Given: 멤버가 유효기간이 다른 여러 포인트 트랜잭션을 가지고 있는 상태
//        PointTransaction transaction1 = new PointTransaction(100, 0, LocalDate.now(), LocalDate.now().plusDays(1), member, PointTransactionType.CHARGE);
//        PointTransaction transaction2 = new PointTransaction(200, 0, LocalDate.now(), LocalDate.now().plusDays(3), member, PointTransactionType.CHARGE);
//
//        List<PointTransaction> transactions = Arrays.asList(transaction1, transaction2);
//        given(pointTransactionReadRepository.findTransactionsOrderedByExpiration(member.getId())).willReturn(transactions);
//
//        // When: 150 포인트 사용
//        pointService.usePointsV2(member, PointTransactionType.GAME_PLAY, 300);
//
//        // Then: 첫 번째 트랜잭션(유효기간이 짧은)이 전부 사용되고, 두 번째 트랜잭션에서 50 포인트 사용
//        assertThat(transaction1.getUsedAmount()).isEqualTo(100);
//        assertThat(transaction2.getUsedAmount()).isEqualTo(200);
//    }
//
//    @Test
//    void 가지고있는_금액보다_더_사용() {
//        // Given: 멤버가 유효기간이 다른 여러 포인트 트랜잭션을 가지고 있는 상태
//        PointTransaction transaction1 = new PointTransaction(100, 0, LocalDate.now(), LocalDate.now().plusDays(1), member, PointTransactionType.CHARGE);
//        PointTransaction transaction2 = new PointTransaction(200, 0, LocalDate.now(), LocalDate.now().plusDays(3), member, PointTransactionType.CHARGE);
//
//        List<PointTransaction> transactions = Arrays.asList(transaction1, transaction2);
//        given(pointTransactionReadRepository.findTransactionsOrderedByExpiration(member.getId())).willReturn(transactions);
//
//        assertThrows(IllegalStateException.class, () -> pointService.usePointsV2(member, PointTransactionType.GAME_PLAY, 400));
//    }
//
//}