package com.funnyland.funnylive;

import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


@SpringBootApplication
@EnableJpaAuditing
public class FunnyLiveAPIApplication {

    public static void main(String[] args) {
        String profile = System.getProperty("spring.profiles.active");

        if ("local".equals(profile)) {
            Dotenv dotenv = Dotenv.load();
            System.setProperty("AWS_ACCESS_KEY", dotenv.get("AWS_ACCESS_KEY"));
            System.setProperty("AWS_SECRET_KEY", dotenv.get("AWS_SECRET_KEY"));
            System.setProperty("AWS_RDS_USER", dotenv.get("AWS_RDS_USER"));
            System.setProperty("AWS_RDS_PASSWORD", dotenv.get("AWS_RDS_PASSWORD"));
            System.setProperty("KAKAO_CLIENT_ID", dotenv.get("KAKAO_CLIENT_ID"));
            System.setProperty("KAKAO_CLIENT_SECRET", dotenv.get("KAKAO_CLIENT_SECRET"));
            System.setProperty("NAVER_CLIENT_ID", dotenv.get("NAVER_CLIENT_ID"));
            System.setProperty("NAVER_CLIENT_SECRET", dotenv.get("NAVER_CLIENT_SECRET"));
            System.setProperty("GOOGLE_CLIENT_ID", dotenv.get("GOOGLE_CLIENT_ID"));
            System.setProperty("GOOGLE_CLIENT_SECRET", dotenv.get("GOOGLE_CLIENT_SECRET"));
            System.setProperty("SLACK_OAUTH_TOKEN", dotenv.get("SLACK_OAUTH_TOKEN"));
        }

        SpringApplication.run(FunnyLiveAPIApplication.class, args);
    }

}
