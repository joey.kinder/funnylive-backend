package com.funnyland.funnylive.api.endpoint.upload;

import com.funnyland.funnylive.api.service.StorageService;
import com.funnyland.funnylive.api.service.dto.StorageType;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@Tag(name="업로드", description = "업로드 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/upload")
@Slf4j
public class UploadController {

    private final StorageService storageService;

    @Operation(summary = "이미지 파일 업로드", description = "이미지 파일 업로드")
    @PostMapping
    public String create(@ModelAttribute MultipartFile attachment) {
        if (!ObjectUtils.isEmpty(attachment)) {
            // 파일 크기 제한 검사 (30MB)
            if (attachment.getSize() > 30 * 1024 * 1024) {
                throw new RuntimeException("File size exceeds the limit (30MB)");
            }
            UploadDto uploadDto = storageService.upload(attachment, StorageType.IMAGE);
            return uploadDto.getFilePath();
        }
        return "실패";
    }

}
