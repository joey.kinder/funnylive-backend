package com.funnyland.funnylive.api.endpoint.delivery.controller;

import com.funnyland.funnylive.api.endpoint.delivery.request.DeliveryRequest;
import com.funnyland.funnylive.api.endpoint.delivery.response.DeliveryDetailResponse;
import com.funnyland.funnylive.api.endpoint.delivery.response.DeliveryResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.DeliveryService;
import com.funnyland.funnylive.api.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="배송", description = "배송 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/deliveries")
public class DeliveryController {

    private final DeliveryService deliveryService;

    @PostMapping
    @Operation(summary = "배송 요청", description = "배송 요청")
    public void delivery(@RequestBody @Valid DeliveryRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        deliveryService.processDelivery(user, request);
    }

    @GetMapping("/me")
    @Operation(summary = "내 배송 리스트 조회", description = "내 배송 리스트 조회")
    public DeliveryResponse getMyDeliveries(Pageable pageable) {
        SecurityUserDto user = SecurityUtils.getUser();
        return deliveryService.getDeliveries(user.getMemberId(), pageable);
    }

    @GetMapping("/{deliveryId}")
    @Operation(summary = "배송 상세 조회", description = "배송 상세 조회")
    public DeliveryDetailResponse getDelivery(@PathVariable Long deliveryId) {
        SecurityUserDto user = SecurityUtils.getUser();
        return deliveryService.getDelivery(user, deliveryId);
    }

}
