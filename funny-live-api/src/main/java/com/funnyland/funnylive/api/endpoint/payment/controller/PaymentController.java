package com.funnyland.funnylive.api.endpoint.payment.controller;

import com.funnyland.funnylive.api.endpoint.payment.request.PaymentRequest;
import com.funnyland.funnylive.api.service.PaymentService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/payments")
public class PaymentController {

    private final PaymentService paymentService;

    @PostMapping
    @Operation(summary = "결제 검증", description = "결제 검증")
    public boolean create(@RequestBody PaymentRequest request) {
        return paymentService.processGoogleInAppPurchase(request.getPackageName(), request.getProductId(), request.getPurchaseToken());
    }
}
