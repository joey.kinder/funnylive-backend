package com.funnyland.funnylive.api.endpoint.livebroadcast.response;

import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcast;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LiveBroadcastResponse {

    private Long liveBroadcastId;

    private String title;
    private String description;
    private String url;

    public static LiveBroadcastResponse of(LiveBroadcast liveBroadcast) {
        return LiveBroadcastResponse.builder()
                .liveBroadcastId(liveBroadcast.getId())
                .title(liveBroadcast.getTitle())
                .url(liveBroadcast.getUrl())
                .build();
    }
}
