package com.funnyland.funnylive.api.endpoint.point.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PointRequest {
    @NotNull
    private int amount;
    @NotNull
    private int bonusPercent;

    private String orderId;
    @NotNull
    private String packageName;
    @NotNull
    private String productId;
    private String purchaseTime;
    private int purchaseState;
    @NotNull
    private String purchaseToken;
    private int quantity;
    private boolean acknowledged;

    @Override
    public String toString() {
        return "PointRequest{" +
                "amount=" + amount +
                ", bonusPercent=" + bonusPercent +
                ", orderId='" + orderId + '\'' +
                ", packageName='" + packageName + '\'' +
                ", productId='" + productId + '\'' +
                ", purchaseTime='" + purchaseTime + '\'' +
                ", purchaseState=" + purchaseState +
                ", purchaseToken='" + purchaseToken + '\'' +
                ", quantity=" + quantity +
                ", acknowledged=" + acknowledged +
                '}';
    }

}
