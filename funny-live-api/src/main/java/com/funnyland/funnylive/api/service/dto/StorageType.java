package com.funnyland.funnylive.api.service.dto;

public enum StorageType {
    IMAGE,
    ETC;

    public boolean isImage() {
        return this.equals(IMAGE);
    }
}
