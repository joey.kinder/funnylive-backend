package com.funnyland.funnylive.api.slack;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum SlackConstant {

    ERROR_CHANNEL("#크레인알림");

    private String channel;

}
