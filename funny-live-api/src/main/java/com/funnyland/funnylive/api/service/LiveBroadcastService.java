package com.funnyland.funnylive.api.service;

import com.funnyland.funnylive.api.endpoint.livebroadcast.response.LiveBroadcastResponse;
import com.funnyland.funnylive.api.repository.LiveBroadcastReadRepository;
import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcast;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class LiveBroadcastService {

    private final LiveBroadcastReadRepository liveBroadcastReadRepository;

    /**
     * 라이브 방송 리스트 조회
     */
    public Page<LiveBroadcastResponse> getLiveBroadcasts(Pageable pageable) {
        Page<LiveBroadcast> findLiveBroadcasts = liveBroadcastReadRepository.findAll(pageable);
        return findLiveBroadcasts.map(LiveBroadcastResponse::of);
    }
}
