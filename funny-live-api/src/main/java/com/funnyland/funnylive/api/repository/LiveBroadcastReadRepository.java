package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcast;
import org.springframework.data.jpa.repository.JpaRepository;


public interface LiveBroadcastReadRepository extends JpaRepository<LiveBroadcast, Long> {
}
