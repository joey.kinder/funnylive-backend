package com.funnyland.funnylive.api.endpoint.inquiry.response;

import java.util.List;

import com.funnyland.funnylive.domain.inquiry.Inquiry;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class InquiryMyPageResponse {

    private Long notReplyCount;
    private Long completeReplyCount;

    public static InquiryMyPageResponse of(List<Inquiry> inquiries) {
        long notReplyCount = inquiries.stream()
                .filter(inquiry -> inquiry.getStatus() == Inquiry.InquiryStatus.PENDING)
                .count();
        long completeReplyCount = inquiries.size() - notReplyCount;
        return new InquiryMyPageResponse(notReplyCount, completeReplyCount);
    }
}
