package com.funnyland.funnylive.api.service;

import java.time.LocalDateTime;
import java.util.Optional;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.api.repository.ReceiptRepository;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.receipt.PaymentMethod;
import com.funnyland.funnylive.domain.receipt.Receipt;
import com.funnyland.funnylive.domain.receipt.ReceiptState;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
@Slf4j
public class ReceiptService {

    private final ReceiptRepository receiptRepository;

    @Transactional
    public void saveReceipt(Member member, String packageName, String orderId, String productId, String purchaseToken, LocalDateTime purchaseTime, int amount, int totalPoint) {
        Receipt receipt = Receipt.builder()
                .member(member)
                .packageName(packageName)
                .orderId(orderId)
                .productId(productId)
                .purchaseToken(purchaseToken)
                .purchaseTime(purchaseTime)
                .acknowledged(false)
                .paymentMethod(PaymentMethod.CARD)
                .state(ReceiptState.PENDING)
                .paymentAmount(amount)
                .chargeAmount(totalPoint)
                .build();
        receiptRepository.save(receipt);
    }

    @Transactional
    public void acknowledgeReceipt(String purchaseToken) {
        Optional<Receipt> receiptOptional = receiptRepository.findByPurchaseToken(purchaseToken);
        if (receiptOptional.isPresent()) {
            Receipt receipt = receiptOptional.get();
            receipt.setAcknowledged(true);
            receipt.setState(ReceiptState.COMPLETE);
        } else {
            log.error("ReceiptService.acknowledgeReceipt : receipt not found purchaseToken={}", purchaseToken);
            throw new CustomException(ErrorCode.DOMAIN_NOT_FOUND);
        }
    }
}
