package com.funnyland.funnylive.api.endpoint.member.controller;

import java.util.List;

import com.funnyland.funnylive.api.endpoint.member.facade.MemberFacade;
import com.funnyland.funnylive.api.endpoint.member.request.MemberRequest;
import com.funnyland.funnylive.api.endpoint.member.request.MemberUpdateRequest;
import com.funnyland.funnylive.api.endpoint.member.request.MemberWithdrawRequest;
import com.funnyland.funnylive.api.endpoint.member.response.MemberResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Tag(name="사용자", description = "사용자 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/members")
public class MemberController {

    private final MemberFacade memberFacade;

    @GetMapping("/{memberId}")
    @Operation(summary = "사용자 조회", description = "사용자 조회")
    public MemberResponse getMember(@PathVariable Long memberId) {
        SecurityUserDto user = SecurityUtils.getUser();
        return memberFacade.getMember(memberId, user);
    }

    @GetMapping
    @Operation(summary = "사용자 리스트 조회", description = "사용자 리스트 조회")
    public List<MemberResponse> getMembers() {
        return memberFacade.getMembers();
    }

    @PostMapping("/check")
    @Operation(summary = "사용자 닉네임 체크", description = "사용자 닉네임 체크")
    public boolean checkMemberName(@RequestParam String nickName) {
        return memberFacade.checkNickName(nickName);
    }

    @PutMapping("/{memberId}/withdraw")
    @Operation(summary = "사용자 탈퇴", description = "사용자 탈퇴")
    public boolean withdraw(@PathVariable Long memberId, @RequestBody @Valid MemberWithdrawRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        return memberFacade.withdraw(memberId, request.getReason(), user);
    }

    @PostMapping
    @Operation(summary = "사용자 회원가입", description = "사용자 회원가입")
    public MemberResponse createMember(@Valid @RequestBody MemberRequest memberRequest) {
        MemberResponse member = memberFacade.createMember(memberRequest);
        return member;
    }

    @PutMapping("/{memberId}")
    @Operation(summary = "사용자 정보 수정", description = "사용자 정보 수정")
    public MemberResponse updateMember(@PathVariable Long memberId, @RequestBody MemberUpdateRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        return memberFacade.updateMember(memberId, request, user);
    }

    @GetMapping("/me")
    @Operation(summary = "내 정보 조회", description = "내 정보 조회")
    public MemberResponse getMember() {
        SecurityUserDto user = SecurityUtils.getUser();
        return memberFacade.getMember(user.getMemberId(), user);
    }

}