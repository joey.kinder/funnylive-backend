package com.funnyland.funnylive.api.endpoint.inquiry.facade;

import java.util.List;

import com.funnyland.funnylive.api.endpoint.inquiry.request.InquiryRequest;
import com.funnyland.funnylive.api.endpoint.inquiry.response.InquiryMyPageResponse;
import com.funnyland.funnylive.api.endpoint.inquiry.response.InquiryResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.InquiryService;
import com.funnyland.funnylive.api.service.MemberService;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.member.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
public class InquiryFacade {

    private final InquiryService inquiryService;
    private final MemberService memberService;

    public InquiryResponse create(InquiryRequest request, Long userId, MultipartFile file) {
        Member member = memberService.getMember(userId);
        Inquiry savedInquiry = inquiryService.create(request, member, file);
        return InquiryResponse.of(savedInquiry);
    }

    public Page<InquiryResponse> getInquiries(Pageable pageable) {
        Page<Inquiry> inquiries = inquiryService.getInquiries(pageable);
        return inquiries.map(InquiryResponse::of);
    }

    public InquiryResponse update(Long inquiryId, InquiryRequest request, MultipartFile file, SecurityUserDto user) {

        Inquiry savedInquiry = inquiryService.update(inquiryId, request, file, user);
        return InquiryResponse.of(savedInquiry);
    }

    public InquiryResponse getInquiry(Long inquiryId) {
        Inquiry findInquiry = inquiryService.getInquiry(inquiryId);
        return InquiryResponse.of(findInquiry);
    }

    public Page<InquiryResponse> getMyInquiry(Long memberId, Pageable pagable) {
        Member member = memberService.getMember(memberId);
        Page<Inquiry> inquiries = inquiryService.getInquiries(member, pagable);
        return inquiries.map(InquiryResponse::of);
    }

    public InquiryMyPageResponse getMyInquiryInMyPage(Long memberId) {
        Member member = memberService.getMember(memberId);
        List<Inquiry> inquiries = inquiryService.getInquiries(member);
        return InquiryMyPageResponse.of(inquiries);
    }
}
