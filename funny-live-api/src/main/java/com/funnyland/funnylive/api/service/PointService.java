package com.funnyland.funnylive.api.service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.api.repository.PointTransactionDetailRepository;
import com.funnyland.funnylive.api.repository.PointTransactionReadRepository;
import com.funnyland.funnylive.api.repository.PointTransactionRepository;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.point.PointTransactionDetail;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;


@RequiredArgsConstructor
@Service
@Slf4j
@Transactional
public class PointService {

    private final MemberDomainService memberDomainService;
    private final ReceiptService receiptService;
    private final PointTransactionRepository pointTransactionRepository;
    private final PointTransactionReadRepository pointTransactionReadRepository;
    private final PaymentService paymentService;
    private final Environment environment;

    /**
     * 포인트 충전
     */
    @Transactional
    public void chargePoints(Member member, int amount, double bonusPercent, String packageName, String productId, String purchaseToken, String orderId) {
        log.info("PointService.chargePoints : Charging points for member={}, amount={}", member.getId(), amount);
        int bonusPoint = (int) (amount * ((double) bonusPercent / 100.0));
        int totalPoint = amount + bonusPoint;
        boolean isPurchaseSuccessful = true;

        if (!isLocalEnvironment()) {
            isPurchaseSuccessful = processInAppPurchase(member, packageName, productId, purchaseToken, orderId, amount, totalPoint);
        }
        if (isPurchaseSuccessful) {
            processPointTransaction(totalPoint, bonusPoint, member, PointTransactionType.CHARGE);
            log.info("PointService.chargePoints : Points charged for member={}, total points={}", member.getId(), totalPoint);
        }
    }

    /**
     * 포인트 사용
     */
    @Transactional
    public void usePoints(Member member, PointTransactionType type, int amount) {
        List<PointTransaction> transactions = pointTransactionReadRepository
                .findTransactionsOrderedByExpiration(member.getId());

        int remainingAmountToUse = amount;

        PointTransaction totalUsedTransaction = PointTransaction.builder()
                                                                .amount(-amount)
                                                                .bonusAmount(0)
                                                                .member(member)
                                                                .transactionType(type)
                                                                .transactionChange(PointTransactionType.TransactionChange.DECREASE)
                                                                .build();
        pointTransactionRepository.save(totalUsedTransaction);

        for (PointTransaction transaction : transactions) {
            int availablePoints = getAvailablePointsForTransaction(transaction); // 남은 포인트 사용
            if(availablePoints <= 0 ){
                continue;
            }
            int usedAmount = Math.min(availablePoints, remainingAmountToUse);
            transaction.createDetail(-usedAmount, member, totalUsedTransaction, type); // PointTransactionDetail 생성
            remainingAmountToUse -= usedAmount;
            if (remainingAmountToUse == 0) {
                break; // 남은 금액이 0이면 루프 종료
            }
        }

        if (remainingAmountToUse > 0) {
            throw new CustomException(ErrorCode.POINT_NOT_ENOUGH);
        }
    }

    /**
     * 포인트 트랜잭션 조회
     */
    public Page<PointTransaction> getPointTransactions(Member member, Pageable pageable) {
        return pointTransactionRepository.findAllByMemberId(member.getId(), pageable);
    }

    /**
     * 포인트 환불
     */
    public void refundPoints(Member member, int amount) {
        memberDomainService.save(member);
        PointTransaction transaction = new PointTransaction(amount, member, PointTransactionType.REFUND);
        pointTransactionRepository.save(transaction);
    }

    /**
     * 포인트 내역 삭제
     */
    public void deletePointTransactionForWithdraw(Long memberId) {
        pointTransactionRepository.deleteByMemberId(memberId);
        log.info("포인트 내역 삭제 성공. memberId: {} ", memberId);
    }

    /**
     * 포인트 조회
     */
    public int getPoint(Long memberId) {
        return pointTransactionReadRepository.getSumPoint(memberId);
    }

    /**
     * 가능한 사용포인트 계산
     */
    private int getAvailablePointsForTransaction(PointTransaction transaction) {
        if (transaction == null || transaction.getDetails() == null) {
            return transaction.getAmount();
        }
        int availablePoint = transaction.getDetails()
                             .stream()
                             .mapToInt(PointTransactionDetail::getAmount)
                             .sum();

        return availablePoint;
    }

    /**
     * 포인트 트랜잭션 생성
     */
    private void processPointTransaction(int totalPoint, int bonusPoint, Member member, PointTransactionType pointTransactionType) {
        PointTransaction transaction = PointTransaction.builder()
                .amount(totalPoint)
                .bonusAmount(bonusPoint)
                .member(member)
                .transactionType(pointTransactionType)
                .transactionChange(pointTransactionType.getChange())
                .build();
        transaction.createDetail(totalPoint, member, transaction, pointTransactionType);
        pointTransactionRepository.save(transaction);
    }

    /**
     * 인앱 결제 검증 (결제 검증, 영수증 db 저장, 아이템 소비 로직)
     */
    private boolean processInAppPurchase(Member member, String packageName, String productId, String purchaseToken, String orderId, int amount, int totalPoint) {
        boolean paymentVerified = paymentService.processGoogleInAppPurchase(packageName, productId, purchaseToken);

        if (!paymentVerified) {
            log.warn("PointService.processInAppPurchase : Payment verification failed for member={}", member.getId());
            return false;
        }

        receiptService.saveReceipt(member, packageName, orderId, productId, purchaseToken, LocalDateTime.now(), amount, totalPoint);
        log.info("PointService.processInAppPurchase : Receipt saved for member={}, purchaseToken={}", member.getId(), purchaseToken);

        boolean isConsumed = paymentService.consumeProduct(packageName, productId, purchaseToken);
        if(isConsumed) {
            receiptService.acknowledgeReceipt(purchaseToken);
        }
        return true;
    }

    private boolean isLocalEnvironment() {
        return Arrays.asList(environment.getActiveProfiles()).contains("local");
    }
}
