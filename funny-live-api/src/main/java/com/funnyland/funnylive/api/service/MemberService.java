package com.funnyland.funnylive.api.service;

import java.util.List;

import com.funnyland.funnylive.api.endpoint.member.request.MemberUpdateRequest;
import com.funnyland.funnylive.api.repository.MemberReadRepository;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
@Slf4j
public class MemberService {

    private final MemberReadRepository memberReadRepository;
    private final MemberDomainService memberDomainService;
    private final PointService pointService;

    /**
     * id로 member 상세 조회
     */
    @Transactional(readOnly = true)
    public Member getMember(Long memberId) {
        return memberDomainService.find(memberId);
    }


    /**
     * email로 member 상세 조회
     */
    @Transactional(readOnly = true)
    public Member getMember(String email) {
        return memberReadRepository.findByEmail(email).orElseThrow(()-> new CustomException(ErrorCode.USER_NOT_FOUND));
    }

    /**
     * member 중복 여부
     */
    public boolean existMember(String nickName) {
        return memberReadRepository.existsMemberByNickName(nickName);
    }

    /**
     * member 회원가입
     */
    @Transactional
    public Member saveMember(Member member) {
        Member savedMember = memberDomainService.save(member);
        return savedMember;
    }

    /**
     * member 탈퇴
     * 탈퇴시 탈퇴 이유와 상태를 제외하고 다 제거
     * 포인트 내역도 삭제
     */
    @Transactional
    public void withdraw(Long memberId, String withdrawReason) {
        memberDomainService.delete(memberId, withdrawReason);
        pointService.deletePointTransactionForWithdraw(memberId);
    }

    /**
     * member 리스트 조회
     */
    public List<Member> getMembers() {
        List<Member> members = memberDomainService.list();
        return members;
    }

    /**
     * member 업데이트
     */
    @Transactional
    public Member update(Long memberId, MemberUpdateRequest request, SecurityUserDto user) {
        Member findMember = memberDomainService.find(memberId);
        user.authVerify(findMember.getId());

        Member updateMember = findMember.edit(request.toDomain());
        return updateMember;
    }

    /**
     * member 방문시간 기록
     */
    @Transactional
    public void saveLoginTime(String email) {
        Member member = getMember(email);
        member.saveLoginTime();
        log.debug("MemberService.saveLoginTime : memberId = {}", member.getId());
    }
}
