package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.api.domain.CraneGameHeartbeat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CraneGameHeartbeatRepository extends CrudRepository<CraneGameHeartbeat, Long> {
}
