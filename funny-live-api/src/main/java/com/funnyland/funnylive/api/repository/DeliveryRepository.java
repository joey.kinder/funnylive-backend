package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.domain.delivery.Delivery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DeliveryRepository extends JpaRepository<Delivery, Long> {

    long countByMemberIdAndStatus(Long memberId, Delivery.DeliveryStatus status);

    Page<Delivery> findByMemberId(Long memberId, Pageable pageable);

}