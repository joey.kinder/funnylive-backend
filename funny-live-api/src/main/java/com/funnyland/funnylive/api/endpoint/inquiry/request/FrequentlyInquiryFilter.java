package com.funnyland.funnylive.api.endpoint.inquiry.request;

import static com.funnyland.funnylive.domain.inquiry.QFrequentlyInquiry.frequentlyInquiry;

import com.funnyland.funnylive.domain.inquiry.InquiryType;
import com.querydsl.core.BooleanBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(title = "자주묻는 질문 검색필터")
public class FrequentlyInquiryFilter {

    @Schema(title = "자주묻는 질문 타입 id", example = "1")
    private Long typeId;

    /**
     * 타입 아이디 추가.
     * - frequentlyInquiry.typeId in (this.typeId)
     *
     * @param builder query dsl boolean builder
     * */
    private void typeBuilder(BooleanBuilder builder) {
        if(this.typeId != null) {
            builder.and(frequentlyInquiry.type.id.eq(this.typeId));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        typeBuilder(builder);

        return builder;
    }
}
