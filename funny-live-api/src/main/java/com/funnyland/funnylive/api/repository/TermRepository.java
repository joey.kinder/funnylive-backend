package com.funnyland.funnylive.api.repository;

import java.util.Optional;

import com.funnyland.funnylive.domain.term.Term;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TermRepository extends JpaRepository<Term, Long> {
    Optional<Term> findTopByTypeOrderByCreatedAtDesc(Term.TermType type);
}
