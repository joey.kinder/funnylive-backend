package com.funnyland.funnylive.api.repository;

import static com.funnyland.funnylive.domain.product.QProduct.product;

import java.util.List;

import com.funnyland.funnylive.domain.product.Product;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class ProductReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public ProductReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(Product.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<Product> getProducts(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<Product> query = jpaQueryFactory.selectFrom(product)
                .where(builder);

        List<Product> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(product.count())
                .from(product)
                .where(builder);
        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }
}
