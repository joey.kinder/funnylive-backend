package com.funnyland.funnylive.api.endpoint.inquiry.request;

import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FrequentlyInquiryRequest {
    @NotNull
    private String question;
    @NotNull
    private String answer;
    @NotNull
    private Long inquiryTypeId;

    public FrequentlyInquiry toDomain() {
        FrequentlyInquiry frequentlyInquiry = FrequentlyInquiry.builder()
                .question(this.question)
                .answer(this.answer)
                 .build();
        return frequentlyInquiry;
    }
}