package com.funnyland.funnylive.api.endpoint.cranegame.request;

import static com.funnyland.funnylive.domain.cranegame.QCraneGame.craneGame;

import com.querydsl.core.BooleanBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(title = "크레인 기기 검색필터")
public class CraneGameFilter {

    @Schema(title = "플레이 요금", example = "500")
    private Integer playFee;

    /**
     * 플레이 요금 조건 추가.
     * - crane_game.playFee in (this.providerType)
     *
     * @param builder query dsl boolean builder
     * */
    private void playFeeBuilder(BooleanBuilder builder) {
        if(this.playFee != null) {
            builder.and(craneGame.playFee.eq(this.playFee));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        playFeeBuilder(builder);

        return builder;
    }
}
