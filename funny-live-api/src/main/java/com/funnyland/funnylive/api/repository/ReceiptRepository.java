package com.funnyland.funnylive.api.repository;

import java.util.Optional;

import com.funnyland.funnylive.domain.receipt.Receipt;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ReceiptRepository extends JpaRepository<Receipt, Long> {
    Optional<Receipt> findByPurchaseToken(String purchaseToken);
}
