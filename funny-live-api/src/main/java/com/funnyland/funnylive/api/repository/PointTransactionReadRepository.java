package com.funnyland.funnylive.api.repository;

import static com.funnyland.funnylive.domain.point.QPointTransaction.pointTransaction;
import static com.funnyland.funnylive.domain.point.QPointTransactionDetail.pointTransactionDetail;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import com.funnyland.funnylive.domain.point.QPointTransactionDetail;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;


@Repository
public class PointTransactionReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public PointTransactionReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(PointTransaction.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public List<PointTransaction> findTransactionsOrderedByExpiration(Long memberId) {

        LocalDate today = LocalDate.now();
        BooleanExpression isStartDateValid = pointTransaction.expirationStartDate.isNotNull()
                                                                                 .and(pointTransaction.expirationStartDate.loe(today));
        BooleanExpression isEndDateValid = pointTransaction.expirationEndDate.isNotNull()
                                                                             .and(pointTransaction.expirationEndDate.goe(today));
        BooleanExpression isExpirationDateValid = isStartDateValid.and(isEndDateValid);
        BooleanExpression isExpirationDateNull = pointTransaction.expirationEndDate.isNull();

        // 서브쿼리: 각 포인트 트랜잭션에 대한 사용된 포인트의 합계 계산
        JPQLQuery<Integer> usedAmountSubquery = JPAExpressions
                .select(new CaseBuilder()
                        .when(pointTransactionDetail.amount.sum().isNull())
                        .then(0)
                        .otherwise(pointTransactionDetail.amount.sum()))
                .from(pointTransactionDetail)
                .where(pointTransactionDetail.savePointTransaction.id.eq(pointTransaction.id)
                                                                     .and(pointTransactionDetail.transactionChange.eq(PointTransactionType.TransactionChange.DECREASE)));


        // 결과 프로젝션
        QBean<PointTransaction> projection = Projections.bean(
                PointTransaction.class,
                pointTransaction.id,
                pointTransaction.transactionType,
                pointTransaction.transactionChange,
                pointTransaction.amount,
                pointTransaction.bonusAmount,
                pointTransaction.expirationStartDate,
                pointTransaction.expirationEndDate,
                pointTransaction.reason,
                pointTransaction.member,
                Expressions.asNumber(pointTransaction.amount.add(usedAmountSubquery))
                           .as("remainingAmount")
        );

        // 사용 가능한 포인트가 있는 트랜잭션만 선택
        BooleanExpression hasAvailablePoints = pointTransaction.amount
                                                                      .add(usedAmountSubquery)
                                                                      .gt(0);

        OrderSpecifier<Integer> orderByExpirationValidity = new CaseBuilder()
                .when(isExpirationDateValid).then(1)
                .otherwise(2).asc();
        OrderSpecifier<LocalDate> orderByExpirationDate = pointTransaction.expirationEndDate.asc();

        return jpaQueryFactory
                .selectFrom(pointTransaction)
                .leftJoin(pointTransaction.details, pointTransactionDetail)
                .fetchJoin()
                .where(pointTransaction.member.id.eq(memberId)
                                                 .and(pointTransaction.transactionChange.eq(PointTransactionType.TransactionChange.INCREASE))
                                                 .and(hasAvailablePoints)
                                                 .and(isExpirationDateValid.or(isExpirationDateNull)))
                .orderBy(orderByExpirationValidity, orderByExpirationDate)
                .fetch();
    }

    public int getSumPoint(Long memberId) {
        Integer sum = jpaQueryFactory
                .select(pointTransaction.amount.sum())
                .from(pointTransaction)
                .where(pointTransaction.member.id.eq(memberId))
                .fetchOne();

        return sum != null ? sum : 0;
    }
}
