package com.funnyland.funnylive.api.util;

import com.funnyland.funnylive.api.security.SecurityUserDto;
import org.springframework.security.core.context.SecurityContextHolder;


public abstract class SecurityUtils {

    public static String getUserId() {
        return ((SecurityUserDto)(SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getEmail();
    }

    public static SecurityUserDto getUser() {
        return (SecurityUserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
