package com.funnyland.funnylive.api.repository;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.deliveryaddress.DeliveryAddress;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DeliveryAddressRepository extends JpaRepository<DeliveryAddress, Long> {

    List<DeliveryAddress> findByMemberId(Long memberId);
    Optional<DeliveryAddress> findByIdAndMemberId(Long id, Long memberId);

    Optional<DeliveryAddress> deleteByIdAndMemberId(Long id, Long memberId);

}
