package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.domain.point.PointTransaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PointTransactionRepository extends JpaRepository<PointTransaction, Long> {

    Page<PointTransaction> findAllByMemberId(Long memberId, Pageable pageable);

    void deleteByMemberId(Long memberId);

}
