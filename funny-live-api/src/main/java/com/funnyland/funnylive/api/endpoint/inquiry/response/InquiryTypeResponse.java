package com.funnyland.funnylive.api.endpoint.inquiry.response;

import com.funnyland.funnylive.domain.inquiry.InquiryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class InquiryTypeResponse {

    private Long inquiryTypeId;
    private String name;

    public static InquiryTypeResponse of(InquiryType inquiryType) {
        return InquiryTypeResponse.builder()
                .inquiryTypeId(inquiryType.getId())
                .name(inquiryType.getName())
                .build();
    }


}
