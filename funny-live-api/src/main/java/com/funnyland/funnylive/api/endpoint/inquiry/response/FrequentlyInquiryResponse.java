package com.funnyland.funnylive.api.endpoint.inquiry.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class FrequentlyInquiryResponse {

    private Long frequentlyInquiryId;
    private String question;
    private String answer;
    private Long typeId;
    private String type;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;

    public static FrequentlyInquiryResponse of(FrequentlyInquiry frequentlyInquiry) {

        return FrequentlyInquiryResponse.builder()
                                        .frequentlyInquiryId(frequentlyInquiry.getId())
                                        .question(frequentlyInquiry.getQuestion())
                                        .answer(frequentlyInquiry.getAnswer())
                                        .createdAt(frequentlyInquiry.getCreatedAt())
                                        .type(frequentlyInquiry.getType() != null ? frequentlyInquiry.getType().getName() : null)
                                        .typeId(frequentlyInquiry.getType() != null ? frequentlyInquiry.getType().getId() : null)
                                        .build();
    }

}
