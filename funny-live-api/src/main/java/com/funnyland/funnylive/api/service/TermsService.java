package com.funnyland.funnylive.api.service;

import com.funnyland.funnylive.api.endpoint.terms.request.TermsRequest;
import com.funnyland.funnylive.api.endpoint.terms.response.TermsResponse;
import com.funnyland.funnylive.api.repository.TermRepository;
import com.funnyland.funnylive.domain.term.Term;
import com.funnyland.funnylive.domain.term.TermDomainService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class TermsService {

    private final TermRepository termRepository;
    private final TermDomainService termDomainService;

    @Transactional
    public TermsResponse getTerms(Long termsId) {
        Term findTerm = termRepository.findById(termsId)
                .orElseThrow(() -> new RuntimeException("terms not exist"));

        findTerm.increaseViewCount();

        return TermsResponse.of(findTerm);
    }

    public Page<TermsResponse> getTerms(Pageable pageable) {
        Page<Term> terms = termRepository.findAll(pageable);
        return terms.map(TermsResponse::of);
    }

    public TermsResponse createTerm(TermsRequest termsRequest) {
        Term term = Term.builder()
                .title(termsRequest.getTitle())
                .content(termsRequest.getContent())
                .type(termsRequest.getType())
                .build();
        Term savedNotice = termRepository.save(term);
        return TermsResponse.of(savedNotice);
    }

    public TermsResponse getTermsLatest(Term.TermType type) {
        Term findTerm = termRepository.findTopByTypeOrderByCreatedAtDesc(type)
                .orElseThrow(() -> new RuntimeException("terms not exist"));
        return TermsResponse.of(findTerm);
    }

    @Transactional
    public TermsResponse update(Long termsId, TermsRequest termsRequest) {
        Term updateTerm = termsRequest.toDomain();
        return TermsResponse.of(termDomainService.update(termsId, updateTerm));
    }
}
