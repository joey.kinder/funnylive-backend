package com.funnyland.funnylive.api.service.dto;


import com.funnyland.funnylive.api.domain.OAuthUserInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserInfo {
    private String email;
    private String provider;
    private String name;
    private String phone;

    public static UserInfo from(OAuthUserInfo oAuthUserInfo) {
        return UserInfo.builder()
                .email(oAuthUserInfo.getEmail())
                .provider(oAuthUserInfo.getProvider())
                .name(oAuthUserInfo.getName())
                .phone(oAuthUserInfo.getPhone()).build();
    }
}
