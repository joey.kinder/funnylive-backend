package com.funnyland.funnylive.api.endpoint.cranegame.controller;

import java.util.List;

import com.funnyland.funnylive.api.endpoint.cranegame.facade.CraneGameFacade;
import com.funnyland.funnylive.api.endpoint.cranegame.request.CraneGameDrawRequest;
import com.funnyland.funnylive.api.endpoint.cranegame.request.CraneGameFilter;
import com.funnyland.funnylive.api.endpoint.cranegame.request.CraneGameStateRequest;
import com.funnyland.funnylive.api.endpoint.cranegame.response.CraneFeeResponse;
import com.funnyland.funnylive.api.endpoint.cranegame.response.CraneGameResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.CraneGameService;
import com.funnyland.funnylive.api.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@Tag(name="크레인 게임", description = "크레인 게임 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/crane-games")
public class CraneGameController {

    private final CraneGameService craneGameService;
    private final CraneGameFacade craneGameFacade;

    @GetMapping
    @Operation(summary = "크레인 게임 리스트 조회", description = "크레인 게임 리스트 조회")
    public List<CraneGameResponse> getCraneGames(CraneGameFilter filter) {
        return craneGameService.getCraneGames(filter);
    }

    @GetMapping("/{craneGameId}")
    @Operation(summary = "크레인 게임 조회", description = "크레인 게임 조회")
    public CraneGameResponse getCraneGame(@PathVariable Long craneGameId) {
        return craneGameService.getCraneGame(craneGameId);
    }

    @PutMapping("/{craneGameId}/upload")
    @Operation(summary = "크레인 썸네일 업로드", description = "크레인 썸네일 업로드")
    public CraneGameResponse updateThumbnailCraneGame(@PathVariable Long craneGameId, @RequestPart("image") MultipartFile image) {
        return craneGameFacade.updateThumbnailCraneGame(craneGameId, image);
    }

    @GetMapping("/play-fee")
    @Operation(summary = "크레인 게임 요금 그룹 조회", description = "크레인 게임 요금 그룹 조회")
    public List<CraneFeeResponse> playFeeGroup() {
        return craneGameService.playFeeGroup();
    }

    @PutMapping("/{craneGameId}/state")
    @Operation(summary = "크레인 게임 상태 업데이트", description = "크레인 게임 상태 업데이트")
    public CraneGameResponse updateCraneGameState(@PathVariable Long craneGameId, @RequestBody @Valid CraneGameStateRequest request) {
        return craneGameFacade.updateCraneGameState(craneGameId, request);
    }

    @PostMapping("/{craneGameId}/heartbeat")
    @Operation(summary = "크레인 게임 하트비트", description = "크레인 게임 하트비트")
    public void heartbeat(@PathVariable Long craneGameId) {
        craneGameFacade.heartbeat(craneGameId);
    }

    @PostMapping("/{craneGameId}/game/play")
    @Operation(summary = "크레인 게임 도전", description = "크레인 게임 도전")
    public boolean startGame(@PathVariable Long craneGameId, @RequestParam Long timeoutSecond) {
        return craneGameFacade.startGame(craneGameId, timeoutSecond);
    }

    @PostMapping("/{craneGameId}/game/replay")
    @Operation(summary = "크레인 게임 재도전", description = "크레인 게임 재도전")
    public boolean restartGame(@PathVariable Long craneGameId, @RequestParam Long timeoutSecond) {
        return craneGameFacade.restartGame(craneGameId, timeoutSecond);
    }

    @PostMapping("/{craneGameId}/game/end")
    @Operation(summary = "크레인 게임 종료", description = "크레인 게임 종료")
    public boolean endGame(@PathVariable Long craneGameId) {
        return craneGameFacade.endGame(craneGameId);
    }

    @PostMapping("/{craneGameId}/game/draw")
    @Operation(summary = "크레인 게임 뽑기 성공", description = "크레인 게임 뽑기 성공")
    public boolean draw(@PathVariable Long craneGameId, @RequestBody @Valid CraneGameDrawRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        return craneGameFacade.draw(user, craneGameId, request);
    }
}
