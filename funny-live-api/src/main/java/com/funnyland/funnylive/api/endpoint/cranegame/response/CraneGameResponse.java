package com.funnyland.funnylive.api.endpoint.cranegame.response;

import java.util.List;
import java.util.stream.Collectors;

import com.funnyland.funnylive.api.endpoint.product.response.ProductResponse;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.cranegame.CraneGameProduct;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CraneGameResponse {

    private Long craneGameId;
    private String uuid;
    private String description;
    private String imageUrl;
    private String ip;
    private Integer timer;
    private Integer playFee;
    private List<ProductResponse> products;
    private String gameState;
    private String gameStateName;

    public static CraneGameResponse of(CraneGame craneGame) {
        List<ProductResponse> products = craneGame.getCraneGameProducts()
                .stream()
                .map(CraneGameProduct::getProduct)
                .map(ProductResponse::of)
                .collect(Collectors.toList());
        return CraneGameResponse.builder()
                .craneGameId(craneGame.getId())
                .uuid(craneGame.getUuid())
                .description(craneGame.getDescription())
                .imageUrl(UrlUtil.convertToCdnUrl(craneGame.getImageUrl()))
                .ip(craneGame.getIp())
                .timer(craneGame.getTimer())
                .playFee(craneGame.getPlayFee())
                .products(products)
                .gameState(craneGame.getGameState().name())
                .gameStateName(craneGame.getGameState().getName())
                .build();
    }

    public static CraneGameResponse of(CraneGame craneGame, CraneGame.CraneGameState state) {
        List<ProductResponse> products = craneGame.getCraneGameProducts()
                .stream()
                .map(CraneGameProduct::getProduct)
                .map(ProductResponse::of)
                .collect(Collectors.toList());
        return CraneGameResponse.builder()
                .craneGameId(craneGame.getId())
                .uuid(craneGame.getUuid())
                .description(craneGame.getDescription())
                .imageUrl(UrlUtil.convertToCdnUrl(craneGame.getImageUrl()))
                .playFee(craneGame.getPlayFee())
                .products(products)
                .timer(craneGame.getTimer())
                .ip(craneGame.getIp())
                .gameState(state.name())
                .gameStateName(state.getName())
                .build();
    }

}
