package com.funnyland.funnylive.api.endpoint.delivery.request;

import java.util.List;

import com.funnyland.funnylive.domain.delivery.Delivery;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryRequest {

    @NotNull
    private Long memberId;

    @NotNull
    private String roadNameAddress;
    @NotNull
    private String inputDetail;
    @NotNull
    private String postalCode;
    @NotNull
    private String recipientName;
    @NotNull
    private String recipientPhone;

    private String requirement;

    @NotNull @Valid
    private List<DeliveryItemRequest> items;


    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeliveryItemRequest {
        @NotNull
        private Long productId;
        @NotNull
        private Integer quantity;
    }

    public Delivery toDomain() {
        return Delivery.builder()
                .roadNameAddress(this.roadNameAddress)
                .inputDetail(this.inputDetail)
                .postalCode(this.postalCode)
                .recipientName(this.recipientName)
                .recipientPhone(this.recipientPhone)
                .requirement(this.requirement)
                .build();
    }
}
