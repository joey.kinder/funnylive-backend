package com.funnyland.funnylive.api.endpoint.member.request;

import com.funnyland.funnylive.domain.deliveryaddress.DeliveryAddress;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.OAuthProvider;
import com.funnyland.funnylive.domain.member.Role;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MemberRequest {
    @Size(max = 10, message = "이름은 10자 이하여야 합니다")
    private String name;

    @Pattern(regexp = "^\\d{3}-\\d{3,4}-\\d{4}$", message = "핸드폰 형식이 맞지 않습니다")
    private String phone;

    @Email(message = "이메일 형식이 맞지 않습니다")
    private String email;

    @Size(max = 10, message = "닉네임은 10자 이하여야 합니다")
    private String nickName;

    @NotNull
    private String provider;

    @NotNull
    private boolean agreeReceiveEmail;

    @NotNull
    private boolean agreeReceivePhone;


    public Member toDomain() {
        Member member = Member.builder()
                .name(this.name)
                .phone(this.phone)
                .email(this.email)
                .nickName(this.nickName)
                .oAuthProvider(OAuthProvider.valueOf(provider))
                .role(Role.USER)
                .agreeReceiveEmail(this.agreeReceiveEmail)
                .agreeReceivePhone(this.agreeReceivePhone)
                .build();
        return member;
    }



}
