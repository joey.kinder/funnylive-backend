package com.funnyland.funnylive.api.endpoint.product.request;

import static com.funnyland.funnylive.domain.product.QProduct.product;

import com.funnyland.funnylive.domain.ApiPageRequest;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductFilter extends ApiPageRequest {
    private Long id;
    private String name;
    private Integer price;

    /**
     * 이름 조건 추가.
     * - product.name in (this.name)
     *
     * @param builder query dsl boolean builder
     * */
    private void nameBuilder(BooleanBuilder builder) {
        if(this.name != null) {
            builder.and(product.name.eq(this.name));
        }
    }
    /**
     * 가격 조건 추가.
     * - product.price in (this.price)
     *
     * @param builder query dsl boolean builder
     * */
    private void priceBuilder(BooleanBuilder builder) {
        if(this.price != null) {
            builder.and(product.price.eq(this.price));
        }
    }

    /**
     * id 조건 추가.
     * - product.id in (this.id)
     *
     * @param builder query dsl boolean builder
     * */
    private void idBuilder(BooleanBuilder builder) {
        if(this.id != null) {
            builder.and(product.id.eq(this.id));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        idBuilder(builder);
        nameBuilder(builder);
        priceBuilder(builder);

        return builder;
    }
}
