package com.funnyland.funnylive.api.service;

import java.util.Optional;

import com.funnyland.funnylive.api.service.dto.StorageType;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
@Slf4j
public class UploadService {

    private final StorageService storageService;
    private static final long FILE_MAX_SIZE = 10 * 1024 * 1024;

    /**
     * 파일 업로드
     */
    public Optional<UploadDto> upload(MultipartFile file, StorageType type) {
        log.info("upload() file : {}, type: {}", file.getName(), type.name());
        if (!ObjectUtils.isEmpty(file)) {
            // 파일 크기 제한 검사 (10MB)
            if (file.getSize() > FILE_MAX_SIZE) {
                throw new CustomException(ErrorCode.INQUIRY_FILE_SIZE_EXCEED);
            }
            UploadDto upload = storageService.upload(file, type);
            UploadDto uploadDto = upload;
            log.info("upload() success file : {}, type: {}", file.getName(), type.name());
            return Optional.of(uploadDto);
        }
        return Optional.empty();
    }

}
