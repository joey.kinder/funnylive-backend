package com.funnyland.funnylive.api.endpoint.order.controller;

import java.util.Optional;

import com.funnyland.funnylive.api.endpoint.order.request.OrderItemsRequest;
import com.funnyland.funnylive.api.repository.MemberReadRepository;
import com.funnyland.funnylive.api.service.OrderService;
import com.funnyland.funnylive.domain.member.Member;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/orders")
public class OrderController {

    private final OrderService orderService;
    private final MemberReadRepository memberReadRepository;

    @PostMapping
    @Operation(hidden = true)
    public ResponseEntity<String> createOrder(@RequestBody OrderItemsRequest orderItemsRequest) {
        try {
            Optional<Member> findUser = memberReadRepository.findById(1L);
            orderService.placeOrder(findUser.get(), orderItemsRequest);
            return ResponseEntity.ok("주문 생성 성공");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("주문 생성에 실패했습니다. " + e.getMessage());
        }
    }
}
