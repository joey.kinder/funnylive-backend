package com.funnyland.funnylive.api.endpoint.point.request;

import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PointTransactionRequest {
    @NotNull
    private int amount;
    @NotNull
    private PointTransactionType type;
}
