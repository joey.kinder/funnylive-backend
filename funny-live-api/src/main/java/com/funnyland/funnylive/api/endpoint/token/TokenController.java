package com.funnyland.funnylive.api.endpoint.token;

import com.funnyland.funnylive.api.domain.OAuthUserInfo;
import com.funnyland.funnylive.api.domain.RefreshToken;
import com.funnyland.funnylive.api.endpoint.token.response.TokenResponse;
import com.funnyland.funnylive.api.exception.StatusResponseDto;
import com.funnyland.funnylive.api.config.jwt.JwtTokenProvider;
import com.funnyland.funnylive.api.repository.OAuthUserInfoRepository;
import com.funnyland.funnylive.api.repository.RefreshTokenRepository;
import com.funnyland.funnylive.api.service.RefreshTokenService;
import com.funnyland.funnylive.api.service.dto.UserInfo;
import com.funnyland.funnylive.api.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Tag(name="토큰", description = "토큰 API")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/token")
public class TokenController {

    private final RefreshTokenService tokenService;
    private final RefreshTokenRepository tokenRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final OAuthUserInfoRepository oAuthUserInfoRepository;

    @Operation(summary = "로그아웃", description = "토큰 삭제")
    @PostMapping("/logout")
    public ResponseEntity<StatusResponseDto> logout(@RequestHeader(value = "Authorization") final String accessToken) {
        // 엑세스 토큰으로 현재 Redis 정보 삭제
        tokenService.removeRefreshToken(accessToken, SecurityUtils.getUser());

        return ResponseEntity.ok().build();
    }

    @Operation(summary = "토큰 갱신", description = "엑세스 토큰 갱신",
            parameters = {
                    @Parameter(in = ParameterIn.HEADER, name = "Refresh-Token", description = "Refresh Token", required = true, schema = @Schema(implementation = String.class))})
    @PostMapping("/refresh")
    public ResponseEntity<TokenResponse> refresh(@RequestHeader("Refresh-Token") final String refreshToken) {
        try {
            TokenResponse newTokens = tokenService.republishAccessToken(refreshToken);
            return ResponseEntity.ok(newTokens);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @Operation(summary = "소셜 로그인 유저 정보 조회", description = "소셜 로그인 유저 정보 조회")
    @GetMapping("/user-info")
    public ResponseEntity<?> getUserInfo(@RequestParam("authToken") String token) {
        try {
            OAuthUserInfo oAuthUserInfo = oAuthUserInfoRepository.findById(token)
                    .orElseThrow(() -> new RuntimeException("소셜 인증 실패한 사용자입니다"));
            // 토큰에서 사용자 정보 추출
            UserInfo userInfo = UserInfo.from(oAuthUserInfo);
            return ResponseEntity.ok(userInfo);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");
        }
    }

    @PostMapping
    public void create(@RequestBody RefreshToken refreshToken) {
        tokenRepository.save(refreshToken);
    }
}
