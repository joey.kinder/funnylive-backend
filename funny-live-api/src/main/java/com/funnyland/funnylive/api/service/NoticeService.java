package com.funnyland.funnylive.api.service;

import com.funnyland.funnylive.api.endpoint.notice.response.NoticeResponse;
import com.funnyland.funnylive.api.repository.NoticeRepository;
import com.funnyland.funnylive.domain.notice.Notice;
import com.funnyland.funnylive.domain.notice.NoticeDomainService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class NoticeService {

    private final NoticeRepository noticeRepository;
    private final NoticeDomainService noticeDomainService;

    /**
     * 공지사항 리스트 조회
     */
    public Page<NoticeResponse> getNotices(Pageable pageable) {
        Page<Notice> notices = noticeRepository.findAll(pageable);
        return notices.map(NoticeResponse::of);
    }

    /**
     * 공지사항 조회
     * 조회시 조회수 증가
     */
    @Transactional
    public NoticeResponse getNotice(Long noticeId) {
        Notice findNotice = noticeRepository.findById(noticeId)
                .orElseThrow(() -> new RuntimeException("notice not exist"));
        noticeRepository.updateViewCount(findNotice.getId());
        return NoticeResponse.of(findNotice);
    }

    /**
     * 공지사항 삭제
     */
    @Transactional
    public void deleteNotice(Long noticeId) {
        noticeDomainService.delete(noticeId);
    }
}
