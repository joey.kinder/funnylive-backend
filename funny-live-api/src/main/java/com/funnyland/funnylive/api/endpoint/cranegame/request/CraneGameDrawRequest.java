package com.funnyland.funnylive.api.endpoint.cranegame.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CraneGameDrawRequest {

    @NotNull
    private Long memberId;
    @NotNull
    private Long productId;
    
}