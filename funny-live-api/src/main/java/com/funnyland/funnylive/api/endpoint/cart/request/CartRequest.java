package com.funnyland.funnylive.api.endpoint.cart.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "카트 담기 정보")
@ToString
public class CartRequest {

    @NotNull
    @Schema(title = "보관함 id", example = "1", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long storageBoxId;
    @NotNull
    @Schema(title = "유저 id", example = "1", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long memberId;
    @Schema(title = "상품 id", example = "1", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long productId;
    @Schema(title = "카트 담을 개수", example = "3", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer quantity;
}
