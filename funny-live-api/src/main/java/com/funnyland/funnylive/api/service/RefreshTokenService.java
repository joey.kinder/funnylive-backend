package com.funnyland.funnylive.api.service;

import java.util.Optional;

import com.funnyland.funnylive.api.domain.RefreshToken;
import com.funnyland.funnylive.api.config.jwt.JwtTokenProvider;
import com.funnyland.funnylive.api.endpoint.token.response.TokenResponse;
import com.funnyland.funnylive.api.repository.RefreshTokenRepository;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Slf4j
@RequiredArgsConstructor
public class RefreshTokenService {

    private final RefreshTokenRepository tokenRepository;
    private final JwtTokenProvider jwtTokenProvider;

    @Transactional
    public void removeRefreshToken(String accessToken, SecurityUserDto userDto) {
        String email = jwtTokenProvider.getUid(accessToken);

        RefreshToken token = tokenRepository.findById(email)
                                            .orElseThrow(IllegalArgumentException::new);

        tokenRepository.delete(token);
    }

    @Transactional
    public TokenResponse republishAccessToken(String refreshToken) {
        String email = jwtTokenProvider.getUid(refreshToken);
        // 액세스 토큰으로 Refresh 토큰 객체를 조회
        RefreshToken findRefreshToken = tokenRepository.findById(email).orElseThrow(() -> new RuntimeException("토큰을 찾을수 없음"));

        if (!jwtTokenProvider.validateRefreshToken(findRefreshToken.getRefreshToken())) {
            log.info("refresh token 만료");
            throw new RuntimeException("Refresh Token 만료됨");
        }

        // RefreshToken이 존재하고 유효하다면 실행

        if(!refreshToken.equals(findRefreshToken.getRefreshToken())) {
            log.info("refresh token 불일치");
            throw new RuntimeException("Refresh Token 일치하지 않음");
        }
        // RefreshToken 객체를 꺼내온다.
        // 권한과 아이디를 추출해 새로운 액세스토큰을 만든다.
        String newAccessToken = jwtTokenProvider.generateAccessToken(findRefreshToken.getRefreshToken());
        String newRefreshToken = refreshToken;

        if (jwtTokenProvider.isRefreshTokenExpiringSoon(findRefreshToken.getRefreshToken())) {
            log.info("refresh token 만료 시간 임박");
            newRefreshToken = jwtTokenProvider.generateRefreshToken(findRefreshToken);
        }
        // 새로운 액세스 토큰을 반환해준다.
        return TokenResponse.of(newAccessToken, newRefreshToken);

    }
}
