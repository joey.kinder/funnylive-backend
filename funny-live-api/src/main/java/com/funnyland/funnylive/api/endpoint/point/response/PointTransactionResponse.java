package com.funnyland.funnylive.api.endpoint.point.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PointTransactionResponse {

    private Long pointTransactionId;
    private String transactionType;
    private PointTransactionType.TransactionChange change;
    private int amount;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    public static PointTransactionResponse of(PointTransaction pointTransaction) {
        return PointTransactionResponse.builder()
                .pointTransactionId(pointTransaction.getId())
                .transactionType(pointTransaction.getTransactionType() != null ? pointTransaction.getTransactionType().getDescription() : null)
                .change(pointTransaction.getTransactionType() != null ?pointTransaction.getTransactionType().getChange() : null)
                .amount(pointTransaction.getAmount())
                .createdAt(pointTransaction.getCreatedAt())
                .build();
    }
}
