package com.funnyland.funnylive.api.repository;

import static com.funnyland.funnylive.domain.inquiry.QFrequentlyInquiry.frequentlyInquiry;

import java.util.List;

import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;


@Repository
@RequiredArgsConstructor
public class FrequentlyInquiryReadRepository {

    private final JPAQueryFactory jpaQueryFactory;


    public List<FrequentlyInquiry> getAll(BooleanBuilder builder) {
        return jpaQueryFactory.selectFrom(frequentlyInquiry)
                .where(builder)
                .fetch();
    }
}
