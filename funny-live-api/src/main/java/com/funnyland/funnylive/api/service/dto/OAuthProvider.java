package com.funnyland.funnylive.api.service.dto;

public enum OAuthProvider {
    KAKAO, NAVER, GOOGLE
}
