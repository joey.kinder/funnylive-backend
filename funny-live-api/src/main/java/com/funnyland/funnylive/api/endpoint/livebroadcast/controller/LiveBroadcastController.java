package com.funnyland.funnylive.api.endpoint.livebroadcast.controller;

import com.funnyland.funnylive.api.endpoint.livebroadcast.response.LiveBroadcastResponse;
import com.funnyland.funnylive.api.service.LiveBroadcastService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="라이브방송", description = "라이브방송 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/live-broadcast")
public class LiveBroadcastController {

    private final LiveBroadcastService liveBroadcastService;

    @Operation(summary = "라이브방송 리스트 조회", description = "라이브방송 리스트 조회")
    @GetMapping
    public Page<LiveBroadcastResponse> getLiveBroadcasts(Pageable pageable) {
        return liveBroadcastService.getLiveBroadcasts(pageable);
    }

}
