package com.funnyland.funnylive.api.config;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

import java.util.Arrays;

import com.funnyland.funnylive.api.config.jwt.JwtAccessDeniedHandler;
import com.funnyland.funnylive.api.config.jwt.JwtAuthenticationEntryPoint;
import com.funnyland.funnylive.api.config.jwt.JwtAuthenticationFilter;
import com.funnyland.funnylive.api.config.jwt.JwtExceptionFilter;
import com.funnyland.funnylive.api.security.CustomOAuth2UserService;
import com.funnyland.funnylive.api.security.OAuth2AuthenticationFailureHandler;
import com.funnyland.funnylive.api.security.OAuth2AuthenticationSuccessHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private final CustomOAuth2UserService customOAuth2UserService;
    private final OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;
    private final OAuth2AuthenticationSuccessHandler oAuth2AuthenticationSuccessHandler;
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final JwtAccessDeniedHandler jwtAccessDeniedHandler;
    private final JwtExceptionFilter jwtExceptionFilter;
    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring()
                .requestMatchers(new AntPathRequestMatcher( "/favicon.ico"))
                .requestMatchers(new AntPathRequestMatcher( "/css/**"))
                .requestMatchers(new AntPathRequestMatcher( "/js/**"))
                .requestMatchers(new AntPathRequestMatcher( "/img/**"))
                .requestMatchers(new AntPathRequestMatcher( "/lib/**"));
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(Arrays.asList("http://localhost:8080", "https://dev.funnylive.co.kr"));
        configuration.setAllowedOriginPatterns(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Content-Type", "X-Requested-With"));
        configuration.setAllowCredentials(true);
        configuration.setMaxAge(3600L); // 1시간

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .cors(Customizer.withDefaults()) // rest api로 사용예정이라 cors 활성화
                .httpBasic(AbstractHttpConfigurer::disable) //Http 기본 인증 비활성화
                .csrf(AbstractHttpConfigurer::disable)
                .formLogin(AbstractHttpConfigurer::disable)
                .rememberMe(AbstractHttpConfigurer::disable)
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
                // 세션이 있으면 안쓰고 , 없으면 만들지도 않음

        http
                .authorizeHttpRequests(auth ->
                                               //요청에 대한 권한 설정
                                               auth
                                                       .requestMatchers(antMatcher(HttpMethod.GET,"/api/terms/**")).permitAll()
                                                       .requestMatchers(antMatcher("/api/members/check"),
                                                               antMatcher("/api/crane-games/*/heartbeat"),
                                                               antMatcher("/api/crane-games/*/state"),
                                                               antMatcher("/api/crane-games/*/upload")
                                                               ).permitAll()

//                                                       .requestMatchers(antMatcher("/api/**")).hasAuthority("ADMIN")
                                                       .requestMatchers(antMatcher("/oauth2/**"))
                                                   .permitAll()
                                                   .requestMatchers(antMatcher("/api/token/**"))
                                                   .permitAll()
                                                   .requestMatchers(antMatcher(HttpMethod.POST, "/api/members")).permitAll()
                                                   .requestMatchers(antMatcher("/api/**")).authenticated()
                                                   .anyRequest()
                                                   .permitAll());

        http
                // 소셜 로그인 url
                .oauth2Login(o -> o.authorizationEndpoint(ae -> ae.baseUri("/oauth2/authorize"))
                        .userInfoEndpoint(uie -> uie.userService(customOAuth2UserService))
                        .successHandler(oAuth2AuthenticationSuccessHandler)
                        .failureHandler(oAuth2AuthenticationFailureHandler));

        //jwt filter 설정
        http.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(jwtExceptionFilter, JwtAuthenticationFilter.class);

        http.exceptionHandling(ehc -> ehc.accessDeniedHandler(jwtAccessDeniedHandler)
                .authenticationEntryPoint(jwtAuthenticationEntryPoint));

        return http.build();
    }


}
