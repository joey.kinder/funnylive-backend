package com.funnyland.funnylive.api.config;

import lombok.Getter;


@Getter
public enum Role {
    ADMIN,
    USER
}
