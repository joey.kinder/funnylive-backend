package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.domain.inquiry.InquiryReply;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InquiryResponseRepository extends JpaRepository<InquiryReply, Long> {
}
