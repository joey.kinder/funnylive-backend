package com.funnyland.funnylive.api.endpoint.payment.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentRequest {
    private String orderId;
    private String packageName;
    private String productId;
    private String purchaseTime;
    private int purchaseState;
    private String purchaseToken;
    private int quantity;
    private boolean acknowledged;


    /*
    {"orderId":"GPA.3365-8839-8724-30999","packageName":"im.pickl.android","productId":"100_diamond","purchaseTime":1701647804175,"purchaseState":0,"purchaseToken":"dedbfjhpfmnnkebjceaapjim.AO-J1Oyozlk84PFHu5DtizboBQtX-ym_j3UfH-usVZImBQtJ9ChdJNXSRl-qkmIfo4aXsSq5OtDgxunRqi3kgb-_Z49xVRJUNQ","quantity":1,"acknowledged":false}
     **/

}