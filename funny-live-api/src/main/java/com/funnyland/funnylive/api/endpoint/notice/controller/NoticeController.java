package com.funnyland.funnylive.api.endpoint.notice.controller;

import com.funnyland.funnylive.api.endpoint.notice.response.NoticeResponse;
import com.funnyland.funnylive.api.service.NoticeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name="공지사항", description = "공지사항 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/notices")
public class NoticeController {

    private final NoticeService noticeService;

    @Operation(summary = "공지사항 리스트 조회", description = "공지사항 리스트 조회")
    @GetMapping
    public Page<NoticeResponse> getNotices(Pageable pageable) {
        return noticeService.getNotices(pageable);
    }

    @Operation(summary = "공지사항 상세 조회", description = "공지사항 상세 조회")
    @GetMapping("/{noticeId}")
    public NoticeResponse getNotices(@Parameter(description = "조회할 공지사항 아이디", example = "1")
                                         @PathVariable Long noticeId) {
        return noticeService.getNotice(noticeId);
    }

}
