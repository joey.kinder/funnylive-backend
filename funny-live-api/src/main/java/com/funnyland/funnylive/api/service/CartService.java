package com.funnyland.funnylive.api.service;

import java.util.Optional;

import com.funnyland.funnylive.api.endpoint.cart.request.CartRequest;
import com.funnyland.funnylive.api.endpoint.cart.request.CartUpdateRequest;
import com.funnyland.funnylive.api.endpoint.cart.response.CartResponse;
import com.funnyland.funnylive.api.repository.CartItemRepository;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.domain.cart.CartItem;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import com.funnyland.funnylive.domain.product.ProductDomainService;
import com.funnyland.funnylive.domain.storagebox.AcquisitionMethod;
import com.funnyland.funnylive.domain.storagebox.StorageBox;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class CartService {

    private final MemberDomainService memberDomainService;
    private final StorageBoxService storageBoxService;
    private final CartItemRepository cartItemRepository;

    @Transactional
    public void create(CartRequest request, SecurityUserDto user) {
//        Member member = memberDomainService.find(request.getMemberId());
//        user.authVerify(member.getId());
//
//
//        StorageBox findStorageBox = storageBoxService.get(request.getStorageBoxId());
//        findStorageBox.decreaseQuantity(request.getQuantity(), AcquisitionMethod.DELIVERY);
//        storageBoxService.saveOrUpdate(findStorageBox);
//
//
//        // 사용자의 카트에서 동일한 상품이 있는지 확인
//        Optional<CartItem> existingCartItem = cartItemRepository.findByMemberIdAndProductId(member.getId(), findStorageBox.getProductId());
//        if (existingCartItem.isPresent()) {
//            // 이미 존재하는 상품인 경우, 수량만 업데이트
//            CartItem cartItem = existingCartItem.get();
//
//            cartItem.setQuantity(cartItem.getQuantity() + request.getQuantity());
//            cartItemRepository.save(cartItem);
//        } else {
//            CartItem newCartItem = CartItem.createFromStorageBox(findStorageBox, request.getQuantity());
//            member.addCartItem(newCartItem);
//        }

    }

    public CartResponse list(Long memberId) {
        Member member = memberDomainService.find(memberId);
        return CartResponse.of(member);
    }

    @Transactional
    public CartResponse.CartItemResponse update(Long cartItemId, CartUpdateRequest request, SecurityUserDto user) {
        CartItem updatedCartItem = cartItemRepository.findById(cartItemId)
                .orElseThrow(() -> new RuntimeException("해당 카트 아이템이 없습니다"))
                .edit(request.getQuantity());

        return CartResponse.CartItemResponse.of(updatedCartItem);
    }

    public void delete(Long cartItemId, SecurityUserDto user) {
        CartItem findCartItem = cartItemRepository.findById(cartItemId)
                .orElseThrow(() -> new RuntimeException("해당 카트 아이템이 없습니다"));
        cartItemRepository.delete(findCartItem);
    }
}
