package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.domain.order.Order;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderRepository extends JpaRepository<Order, Long> {
}
