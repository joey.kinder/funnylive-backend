package com.funnyland.funnylive.api.repository;


import static com.funnyland.funnylive.domain.member.QMember.member;
import static com.funnyland.funnylive.domain.deliveryaddress.QDeliveryAddress.deliveryAddress;
import static com.funnyland.funnylive.domain.storagebox.QStorageBox.storageBox;
import static com.funnyland.funnylive.domain.cart.QCartItem.cartItem;
import static com.funnyland.funnylive.domain.inquiry.QInquiry.inquiry;
import static com.funnyland.funnylive.domain.delivery.QDelivery.delivery;

import java.util.Optional;

import com.funnyland.funnylive.domain.member.Member;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;


@Repository
public class MemberReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public MemberReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(Member.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Optional<Member> findById(Long id) {
        Member findMember = jpaQueryFactory.selectFrom(member)
                .where(member.id.eq(id))
                .fetchOne();
        return Optional.ofNullable(findMember);
    }

    public boolean existsMemberByNickName(String nickName) {
        Integer fetched = jpaQueryFactory.selectOne()
                .from(member)
                .where(member.nickName.eq(nickName))
                .fetchFirst();
        return fetched != null;
    }

    public Optional<Member> findByEmail(String email) {
        Member findMember = jpaQueryFactory.selectFrom(member)
                .where(member.email.eq(email))
                .fetchOne();
        return Optional.ofNullable(findMember);
    }

    public Optional<Member> getMember(Long id) {
        Member findMember = jpaQueryFactory.selectFrom(member)
                .leftJoin(member.storageBox, storageBox).fetchJoin()
                .leftJoin(member.inquiries, inquiry).fetchJoin()
                .leftJoin(member.deliveries, delivery).fetchJoin()
                .where(member.id.eq(id))
                .fetchOne();
        return Optional.ofNullable(findMember);
    }


}
