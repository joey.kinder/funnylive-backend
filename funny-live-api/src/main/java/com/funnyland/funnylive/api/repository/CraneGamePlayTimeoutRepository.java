package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.domain.cranegame.CraneGamePlayTimeout;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CraneGamePlayTimeoutRepository extends CrudRepository<CraneGamePlayTimeout, Long> {
}
