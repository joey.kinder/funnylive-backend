package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.domain.point.PointTransactionDetail;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PointTransactionDetailRepository extends JpaRepository<PointTransactionDetail, Long> {

}
