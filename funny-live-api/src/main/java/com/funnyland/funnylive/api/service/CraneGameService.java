package com.funnyland.funnylive.api.service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.funnyland.funnylive.api.domain.CraneGameHeartbeat;
import com.funnyland.funnylive.api.endpoint.cranegame.request.CraneGameDrawRequest;
import com.funnyland.funnylive.api.endpoint.cranegame.request.CraneGameFilter;
import com.funnyland.funnylive.api.endpoint.cranegame.request.CraneGameStateRequest;
import com.funnyland.funnylive.api.endpoint.cranegame.response.CraneFeeResponse;
import com.funnyland.funnylive.api.endpoint.cranegame.response.CraneGameResponse;
import com.funnyland.funnylive.api.repository.CraneGameHeartbeatRepository;
import com.funnyland.funnylive.api.repository.CraneGamePlayTimeoutRepository;
import com.funnyland.funnylive.api.repository.CraneGameReadRepository;
import com.funnyland.funnylive.api.repository.CraneGameRepository;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.dto.StorageType;
import com.funnyland.funnylive.api.slack.SlackConstant;
import com.funnyland.funnylive.api.slack.SlackErrorMessage;
import com.funnyland.funnylive.api.slack.SlackService;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.cranegame.CraneGameDomainService;
import com.funnyland.funnylive.domain.cranegame.CraneGamePlayTimeout;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import com.funnyland.funnylive.domain.storagebox.AcquisitionMethod;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


@RequiredArgsConstructor
@Service
@Slf4j
public class CraneGameService {

    private final CraneGameReadRepository craneGameReadRepository;
    private final CraneGameDomainService craneGameDomainService;
    private final UploadService uploadService;
    private final CraneGameHeartbeatRepository craneGameHeartbeatRepository;
    private final CraneGamePlayTimeoutRepository craneGamePlayTimeoutRepository;
    private final StorageBoxService storageBoxService;
    private final SlackService slackService;
    private final ThreadPoolTaskExecutor executor;

    /**
     * craneGame 상태 업데이트
     * gameState가 오류일때 슬랙 에러메시지 전송
     */
    @Transactional
    public CraneGame updateCraneGameState(Long craneGameId, CraneGameStateRequest request) {
        CraneGame updatedCraneGame = craneGameDomainService.update(craneGameId, request.toDomain());

        if(CraneGame.CraneGameState.GAME_ERROR.equals(updatedCraneGame.getGameState())) {
            executor.execute(() -> {
                slackService.sendSlackMessage(new SlackErrorMessage(LocalDateTime.now(),craneGameId + "번 크레인 오류 발생!!"), SlackConstant.ERROR_CHANNEL);
            });
        }
        return updatedCraneGame;
    }

    /**
     * craneGame 게임 시작
     * 시작할때 craneGamePlayTimout(도전 할때까지 대기시간 + 게임기 타이머 시간 + 대기실 나가기전까지 대기시간) 기록
     */
    //TODO: 레디스와 같이 트랜젹션 묶일수 있나?
    @Transactional
    public boolean startGame(Long craneGameId, Long timeoutSecond) {
        CraneGame findCraneGame = craneGameDomainService.find(craneGameId);

        boolean isStarted = findCraneGame.start();
        if(isStarted) {
            log.info("CraneGameService.startGame : craneGame started craneGameId={}, timeoutSecond={}", craneGameId, timeoutSecond);
            LocalDateTime timeoutTime = LocalDateTime.now().plus(timeoutSecond, ChronoUnit.SECONDS);

            CraneGamePlayTimeout craneGamePlayTimeout = CraneGamePlayTimeout.of(craneGameId, timeoutTime, timeoutSecond);

            craneGamePlayTimeoutRepository.save(craneGamePlayTimeout);
        }
        return isStarted;
    }

    /**
     * craneGame thumbnail 업데이트
     */
    @Transactional
    public CraneGame updateThumbnailCraneGame(Long craneGameId, MultipartFile image) {
        CraneGame findCraneGame = craneGameDomainService.find(craneGameId);

        Optional<UploadDto> uploadDto = Optional.ofNullable(image)
                .flatMap(file -> uploadService.upload(file, StorageType.IMAGE));

        uploadDto.ifPresent(findCraneGame::addImage);
        return findCraneGame;
    }


    /**
     * craneGame 리스트 조회
     */
    public List<CraneGameResponse> getCraneGames(CraneGameFilter filter) {
        List<CraneGame> findCraneGames = craneGameReadRepository.getCraneGames(filter.generateBooleanBuilder());
        return findCraneGames.stream()
                .map(this::mapToCraneGameResponse)
                .collect(Collectors.toList());
    }

    /**
     * craneGame 상세 조회
     */
    public CraneGameResponse getCraneGame(Long craneGameId) {
        CraneGame findCraneGame = craneGameReadRepository.findById(craneGameId)
                .orElseThrow(() -> new CustomException(ErrorCode.CRANE_GAME_NOT_FOUND));
        return CraneGameResponse.of(findCraneGame);
    }

    /**
     * craneGame 요금 그룹 조회
     */
    public List<CraneFeeResponse> playFeeGroup() {
        List<Integer> integers = craneGameReadRepository.countGamesGroupedByPlayFee();
        List<CraneFeeResponse> craneFeeResponses = integers.stream()
                .map(CraneFeeResponse::new)
                .toList();
        return craneFeeResponses;
    }

    /**
     * heartBeatTime 기록
     */
    public void heartbeat(Long craneGameId) {
        CraneGame findCraneGame = craneGameReadRepository.findById(craneGameId)
                .orElseThrow(() -> new CustomException(ErrorCode.CRANE_GAME_NOT_FOUND));

        CraneGameHeartbeat heartbeat = CraneGameHeartbeat.of(findCraneGame);
        craneGameHeartbeatRepository.save(heartbeat);
    }

    /**
     * craneGameResponse 매핑
     */
    private CraneGameResponse mapToCraneGameResponse(CraneGame craneGame) {
        CraneGame.CraneGameState communicationStatus = determineCommunicationStatus(craneGame);
        return CraneGameResponse.of(craneGame, communicationStatus);
    }

    /**
     * heartBeatTime 조회 후 최근 heartBeatTime 1분동안 없으면 통신오류로 기록
     */
    private CraneGame.CraneGameState determineCommunicationStatus(CraneGame craneGame) {
        return craneGameHeartbeatRepository.findById(craneGame.getId())
                .map(craneGameHeartbeat -> {
                    LocalDateTime lastHeartbeatTime = craneGameHeartbeat.getLastHeartbeatTime();
                    if (lastHeartbeatTime.isBefore(LocalDateTime.now().minusMinutes(1))) {
                        return CraneGame.CraneGameState.GAME_ERROR; // 통신 오류
                    } else {
                        return craneGame.getGameState();
                    }
                })
                .orElse(craneGame.getGameState());
    }

    /**
     * craneGame 게임 재시작
     * 재시작할때 craneGamePlayTimout(게임기 타이머 시간 + 대기실 나가기전까지 대기시간) 기록
     */
    @Transactional
    public boolean restartGame(Long craneGameId, Long timeoutSecond) {
        CraneGame findCraneGame = craneGameDomainService.find(craneGameId);

        boolean isRestarted = findCraneGame.restart();
        if(isRestarted) {
            log.info("CraneGameService.restartGame : craneGame restartGame craneGameId={}, timeoutSecond={}", craneGameId, timeoutSecond);
            LocalDateTime timeoutTime = LocalDateTime.now().plus(timeoutSecond, ChronoUnit.SECONDS);

            CraneGamePlayTimeout craneGamePlayTimeout = CraneGamePlayTimeout.of(craneGameId, timeoutTime, timeoutSecond);

            craneGamePlayTimeoutRepository.save(craneGamePlayTimeout);
        }
        return isRestarted;
    }

    /**
     * craneGame 게임 종료
     * 종료할때 craneGamePlayTimout 제거
     */
    @Transactional
    public boolean endGame(Long craneGameId) {
        CraneGame findCraneGame = craneGameDomainService.find(craneGameId);

        boolean isEnded = findCraneGame.end();
        if(isEnded) {
            log.info("CraneGameService.endGame : craneGame endGame craneGameId={}", craneGameId);
            craneGamePlayTimeoutRepository.deleteById(craneGameId);
        }
        return isEnded;
    }

    /**
     * 뽑기 성공 시 보관함에 상품 추가
     */
    public boolean draw(SecurityUserDto user, Long craneGameId, CraneGameDrawRequest request) {
        storageBoxService.addStorageBox(user, request.getProductId(), request.getMemberId(), AcquisitionMethod.DRAW);
        log.info("CraneGameService.draw : craneGame draw success craneGameId={}, memberId={}, productId={}", craneGameId, request.getMemberId(), request.getProductId());
        return true;
    }

    //    @Transactional
    //    public CraneGame updateCraneGame(Long craneGameId, CraneGameRequest request, MultipartFile image) {
    //        Set<Product> findProducts = request.getProductIds()
    //                .stream()
    //                .map(productService::getProduct)
    //                .collect(Collectors.toSet());
    //        CraneGame craneGame = request.toDomain();
    //        craneGame.add(findProducts);
    //
    //        Optional<UploadDto> uploadDto = Optional.ofNullable(image)
    //                .flatMap(file -> uploadService.upload(file, StorageType.IMAGE));
    //
    //        uploadDto.ifPresent(craneGame::addImage);
    //        return craneGameDomainService.update(craneGameId, craneGame);
    //    }


}
