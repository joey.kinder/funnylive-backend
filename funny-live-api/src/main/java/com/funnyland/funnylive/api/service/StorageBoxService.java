package com.funnyland.funnylive.api.service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.api.endpoint.storagebox.request.StorageBoxRequest;
import com.funnyland.funnylive.api.endpoint.storagebox.response.StorageBoxResponse;
import com.funnyland.funnylive.api.repository.MemberReadRepository;
import com.funnyland.funnylive.api.repository.ProductRepository;
import com.funnyland.funnylive.api.repository.StorageBoxHistoryRepository;
import com.funnyland.funnylive.api.repository.StorageBoxRepository;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.storagebox.AcquisitionMethod;
import com.funnyland.funnylive.domain.storagebox.StorageBox;
import com.funnyland.funnylive.domain.storagebox.StorageBoxHistory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class StorageBoxService {

    private final StorageBoxRepository storageBoxRepository;
    private final StorageBoxHistoryRepository storageBoxHistoryRepository;
    private final ProductRepository productRepository;
    private final MemberReadRepository memberReadRepository;
    private final MemberDomainService memberDomainService;

    public Page<StorageBoxResponse> getStorageBoxes(Long memberId, Pageable pageable) {
        Member findMember = memberDomainService.find(memberId);

        if (findMember.getStorageBox() == null) {
            // StorageBox가 없는 경우 비어 있는 페이지 반환
            return Page.empty(pageable);
        }

        List<StorageBoxHistory> allHistories = findMember.getStorageBox()
                                                         .getHistories();

        Map<Long, Long> quantityByProductId = allHistories.stream()
                                                        .filter(storageBoxHistory -> !storageBoxHistory.isDeliveryRequested())
                                                          .collect(Collectors.groupingBy(
                                                                  StorageBoxHistory::getProductId,
                                                                  Collectors.counting()
                                                          ));

        // 각 productId에 대한 대표 StorageBoxHistory를 선택합니다.
        List<StorageBoxHistory> representativeHistories = allHistories.stream()
                                                                      .filter(h -> quantityByProductId.containsKey(h.getProductId()))
                                                                      .collect(Collectors.toMap(
                                                                              StorageBoxHistory::getProductId,
                                                                              Function.identity(),
                                                                              (existing, replacement) -> existing
                                                                      )).values().stream().collect(Collectors.toList());

        // 각 대표 StorageBoxHistory 객체를 StorageBoxResponse 객체로 변환합니다.
        List<StorageBoxResponse> responses = representativeHistories.stream().map(history -> {
            Long totalQuantity = quantityByProductId.get(history.getProductId());
            return StorageBoxResponse.builder()
                                     .storageBoxId(history.getStorageBox().getId())
                                     .productId(history.getProductId())
                                     .productName(history.getProductName())
                                     .productImageUrl(UrlUtil.convertToCdnUrl(history.getProductImageURL()))
                                     .memberId(history.getStorageBox().getMember().getId())
                                     .memberName(history.getStorageBox().getMember().getName())
                                     .quantity(totalQuantity.intValue())
                                     .acquisitionPoint(history.getProductPrice())
                                     .build();
        }).collect(Collectors.toList());

        // 최종적으로 Pageable 객체를 사용하여 페이징 처리된 결과를 반환합니다.
        return new PageImpl<>(responses, pageable, responses.size());
    }

    @Transactional
    public StorageBoxResponse addStorageBox(SecurityUserDto user, Long productId, Long memberId, AcquisitionMethod acquisitionMethod) {
        user.authVerify(memberId);

        Product findProduct = productRepository.findById(productId).orElseThrow(() -> new DomainException("product not exist"));

        StorageBox storageBox = storageBoxRepository.findByMemberId(user.getMemberId())
                                                    .orElseGet(() -> createNewStorageBox(user.getMemberId()));

        storageBox.addHistory(findProduct, acquisitionMethod);
        StorageBox savedStorageBox = storageBoxRepository.save(storageBox);
        return StorageBoxResponse.of(savedStorageBox);
    }

    private StorageBox createNewStorageBox(Long memberId) {
        Member member = memberReadRepository.findById(memberId)
                                        .orElseThrow(() -> new DomainException("Member not exist"));

        return new StorageBox(member);
    }

    public StorageBoxResponse getStorageBox(Long memberId, Long storageBoxId) {
        Member findMember = memberDomainService.find(memberId);
        StorageBox findStorageBox = storageBoxRepository.findByMemberIdAndId(findMember.getId(), storageBoxId)
                .orElseThrow(() -> new RuntimeException("storageBox not exist"));
        return StorageBoxResponse.of(findStorageBox);
    }

    public StorageBox get(Long storageBoxId) {
        StorageBox findStorageBox = storageBoxRepository.findById(storageBoxId)
                .orElseThrow(() -> new RuntimeException("storageBox not exist"));
        return findStorageBox;
    }

//    public void saveOrUpdate(StorageBox storageBox) {
//        if (storageBox.getQuantity() <= 0) {
//            // 수량이 0이면 보관함에서 해당 항목을 삭제
//            storageBoxRepository.delete(storageBox);
//        } else {
//            // 수량이 남아있다면 업데이트
//            storageBoxRepository.save(storageBox);
//        }
//    }


//    @Transactional
//    public StorageBoxResponse addProduct(SecurityUserDto user, StorageBoxRequest request) {
//        Member member = memberDomainService.find(request.getMemberId());
//        user.authVerify(request.getMemberId());
//
//        Product findProduct = productDomainService.find(request.getProductId());
//
//        StorageBox savedStorageBox;
//        Optional<StorageBox> findStorageBox = storageBoxRepository.findByMemberIdAndProductId(request.getMemberId(), request.getProductId());
//        StorageBox storageBox;
//        if(findStorageBox.isPresent()) {
//            storageBox = findStorageBox.get();
//            storageBox.increaseQuantity(1);
//        } else {
//            storageBox = StorageBox.builder()
//                    .product(findProduct)
//                    .member(member)
//                    .build();
//        }
//        savedStorageBox = storageBoxRepository.save(storageBox);
//
//        return StorageBoxResponse.of(savedStorageBox);
//    }
}
