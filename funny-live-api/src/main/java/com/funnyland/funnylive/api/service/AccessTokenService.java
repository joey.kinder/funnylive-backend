package com.funnyland.funnylive.api.service;

import com.funnyland.funnylive.api.domain.RefreshToken;
import com.funnyland.funnylive.api.repository.RefreshTokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class AccessTokenService {

    private final RefreshTokenRepository tokenRepository;

    @Transactional
    public void saveTokenInfo(String email, String refreshToken, Long timeToLive) {
        tokenRepository.save(RefreshToken.createRefreshToken(email, refreshToken, timeToLive));
    }

    public RefreshToken getTokenInfo(String email) {
        return tokenRepository.findById(email).orElseThrow(() -> new RuntimeException("token이 없습니다"));
    }
}
