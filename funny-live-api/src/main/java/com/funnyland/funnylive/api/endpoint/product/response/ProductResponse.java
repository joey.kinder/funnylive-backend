package com.funnyland.funnylive.api.endpoint.product.response;

import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class ProductResponse {

    private Long productId;
    private String name;
    private double price;
    private String imageUrl;
    private String status;

    public static ProductResponse of(Product product) {
        return ProductResponse.builder()
                .productId(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .imageUrl(UrlUtil.convertToCdnUrl(product.getImageURL()))
                .status(product.getStatus() != null? product.getStatus().name() : null)
                .build();
    }

}
