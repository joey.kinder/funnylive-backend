package com.funnyland.funnylive.api.endpoint.storagebox.request;

import com.funnyland.funnylive.domain.storagebox.AcquisitionMethod;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@AllArgsConstructor
@NoArgsConstructor
public class StorageBoxRequest {
    @NotNull
    private Long memberId;
    @NotNull
    private Long productId;
    @NotNull
    private AcquisitionMethod acquisitionMethod;

}
