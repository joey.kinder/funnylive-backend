package com.funnyland.funnylive.api.endpoint.inquiry.request;

import com.funnyland.funnylive.domain.inquiry.InquiryType;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InquiryTypeRequest {
    @NotNull
    private String name;

    public InquiryType toDomain() {
        InquiryType inquiry = InquiryType.builder()
                .name(this.name)
                 .build();
        return inquiry;
    }
}