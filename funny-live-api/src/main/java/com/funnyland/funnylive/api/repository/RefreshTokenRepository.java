package com.funnyland.funnylive.api.repository;

import java.util.Optional;

import com.funnyland.funnylive.api.domain.RefreshToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RefreshTokenRepository extends CrudRepository<RefreshToken, String> {
    Optional<RefreshToken> findById(String email);
}
