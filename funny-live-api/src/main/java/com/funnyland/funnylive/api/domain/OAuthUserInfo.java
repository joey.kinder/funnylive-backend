package com.funnyland.funnylive.api.domain;

import java.io.Serial;
import java.io.Serializable;

import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("OAuthUserInfo")
@Builder
public class OAuthUserInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 35345678231237656L;

    @Id
    private String id;

    private String email;

    private String name;

    private String phone;

    private String provider;

    @TimeToLive
    private Long expiration;
}
