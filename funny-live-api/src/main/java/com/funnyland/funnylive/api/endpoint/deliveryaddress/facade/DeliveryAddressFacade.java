package com.funnyland.funnylive.api.endpoint.deliveryaddress.facade;

import java.util.List;
import java.util.stream.Collectors;

import com.funnyland.funnylive.api.endpoint.deliveryaddress.request.DeliveryAddressRequest;
import com.funnyland.funnylive.api.endpoint.deliveryaddress.response.DeliveryAddressResponse;
import com.funnyland.funnylive.api.service.DeliveryAddressService;
import com.funnyland.funnylive.api.service.MemberService;
import com.funnyland.funnylive.domain.deliveryaddress.DeliveryAddress;
import com.funnyland.funnylive.domain.member.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeliveryAddressFacade {

    private final DeliveryAddressService deliveryAddressService;
    private final MemberService memberService;


    public DeliveryAddressResponse createDeliveryAddress(Long userId, DeliveryAddressRequest deliveryAddressRequest) {
        Member member = memberService.getMember(userId);
        DeliveryAddress savedDeliveryAddress = deliveryAddressService.createDeliveryAddress(member, deliveryAddressRequest.toDomain());
        return DeliveryAddressResponse.of(savedDeliveryAddress);
    }


    public List<DeliveryAddressResponse> getDeliveryAddressList(Long userId) {
        Member member = memberService.getMember(userId);
        List<DeliveryAddress> findDeliveryAddressList = deliveryAddressService.getDeliveryAddressList(member);
        return findDeliveryAddressList.stream()
                .map(DeliveryAddressResponse::of)
                .collect(Collectors.toList());
    }

    public DeliveryAddressResponse getDeliveryAddress(Long userId, Long deliveryAddressId) {
        Member member = memberService.getMember(userId);
        DeliveryAddress findDeliveryAddress = deliveryAddressService.getDeliveryAddress(member, deliveryAddressId);
        return DeliveryAddressResponse.of(findDeliveryAddress);
    }

    public void deleteDeliveryAddress(Long userId, Long deliveryAddressId) {
        Member member = memberService.getMember(userId);
        deliveryAddressService.deleteDeliveryAddress(member, deliveryAddressId);
    }
}
