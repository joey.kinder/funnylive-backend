package com.funnyland.funnylive.api.service;

import java.util.List;

import com.funnyland.funnylive.api.repository.DeliveryAddressRepository;
import com.funnyland.funnylive.domain.deliveryaddress.DeliveryAddress;
import com.funnyland.funnylive.domain.member.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class DeliveryAddressService {

    private final DeliveryAddressRepository deliveryAddressRepository;

    @Transactional
    public DeliveryAddress createDeliveryAddress(Member member, DeliveryAddress deliveryAddress) {
       //TODO: 대표 배송지 변경
        DeliveryAddress deliveryAddressWithUser = deliveryAddress.setMember(member);
        return deliveryAddressRepository.save(deliveryAddressWithUser);
    }

    public List<DeliveryAddress> getDeliveryAddressList(Member member) {
        List<DeliveryAddress> findDeliveryAddressList = deliveryAddressRepository.findByMemberId(member.getId());
        return findDeliveryAddressList;
    }

    public DeliveryAddress getDeliveryAddress(Member member, Long deliveryAddressId) {
        return deliveryAddressRepository.findByIdAndMemberId(deliveryAddressId, member.getId())
                .orElseThrow(() -> new RuntimeException("deliveryAddress not exist"));
    }

    @Transactional
    public void deleteDeliveryAddress(Member member, Long deliveryAddressId) {
        DeliveryAddress findDeliveryAddress = deliveryAddressRepository.findByIdAndMemberId(deliveryAddressId, member.getId())
                .orElseThrow(() -> new RuntimeException("deliveryAddress not exist"));

        deliveryAddressRepository.delete(findDeliveryAddress);
    }
}
