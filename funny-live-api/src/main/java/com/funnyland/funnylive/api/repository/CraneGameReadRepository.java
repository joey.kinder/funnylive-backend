package com.funnyland.funnylive.api.repository;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import static com.funnyland.funnylive.domain.cranegame.QCraneGame.craneGame;
import static com.funnyland.funnylive.domain.inquiry.QInquiry.inquiry;


@Repository
@RequiredArgsConstructor
public class CraneGameReadRepository {

    private final JPAQueryFactory jpaQueryFactory;


    public List<CraneGame> getCraneGames(BooleanBuilder builder) {
        return jpaQueryFactory.selectFrom(craneGame)
                .where(builder)
                .fetch();
    }

    public Optional<CraneGame> findById(Long craneGameId) {
        JPAQuery<CraneGame> query = jpaQueryFactory.selectFrom(craneGame)
                .where(craneGame.id.eq(craneGameId));
        return Optional.ofNullable(query.fetchOne());
    }


    public List<Integer> countGamesGroupedByPlayFee() {
        JPQLQuery<Integer> query = jpaQueryFactory
                .select(craneGame.playFee)
                .from(craneGame)
                .groupBy(craneGame.playFee);
        return query.fetch();
    }
}
