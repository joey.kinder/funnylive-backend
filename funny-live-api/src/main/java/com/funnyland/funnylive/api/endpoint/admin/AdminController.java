package com.funnyland.funnylive.api.endpoint.admin;

import com.funnyland.funnylive.api.endpoint.inquiry.request.InquiryTypeRequest;
import com.funnyland.funnylive.api.endpoint.inquiry.response.InquiryTypeResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.FrequentlyInquiryService;
import com.funnyland.funnylive.api.service.InquiryTypeService;
import com.funnyland.funnylive.api.service.NoticeService;
import com.funnyland.funnylive.api.util.SecurityUtils;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="어드민", description = "어드민 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/api")
public class AdminController {

    private final InquiryTypeService inquiryTypeService;
    private final NoticeService noticeService;

    @Operation(summary = "문의 타입 추가", description = "문의 타입 추가")
    @PostMapping("/inquiry/types")
    public InquiryTypeResponse create(@RequestBody @Valid InquiryTypeRequest request) {
        return InquiryTypeResponse.of(inquiryTypeService.create(request));
    }

    @Operation(summary = "문의 타입 수정", description = "문의 타입 수정")
    @PutMapping("/inquiry/types/{inquiryTypeId}")
    public InquiryTypeResponse update(@PathVariable Long inquiryTypeId, @RequestBody @Valid InquiryTypeRequest request) {
        InquiryType updatedInquiryType = inquiryTypeService.update(inquiryTypeId, request);

        return InquiryTypeResponse.of(updatedInquiryType);
    }

    @Operation(summary = "문의 타입 삭제", description = "문의 타입 삭제")
    @DeleteMapping("/inquiry/types/{inquiryTypeId}")
    public void deleteInquiryType(@PathVariable Long inquiryTypeId) {
        inquiryTypeService.delete(inquiryTypeId);
    }

    @Operation(summary = "공지사항 삭제", description = "공지사항 삭제")
    @DeleteMapping("/notices/{noticeId}")
    public void deleteNotice(@Parameter(description = "삭제할 공지사항 아이디", example = "1") @PathVariable Long noticeId) {
        noticeService.deleteNotice(noticeId);
    }

}
