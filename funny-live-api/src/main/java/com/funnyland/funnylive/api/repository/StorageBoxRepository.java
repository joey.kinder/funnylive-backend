package com.funnyland.funnylive.api.repository;

import java.util.Optional;

import com.funnyland.funnylive.domain.storagebox.StorageBox;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StorageBoxRepository extends JpaRepository<StorageBox, Long> {

    Page<StorageBox> findByMemberId(Long memberId, Pageable pageable);
    Optional<StorageBox> findByMemberIdAndId(Long memberId, Long storageBoxId);
    Optional<StorageBox> findByMemberId(Long memberId);
    Optional<StorageBox> findById(Long storageBoxId);

}
