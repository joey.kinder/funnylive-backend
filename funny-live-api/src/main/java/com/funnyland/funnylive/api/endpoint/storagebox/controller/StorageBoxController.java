package com.funnyland.funnylive.api.endpoint.storagebox.controller;

import com.funnyland.funnylive.api.endpoint.storagebox.request.StorageBoxRequest;
import com.funnyland.funnylive.api.endpoint.storagebox.response.StorageBoxResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.StorageBoxService;
import com.funnyland.funnylive.api.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name="보관함", description = "보관함 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/storage-boxes")
public class StorageBoxController {

    private final StorageBoxService storageBoxService;

    @Operation(summary = "내 보관함 리스트 조회", description = "내 보관함 리스트 조회")
    @GetMapping("/me")
    public Page<StorageBoxResponse> getMyStorageBoxes(Pageable pageable) {
        SecurityUserDto user = SecurityUtils.getUser();
        return storageBoxService.getStorageBoxes(user.getMemberId(), pageable);
    }

    @Operation(summary = "보관함 상세 조회", description = "보관함 상세 조회")
    @GetMapping("/{storageBoxId}")
    public StorageBoxResponse getStorageBox(@PathVariable Long storageBoxId) {
        SecurityUserDto user = SecurityUtils.getUser();
        return storageBoxService.getStorageBox(user.getMemberId(), storageBoxId);
    }

    @Operation(summary = "보관함 상품 추가", description = "보관함 상품 추가")
    @PostMapping
    public StorageBoxResponse getStorageBox(@RequestBody @Valid StorageBoxRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        return storageBoxService.addStorageBox(user, request.getProductId(), request.getMemberId(), request.getAcquisitionMethod());
    }

}
