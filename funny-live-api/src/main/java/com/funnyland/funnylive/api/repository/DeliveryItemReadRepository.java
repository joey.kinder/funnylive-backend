package com.funnyland.funnylive.api.repository;


import static com.funnyland.funnylive.domain.delivery.QDelivery.delivery;
import static com.funnyland.funnylive.domain.delivery.QDeliveryItem.deliveryItem;

import java.util.List;

import com.funnyland.funnylive.domain.delivery.DeliveryItem;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class DeliveryItemReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public DeliveryItemReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(DeliveryItem.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<DeliveryItem> getDeliveryItems(Long memberId, Pageable pageable) {
        JPAQuery<DeliveryItem> query = jpaQueryFactory.selectFrom(deliveryItem)
                .join(deliveryItem.delivery, delivery)
                .where(delivery.member.id.eq(memberId))
                .orderBy(delivery.createdAt.desc());

        List<DeliveryItem> content = getQuerydsl().applyPagination(pageable, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(deliveryItem.count())
                .from(deliveryItem)
                .join(deliveryItem.delivery, delivery)
                .where(delivery.member.id.eq(memberId));

        return PageableExecutionUtils.getPage(content, pageable, countQuery::fetchOne);
    }
}
