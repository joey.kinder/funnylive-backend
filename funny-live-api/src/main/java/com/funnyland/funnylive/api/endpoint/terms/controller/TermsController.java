package com.funnyland.funnylive.api.endpoint.terms.controller;

import com.funnyland.funnylive.api.endpoint.terms.request.TermsRequest;
import com.funnyland.funnylive.api.endpoint.terms.response.TermsResponse;
import com.funnyland.funnylive.api.service.TermsService;
import com.funnyland.funnylive.domain.term.Term;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="약관", description = "약관 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/terms")
public class TermsController {

    private final TermsService termsService;

    @Operation(summary = "약관 상세 조회", description = "약관 상세 조회")
    @GetMapping("/{termsId}")
    public TermsResponse getTerm(@Parameter(description = "조회할 약관 아이디", example = "1")
                                         @PathVariable Long termsId) {
        return termsService.getTerms(termsId);
    }

    @Operation(summary = "약관 타입별 최신 조회", description = "약관 타입별 최신 조회")
    @GetMapping("/{type}/latest")
    public TermsResponse getTerm(@Parameter(description = "조회할 약관 타입", example = "PRIVACY_POLICY")
                                 @PathVariable Term.TermType type) {
        return termsService.getTermsLatest(type);
    }


}
