package com.funnyland.funnylive.api.interceptor;

import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.MemberService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;


@RequiredArgsConstructor
@Component
public class MemberInterceptor implements HandlerInterceptor {

    private final MemberService memberService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof SecurityUserDto) {
            SecurityUserDto user = (SecurityUserDto) authentication.getPrincipal();
            memberService.saveLoginTime(user.getEmail());
        }
        return true;
    }
}
