package com.funnyland.funnylive.api.endpoint.product.controller;

import com.funnyland.funnylive.api.endpoint.product.facade.ProductFacade;
import com.funnyland.funnylive.api.endpoint.product.request.ProductFilter;
import com.funnyland.funnylive.api.endpoint.product.request.ProductRequest;
import com.funnyland.funnylive.api.endpoint.product.response.ProductResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Tag(name="상품", description = "상품 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/products")
public class ProductController {
    private final ProductFacade productFacade;

    @GetMapping("/{productId}")
    @Operation(summary = "상품 상세 조회", description = "상품 상세 조회")
    public ProductResponse getProduct(@PathVariable Long productId) {
        return productFacade.getProduct(productId);
    }


    @GetMapping
    @Operation(summary = "상품 리스트 조회", description = "상품 리스트 조회")
    public Page<ProductResponse> getProducts(ProductFilter filter) {
        return productFacade.getProducts(filter);
    }


    @PostMapping("/upload")
    @Operation(hidden = true)
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
        String imageUrl = productFacade.uploadImage(file);
        return ResponseEntity.ok(imageUrl);
    }

    @PutMapping("/{productId}/active")
    @Operation(hidden = true)
    public ProductResponse active(@PathVariable Long productId) {
        return productFacade.active(productId);
    }

    @PutMapping("/{productId}/inactive")
    @Operation(hidden = true)
    public ProductResponse inActive(@PathVariable Long productId) {
        return productFacade.inActive(productId);
    }


}
