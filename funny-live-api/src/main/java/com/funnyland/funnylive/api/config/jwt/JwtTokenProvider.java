package com.funnyland.funnylive.api.config.jwt;

import static com.funnyland.funnylive.common.exception.BaseResponseStatus.EXPIRED_JWT;
import static com.funnyland.funnylive.common.exception.BaseResponseStatus.INVALID_JWT;
import static com.funnyland.funnylive.common.exception.BaseResponseStatus.NOT_EXIST_REFRESH_JWT;

import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.crypto.SecretKey;

import com.funnyland.funnylive.api.domain.RefreshToken;
import com.funnyland.funnylive.api.repository.MemberReadRepository;
import com.funnyland.funnylive.api.repository.RefreshTokenRepository;
import com.funnyland.funnylive.api.security.GeneratedToken;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.AccessTokenService;
import com.funnyland.funnylive.api.service.MemberService;
import com.funnyland.funnylive.api.service.dto.UserInfo;
import com.funnyland.funnylive.common.exception.BaseException;
import com.funnyland.funnylive.domain.member.Member;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final JwtProperties jwtProperties;
    private final AccessTokenService tokenService;
    private final RefreshTokenRepository tokenRepository;
    private final MemberService memberService;
    private SecretKey secretKey;
//    private static final Long REFRESH_PERIOD = 1000L * 60L * 60L * 24L * 14; // 2주
    private static final Long REFRESH_PERIOD = 1000L * 60L * 24L * 7; // 2주
    private static final Long THRESHOLD = 1000L * 60L * 60L * 24L * 4; // 4일
    private static final Long ACCESS_PERIOD = 1000L * 60L * 20L; // 30분
//    private static final Long ACCESS_PERIOD = 1000L * 60L * 30L; // 30분

    @PostConstruct
    protected void init() {
        byte[] keyBytes = Base64.getDecoder()
                              .decode(jwtProperties.getSecret());
        this.secretKey = Keys.hmacShaKeyFor(keyBytes);
    }


    public GeneratedToken generateToken(String email, String role) {
        // refreshToken과 accessToken을 생성한다.
        String refreshToken = generateRefreshToken(email, role);
        String accessToken = generateAccessToken(email, role);

        return new GeneratedToken(accessToken, refreshToken);
    }

    public String generateRefreshToken(String email, String role) {
        // 토큰의 유효 기간을 밀리초 단위로 설정.


        // 새로운 클레임 객체를 생성하고, 이메일과 역할(권한)을 셋팅
        Claims claims = Jwts.claims().setSubject(email);
        claims.put("role", role);

        // 현재 시간과 날짜를 가져온다.
        Date now = new Date();


        String refreshToken = Jwts.builder()
                // Payload를 구성하는 속성들을 정의한다.
                .setClaims(claims)
                // 발행일자를 넣는다.
                .setIssuedAt(now)
                // 토큰의 만료일시를 설정한다.
                .setExpiration(new Date(now.getTime() + REFRESH_PERIOD))
                // 지정된 서명 알고리즘과 비밀 키를 사용하여 토큰을 서명한다.
                .signWith(this.secretKey, SignatureAlgorithm.HS256)
                .compact();

        // 토큰을 Redis에 저장한다.
        tokenService.saveTokenInfo(email, refreshToken, REFRESH_PERIOD);
        return refreshToken;
    }


    public String generateAccessToken(String email, String role) {
        Claims claims = Jwts.claims().setSubject(email);
        claims.put("role", role);

        Date now = new Date();
        return
                Jwts.builder()
                    // Payload를 구성하는 속성들을 정의한다.
                    .setClaims(claims)
                    // 발행일자를 넣는다.
                    .setIssuedAt(now)
                    // 토큰의 만료일시를 설정한다.
                    .setExpiration(new Date(now.getTime() + ACCESS_PERIOD))
                    // 지정된 서명 알고리즘과 비밀 키를 사용하여 토큰을 서명한다.
                    .signWith(this.secretKey, SignatureAlgorithm.HS256)
                    .compact();

    }

    public String generateRefreshToken(RefreshToken refreshToken) {
        Claims claims = Jwts.parserBuilder()
                            .setSigningKey(this.secretKey)
                            .build()
                            .parseClaimsJws(refreshToken.getRefreshToken())
                            .getBody();
        Date now = new Date();

        String newRefreshToken =
                Jwts.builder()
                    // Payload를 구성하는 속성들을 정의한다.
                    .setClaims(claims)
                    // 발행일자를 넣는다.
                    .setIssuedAt(now)
                    // 토큰의 만료일시를 설정한다.
                    .setExpiration(new Date(now.getTime() + REFRESH_PERIOD))
                    // 지정된 서명 알고리즘과 비밀 키를 사용하여 토큰을 서명한다.
                    .signWith(this.secretKey, SignatureAlgorithm.HS256)
                    .compact();
        refreshToken.setRefreshToken(newRefreshToken);
        refreshToken.setExpiration(REFRESH_PERIOD/1000);
        tokenRepository.save(refreshToken);

        return newRefreshToken;
    }

    public String generateAccessToken(String refreshToken) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(this.secretKey)
                .build()
                .parseClaimsJws(refreshToken)
                .getBody();
        Date now = new Date();
        return
                Jwts.builder()
                        // Payload를 구성하는 속성들을 정의한다.
                        .setClaims(claims)
                        // 발행일자를 넣는다.
                        .setIssuedAt(now)
                        // 토큰의 만료일시를 설정한다.
                        .setExpiration(new Date(now.getTime() + ACCESS_PERIOD))
                        // 지정된 서명 알고리즘과 비밀 키를 사용하여 토큰을 서명한다.
                        .signWith(this.secretKey, SignatureAlgorithm.HS256)
                        .compact();

    }


    public boolean verifyToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parserBuilder()
                    .setSigningKey(this.secretKey)// 비밀키를 설정하여 파싱한다.
                    .build()
                    .parseClaimsJws(token); // 주어진 토큰을 파싱하여 Claims 객체를 얻는다.

            // 토큰의 만료 시간과 현재 시간비교
            return claims.getBody()
                         .getExpiration()
                         .after(new Date());  // 만료 시간이 현재 시간 이후인지 확인하여 유효성 검사 결과를 반환
        } catch (Exception e) {
            return false;
        }
    }


    // 토큰에서 Email을 추출한다.
    public String getUid(String token) {
        try{
            return Jwts.parserBuilder()
                    .setSigningKey(this.secretKey)
                    .build()
                    .parseClaimsJws(token).getBody().getSubject();
        }
        catch (Exception e) {
            log.info("토큰 만료");
            return null;
        }

    }

    // 토큰에서 ROLE(권한)만 추출한다.
    public String getRole(String token) {
        return Jwts.parserBuilder()
                                        .setSigningKey(this.secretKey)// 비밀키를 설정하여 파싱한다.
                                        .build()
                                        .parseClaimsJws(token).getBody().get("role", String.class);
    }

    public Authentication getAuthentication(String token) {
        String email = Jwts.parserBuilder()
                .setSigningKey(this.secretKey)// 비밀키를 설정하여 파싱한다.
                .build()
                .parseClaimsJws(token)
                .getBody().getSubject();
        Member findMember = memberService.getMember(email);

        SecurityUserDto userDto = SecurityUserDto.builder()
                .memberId(findMember.getId())
                .email(findMember.getEmail())
                .role(findMember.getRole().name())
                .nickname(findMember.getNickName())
                .build();
        return new UsernamePasswordAuthenticationToken(userDto, "",
                                                       List.of(new SimpleGrantedAuthority(userDto.getRole())));
    }


    public String generateTokenWithUserInfo(String email) {
        long tokenPeriod = 1000L * 60L * 3L; // 3분
        Claims claims = Jwts.claims().setSubject(email);
        claims.put("email", email);

        Date now = new Date();
        return
                Jwts.builder()
                    // Payload를 구성하는 속성들을 정의한다.
                    .setClaims(claims)
                    // 발행일자를 넣는다.
                    .setIssuedAt(now)
                    // 토큰의 만료일시를 설정한다.
                    .setExpiration(new Date(now.getTime() + tokenPeriod))
                    // 지정된 서명 알고리즘과 비밀 키를 사용하여 토큰을 서명한다.
                    .signWith(this.secretKey, SignatureAlgorithm.HS256)
                    .compact();
    }

    public UserInfo parseToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parserBuilder()
                                     .setSigningKey(this.secretKey)// 비밀키를 설정하여 파싱한다.
                                     .build()
                                     .parseClaimsJws(token); // 주어진 토큰을 파싱하여 Claims 객체를 얻는다.

            // 토큰의 만료 시간과 현재 시간비교
            Claims body = claims.getBody();
            String email = body.get("email", String.class);
            String provider = body.get("provider", String.class);
            String name = body.get("name", String.class);
            String phone = body.get("phone", String.class);
            return new UserInfo(email, provider, name, phone);
        } catch (Exception e) {
            log.error("jwt parsing error", e);
        }
        return new UserInfo();
    }

    public String resolveToken(HttpServletRequest req) {
        return req.getHeader("Authorization");
    }

    /**
     * Access 토큰을 검증
     */
    public boolean validateToken(String token){
        try{
            Jwts.parserBuilder()
                    .setSigningKey(this.secretKey)// 비밀키를 설정하여 파싱한다.
                    .build()
                    .parseClaimsJws(token); // 주어진 토큰을 파싱하여 Claims 객체를 얻는다.
            return true;
        } catch(ExpiredJwtException e) {
            log.error(EXPIRED_JWT.getMessage());
            throw e;
        } catch(JwtException e) {
            log.error(INVALID_JWT.getMessage());
            throw new BaseException(INVALID_JWT);
        }
    }

    /**
     * Refresh 토큰을 검증
     */
    public boolean validateRefreshToken(String token){
        try{
            Jwts.parserBuilder()
                    .setSigningKey(this.secretKey)// 비밀키를 설정하여 파싱한다.
                    .build()
                    .parseClaimsJws(token); // 주어진 토큰을 파싱하여 Claims 객체를 얻는다.
            return true;
        } catch(ExpiredJwtException e) {
            log.error(NOT_EXIST_REFRESH_JWT.getMessage());
            return false;
        } catch(JwtException e) {
            log.error(INVALID_JWT.getMessage());
            return false;
        }
    }

    public boolean isRefreshTokenExpiringSoon(String refreshToken) {
        try {
            Date expiration = Jwts.parserBuilder()
                                  .setSigningKey(this.secretKey)
                                  .build()
                                  .parseClaimsJws(refreshToken)
                                  .getBody()
                                  .getExpiration();
            long diff = expiration.getTime() - System.currentTimeMillis();
            return diff < THRESHOLD;
        } catch (Exception e) {
            log.error("Error checking if refreshToken is expiring soon", e);
            return false;
        }
    }

    //    public void validateRefreshToken(String refreshToken, String oldAccessToken) {
//        tokenService.
//    }
}
