package com.funnyland.funnylive.api.endpoint.cranegame.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CraneFeeResponse {
    private int playFee;


}
