package com.funnyland.funnylive.api.domain;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.funnyland.funnylive.domain.cranegame.CraneGame;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("CraneGameHeartbeat")
@Builder
public class CraneGameHeartbeat implements Serializable {

    @Serial
    private static final long serialVersionUID = 1234567890123456L;

    @Id
    private Long id; // 게임 기기의 고유 ID

    private LocalDateTime lastHeartbeatTime; // 마지막 Heartbeat 통신 시간

    public static CraneGameHeartbeat of(CraneGame craneGame) {
        CraneGameHeartbeat heartbeat = CraneGameHeartbeat.builder()
                .id(craneGame.getId())
                .lastHeartbeatTime(LocalDateTime.now())
                .build();
        return heartbeat;
    }
}
