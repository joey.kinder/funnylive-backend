package com.funnyland.funnylive.api.endpoint.member.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MemberWithdrawRequest {

    @NotBlank
    @Size(min = 10, max = 100, message = "Reason must be between 10 and 100 characters")
    private String reason;
}
