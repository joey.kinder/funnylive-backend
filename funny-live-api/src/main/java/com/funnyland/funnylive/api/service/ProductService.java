package com.funnyland.funnylive.api.service;

import com.funnyland.funnylive.api.endpoint.product.request.ProductFilter;
import com.funnyland.funnylive.api.endpoint.product.request.ProductRequest;
import com.funnyland.funnylive.api.repository.ProductReadRepository;
import com.funnyland.funnylive.api.repository.ProductRepository;
import com.funnyland.funnylive.api.service.dto.StorageType;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.product.ProductDomainService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final StorageService storageService;
    private final ProductDomainService productDomainService;
    private final ProductReadRepository productReadRepository;

    public Page<Product> getProducts(ProductFilter filter) {
        return productReadRepository.getProducts(filter.generateBooleanBuilder(), filter.getPageRequest());
    }


    public Product createProduct(ProductRequest request) {
        Product product = request.toDomain();

        MultipartFile image = request.getImage();
        if (!ObjectUtils.isEmpty(image)) {
            // 파일 크기 제한 검사 (30MB)
            if (image.getSize() > 30 * 1024 * 1024) {
                throw new RuntimeException("File size exceeds the limit (30MB)");
            }
            UploadDto uploadDto = storageService.upload(image, StorageType.IMAGE);
            product.addImage(uploadDto);
        }

        Product savedProduct = productRepository.save(product);
        return savedProduct;
    }

    public String uploadImage(MultipartFile file) {
        UploadDto uploadDto = storageService.upload(file, StorageType.IMAGE);
        return uploadDto.getFilePath();
    }

    public Product getProduct(Long productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product not exist"));
    }

    @Transactional
    public Product updateProduct(Long productId, ProductRequest request) {
        Product updateProduct = request.toDomain();

        MultipartFile image = request.getImage();
        if (!ObjectUtils.isEmpty(image)) {
            // 파일 크기 제한 검사 (30MB)
            if (image.getSize() > 30 * 1024 * 1024) {
                throw new RuntimeException("File size exceeds the limit (30MB)");
            }
            UploadDto uploadDto = storageService.upload(image, StorageType.IMAGE);
            updateProduct.addImage(uploadDto);
        }

        return productDomainService.update(productId, updateProduct);
    }

    @Transactional
    public Product active(Long productId) {
        return productRepository.findById(productId)
                .map(Product::active)
                .orElseThrow(() -> new RuntimeException("Product not exist"));
    }

    @Transactional
    public Product inActive(Long productId) {
        return productRepository.findById(productId)
                .map(Product::inActive)
                .orElseThrow(() -> new RuntimeException("Product not exist"));
    }
}
