package com.funnyland.funnylive.api.endpoint.deliveryaddress.controller;

import java.util.List;

import com.funnyland.funnylive.api.endpoint.deliveryaddress.facade.DeliveryAddressFacade;
import com.funnyland.funnylive.api.endpoint.deliveryaddress.request.DeliveryAddressRequest;
import com.funnyland.funnylive.api.endpoint.deliveryaddress.response.DeliveryAddressResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/delivery-address")
public class DeliveryAddressController {

    private final DeliveryAddressFacade deliveryAddressFacade;

    @PostMapping
    @Operation(hidden = true)
    public DeliveryAddressResponse createDeliveryAddress(@RequestBody DeliveryAddressRequest deliveryAddressRequest) {
        SecurityUserDto user = SecurityUtils.getUser();
        return deliveryAddressFacade.createDeliveryAddress(user.getMemberId(), deliveryAddressRequest);
    }

    @GetMapping
    @Operation(hidden = true)
    public List<DeliveryAddressResponse> getDeliveryAddressList() {
        Long userId = 1L;
        return deliveryAddressFacade.getDeliveryAddressList(userId);
    }

    @GetMapping("/{deliveryAddressId}")
    @Operation(hidden = true)
    public DeliveryAddressResponse getDeliveryAddress(@PathVariable Long deliveryAddressId) {
        Long userId = 1L;
        return deliveryAddressFacade.getDeliveryAddress(userId, deliveryAddressId);
    }

    @DeleteMapping("/{deliveryAddressId}")
    @Operation(hidden = true)
    public ResponseEntity<String> deleteDeliveryAddress(@PathVariable Long deliveryAddressId) {
        try {
            Long userId = 1L;
            deliveryAddressFacade.deleteDeliveryAddress(userId, deliveryAddressId);
            return ResponseEntity.ok("배송지 삭제 성공");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("배송지 삭제에 실패했습니다. " + e.getMessage());
        }

    }
}
