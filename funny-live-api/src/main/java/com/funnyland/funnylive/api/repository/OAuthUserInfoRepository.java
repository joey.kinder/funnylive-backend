package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.api.domain.OAuthUserInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OAuthUserInfoRepository extends CrudRepository<OAuthUserInfo, String> {
}
