package com.funnyland.funnylive.api.config.jwt;

import static com.funnyland.funnylive.common.exception.BaseResponseStatus.*;
import static com.funnyland.funnylive.common.exception.BaseResponseStatus.REDIS_ERROR;

import java.io.IOException;

import com.funnyland.funnylive.common.exception.BaseException;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;


@RequiredArgsConstructor
@Slf4j
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtTokenProvider jwtTokenProvider;

//    @Override
//    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
//        return request.getRequestURI().contains("token/refresh");
//    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // request Header에서 AccessToken을 가져온다.
        String accessToken = jwtTokenProvider.resolveToken(request);

        // 토큰 검사 생략(모두 허용 URL의 경우 토큰 검사 통과)
        if (!StringUtils.hasText(accessToken)) {
            doFilter(request, response, filterChain);
            return;
        }

        // AccessToken을 검증하고, 만료되었을경우 예외를 발생시킨다.
        try {
            if (accessToken != null && jwtTokenProvider.validateToken(accessToken)) {
                Authentication auth = jwtTokenProvider.getAuthentication(accessToken);
                SecurityContextHolder.getContext()
                        .setAuthentication(auth); // 정상 토큰이면 SecurityContext에 저장
            }
        } catch (ExpiredJwtException e) {
            log.info("accessToken 만료");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "accessToken 만료.");
            return;
        } catch (RedisConnectionFailureException e) {
            log.info("redis 연결 실패");
            SecurityContextHolder.clearContext();
            throw new BaseException(REDIS_ERROR);
        } catch (Exception e) {
            log.info("잘못된 토큰");
            throw new BaseException(INVALID_JWT);
        }

        filterChain.doFilter(request, response);
    }

}
