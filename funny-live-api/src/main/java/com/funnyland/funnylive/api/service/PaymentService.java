package com.funnyland.funnylive.api.service;

import com.funnyland.funnylive.api.config.GoogleCredentialsConfig;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.model.ProductPurchase;
import com.google.api.services.androidpublisher.model.ProductPurchasesAcknowledgeRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
public class PaymentService {
    private final GoogleCredentialsConfig googleCredentialsConfig;

    public boolean processGoogleInAppPurchase(String packageName, String productId, String purchaseToken) {
        try {
            // 영수증 검증
            if (googleInAppPurchaseVerify(packageName, productId, purchaseToken)) {
                // 결제 승인
                acknowledgePurchase(packageName, productId, purchaseToken);
                return true;
            }
        } catch (CustomException e) {
            log.error("PaymentService.processGoogleInAppPurchase : Error processing Google In-App Purchase: {}", e.getMessage());
        }
        return false;
    }


    public boolean googleInAppPurchaseVerify(String packageName, String productId, String purchaseToken) {

        try {
            AndroidPublisher publisher = googleCredentialsConfig.androidPublisher();

            AndroidPublisher.Purchases.Products.Get get = publisher.purchases()
                                                                   .products().
                                                                   get(packageName, productId, purchaseToken);
            ProductPurchase purchase = get.execute();

            /**
             * purchaseState = 0 : 결제완료
             * purchaseState = 1 : 취소된 결제
             */
            //
            if (purchase.getPurchaseState() == 1) {
                throw new CustomException(ErrorCode.PAYMENT_ALREADY_CANCELED);
            }

            return true;
        }catch (Exception e) {
            log.error("PaymentService.googleInAppPurchaseVerify", e);
        }

        return false;
    }

    public boolean consumeProduct(String packageName, String productId, String purchaseToken) {
        try {
            AndroidPublisher publisher = googleCredentialsConfig.androidPublisher();
            // 상품 소비 로직 구현
            publisher.purchases().products().consume(packageName, productId, purchaseToken).execute();
            return true;
        } catch (Exception e) {
            log.error("PaymentService.consumeProduct : Error consuming product={}, purchaseToken={}", productId, purchaseToken);
            throw new CustomException(ErrorCode.PAYMENT_CONSUME_PRODUCT);
        }
    }

    private void acknowledgePurchase(String packageName, String productId, String purchaseToken) {
        try {
            AndroidPublisher publisher = googleCredentialsConfig.androidPublisher();
            // 구매 상태 확인
            ProductPurchase purchase = publisher.purchases().products().get(packageName, productId, purchaseToken).execute();
            /**
             * acknowledgementState = 1 : 구매 승인 완료
             */
            if (purchase.getAcknowledgementState() == 1) {
                log.debug("PaymentService.acknowledge : Purchase already acknowledged package={}, productId={}, purchaseToken={}", packageName, productId, purchaseToken);
                return; // 이미 승인된 경우, 추가 승인 절차 건너뜀
            }

            // 구매 승인
            ProductPurchasesAcknowledgeRequest acknowledgeRequest = new ProductPurchasesAcknowledgeRequest();
            publisher.purchases().products().acknowledge(packageName, productId, purchaseToken, acknowledgeRequest).execute();
        } catch (Exception e) {
            log.error("PaymentService.acknowledge : PurchaseError acknowledging purchase");
            throw new CustomException(ErrorCode.PAYMENT_FAILED_ACKNOWLEDGE);
        }
    }

    // 필요한 경우 상품 소비 처리
    public boolean consumeProductIfNeeded(String packageName, String productId, String purchaseToken) {
        try {
            AndroidPublisher publisher = googleCredentialsConfig.androidPublisher();
            AndroidPublisher.Purchases.Products.Get get = publisher.purchases().products().get(packageName, productId, purchaseToken);
            ProductPurchase purchase = get.execute();
            /**
             * purchaseType = 0 : 일회성 상품
             * consumptionState = 0 : 소비 전 상품
             */
            if (purchase.getPurchaseType() == 0 && purchase.getConsumptionState() == 0) {
                consumeProduct(packageName, productId, purchaseToken);
            }
            return true;
        } catch (Exception e) {
            log.error("PaymentService.consumeProductIfNeeded : Error processing product consumption package={}, productId={}, purchaseToken={}", packageName, productId, purchaseToken);
            throw new CustomException(ErrorCode.PAYMENT_ALREADY_CONSUMED);
        }
    }
}
