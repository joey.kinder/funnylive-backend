package com.funnyland.funnylive.api.repository;

import com.funnyland.funnylive.domain.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductRepository extends JpaRepository<Product, Long> {
}
