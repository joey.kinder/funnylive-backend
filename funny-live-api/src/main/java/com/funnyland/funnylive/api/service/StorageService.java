package com.funnyland.funnylive.api.service;

import com.funnyland.funnylive.api.service.dto.StorageType;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import org.springframework.web.multipart.MultipartFile;


public interface StorageService {
    UploadDto upload(MultipartFile file, StorageType type);
}
