package com.funnyland.funnylive.api.endpoint.cranegame.facade;

import com.funnyland.funnylive.api.endpoint.cranegame.request.CraneGameDrawRequest;
import com.funnyland.funnylive.api.endpoint.cranegame.request.CraneGameStateRequest;
import com.funnyland.funnylive.api.endpoint.cranegame.response.CraneGameResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.CraneGameService;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
public class CraneGameFacade {

    private final CraneGameService craneGameService;

    public CraneGameResponse updateCraneGameState(Long craneGameId, CraneGameStateRequest craneGameStateRequest) {
        CraneGame updatedCraneGame = craneGameService.updateCraneGameState(craneGameId, craneGameStateRequest);
        return CraneGameResponse.of(updatedCraneGame);
    }

    public void heartbeat(Long craneGameId) {
        craneGameService.heartbeat(craneGameId);
    }

    public CraneGameResponse updateThumbnailCraneGame(Long craneGameId, MultipartFile image) {
        CraneGame updatedCraneGame = craneGameService.updateThumbnailCraneGame(craneGameId, image);
        return CraneGameResponse.of(updatedCraneGame);
    }

    public boolean startGame(Long craneGameId, Long timeoutSecond) {
        return craneGameService.startGame(craneGameId, timeoutSecond);
    }

    public boolean restartGame(Long craneGameId, Long timeoutSecond) {
        return craneGameService.restartGame(craneGameId, timeoutSecond);
    }

    public boolean endGame(Long craneGameId) {
        return craneGameService.endGame(craneGameId);
    }

    public boolean draw(SecurityUserDto user, Long craneGameId, CraneGameDrawRequest request) {
        return craneGameService.draw(user, craneGameId, request);
    }
}
