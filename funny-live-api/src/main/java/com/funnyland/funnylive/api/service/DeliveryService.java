package com.funnyland.funnylive.api.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import com.funnyland.funnylive.api.endpoint.delivery.request.DeliveryRequest;
import com.funnyland.funnylive.api.endpoint.delivery.response.DeliveryDetailResponse;
import com.funnyland.funnylive.api.endpoint.delivery.response.DeliveryResponse;
import com.funnyland.funnylive.api.repository.DeliveryReadRepository;
import com.funnyland.funnylive.api.repository.StorageBoxRepository;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.delivery.DeliveryDomainService;
import com.funnyland.funnylive.domain.delivery.DeliveryItem;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import com.funnyland.funnylive.domain.storagebox.StorageBox;
import com.funnyland.funnylive.domain.storagebox.StorageBoxHistory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class DeliveryService {

    private final MemberDomainService memberDomainService;
    private final StorageBoxRepository storageBoxRepository;
    private final DeliveryReadRepository deliveryReadRepository;
    private final DeliveryDomainService deliveryDomainService;
    private final PointService pointService;
    private static final int FREE_DELIVERY_THRESHOLD = 3;
    private static final int DELIVERY_FEE = 3000;

    /**
     * delivery 상세 조회
     */
    public DeliveryDetailResponse getDelivery(SecurityUserDto user, Long deliveryId) {
        Delivery findDelivery = deliveryReadRepository.findById(deliveryId)
                .orElseThrow(() -> new CustomException(ErrorCode.DELIVERY_NOT_FOUND));

        user.authVerify(findDelivery.getMember().getId());

        return DeliveryDetailResponse.of(findDelivery);
    }

    /**
     * delivery 생성
     */
    @Transactional
    public void processDelivery(SecurityUserDto user, DeliveryRequest request) {
        // 회원 찾기
        Member member = memberDomainService.find(user.getMemberId());
        user.authVerify(request.getMemberId());

        StorageBox findStorageBox = storageBoxRepository.findByMemberId(member.getId())
                .orElseThrow(() -> new CustomException(ErrorCode.STORAGE_BOX_NOT_FOUND));

        // 사용 가능한 상품 수량 검증
        verifyAvailableQuantities(findStorageBox, request.getItems());

        // 배송 아이템을 3개 단위로 분할
        List<List<DeliveryRequest.DeliveryItemRequest>> splitBundles = splitDeliveryItems(request.getItems());

        for (List<DeliveryRequest.DeliveryItemRequest> bundle : splitBundles) {
            int bundleQuantity = bundle.stream().mapToInt(DeliveryRequest.DeliveryItemRequest::getQuantity).sum();
            int deliveryFee = calculateDeliveryFee(bundleQuantity);
            Delivery delivery = createDelivery(member, request, deliveryFee);

            // 배송 요청 처리
            processBundle(member, delivery, bundle);

            deliveryDomainService.save(delivery);
            pointService.usePoints(member, PointTransactionType.SHIPPING_FEE, deliveryFee);
        }

    }

    /**
     * 배송 리스트 조회
     */
    public DeliveryResponse getDeliveries(Long memberId, Pageable pageable) {
        long pendingCount = deliveryReadRepository.countByMemberIdAndStatus(memberId, Delivery.DeliveryStatus.PENDING);
        long shippedCount = deliveryReadRepository.countByMemberIdAndStatus(memberId, Delivery.DeliveryStatus.SHIPPED);
        long deliveredCount = deliveryReadRepository.countByMemberIdAndStatus(memberId, Delivery.DeliveryStatus.DELIVERED);

        Page<Delivery> deliveries = deliveryReadRepository.getDeliveries(memberId, pageable);
        Page<DeliveryResponse.DeliveryItemResponse> deliveryResponses = deliveries.map(DeliveryResponse.DeliveryItemResponse::of);

        return DeliveryResponse.of(pendingCount, shippedCount, deliveredCount, deliveryResponses);
    }


    //----------------------------------------------------------------------------------------------------------------------------------------------------------
    // private
    //----------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * 묶음 배송을 보관함에서 배송 처리
     */
    private void processBundle(Member member, Delivery delivery, List<DeliveryRequest.DeliveryItemRequest> bundle) {
        for (DeliveryRequest.DeliveryItemRequest itemRequest : bundle) {
            StorageBox findStorageBox = storageBoxRepository.findByMemberId(member.getId())
                    .orElseThrow(() -> new RuntimeException("StorageBox not found"));
            List<StorageBoxHistory> selectedHistories = findStorageBox.processDelivery(itemRequest.getProductId(), itemRequest.getQuantity());

            for (StorageBoxHistory history : selectedHistories) {
                createDeliveryItems(delivery, history);
            }
        }
    }

    /**
     * 배송 아이템 생성
     */
    private void createDeliveryItems(Delivery delivery, StorageBoxHistory history) {
        DeliveryItem deliveryItem = DeliveryItem.builder()
                .productId(history.getProductId())
                .productName(history.getProductName())
                .productPrice(history.getProductPrice())
                .productImageURL(history.getProductImageURL())
                .originProduct(history.getOriginProduct())
                .storageBoxHistory(history)
                .quantity(1)
                .build();
        delivery.addDeliveryItem(deliveryItem);
    }

    /**
     * 배송 요청 가능한 수량 검증
     */
    private void verifyAvailableQuantities(StorageBox storageBox, List<DeliveryRequest.DeliveryItemRequest> items) {
        Map<Long, Long> availableQuantities = storageBox.getHistories()
                .stream()
                .filter(history -> !history.isDeliveryRequested())
                .collect(Collectors.groupingBy(StorageBoxHistory::getProductId,
                                               Collectors.counting()));

        // 요청된 상품이 실제로 사용 가능한지 검증
        for (DeliveryRequest.DeliveryItemRequest item : items) {
            long availableQuantity = availableQuantities.getOrDefault(item.getProductId(), 0L);
            if (item.getQuantity() > availableQuantity) {
                throw new IllegalArgumentException("Requested quantity for productId " + item.getProductId() +
                                                           " exceeds available quantity. Available: " + availableQuantity + ", Requested: " + item.getQuantity());
            }
        }
    }



    /**
     * delivery 도메인 생성
     */
    private Delivery createDelivery(Member member, DeliveryRequest request, int deliveryFee) {
        String orderNumber = generateOrderNumber(); // 주문 번호 생성
        return Delivery.builder()
                .orderNumber(orderNumber)
                .member(member)
                .roadNameAddress(request.getRoadNameAddress())
                .inputDetail(request.getInputDetail())
                .postalCode(request.getPostalCode())
                .recipientName(request.getRecipientName())
                .recipientPhone(request.getRecipientPhone())
                .requirement(request.getRequirement())
                .deliveryFee(deliveryFee)
                .status(Delivery.DeliveryStatus.PENDING)
                .build();
    }

    /**
     * 배송비 계산
     */
    private int calculateDeliveryFee(int numberOfItems) {
        int chargeableItems = numberOfItems % FREE_DELIVERY_THRESHOLD;
        return chargeableItems > 0 ? DELIVERY_FEE : 0;
    }

    /**
     * 주문번호 생성 yyyyMMddHHmmssSSS + 랜덤숫자 4자리
     */
    private String generateOrderNumber() {
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        int randomNum = ThreadLocalRandom.current().nextInt(1000, 10000);
        return timestamp + randomNum;
    }

    /**
     * 배송 요청한 아이템 N개씩 배송 아이템으로 분리
     */
    private List<List<DeliveryRequest.DeliveryItemRequest>> splitDeliveryItems(List<DeliveryRequest.DeliveryItemRequest> items) {
        List<List<DeliveryRequest.DeliveryItemRequest>> splitBundles = new ArrayList<>();
        List<DeliveryRequest.DeliveryItemRequest> currentBundle = new ArrayList<>();
        int currentBundleQuantity = 0;

        for (DeliveryRequest.DeliveryItemRequest item : items) {
            int remainingQuantity = item.getQuantity();
            while (remainingQuantity > 0) {
                if (currentBundleQuantity < FREE_DELIVERY_THRESHOLD) {
                    int quantityToAdd = Math.min(FREE_DELIVERY_THRESHOLD - currentBundleQuantity, remainingQuantity);
                    currentBundle.add(new DeliveryRequest.DeliveryItemRequest(item.getProductId(), quantityToAdd));
                    currentBundleQuantity += quantityToAdd;
                    remainingQuantity -= quantityToAdd;
                }

                if (currentBundleQuantity == FREE_DELIVERY_THRESHOLD) {
                    splitBundles.add(new ArrayList<>(currentBundle));
                    currentBundle.clear();
                    currentBundleQuantity = 0;
                }
            }
        }

        // 마지막 남은 아이템들을 추가
        if (!currentBundle.isEmpty()) {
            splitBundles.add(currentBundle);
        }

        return splitBundles;
    }

    //    @Transactional
    //    public void createDelivery(SecurityUserDto user, DeliveryRequest request) {
    //        Member member = memberDomainService.find(user.getMemberId());
    //        List<CartItem> cartItems = cartItemRepository.findByMemberId(user.getMemberId());
    //
    //        if (cartItems.isEmpty()) {
    //            throw new IllegalArgumentException("No items in cart");
    //        }
    //
    //        Delivery delivery = Delivery.builder()
    //                .member(member)
    //                .roadNameAddress(request.getRoadNameAddress())
    //                .inputDetail(request.getInputDetail())
    //                .postalCode(request.getPostalCode())
    //                .recipientName(request.getRecipientName())
    //                .recipientPhone(request.getRecipientPhone())
    //                .status(Delivery.DeliveryStatus.PENDING)
    //                .build();
    //
    //        for (CartItem cartItem : cartItems) {
    //            DeliveryItem deliveryItem = DeliveryItem.builder()
    //                    .productId(cartItem.getProductId())
    //                    .productName(cartItem.getProductName())
    //                    .productImageURL(cartItem.getProductImageURL())
    //                    .productPrice(cartItem.getProductPrice())
    //                    .quantity(cartItem.getQuantity())
    //                    .build();
    //            delivery.addDeliveryItem(deliveryItem);
    //        }
    //        deliveryRepository.save(delivery);
    //    }
}
