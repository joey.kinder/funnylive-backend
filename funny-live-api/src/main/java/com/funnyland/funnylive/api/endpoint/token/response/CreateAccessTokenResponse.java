package com.funnyland.funnylive.api.endpoint.token.response;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public class CreateAccessTokenResponse {
    private String accessToken;
}
