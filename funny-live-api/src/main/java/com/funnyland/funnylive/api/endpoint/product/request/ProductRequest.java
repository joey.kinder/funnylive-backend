package com.funnyland.funnylive.api.endpoint.product.request;

import com.funnyland.funnylive.domain.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {
    private String name;
    private Integer price;
    private String productType;
    private MultipartFile image;

    public Product toDomain() {
        Product product = Product.builder()
                .name(this.name)
                .price(this.price)
                .productType(this.productType)
                .build();
        return product;
    }

}