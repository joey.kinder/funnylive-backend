package com.funnyland.funnylive.api.endpoint.inquiry.controller;

import com.funnyland.funnylive.api.endpoint.inquiry.facade.InquiryFacade;
import com.funnyland.funnylive.api.endpoint.inquiry.request.InquiryRequest;
import com.funnyland.funnylive.api.endpoint.inquiry.response.InquiryMyPageResponse;
import com.funnyland.funnylive.api.endpoint.inquiry.response.InquiryResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@Tag(name="문의", description = "문의 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/inquiries")
@Slf4j
public class InquiryController {

    private final InquiryFacade inquiryFacade;

    @Operation(summary = "문의 접수", description = "문의 접수")
    @PostMapping
    public InquiryResponse create(@RequestPart("inquiry") @Valid InquiryRequest request, @RequestPart(value = "file", required = false) MultipartFile file) {
        SecurityUserDto user = SecurityUtils.getUser();
        return inquiryFacade.create(request, user.getMemberId(), file);
    }

    @Operation(summary = "문의 리스트 조회", description = "문의 리스트 조회")
    @GetMapping
    public Page<InquiryResponse> getInquiries(Pageable pageable) {
        return inquiryFacade.getInquiries(pageable);
    }

    @Operation(summary = "문의 상세 조회", description = "문의 상세 조회")
    @GetMapping("/{inquiryId}")
    public InquiryResponse getInquiry(@Parameter(description = "조회할 문의 아이디", example = "1") @PathVariable Long inquiryId) {
        return inquiryFacade.getInquiry(inquiryId);
    }

    @Operation(summary = "문의 수정", description = "문의 수정")
    @PutMapping("{inquiryId}")
    public InquiryResponse update(@Parameter(description = "수정할 문의 아이디", example = "1") @PathVariable Long inquiryId,
                                  @RequestPart("inquiry") @Valid InquiryRequest inquiryRequest, @RequestPart(value = "file", required = false) MultipartFile file) {
        SecurityUserDto user = SecurityUtils.getUser();
        return inquiryFacade.update(inquiryId, inquiryRequest, file, user);
    }

    @Operation(summary = "내 문의 조회", description = "내 문의 조회")
    @GetMapping("/me")
    public Page<InquiryResponse> getMyInquiry(Pageable pageable) {
        SecurityUserDto user = SecurityUtils.getUser();
        return inquiryFacade.getMyInquiry(user.getMemberId(), pageable);
    }

    @Operation(summary = "마이페이지 내 온라인 문의 조회", description = "마이페이지 내 온라인 문의 조회")
    @GetMapping("/me/count")
    public InquiryMyPageResponse getMyInquiry() {
        SecurityUserDto user = SecurityUtils.getUser();
        return inquiryFacade.getMyInquiryInMyPage(user.getMemberId());
    }

}
