package com.funnyland.funnylive.api.endpoint.cart.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.cart.CartItem;
import com.funnyland.funnylive.domain.deliveryaddress.DeliveryAddress;
import com.funnyland.funnylive.domain.member.Member;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartResponse {

    private Long memberId;
    private String memberName;
    private String phone;
    private String roadNameAddress;
    private String inputDetail;
    private String postalCode;
    private List<CartItemResponse> cartItems;

    public static CartResponse of(Member member) {
        Optional<DeliveryAddress> deliveryAddress = member.getDeliveryAddresses()
                .stream()
                .filter(DeliveryAddress::isDefault)
                .findFirst();

        String roadNameAddress = deliveryAddress.map(DeliveryAddress::getRoadNameAddress).orElse(null);
        String postalCode = deliveryAddress.map(DeliveryAddress::getPostalCode).orElse(null);
        String inputDetail = deliveryAddress.map(DeliveryAddress::getInputDetail).orElse(null);

//        List<CartItemResponse> cartItems = member.getCartItems()
//                .stream()
//                .map(CartItemResponse::of)
//                .collect(Collectors.toList());
//        return CartResponse.builder()
//                .memberId(member.getId())
//                .memberName(member.getName())
//                .phone(member.getPhone())
//                .roadNameAddress(roadNameAddress)
//                .postalCode(postalCode)
//                .inputDetail(inputDetail)
//                .cartItems(cartItems)
//                .build();
        return new CartResponse();
    }


    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    public static class CartItemResponse {
        private Long cartItemId;
        private Long productId;
        private String productName;
        private String productImageUrl;
        private Integer quantity;

        public static CartItemResponse of(CartItem cartItem) {
            return CartItemResponse.builder()
                    .cartItemId(cartItem.getId())
                    .productId(cartItem.getProductId())
                    .productName(cartItem.getProductName())
                    .productImageUrl(UrlUtil.convertToCdnUrl(cartItem.getProductImageURL()))
                    .quantity(cartItem.getQuantity())
                    .build();
        }
    }



}
