package com.funnyland.funnylive.api.endpoint.notice.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.notice.Notice;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class NoticeResponse {

    private Long noticeId;
    private String type;
    private String title;
    private String content;
    private String updater;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;

    public static NoticeResponse of(Notice notice) {
        return NoticeResponse.builder()
                .noticeId(notice.getId())
                .type(notice.getNoticeType().getName())
                .title(notice.getTitle())
                .content(notice.getContent())
                .updater(notice.getLastModifiedBy())
                .createdAt(notice.getCreatedAt())
                .build();
    }

}
