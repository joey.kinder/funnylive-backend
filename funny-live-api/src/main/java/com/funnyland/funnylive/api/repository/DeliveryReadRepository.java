package com.funnyland.funnylive.api.repository;


import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.product.Product;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import static com.funnyland.funnylive.domain.cranegame.QCraneGame.craneGame;
import static com.funnyland.funnylive.domain.delivery.QDelivery.delivery;
import static com.funnyland.funnylive.domain.delivery.QDeliveryItem.deliveryItem;
import static com.funnyland.funnylive.domain.product.QProduct.product;


@Repository
public class DeliveryReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public DeliveryReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(Delivery.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<Delivery> getDeliveries(Long memberId, Pageable pageable) {
        JPAQuery<Delivery> query = jpaQueryFactory.selectFrom(delivery)
                .leftJoin(delivery.items, deliveryItem).fetchJoin()
                .where(delivery.member.id.eq(memberId))
                .orderBy(delivery.createdAt.desc());

        List<Delivery> content = getQuerydsl().applyPagination(pageable, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(delivery.count())
                .from(delivery)
                .where(delivery.member.id.eq(memberId));
        return PageableExecutionUtils.getPage(content, pageable, countQuery::fetchOne);
    }

    public Optional<Delivery> findById(Long deliveryId) {
        JPAQuery<Delivery> query = jpaQueryFactory.selectFrom(delivery)
                .where(delivery.id.eq(deliveryId));
        return Optional.ofNullable(query.fetchOne());
    }

    public Long countByMemberIdAndStatus(Long memberId, Delivery.DeliveryStatus state) {
        return jpaQueryFactory.select(delivery.id.count())
                .from(delivery)
                .where(delivery.member.id.eq(memberId).and(delivery.status.eq(state)))
                .fetchOne();
    }

}
