package com.funnyland.funnylive.api.repository;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.cart.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CartItemRepository extends JpaRepository<CartItem, Long> {

    List<CartItem> findByMemberId(Long memberId);

    Optional<CartItem> findByMemberIdAndProductId(Long member, Long product);
}