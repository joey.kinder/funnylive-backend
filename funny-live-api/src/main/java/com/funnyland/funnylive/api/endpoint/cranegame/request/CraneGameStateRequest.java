package com.funnyland.funnylive.api.endpoint.cranegame.request;

import com.funnyland.funnylive.domain.cranegame.CraneGame;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Getter
public class CraneGameStateRequest {

    @NotNull
    private CraneGame.CraneGameState state;

    public CraneGame toDomain() {
        return CraneGame.builder()
                        .gameState(this.state)
                        .build();
    }
}
