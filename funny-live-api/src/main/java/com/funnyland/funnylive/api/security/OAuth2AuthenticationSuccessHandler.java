package com.funnyland.funnylive.api.security;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import com.funnyland.funnylive.api.config.jwt.JwtTokenProvider;
import com.funnyland.funnylive.api.domain.OAuthUserInfo;
import com.funnyland.funnylive.api.repository.OAuthUserInfoRepository;
import com.funnyland.funnylive.api.service.MemberService;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;


@Component
@Slf4j
@RequiredArgsConstructor
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final JwtTokenProvider jwtTokenProvider;

    private final OAuthUserInfoRepository oAuthUserInfoRepository;
    private final MemberService memberService;
    private final MemberDomainService memberDomainService;

    @Value("${app.cookie.secure}")
    private boolean cookieSecure;

    @Value("${app.base-url}")
    private String baseUrl;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        // OAuth2User로 캐스팅하여 인증된 사용자 정보를 가져온다.
        OAuth2User oAuth2User = (OAuth2User) authentication.getPrincipal();
        // 사용자 이메일을 가져온다.
        log.info("onAuthenticationSuccess()");
        String email = oAuth2User.getAttribute("email");
        // 서비스 제공 플랫폼(GOOGLE, KAKAO, NAVER)이 어디인지 가져온다.
        String provider = oAuth2User.getAttribute("provider");
        String name = oAuth2User.getAttribute("name");
        String phone = oAuth2User.getAttribute("phone");

        log.debug("Authenticated user email: {}", email);
        log.debug("OAuth2 provider: {}", provider);

        // CustomOAuth2UserService에서 셋팅한 로그인한 회원 존재 여부를 가져온다.
        boolean isExist = oAuth2User.getAttribute("exist");

        log.debug("User exists: {}", isExist);

        // OAuth2User로 부터 Role을 얻어온다.
        String role = oAuth2User.getAuthorities().stream().
                                findFirst() // 첫번째 Role을 찾아온다.
                                .orElseThrow(IllegalAccessError::new) // 존재하지 않을 시 예외를 던진다.
                                .getAuthority(); // Role을 가져온다.

        log.debug("User role: {}", role);

        // 회원이 존재할경우
        if (isExist) {
            // 회원이 존재하면 jwt token 발행을 시작한다.
            GeneratedToken token = jwtTokenProvider.generateToken(email, role);


            log.info("jwtToken = {}", token.getAccessToken());

            String targetUrl = UriComponentsBuilder.fromUriString(baseUrl + "/page/login/success")
                                                   .build()
                                                   .encode(StandardCharsets.UTF_8)
                                                   .toUriString();
            log.info("Redirect URL: {}", targetUrl);
            // 로그인 확인 페이지로 리다이렉트 시킨다.

            memberService.saveLoginTime(email);

            addCookie(response, "refreshToken", token.getRefreshToken());
            getRedirectStrategy().sendRedirect(request, response, targetUrl);


        } else {

            String userInfoToken = jwtTokenProvider.generateTokenWithUserInfo(email);
            oAuthUserInfoRepository.save(new OAuthUserInfo(userInfoToken, email, name, phone, provider, 60L * 3L));

            String targetUrl = UriComponentsBuilder.fromUriString(baseUrl + "/page/join/agree")
                                                   .build()
                                                   .encode(StandardCharsets.UTF_8)
                                                   .toUriString();
            log.info("Redirect URL for registration: {}", targetUrl);
            addCookie(response, "authToken", userInfoToken);
            // 회원가입 페이지로 리다이렉트 시킨다.
            getRedirectStrategy().sendRedirect(request, response, targetUrl);
        }
    }

    private HttpServletResponse addCookie(HttpServletResponse response,String cookieName, String accessToken) {
        Cookie cookie = new Cookie(cookieName, accessToken);
        cookie.setHttpOnly(true);
        cookie.setSecure(cookieSecure); // HTTPS를 통해서만 쿠키 전송
        cookie.setPath("/");

        response.addCookie(cookie);
        return response;
    }

}
