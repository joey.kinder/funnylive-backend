package com.funnyland.funnylive.api.endpoint.point.controller;

import com.funnyland.funnylive.api.endpoint.point.facade.PointFacade;
import com.funnyland.funnylive.api.endpoint.point.request.PointRequest;
import com.funnyland.funnylive.api.endpoint.point.request.PointTransactionRequest;
import com.funnyland.funnylive.api.endpoint.point.response.PointTransactionResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name="포인트", description = "포인트 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/points")
@Slf4j
public class PointController {

    private final PointFacade pointFacade;

    @Operation(summary = "포인트 충전", description = "포인트 충전")
    @PostMapping("/charge")
    public void chargePoints(@RequestBody @Valid PointRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        pointFacade.chargePoints(user.getMemberId(), request);
    }

    @Operation(summary = "포인트 사용", description = "포인트 사용")
    @PostMapping("/use")
    public void usePoints(@RequestBody PointTransactionRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        pointFacade.usePoints(user.getMemberId(), request);
    }

    @Operation(summary = "포인트 조회", description = "포인트 조회")
    @GetMapping
    public Integer getPoint() {
        SecurityUserDto user = SecurityUtils.getUser();
        return pointFacade.getPoint(user.getMemberId());
    }

    @Operation(summary = "포인트 내역 조회", description = "포인트 내역 조회")
    @GetMapping("/transactions")
    public Page<PointTransactionResponse> getPointTransactions(Pageable pageable) {
        SecurityUserDto user = SecurityUtils.getUser();
        return pointFacade.getPointTransactions(user.getMemberId(), pageable);
    }

    @Operation(summary = "포인트 환불", description = "포인트 환불")
    @PostMapping("/refund")
    public void refundPoints(@RequestBody PointTransactionRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        pointFacade.refundPoints(user.getMemberId(), request.getAmount());
    }
}
