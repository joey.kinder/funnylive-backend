package com.funnyland.funnylive.api.endpoint.terms.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.term.Term;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class TermsResponse {

    private Long termId;
    private String type;
    private String title;
    private String content;
    private Long viewCount;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    public static TermsResponse of(Term term) {
        return TermsResponse.builder()
                .termId(term.getId())
                .type(term.getType().getName())
                .title(term.getTitle())
                .content(term.getContent())
                .viewCount(term.getViewCount())
                .createdAt(term.getCreatedAt())
                .build();
    }

}
