package com.funnyland.funnylive.api.security;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.domain.member.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;


@NoArgsConstructor
@Getter
@ToString
@AllArgsConstructor
@Builder
public class SecurityUserDto {
    private String email;
    private String nickname;
    private String picture;
    private String role;
    private Long memberId;


    public void authVerify(Long memberId) {
        if(this.role.equals(Role.ADMIN.name())) {
            return;
        }
        if(!this.memberId.equals(memberId)) {
            throw new CustomException(ErrorCode.NOT_AUTHORIZED);
        }
    }

}
