package com.funnyland.funnylive.api.endpoint.point.facade;

import com.funnyland.funnylive.api.endpoint.point.request.PointRequest;
import com.funnyland.funnylive.api.endpoint.point.request.PointTransactionRequest;
import com.funnyland.funnylive.api.endpoint.point.response.PointTransactionResponse;
import com.funnyland.funnylive.api.service.MemberService;
import com.funnyland.funnylive.api.service.PointService;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.point.PointTransaction;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class PointFacade {

    private final PointService pointService;
    private final MemberService memberService;


    public void chargePoints(Long memberId, PointRequest request) {
        Member member = memberService.getMember(memberId);
        pointService.chargePoints(member, request.getAmount(), request.getBonusPercent(), request.getPackageName(), request.getProductId(), request.getPurchaseToken(), request.getOrderId());
    }

    public void usePoints(Long memberId, PointTransactionRequest request) {
        Member member = memberService.getMember(memberId);
        pointService.usePoints(member, request.getType(), request.getAmount());
    }

    public Page<PointTransactionResponse> getPointTransactions(Long memberId, Pageable pagable) {
        Member member = memberService.getMember(memberId);
        Page<PointTransaction> findPointTransactions = pointService.getPointTransactions(member, pagable);
        return findPointTransactions.map(PointTransactionResponse::of);
    }

    public void refundPoints(Long memberId, int amount) {
        Member member = memberService.getMember(memberId);
        pointService.refundPoints(member, amount);
    }

    public Integer getPoint(Long memberId) {
        Member member = memberService.getMember(memberId);
        return pointService.getPoint(member.getId());
    }
}
