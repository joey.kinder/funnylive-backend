package com.funnyland.funnylive.api.endpoint.delivery.response;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.delivery.DeliveryItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class DeliveryDetailResponse {
    private Long deliveryId;
    private String orderNumber;
    private String roadNameAddress;
    private String inputDetail;
    private String postalCode;
    private String recipientName;
    private String recipientPhone;
    private String stateName;
    private String state;
    private String requirement;
    private String invoiceCompanyName;
    private String invoiceNumber;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime createdAt;
    private List<DeliveryItemResponse> items; // 배송 아이템 리스트 추가

    public static DeliveryDetailResponse of(Delivery delivery) {
        List<DeliveryItemResponse> itemResponses = delivery.getItems().stream()
                .map(DeliveryItemResponse::of)
                .collect(Collectors.toList());

        return DeliveryDetailResponse.builder()
                .deliveryId(delivery.getId())
                .orderNumber(delivery.getOrderNumber())
                .roadNameAddress(delivery.getRoadNameAddress())
                .inputDetail(delivery.getInputDetail())
                .postalCode(delivery.getPostalCode())
                .recipientName(delivery.getRecipientName())
                .recipientPhone(delivery.getRecipientPhone())
                .stateName(delivery.getStatus() != null ? delivery.getStatus().getName() : null)
                .state(delivery.getStatus() != null ? delivery.getStatus().name() : null)
                .requirement(delivery.getRequirement())
                .invoiceCompanyName(delivery.getInvoiceCompanyName())
                .invoiceNumber(delivery.getInvoiceNumber())
                .createdAt(delivery.getCreatedAt())
                .items(itemResponses) // 배송 아이템 추가
                .build();
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    public static class DeliveryItemResponse {
        private Long productId;
        private String productName;
        private Integer productPrice;
        private String productImageURL;
        private int quantity;

        public static DeliveryItemResponse of(DeliveryItem item) {
            return DeliveryItemResponse.builder()
                    .productId(item.getProductId())
                    .productName(item.getProductName())
                    .productPrice(item.getProductPrice())
                    .productImageURL(UrlUtil.convertToCdnUrl(item.getProductImageURL()))
                    .quantity(item.getQuantity())
                    .build();
        }
    }
}
