package com.funnyland.funnylive.api.repository;

import java.util.List;

import com.funnyland.funnylive.domain.inquiry.InquiryType;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InquiryTypeRepository extends JpaRepository<InquiryType, Long> {
    List<InquiryType> findAllByOrderByIdAsc();
}
