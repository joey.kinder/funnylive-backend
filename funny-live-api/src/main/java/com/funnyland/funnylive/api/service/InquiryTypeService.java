package com.funnyland.funnylive.api.service;


import java.util.List;

import com.funnyland.funnylive.api.endpoint.inquiry.request.InquiryTypeRequest;
import com.funnyland.funnylive.api.repository.InquiryTypeRepository;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import com.funnyland.funnylive.domain.inquiry.InquiryTypeDomainService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class InquiryTypeService {

    private final InquiryTypeRepository inquiryTypeRepository;
    private final InquiryTypeDomainService inquiryTypeDomainService;

    /**
     * 문의타입 생성
     */
    public InquiryType create(InquiryTypeRequest request) {
        InquiryType inquiryType = request.toDomain();

        InquiryType savedInquiryType = inquiryTypeDomainService.save(inquiryType);
        return savedInquiryType;
    }

    /**
     * 문의타입 리스트 조회
     */
    public List<InquiryType> getTypes() {
        List<InquiryType> findInquiryTypes = inquiryTypeRepository.findAllByOrderByIdAsc();
        return findInquiryTypes;
    }

    /**
     * 문의타입 업데이트
     */
    @Transactional
    public InquiryType update(Long inquiryTypeId, InquiryTypeRequest request) {
        InquiryType findInquiryType = inquiryTypeRepository.findById(inquiryTypeId)
                                                       .orElseThrow(() -> new CustomException(ErrorCode.INQUIRY_TYPE_NOT_FOUND));
        InquiryType updatedInquiryType = findInquiryType.edit(request.toDomain());
        return updatedInquiryType;
    }

    /**
     * 문의타입 삭제
     */
    @Transactional
    public void delete(Long inquiryTypeId) {
        inquiryTypeDomainService.delete(inquiryTypeId);
    }
}
