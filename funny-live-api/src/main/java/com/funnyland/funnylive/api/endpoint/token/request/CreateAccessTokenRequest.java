package com.funnyland.funnylive.api.endpoint.token.request;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CreateAccessTokenRequest {
    private String refreshToken;
}
