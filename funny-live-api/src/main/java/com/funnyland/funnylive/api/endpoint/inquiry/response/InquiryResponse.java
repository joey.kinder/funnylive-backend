package com.funnyland.funnylive.api.endpoint.inquiry.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class InquiryResponse {

    private Long inquiryId;
    private String status;
    private String title;
    private String content;
    private String filePath;
    private String fileName;
    private String type;
    private InquiryReplyResponse reply;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;

    public static InquiryResponse of(Inquiry inquiry) {
        InquiryReplyResponse reply = null;

        if (inquiry.getReply() != null) {
            reply = InquiryReplyResponse.builder()
                                        .title(inquiry.getReply().getTitle())
                                        .content(inquiry.getReply().getContent())
                                        .createdAt(inquiry.getReply().getCreatedAt())
                                        .build();
        }


        return InquiryResponse.builder()
                .inquiryId(inquiry.getId())
                .status(inquiry.getStatus().getName())
                .title(inquiry.getTitle())
                .content(inquiry.getContent())
                .createdAt(inquiry.getCreatedAt())
                .type(inquiry.getType() != null ? inquiry.getType().getName() : null)
                .filePath(UrlUtil.convertToCdnUrl(inquiry.getFileUrl()))
                              .fileName(inquiry.getFileName())
              .reply(reply)
                .build();
    }

    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class InquiryReplyResponse {
        private String title;
        private String content;
        @JsonFormat(pattern = "yyyy-MM-dd")
        private LocalDateTime createdAt;
    }

}
