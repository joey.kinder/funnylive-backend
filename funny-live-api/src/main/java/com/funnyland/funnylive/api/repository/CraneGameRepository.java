package com.funnyland.funnylive.api.repository;

import java.util.List;

import com.funnyland.funnylive.domain.cranegame.CraneGame;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface CraneGameRepository extends JpaRepository<CraneGame, Long> {

    @Query("SELECT c.playFee AS playFee FROM CraneGame c GROUP BY c.playFee")
    List<Integer> countGamesGroupedByPlayFee();

    boolean existsCraneGameByUuid(String uuid);
}
