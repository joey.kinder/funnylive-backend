package com.funnyland.funnylive.api.endpoint.member.response;

import java.util.Optional;
import java.util.stream.Collectors;

import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.deliveryaddress.DeliveryAddress;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.storagebox.StorageBox;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class MemberResponse {

    private Long memberId;
    private String name;
    private String email;
    private String nickName;
    private String phone;
    private int point;
    private String provider;
    private String roadNameAddress;
    private String inputDetail;
    private String postalCode;
    private boolean agreeReceiveEmail;
    private boolean agreeReceivePhone;
    private long storageBoxCount;
    private long shippedCount;
    private long inquiryCount;
    private long repliedInquiryCount;

    public static MemberResponse of(Member member) {
        DeliveryAddress defaultAddress = member.getDeliveryAddresses().stream()
                                               .filter(DeliveryAddress::isDefault)
                                               .findFirst()
                                               .orElse(null);

        String roadNameAddress = defaultAddress != null ? defaultAddress.getRoadNameAddress() : null;
        String inputDetail = defaultAddress != null ? defaultAddress.getInputDetail() : null;
        String postalCode = defaultAddress != null ? defaultAddress.getPostalCode() : null;

        long pendingCount = member.getInquiries().stream()
                                  .filter(inquiry -> inquiry.getStatus() == Inquiry.InquiryStatus.PENDING)
                                  .count();

        long answeredCount = member.getInquiries().stream()
                                   .filter(inquiry -> inquiry.getStatus() == Inquiry.InquiryStatus.ANSWERED)
                                   .count();

        long shippedCount = member.getDeliveries().stream()
                                  .flatMap(delivery -> delivery.getItems().stream())
                                  .filter(item -> item.getDelivery().getStatus() == Delivery.DeliveryStatus.SHIPPED)
                                  .count();
        long storageBoxCount = Optional.ofNullable(member.getStorageBox())
                                       .map(storageBox -> storageBox.getHistories().stream()
                                                                    .filter(history -> !history.isDeliveryRequested())
                                                                    .count())
                                       .orElse(0L);

        return MemberResponse.builder()
                             .memberId(member.getId())
                             .name(member.getName())
                             .email(member.getEmail())
                             .nickName(member.getNickName())
                             .phone(member.getPhone())
                             .provider(member.getOAuthProvider().getName())
                             .roadNameAddress(roadNameAddress)
                             .inputDetail(inputDetail)
                             .postalCode(postalCode)
                             .agreeReceiveEmail(member.isAgreeReceiveEmail())
                             .agreeReceivePhone(member.isAgreeReceivePhone())
                             .storageBoxCount(storageBoxCount)
                             .inquiryCount(pendingCount)
                             .repliedInquiryCount(answeredCount)
                             .shippedCount(shippedCount)
                             .build();
    }

    public static MemberResponse of(Member member, int point) {
        DeliveryAddress defaultAddress = member.getDeliveryAddresses().stream()
                .filter(DeliveryAddress::isDefault)
                .findFirst()
                .orElse(null);

        String roadNameAddress = defaultAddress != null ? defaultAddress.getRoadNameAddress() : null;
        String inputDetail = defaultAddress != null ? defaultAddress.getInputDetail() : null;
        String postalCode = defaultAddress != null ? defaultAddress.getPostalCode() : null;

        long pendingCount = member.getInquiries().stream()
                .filter(inquiry -> inquiry.getStatus() == Inquiry.InquiryStatus.PENDING)
                .count();

        long answeredCount = member.getInquiries().stream()
                .filter(inquiry -> inquiry.getStatus() == Inquiry.InquiryStatus.ANSWERED)
                .count();

        long shippedCount = member.getDeliveries().stream()
                .flatMap(delivery -> delivery.getItems().stream())
                .filter(item -> item.getDelivery().getStatus() == Delivery.DeliveryStatus.SHIPPED)
                .count();

        long storageBoxCount = Optional.ofNullable(member.getStorageBox())
                                       .map(storageBox -> storageBox.getHistories().stream()
                                                                    .filter(history -> !history.isDeliveryRequested())
                                                                    .count())
                                       .orElse(0L);

        return MemberResponse.builder()
                .memberId(member.getId())
                .name(member.getName())
                .email(member.getEmail())
                .nickName(member.getNickName())
                .phone(member.getPhone())
                .point(point)
                .provider(member.getOAuthProvider().getName())
                .roadNameAddress(roadNameAddress)
                .inputDetail(inputDetail)
                .postalCode(postalCode)
                .agreeReceiveEmail(member.isAgreeReceiveEmail())
                .agreeReceivePhone(member.isAgreeReceivePhone())
                .storageBoxCount(storageBoxCount)
                .inquiryCount(pendingCount)
                .repliedInquiryCount(answeredCount)
                .shippedCount(shippedCount)
                .build();
    }

}
