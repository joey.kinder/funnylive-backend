package com.funnyland.funnylive.api.config;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.AndroidPublisherScopes;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class GoogleCredentialsConfig {
    @Value("${google.account-file-path}")
    private String googleAccountFilePath;

    @Value("${google.package-name}")
    private String googleApplicationPackageName;

    public AndroidPublisher androidPublisher() throws IOException, GeneralSecurityException {
        Resource resource;

        if (googleAccountFilePath.startsWith("classpath:")) {
            // 클래스패스 리소스
            resource = new ClassPathResource(googleAccountFilePath.substring(10));
        } else {
            // 파일 시스템 리소스
            resource = new FileSystemResource(googleAccountFilePath);
        }

        InputStream inputStream = resource.getInputStream();
        GoogleCredentials credentials = GoogleCredentials.fromStream(inputStream).createScoped(AndroidPublisherScopes.ANDROIDPUBLISHER);

        return new AndroidPublisher.Builder(
                GoogleNetHttpTransport.newTrustedTransport(),
                GsonFactory.getDefaultInstance(),
                new HttpCredentialsAdapter(credentials)
        ).setApplicationName(googleApplicationPackageName).build();
    }
}
