package com.funnyland.funnylive.api.endpoint.member.request;

import com.funnyland.funnylive.domain.deliveryaddress.DeliveryAddress;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.Role;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MemberUpdateRequest {
    @Size(max = 10, message = "이름은 10자 이하여야 합니다")
    private String name;

    @Pattern(regexp = "^\\d{3}-\\d{3,4}-\\d{4}$", message = "핸드폰 형식이 맞지 않습니다")
    private String phone;

    private String nickName;


    @NotNull
    private boolean agreeReceiveEmail;

    @NotNull
    private boolean agreeReceivePhone;

    @NotNull
    private String roadNameAddress;
    @NotNull
    private String inputDetail;
    @NotNull
    private String postalCode;


    public Member toDomain() {
        Member member = Member.builder()
                .name(this.name)
                .phone(this.phone)
                .nickName(this.nickName)
                .role(Role.USER)
                .agreeReceiveEmail(this.agreeReceiveEmail)
                .agreeReceivePhone(this.agreeReceivePhone)
                .build();
        DeliveryAddress deliveryAddress = DeliveryAddress.builder()
                .roadNameAddress(this.roadNameAddress)
                .inputDetail(this.inputDetail)
                .postalCode(this.postalCode)
                .member(member)
                .isDefault(true)
                .build();
        member.addDeliveryAddress(deliveryAddress);
        return member;
    }



}
