package com.funnyland.funnylive.api.repository;

import java.util.List;

import com.funnyland.funnylive.domain.inquiry.Inquiry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InquiryReadRepository extends JpaRepository<Inquiry, Long> {

    Page<Inquiry> findAllByMemberId(Long id, Pageable pagable);
    List<Inquiry> findAllByMemberId(Long id);
}
