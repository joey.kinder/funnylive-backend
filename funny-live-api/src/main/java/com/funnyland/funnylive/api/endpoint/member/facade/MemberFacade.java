package com.funnyland.funnylive.api.endpoint.member.facade;

import java.util.List;
import java.util.stream.Collectors;

import com.funnyland.funnylive.api.endpoint.member.request.MemberRequest;
import com.funnyland.funnylive.api.endpoint.member.request.MemberUpdateRequest;
import com.funnyland.funnylive.api.endpoint.member.response.MemberResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.MemberService;
import com.funnyland.funnylive.api.service.PointService;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import com.funnyland.funnylive.domain.member.UserProfile;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class MemberFacade {

    private final MemberService memberService;
    private final PointService pointService;

    public MemberResponse getMember(Long memberId, SecurityUserDto user) {
        Member findMember = memberService.getMember(memberId);
        user.authVerify(findMember.getId());

        int point = pointService.getPoint(findMember.getId());
        return MemberResponse.of(findMember, point);
    }

    public boolean checkNickName(String nickName) {
        return memberService.existMember(nickName);
    }

    public boolean withdraw(Long memberId, String withdrawReason, SecurityUserDto user) {
        Member findMember = memberService.getMember(memberId);
        user.authVerify(findMember.getId());

        memberService.withdraw(memberId, withdrawReason);
        return true;
    }

    public List<MemberResponse> getMembers() {
        List<Member> findMembers = memberService.getMembers();
        return findMembers.stream()
                   .map(MemberResponse::of).collect(Collectors.toList());
    }

    public MemberResponse updateMember(Long memberId, MemberUpdateRequest request, SecurityUserDto user) {
        Member updatedMember = memberService.update(memberId, request, user);
        return MemberResponse.of(updatedMember);
    }

    public MemberResponse createMember(MemberRequest memberRequest) {
        Member savedMember = memberService.saveMember(memberRequest.toDomain());
        return MemberResponse.of(savedMember);
    }

}
