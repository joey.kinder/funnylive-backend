package com.funnyland.funnylive.api.endpoint.token.response;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public class TokenResponse {

    private String accessToken;
    private String refreshToken;

    public static TokenResponse EMPTY = new TokenResponse(null, null);

    public static TokenResponse of(String accessToken, String refreshToken) {
        return new TokenResponse(accessToken, refreshToken);
    }
}
