package com.funnyland.funnylive.api.repository;

import java.util.List;

import com.funnyland.funnylive.domain.storagebox.StorageBoxHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StorageBoxHistoryRepository extends JpaRepository<StorageBoxHistory, Long> {

    List<StorageBoxHistory> findByStorageBoxId(Long storageBoxId);

}
