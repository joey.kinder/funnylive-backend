package com.funnyland.funnylive.api.endpoint.login;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class MainController {

    @GetMapping({"/user/main"})
    public String main(Authentication authentication) {
        return "main";
    }

    @GetMapping("/user/login")
    public String login(Authentication authentication) {
        if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
            // 이미 로그인된 사용자인 경우 메인 페이지로 리디렉션
            return "redirect:/main";
        }
        // 로그인 페이지 반환
        return "login";
    }

    @GetMapping("/user/verify")
    public String verify() {
        // 인증된 사용자가 이 경로에 접근하면 추가 정보 입력 페이지 반환
        return "verify";
    }
}
