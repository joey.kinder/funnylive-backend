package com.funnyland.funnylive.api.endpoint.cart.controller;

import com.funnyland.funnylive.api.endpoint.cart.request.CartRequest;
import com.funnyland.funnylive.api.endpoint.cart.request.CartUpdateRequest;
import com.funnyland.funnylive.api.endpoint.cart.response.CartResponse;
import com.funnyland.funnylive.api.endpoint.inquiry.facade.InquiryFacade;
import com.funnyland.funnylive.api.endpoint.inquiry.request.InquiryRequest;
import com.funnyland.funnylive.api.endpoint.inquiry.response.InquiryResponse;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.CartService;
import com.funnyland.funnylive.api.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


//@Tag(name="카트", description = "카트 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/carts")
@Slf4j
public class CartController {

    private final CartService cartService;

//    @Operation(summary = "카트 담기", description = "카트 담기")
    @PostMapping
    public void create(@RequestBody CartRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        cartService.create(request, user);
    }

//    @Operation(summary = "카트 아이템 수정", description = "카트 아이템 수정")
    @PutMapping("/items/{cartItemId}")
    public CartResponse.CartItemResponse update(@PathVariable Long cartItemId, @RequestBody CartUpdateRequest request) {
        SecurityUserDto user = SecurityUtils.getUser();
        return cartService.update(cartItemId, request, user);
    }

//    @Operation(summary = "내 카트 조회", description = "내 카트 조회")
    @GetMapping("/me")
    public CartResponse list() {
        SecurityUserDto user = SecurityUtils.getUser();
        return cartService.list(user.getMemberId());
    }

//    @Operation(summary = "카트 아이템 삭제", description = "카트 아이템 삭제")
    @DeleteMapping("/items/{cartItemId}")
    public void delete(@PathVariable Long cartItemId) {
        SecurityUserDto user = SecurityUtils.getUser();
        cartService.delete(cartItemId, user);
    }
}
