package com.funnyland.funnylive.api.service;

import java.util.List;
import java.util.stream.Collectors;

import com.funnyland.funnylive.api.endpoint.inquiry.request.FrequentlyInquiryFilter;
import com.funnyland.funnylive.api.endpoint.inquiry.response.FrequentlyInquiryResponse;
import com.funnyland.funnylive.api.repository.FrequentlyInquiryReadRepository;
import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class FrequentlyInquiryService {

    private final FrequentlyInquiryReadRepository frequentlyInquiryReadRepository;

    /**
     * 자주 묻는 질문 리스트 조회
     */
    public List<FrequentlyInquiryResponse> list(FrequentlyInquiryFilter filter) {
        List<FrequentlyInquiry> findFrequentlyInquiries = frequentlyInquiryReadRepository.getAll(filter.generateBooleanBuilder());
        return findFrequentlyInquiries.stream()
                                      .map(FrequentlyInquiryResponse::of)
                                      .collect(Collectors.toList());
    }

}
