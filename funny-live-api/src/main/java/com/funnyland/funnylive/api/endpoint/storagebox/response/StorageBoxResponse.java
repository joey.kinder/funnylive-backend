package com.funnyland.funnylive.api.endpoint.storagebox.response;

import java.util.List;

import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.storagebox.StorageBox;
import com.funnyland.funnylive.domain.storagebox.StorageBoxHistory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StorageBoxResponse {

    private Long storageBoxId;

    private Long productId;
    private String productName;
    private String productImageUrl;
    private Integer quantity;
    private Integer acquisitionPoint;

    private Long memberId;
    private String memberName;

    public static StorageBoxResponse of(StorageBox storageBox) {
        List<StorageBoxHistory> histories = storageBox.getHistories();
        StorageBoxHistory latestHistory = histories.isEmpty() ? null : histories.get(histories.size() - 1);

        int quantity = (int) histories.stream()
                .filter(h -> h.getProductId().equals(latestHistory.getProductId()))
                .count();

        return StorageBoxResponse.builder()
                .storageBoxId(storageBox.getId())
                .productId(latestHistory != null ? latestHistory.getProductId() : null)
                .productName(latestHistory != null ? latestHistory.getProductName() : null)
                .productImageUrl(latestHistory != null ? UrlUtil.convertToCdnUrl(latestHistory.getProductImageURL()) : null)
                .memberId(storageBox.getMember().getId())
                .memberName(storageBox.getMember().getName())
                .quantity(quantity)
                .acquisitionPoint(latestHistory != null ? latestHistory.getProductPrice() : null)
                .build();
    }


}
