package com.funnyland.funnylive.api.service;


import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.api.endpoint.inquiry.request.InquiryRequest;
import com.funnyland.funnylive.api.repository.InquiryReadRepository;
import com.funnyland.funnylive.api.repository.InquiryTypeRepository;
import com.funnyland.funnylive.api.security.SecurityUserDto;
import com.funnyland.funnylive.api.service.dto.StorageType;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryDomainService;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import com.funnyland.funnylive.domain.member.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
public class InquiryService {

    private final InquiryReadRepository inquiryReadRepository;
    private final InquiryDomainService inquiryDomainService;
    private final UploadService uploadService;
    private final InquiryTypeRepository inquiryTypeRepository;

    /**
     * 문의 등록
     */
    @Transactional
    public Inquiry create(InquiryRequest request, Member member, MultipartFile file) {
        Inquiry inquiry = request.toDomain();
        inquiry.addMember(member);

        // InquiryType 찾기
        InquiryType inquiryType = inquiryTypeRepository.findById(request.getInquiryTypeId())
                .orElseThrow(() -> new CustomException(ErrorCode.INQUIRY_TYPE_NOT_FOUND));

        inquiry.setType(inquiryType);

        Optional<UploadDto> uploadDto = Optional.ofNullable(file)
                .flatMap(fileDto -> uploadService.upload(fileDto, StorageType.ETC));

        uploadDto.ifPresent(inquiry::addFile);
        return inquiryDomainService.save(inquiry);
    }

    /**
     * 사용자 문의 리스트 조회 (페이징)
     */
    public Page<Inquiry> getInquiries(Member member, Pageable pagable) {
        return inquiryReadRepository.findAllByMemberId(member.getId(), pagable);
    }

    /**
     * 문의 리스트 조회 (페이징)
     */
    public Page<Inquiry> getInquiries(Pageable pageable) {
        return inquiryReadRepository.findAll(pageable);
    }

    /**
     * 사용자 문의 리스트 조회
     */
    public List<Inquiry> getInquiries(Member member) {
        return inquiryReadRepository.findAllByMemberId(member.getId());
    }

    /**
     * 문의 업데이트
     */
    @Transactional
    public Inquiry update(Long inquiryId, InquiryRequest request, MultipartFile file, SecurityUserDto user) {
        Inquiry updateInquiry = request.toDomain();

        // InquiryType 찾기
        InquiryType inquiryType = inquiryTypeRepository.findById(request.getInquiryTypeId())
                .orElseThrow(() -> new CustomException(ErrorCode.INQUIRY_TYPE_NOT_FOUND));

        // Inquiry 객체에 InquiryType 설정
        updateInquiry.setType(inquiryType);

        Optional<UploadDto> uploadDto = Optional.ofNullable(file)
                .flatMap(fileDto -> uploadService.upload(fileDto, StorageType.ETC));

        uploadDto.ifPresent(updateInquiry::addFile);
        Inquiry findInquiry = inquiryDomainService.find(inquiryId);
        user.authVerify(findInquiry.getMember()
                                   .getId());
        return findInquiry.edit(updateInquiry);
    }

    /**
     * 문의 상세 조회
     */
    public Inquiry getInquiry(Long inquiryId) {
        return inquiryReadRepository.findById(inquiryId)
                .orElseThrow(() -> new CustomException(ErrorCode.INQUIRY_NOT_FOUND));
    }

}
