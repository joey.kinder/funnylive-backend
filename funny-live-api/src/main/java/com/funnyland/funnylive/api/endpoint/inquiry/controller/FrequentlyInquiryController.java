package com.funnyland.funnylive.api.endpoint.inquiry.controller;

import java.util.List;

import com.funnyland.funnylive.api.endpoint.inquiry.request.FrequentlyInquiryFilter;
import com.funnyland.funnylive.api.endpoint.inquiry.response.FrequentlyInquiryResponse;
import com.funnyland.funnylive.api.service.FrequentlyInquiryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="문의", description = "문의 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/inquiries/frequency")
@Slf4j
public class FrequentlyInquiryController {

    private final FrequentlyInquiryService frequentlyInquiryService;

    @Operation(summary = "자주묻는 질문 문의 목록 조회", description = "자주묻는 질문 목록 조회")
    @GetMapping
    public List<FrequentlyInquiryResponse> list(FrequentlyInquiryFilter filter) {
        return frequentlyInquiryService.list(filter);
    }

}
