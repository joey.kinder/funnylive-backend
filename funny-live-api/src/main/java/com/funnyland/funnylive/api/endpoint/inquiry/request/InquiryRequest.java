package com.funnyland.funnylive.api.endpoint.inquiry.request;

import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InquiryRequest {
    @NotNull
    private String title;
    @NotNull
    private String content;
    @NotNull
    private Long inquiryTypeId;

    public Inquiry toDomain() {
        Inquiry inquiry = Inquiry.builder()
                .title(this.title)
                .content(this.content)
                 .build();
        return inquiry;
    }
}