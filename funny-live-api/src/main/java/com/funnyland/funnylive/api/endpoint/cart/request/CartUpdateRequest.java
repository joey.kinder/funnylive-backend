package com.funnyland.funnylive.api.endpoint.cart.request;

import com.funnyland.funnylive.domain.cart.CartItem;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "카트 업데이트 정보")
@ToString
public class CartUpdateRequest {
    @Schema(title = "상품 id", example = "1", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long productId;
    @Schema(title = "카트 담을 개수", example = "3", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer quantity;

    public CartItem toDomain() {
        return CartItem.builder()
                .quantity(this.quantity)
                .build();
    }
}
