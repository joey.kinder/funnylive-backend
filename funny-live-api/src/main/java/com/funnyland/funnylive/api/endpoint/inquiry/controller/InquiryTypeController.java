package com.funnyland.funnylive.api.endpoint.inquiry.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.funnyland.funnylive.api.endpoint.inquiry.request.InquiryTypeRequest;
import com.funnyland.funnylive.api.endpoint.inquiry.response.InquiryTypeResponse;
import com.funnyland.funnylive.api.service.InquiryTypeService;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="문의 타입", description = "문의 타입 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/inquiry/types")
@Slf4j
public class InquiryTypeController {

    private final InquiryTypeService inquiryTypeService;

    @Operation(summary = "문의 타입 리스트 조회", description = "문의 타입 리스트 조회")
    @GetMapping
    public List<InquiryTypeResponse> getTypes() {
        List<InquiryType> types = inquiryTypeService.getTypes();

        return types.stream()
                .map(InquiryTypeResponse::of)
                .collect(Collectors.toList());
    }

}
