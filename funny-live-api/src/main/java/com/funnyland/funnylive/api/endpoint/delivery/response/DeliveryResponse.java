package com.funnyland.funnylive.api.endpoint.delivery.response;

import java.util.List;
import java.util.stream.Collectors;

import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.delivery.DeliveryItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class DeliveryResponse {
    private long pendingCount;
    private long shippedCount;
    private long deliveredCount;
    private Page<DeliveryItemResponse> deliveries; // 배송 목록 추가

    public static DeliveryResponse of(long pendingCount, long shippedCount, long deliveredCount, Page<DeliveryItemResponse> deliveries) {
        return DeliveryResponse.builder()
                .pendingCount(pendingCount)
                .shippedCount(shippedCount)
                .deliveredCount(deliveredCount)
                .deliveries(deliveries)
                .build();
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    public static class DeliveryItemResponse {
        private Long deliveryId;
        private String stateName;
        private String state;
        private String productImageUrl;
        private String productName;
        private Integer productPrice;

        public static DeliveryItemResponse of(Delivery delivery) {
            String productImageUrl = null;
            String productName = null;
            Integer productPrice = null;

            if (!delivery.getItems().isEmpty()) {
                DeliveryItem deliveryItem = delivery.getItems().get(0);
                productImageUrl = deliveryItem.getProductImageURL();
                productName = deliveryItem.getProductName();
                productPrice = deliveryItem.getProductPrice();
            }
            return DeliveryItemResponse.builder()
                    .deliveryId(delivery.getId())
                    .stateName(delivery.getStatus() != null ? delivery.getStatus().getName() : null)
                    .state(delivery.getStatus() != null ? delivery.getStatus().name() : null)
                    .productImageUrl(UrlUtil.convertToCdnUrl(productImageUrl))
                    .productName(productName)
                    .productPrice(productPrice)
                    .build();
        }
    }
}
