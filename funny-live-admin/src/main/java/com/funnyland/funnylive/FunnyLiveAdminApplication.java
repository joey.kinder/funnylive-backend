package com.funnyland.funnylive;

import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class FunnyLiveAdminApplication {

    public static void main(String[] args) {
        String profile = System.getProperty("spring.profiles.active");

        if ("local".equals(profile)) {
            Dotenv dotenv = Dotenv.load();
            System.setProperty("AWS_ACCESS_KEY", dotenv.get("AWS_ACCESS_KEY"));
            System.setProperty("AWS_SECRET_KEY", dotenv.get("AWS_SECRET_KEY"));
            System.setProperty("AWS_RDS_USER", dotenv.get("AWS_RDS_USER"));
            System.setProperty("AWS_RDS_PASSWORD", dotenv.get("AWS_RDS_PASSWORD"));
        }

        SpringApplication.run(FunnyLiveAdminApplication.class, args);
    }

}
