package com.funnyland.funnylive.admin.endpoint.member.controller;

import java.util.List;

import com.funnyland.funnylive.admin.endpoint.member.request.AdminMemberRequest;
import com.funnyland.funnylive.admin.endpoint.member.request.MemberFilter;
import com.funnyland.funnylive.admin.endpoint.member.request.MemberRequest;
import com.funnyland.funnylive.admin.endpoint.member.response.AdminMemberResponse;
import com.funnyland.funnylive.admin.endpoint.member.response.MemberDetailResponse;
import com.funnyland.funnylive.admin.endpoint.member.response.MemberResponse;
import com.funnyland.funnylive.admin.service.MemberService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="회원", description = "회원 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/members")
public class MemberController {

    private final MemberService memberService;

    @GetMapping
    @Operation(summary = "회원 리스트 조회", description = "회원 리스트 조회")
    public Page<MemberResponse> getMembers(MemberFilter filter) {
        return memberService.getAll(filter);
    }

    @PutMapping("/{memberId}")
    @Operation(summary = "회원 수정", description = "회원 수정")
    public MemberResponse update(@PathVariable Long memberId, @RequestBody MemberRequest request) {
        return memberService.update(memberId, request);
    }

    @GetMapping("/{memberId}")
    @Operation(summary = "회원 상세 조회", description = "회원 상세 조회")
    public MemberDetailResponse getMember(@PathVariable Long memberId) {
        return memberService.get(memberId);
    }

}
