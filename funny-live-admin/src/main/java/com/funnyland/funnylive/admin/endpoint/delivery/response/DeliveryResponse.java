package com.funnyland.funnylive.admin.endpoint.delivery.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.delivery.Delivery;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryResponse {

    private Long deliveryId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;
    private String orderNumber;
    private String productName;
    private String memberName;
    private String memberEmail;
    private String recipientName;
    private String stateName;

    public static DeliveryResponse of(Delivery delivery) {
        String productName = null;

        if (delivery.getItems() != null && !delivery.getItems().isEmpty()) {
            String firstProductName = delivery.getItems()
                                              .get(0)
                                              .getProductName();
            int additionalItemsCount = delivery.getItems()
                                               .size() - 1;

            if (additionalItemsCount > 0) {
                productName = firstProductName + " 외 " + additionalItemsCount + "개";
            } else {
                productName =  firstProductName;
            }
        }

        return DeliveryResponse.builder()
                               .deliveryId(delivery.getId())
                               .createdAt(delivery.getCreatedAt())
                               .orderNumber(delivery.getOrderNumber())
                               .productName(productName)
                               .memberName(delivery.getMember().getName())
                               .memberEmail(delivery.getMember().getEmail())
                               .recipientName(delivery.getRecipientName())
                               .stateName(delivery.getStatus() != null? delivery.getStatus().getName() : null)
                               .build();

    }
}
