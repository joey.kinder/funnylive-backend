package com.funnyland.funnylive.admin.endpoint.inquiry.controller;

import com.funnyland.funnylive.admin.endpoint.inquiry.request.FrequentlyInquiryFilter;
import com.funnyland.funnylive.admin.endpoint.inquiry.request.FrequentlyInquiryRequest;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.FrequentlyInquiryDetailResponse;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.FrequentlyInquiryResponse;
import com.funnyland.funnylive.admin.service.FrequentlyInquiryService;
import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="자주묻는질문", description = "자주묻는질문 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/inquiries/frequency")
public class FrequentlyInquiryController {

    private final FrequentlyInquiryService frequentlyInquiryService;

    @GetMapping
    @Operation(summary = "자주묻는질문 리스트 조회", description = "자주묻는질문 리스트 조회")
    public Page<FrequentlyInquiryResponse> getAll(FrequentlyInquiryFilter filter) {
        return frequentlyInquiryService.getAll(filter);
    }

    @GetMapping("/{frequentlyInquiryId}")
    @Operation(summary = "자주묻는질문 상세 조회", description = "자주묻는질문 상세 조회")
    public FrequentlyInquiryDetailResponse get(@PathVariable Long frequentlyInquiryId) {
        return frequentlyInquiryService.get(frequentlyInquiryId);
    }

    @PostMapping
    @Operation(summary = "자주묻는질문 등록", description = "자주묻는질문 등록")
    public FrequentlyInquiryDetailResponse create(@RequestBody FrequentlyInquiryRequest request) {
        return frequentlyInquiryService.create(request);
    }

    @PutMapping("/{frequentlyInquiryId}")
    @Operation(summary = "자주묻는질문 수정", description = "자주묻는질문 수정")
    public FrequentlyInquiryDetailResponse update(@PathVariable Long frequentlyInquiryId, @RequestBody FrequentlyInquiryRequest request) {
        return frequentlyInquiryService.update(frequentlyInquiryId, request);
    }
}
