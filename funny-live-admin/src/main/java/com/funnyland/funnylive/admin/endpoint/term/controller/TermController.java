package com.funnyland.funnylive.admin.endpoint.term.controller;


import com.funnyland.funnylive.admin.endpoint.term.request.TermFilter;
import com.funnyland.funnylive.admin.endpoint.term.request.TermRequest;
import com.funnyland.funnylive.admin.endpoint.term.response.TermDetailResponse;
import com.funnyland.funnylive.admin.endpoint.term.response.TermResponse;
import com.funnyland.funnylive.admin.service.TermService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="약관", description = "약관 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/terms")
public class TermController {

    private final TermService termService;

    @GetMapping
    @Operation(summary = "약관 리스트 조회", description = "약관 리스트 조회")
    public Page<TermResponse> getAll(TermFilter filter) {
        return termService.getAll(filter);
    }

    @GetMapping("/{termId}")
    @Operation(summary = "약관 상세 조회", description = "약관 상세 조회")
    public TermDetailResponse get(@PathVariable Long termId) {
        return termService.get(termId);
    }

    @PostMapping
    @Operation(summary = "약관 등록", description = "약관 등록")
    public TermDetailResponse create(@RequestBody TermRequest request) {
        return termService.create(request);
    }

    @PutMapping("/{termId}")
    @Operation(summary = "약관 수정", description = "약관 수정")
    public TermDetailResponse update(@PathVariable Long termId, @RequestBody TermRequest request) {
        return termService.update(termId, request);
    }
}


