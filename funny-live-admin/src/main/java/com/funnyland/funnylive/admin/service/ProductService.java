package com.funnyland.funnylive.admin.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.funnyland.funnylive.admin.endpoint.product.request.ProductFilter;
import com.funnyland.funnylive.admin.endpoint.product.request.ProductRequest;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductDetailResponse;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductResponse;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductSimpleResponse;
import com.funnyland.funnylive.admin.repository.ProductReadRepository;
import com.funnyland.funnylive.admin.service.dto.StorageType;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.product.ProductDomainService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductReadRepository productReadRepository;
    private final ProductDomainService productDomainService;
    private final UploadService uploadService;

    public Page<ProductResponse> getAll(ProductFilter filter) {
        Page<Product> findProducts = productReadRepository.getProducts(filter.generateBooleanBuilder(), filter.getPageRequest());
        return findProducts.map(ProductResponse::of);

    }

    public ProductResponse createProduct(ProductRequest request, MultipartFile image) {
        Product product = request.toDomain();

        Optional<UploadDto> uploadDto = Optional.ofNullable(image)
                .flatMap(file -> uploadService.upload(file, StorageType.IMAGE));

        uploadDto.ifPresent(product::addImage);
        Product savedProduct = productDomainService.save(product);
        return ProductResponse.of(savedProduct);
    }

    public Product getProduct(Long productId) {
        return productDomainService.find(productId);
    }

    @Transactional
    public ProductDetailResponse update(Long productId, ProductRequest request, MultipartFile image) {
        Product updateProduct = request.toDomain();


        Optional<UploadDto> uploadDto = Optional.ofNullable(image)
                                                .flatMap(file -> uploadService.upload(file, StorageType.IMAGE));

        uploadDto.ifPresent(updateProduct::addImage);
        Product savedProduct = productDomainService.update(productId, updateProduct);
        return ProductDetailResponse.of(savedProduct);
    }

    public void delete(Long productId) {
        productDomainService.delete(productId);
    }

    public List<ProductSimpleResponse> getAvailable() {
        List<Product> findProducts = productReadRepository.getProducts();
        return findProducts.stream()
                .map(ProductSimpleResponse::of)
                .collect(Collectors.toList());
    }
}
