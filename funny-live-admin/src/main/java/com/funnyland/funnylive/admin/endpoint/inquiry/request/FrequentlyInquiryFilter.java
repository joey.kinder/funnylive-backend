package com.funnyland.funnylive.admin.endpoint.inquiry.request;

import static com.funnyland.funnylive.domain.inquiry.QFrequentlyInquiry.frequentlyInquiry;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FrequentlyInquiryFilter extends ApiPageRequest {
    private FrequentlyInquirySearchType searchType;

    private String searchValue;

    private Long inquiryTypeId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchStartDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchEndDate;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link FrequentlyInquirySearchType#TITLE} 타이틀 검색
     *   frequentlyInquiry.title like '% this.title %'
     *
     * - {@link FrequentlyInquirySearchType#CONTENT} 내용 검색
     *   frequentlyInquiry.content like '% this.content %'
     *
     * - {@link FrequentlyInquirySearchType#UPDATER} 이름 검색
     *   frequentlyInquiry.adminMember.name like '% this.name %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case TITLE:
            builder.and(frequentlyInquiry.question.like("%" + this.searchValue + "%"));
            break;
        case CONTENT:
            builder.and(
                    frequentlyInquiry.answer.like("%" + this.searchValue + "%"));
            break;
        case UPDATER:
            builder.and(
                    frequentlyInquiry.lastModifiedBy.like("%" + this.searchValue + "%"));
            break;
        default:
            break;
        }
    }

    /**
     * 타입 조건 추가.
     * - frequentlyInquiry.type in (this.type)
     *
     * @param builder query dsl boolean builder
     * */
    private void typeBuilder(BooleanBuilder builder) {
        if(this.inquiryTypeId != null) {
            builder.and(frequentlyInquiry.type.id.eq(this.inquiryTypeId));
        }
    }

    /**
     * 검색 시작일 조건 추가.
     * - created_at > this.searchStartDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchStartDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchStartDate)) {
            LocalDateTime startDateTime = this.searchStartDate.atStartOfDay();
            builder.and(frequentlyInquiry.createdAt.gt(startDateTime));
        }
    }

    /**
     * 검색 종료일 조건 추가.
     * - created_at < this.searchEndDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchEndDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchEndDate)) {
            LocalDateTime endDateTime = this.searchEndDate.atTime(23, 59, 59);
            builder.and(frequentlyInquiry.createdAt.lt(endDateTime));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        typeBuilder(builder);
        searchStartDateBuilder(builder);
        searchEndDateBuilder(builder);
        searchTypeAndContentBuilder(builder);

        return builder;
    }
}
