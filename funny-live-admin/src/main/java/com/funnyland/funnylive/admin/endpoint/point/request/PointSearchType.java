package com.funnyland.funnylive.admin.endpoint.point.request;

public enum PointSearchType {
    NAME,
    EMAIL
}
