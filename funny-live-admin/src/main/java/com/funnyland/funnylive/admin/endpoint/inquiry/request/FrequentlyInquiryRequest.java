package com.funnyland.funnylive.admin.endpoint.inquiry.request;

import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FrequentlyInquiryRequest {
    @NotNull
    private String question;
    @NotNull
    private String answer;
    @NotNull
    private Long inquiryTypeId;

    public FrequentlyInquiry toDomain(InquiryType inquiryType) {
        FrequentlyInquiry frequentlyInquiry = FrequentlyInquiry.builder()
                                                               .question(this.question)
                                                               .answer(this.answer)
                .type(inquiryType)
                                                               .build();
        return frequentlyInquiry;
    }
}