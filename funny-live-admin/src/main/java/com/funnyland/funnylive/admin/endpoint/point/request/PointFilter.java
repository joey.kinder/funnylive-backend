package com.funnyland.funnylive.admin.endpoint.point.request;

import static com.funnyland.funnylive.domain.point.QPointTransaction.pointTransaction;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.funnyland.funnylive.admin.endpoint.product.request.ProductSearchType;
import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PointFilter extends ApiPageRequest {

    private PointSearchType searchType;

    private String searchValue;

    private PointTransactionType pointTransactionType;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchStartDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchEndDate;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link ProductSearchType#NAME} 이름 검색
     *   product.name like '% this.name %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case NAME:
            builder.and(
                    pointTransaction.member.name.like("%" + this.searchValue + "%"));
            break;
        case EMAIL:
            builder.and(
                    pointTransaction.member.email.like("%" + this.searchValue + "%"));
            break;
        default:
            break;
        }
    }

    /**
     * 상태 조건 추가.
     * - pointTransaction.pointTransactionType in (this.pointTransactionType)
     *
     * @param builder query dsl boolean builder
     * */
    private void pointTransactionBuilder(BooleanBuilder builder) {
        if(this.pointTransactionType != null) {
            builder.and(pointTransaction.transactionType.eq(this.pointTransactionType));
        }
    }

    /**
     * 검색 시작일 조건 추가.
     * - created_at > this.searchStartDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchStartDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchStartDate)) {
            LocalDateTime startDateTime = this.searchStartDate.atStartOfDay();
            builder.and(pointTransaction.createdAt.gt(startDateTime));
        }
    }

    /**
     * 검색 종료일 조건 추가.
     * - created_at < this.searchEndDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchEndDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchEndDate)) {
            LocalDateTime endDateTime = this.searchEndDate.atTime(23, 59, 59);
            builder.and(pointTransaction.createdAt.lt(endDateTime));
        }
    }


    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        pointTransactionBuilder(builder);
        searchStartDateBuilder(builder);
        searchEndDateBuilder(builder);
        searchTypeAndContentBuilder(builder);

        return builder;
    }
}
