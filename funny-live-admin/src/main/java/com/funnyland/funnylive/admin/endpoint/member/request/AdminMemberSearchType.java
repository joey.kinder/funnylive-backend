package com.funnyland.funnylive.admin.endpoint.member.request;

public enum AdminMemberSearchType {
    EMAIL,
    NAME,
    PHONE
}
