package com.funnyland.funnylive.admin.endpoint.inquiry.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.admin.endpoint.member.response.MemberResponse;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InquiryResponse {

    private Long inquiryId;
    private String typeName;
    private String title;
    private String updater;
    private String stateName;
    private String filePath;
    private String fileName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime replyCreatedAt;

    public static InquiryResponse of(Inquiry inquiry) {
        return InquiryResponse.builder()
                             .inquiryId(inquiry.getId())
                             .typeName(inquiry.getType() != null? inquiry.getType().getName() : null)
                             .title(inquiry.getTitle())
                             .updater(inquiry.getMember() != null ? inquiry.getMember().getName() : null)
                             .createdAt(inquiry.getCreatedAt())
                             .stateName(inquiry.getStatus() != null? inquiry.getStatus().getName() : null)
                             .replyCreatedAt(inquiry.getReply() != null? inquiry.getReply().getCreatedAt(): null)
                             .build();

    }
}
