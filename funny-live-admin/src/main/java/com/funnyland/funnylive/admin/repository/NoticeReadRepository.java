package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.notice.QNotice.notice;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.notice.Notice;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

@Repository
public class NoticeReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public NoticeReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(Notice.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<Notice> getNotices(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<Notice> query = jpaQueryFactory.selectFrom(notice)
                .where(builder);

        List<Notice> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(notice.count())
                .from(notice)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Optional<Notice> findById(Long noticeId) {
        JPAQuery<Notice> query = jpaQueryFactory.selectFrom(notice)
                .where(notice.id.eq(noticeId));

        return Optional.ofNullable(query.fetchOne());
    }
}
