package com.funnyland.funnylive.admin.repository;


import static com.funnyland.funnylive.domain.delivery.QDelivery.delivery;
import static com.funnyland.funnylive.domain.delivery.QDeliveryItem.deliveryItem;
import static com.funnyland.funnylive.domain.inquiry.QInquiry.inquiry;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.admin.endpoint.delivery.request.DeliveryFilter;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class DeliveryReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public DeliveryReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(Delivery.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<Delivery> getDeliveries(DeliveryFilter filter, PageRequest pageRequest) {
        BooleanBuilder booleanBuilder = filter.generateBooleanBuilder(jpaQueryFactory);

        JPAQuery<Delivery> query = jpaQueryFactory.selectFrom(delivery)
                                                .where(booleanBuilder);

        List<Delivery> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(delivery.count())
                .from(delivery)
                .where(booleanBuilder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Optional<Delivery> findById(Long deliveryId) {
        JPAQuery<Delivery> query = jpaQueryFactory
                .selectFrom(delivery)
                .leftJoin(delivery.items, deliveryItem).fetchJoin()
                .where(delivery.id.eq(deliveryId));

        return Optional.ofNullable(query.fetchOne());
    }

    public List<Delivery> findOrderByCreatedAt(int limit) {
        JPAQuery<Delivery> query = jpaQueryFactory
                .selectFrom(delivery)
                .leftJoin(delivery.items, deliveryItem).fetchJoin()
                .orderBy(delivery.createdAt.desc()).limit(limit);
        return query.fetch();
    }

    public List<Delivery> getLatestDeliveries(Long memberId, int limit) {
        return jpaQueryFactory
                .selectFrom(delivery)
                .where(delivery.member.id.eq(memberId))
                .orderBy(delivery.createdAt.desc())
                .limit(limit)
                .fetch();
    }
}
