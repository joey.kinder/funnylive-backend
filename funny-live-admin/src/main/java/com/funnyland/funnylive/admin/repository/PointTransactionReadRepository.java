package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.point.QPointTransaction.pointTransaction;

import java.util.List;

import com.funnyland.funnylive.domain.point.PointTransaction;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class PointTransactionReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public PointTransactionReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(PointTransaction.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<PointTransaction> getPoints(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<PointTransaction> query = jpaQueryFactory.selectFrom(pointTransaction)
                .where(builder);

        List<PointTransaction> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(pointTransaction.count())
                .from(pointTransaction)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public int getSumPoint(Long memberId) {
        Integer sum = jpaQueryFactory
                .select(pointTransaction.amount.sum())
                .from(pointTransaction)
                .where(pointTransaction.member.id.eq(memberId))
                .fetchOne();

        return sum != null ? sum : 0;
    }

    public List<PointTransaction> getLatestPointTransactions(Long memberId, int limit) {
        return jpaQueryFactory
                .selectFrom(pointTransaction)
                .where(pointTransaction.member.id.eq(memberId))
                .orderBy(pointTransaction.createdAt.desc())
                .limit(limit)
                .fetch();
    }
}

