package com.funnyland.funnylive.admin.endpoint.livebroadcast.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcast;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LiveBroadcastDetailResponse {

    private Long liveBroadcastId;
    private String title;
    private String updater;
    private String url;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime modifiedAt;

    public static LiveBroadcastDetailResponse of(LiveBroadcast liveBroadcast) {
        return LiveBroadcastDetailResponse.builder()
                .liveBroadcastId(liveBroadcast.getId())
                .title(liveBroadcast.getTitle())
                .url(liveBroadcast.getUrl())
                .createdAt(liveBroadcast.getCreatedAt())
                .modifiedAt(liveBroadcast.getModifiedAt())
                .updater(liveBroadcast.getLastModifiedBy())
                .build();

    }
}
