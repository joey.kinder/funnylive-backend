package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.notice.QNotice.notice;
import static com.funnyland.funnylive.domain.storagebox.QStorageBox.storageBox;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.storagebox.StorageBox;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class StorageBoxReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public StorageBoxReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(StorageBox.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<StorageBox> getStorageBoxes(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<StorageBox> query = jpaQueryFactory.selectFrom(storageBox)
                              .where(builder);

        List<StorageBox> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(storageBox.count())
                .from(storageBox)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Optional<StorageBox> findById(Long id) {
        JPAQuery<StorageBox> query = jpaQueryFactory.selectFrom(storageBox)
                .where(storageBox.id.eq(id));

        return Optional.ofNullable(query.fetchOne());
    }
}

