package com.funnyland.funnylive.admin.endpoint.livebroadcast.request;

public enum LiveBroadcastSearchType {
    TITLE,
    CONTENT,
    UPDATER;
}
