package com.funnyland.funnylive.admin.endpoint.receipt.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.receipt.Receipt;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReceiptDetailResponse {
    private Long receiptId;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime purchaseTime;
    private Integer paymentAmount;
    private Integer chargeAmount;
    private String stateName;
    private String paymentMethodName;
    private String memo;

    public static ReceiptDetailResponse of(Receipt receipt) {
        return ReceiptDetailResponse.builder()
                              .receiptId(receipt.getId())
                              .name(receipt.getMember().getName())
                                    .paymentMethodName(receipt.getPaymentMethod() != null ? receipt.getPaymentMethod().getName(): null)
                              .purchaseTime(receipt.getPurchaseTime())
                              .paymentAmount(receipt.getPaymentAmount())
                              .chargeAmount(receipt.getChargeAmount())
                            .stateName(receipt.getState() != null ? receipt.getState().getName() : null)
                            .memo(receipt.getMemo())
                              .build();
    }

}
