package com.funnyland.funnylive.admin.service;

import com.funnyland.funnylive.admin.endpoint.member.request.AdminMemberFilter;
import com.funnyland.funnylive.admin.endpoint.member.request.AdminMemberRequest;
import com.funnyland.funnylive.admin.endpoint.member.response.AdminMemberDetailResponse;
import com.funnyland.funnylive.admin.endpoint.member.response.AdminMemberResponse;
import com.funnyland.funnylive.admin.repository.AdminMemberReadRepository;
import com.funnyland.funnylive.domain.member.AdminMember;
import com.funnyland.funnylive.domain.member.AdminMemberDomainService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class AdminMemberService {

    private final AdminMemberDomainService adminMemberDomainService;
    private final AdminMemberReadRepository adminMemberReadRepository;

    public Page<AdminMemberResponse> getAll(AdminMemberFilter filter) {
        Page<AdminMember> findAdminMembers = adminMemberReadRepository.getAdminMembers(filter.generateBooleanBuilder(), filter.getPageRequest());
        return findAdminMembers
                .map(AdminMemberResponse::of);
    }

    public AdminMemberDetailResponse get(Long adminMemberId) {
        AdminMember findAdminMember = adminMemberDomainService.find(adminMemberId);
        return AdminMemberDetailResponse.of(findAdminMember);
    }

    public AdminMemberDetailResponse create(AdminMemberRequest request) {
        AdminMember savedAdminMember = adminMemberDomainService.save(request.toDomain());
        return AdminMemberDetailResponse.of(savedAdminMember);
    }

    @Transactional
    public AdminMemberDetailResponse update(Long adminMemberId, AdminMemberRequest request) {
        AdminMember updatedAdminMember = adminMemberDomainService.update(adminMemberId, request.toDomain());
        return AdminMemberDetailResponse.of(updatedAdminMember);
    }
}
