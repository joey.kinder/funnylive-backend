package com.funnyland.funnylive.admin.endpoint.member.request;

import static com.funnyland.funnylive.domain.member.QAdminMember.adminMember;

import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.member.AdminRole;
import com.funnyland.funnylive.domain.member.AdminStatus;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AdminMemberFilter extends ApiPageRequest {

    private AdminMemberSearchType searchType;

    private String searchValue;

    private AdminRole role;

    private AdminStatus state;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link AdminMemberSearchType#EMAIL} 이메일 검색
     *   adminMember.email like '% this.email %'
     *
     * - {@link AdminMemberSearchType#NAME} 이름 검색
     *   adminMember.name like '% this.name %'

     * - {@link AdminMemberSearchType#PHONE} 핸드폰 검색
     *   adminMember.phone like '% this.phone %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case EMAIL:
            builder.and(adminMember.email.like("%" + this.searchValue + "%"));
            break;
        case NAME:
            builder.and(
                    adminMember.name.like("%" + this.searchValue + "%"));
            break;
        case PHONE:
            builder.and(
                    adminMember.phone.like("%" + this.searchValue + "%"));
            break;
        default:
            break;
        }
    }

    /**
     * 상태 조건 추가.
     * - member.status in (this.status)
     *
     * @param builder query dsl boolean builder
     * */
    private void statusBuilder(BooleanBuilder builder) {
        if(this.state != null) {
            builder.and(adminMember.status.eq(this.state));
        }
    }

    /**
     * 등급 조건 추가.
     * - member.grade in (this.grade)
     *
     * @param builder query dsl boolean builder
     * */
    private void roleBuilder(BooleanBuilder builder) {
        if(this.role != null) {
            builder.and(adminMember.role.eq(this.role));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        statusBuilder(builder);
        roleBuilder(builder);
        searchTypeAndContentBuilder(builder);

        return builder;
    }
}
