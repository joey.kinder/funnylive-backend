package com.funnyland.funnylive.admin.endpoint.storagebox.controller;

import java.util.List;

import com.funnyland.funnylive.admin.endpoint.product.request.ProductFilter;
import com.funnyland.funnylive.admin.endpoint.product.request.ProductRequest;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductDetailResponse;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductResponse;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductSimpleResponse;
import com.funnyland.funnylive.admin.endpoint.storagebox.request.StorageBoxFilter;
import com.funnyland.funnylive.admin.endpoint.storagebox.response.StorageBoxDetailResponse;
import com.funnyland.funnylive.admin.endpoint.storagebox.response.StorageBoxResponse;
import com.funnyland.funnylive.admin.service.ProductService;
import com.funnyland.funnylive.admin.service.StorageBoxService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@Tag(name="보관함", description = "보관함 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/storage-boxes")
public class StorageBoxController {

    private final StorageBoxService storageBoxService;

    @GetMapping
    @Operation(summary = "보관함 리스트 조회", description = "보관함 리스트 조회")
    public Page<StorageBoxResponse> getStorageBoxes(StorageBoxFilter filter) {
        return storageBoxService.getAll(filter);
    }

    @GetMapping("/{storageBoxId}")
    @Operation(summary = "보관함 상세 조회", description = "보관함 상세 조회")
    public StorageBoxDetailResponse getStorageBox(@PathVariable Long storageBoxId) {
        return storageBoxService.get(storageBoxId);
    }

}
