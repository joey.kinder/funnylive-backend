package com.funnyland.funnylive.admin.endpoint.notice.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.notice.Notice;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NoticeResponse {

    private Long noticeId;
    private String type;
    private String title;
    private String updater;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime modifiedAt;
    private Long viewCount;

    public static NoticeResponse of(Notice notice) {
        return NoticeResponse.builder()
                .noticeId(notice.getId())
                .type(notice.getNoticeType() != null? notice.getNoticeType().getName() : null)
                .title(notice.getTitle())
                .createdAt(notice.getCreatedAt())
                .modifiedAt(notice.getModifiedAt())
                .viewCount(notice.getViewCount())
                .updater(notice.getLastModifiedBy())
                .build();

    }
}
