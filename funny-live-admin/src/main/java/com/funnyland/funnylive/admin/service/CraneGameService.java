package com.funnyland.funnylive.admin.service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.cranegame.request.CraneGameFilter;
import com.funnyland.funnylive.admin.endpoint.cranegame.request.CraneGameRequest;
import com.funnyland.funnylive.admin.endpoint.cranegame.response.CraneGameDetailResponse;
import com.funnyland.funnylive.admin.endpoint.cranegame.response.CraneGameResponse;
import com.funnyland.funnylive.admin.repository.CraneGameHeartbeatRepository;
import com.funnyland.funnylive.admin.repository.CraneGameRepository;
import com.funnyland.funnylive.admin.repository.CraneGameReadRepository;
import com.funnyland.funnylive.admin.service.dto.StorageType;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.cranegame.CraneGameDomainRepository;
import com.funnyland.funnylive.domain.cranegame.CraneGameDomainService;
import com.funnyland.funnylive.domain.cranegame.CraneGameHeartbeat;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import com.funnyland.funnylive.domain.product.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


@RequiredArgsConstructor
@Service
public class CraneGameService {

    private final CraneGameRepository craneGameReadRepository;
    private final CraneGameReadRepository craneGameRepository;
    private final CraneGameDomainService craneGameDomainService;
    private final CraneGameDomainRepository craneGameDomainRepository;
    private final ProductService productService;
    private final UploadService uploadService;
    private final CraneGameHeartbeatRepository craneGameHeartbeatRepository;

    public Page<CraneGameResponse> getAll(CraneGameFilter filter) {
        Page<CraneGame> findCraneGames = craneGameRepository.findAll(filter.generateBooleanBuilder(), filter.getPageRequest());

        return findCraneGames.map(this::createCraneGameResponse);
    }

    private CraneGameResponse createCraneGameResponse(CraneGame craneGame) {
        LocalDateTime lastHeartbeatTime = getLastHeartbeatTime(craneGame.getId());
        return CraneGameResponse.of(craneGame, lastHeartbeatTime);
    }

    public CraneGameDetailResponse get(Long craneGameId) {
        CraneGame findCraneGame = craneGameReadRepository.findById(craneGameId)
                                                        .orElseThrow(() -> new DomainException("해당 크레인이 없습니다"));
        LocalDateTime lastHeartbeatTime = getLastHeartbeatTime(findCraneGame.getId());
        return CraneGameDetailResponse.of(findCraneGame, lastHeartbeatTime);
    }

    private LocalDateTime getLastHeartbeatTime(Long craneGameId) {
        return craneGameHeartbeatRepository.findById(craneGameId)
                .map(CraneGameHeartbeat::getLastHeartbeatTime)
                .orElse(null);
    }

    @Transactional
    public CraneGameDetailResponse create(CraneGameRequest request, MultipartFile image) {
        Set<Product> findProducts = Optional.ofNullable(request.getProductIds())
                .orElseGet(Collections::emptyList)
                .stream()
                .map(productService::getProduct)
                .collect(Collectors.toSet());

        CraneGame craneGame = request.toDomain();
        craneGame.add(findProducts);

        Optional<UploadDto> uploadDto = Optional.ofNullable(image)
                .flatMap(file -> uploadService.upload(file, StorageType.IMAGE));

        uploadDto.ifPresent(craneGame::addImage);
        return CraneGameDetailResponse.of(craneGameDomainRepository.save(craneGame));
    }

    public boolean check(String uuid) {
        return craneGameRepository.check(uuid);
    }

    @Transactional
    public CraneGameDetailResponse update(Long craneGameId, CraneGameRequest request) {
        Set<Product> findProducts = Optional.ofNullable(request.getProductIds())
                .orElseGet(Collections::emptyList)
                .stream()
                .map(productService::getProduct)
                .collect(Collectors.toSet());

        CraneGame updatedCraneGame = request.toDomain();

        CraneGame savedCraneGame = craneGameDomainService.update(craneGameId, updatedCraneGame);
        savedCraneGame.add(findProducts);

        return CraneGameDetailResponse.of(savedCraneGame);
    }

    public void delete(Long craneGameId) {
        craneGameDomainService.delete(craneGameId);
    }
}
