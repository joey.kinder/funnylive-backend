package com.funnyland.funnylive.admin.endpoint.login;

import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Getter
public class LoginRequest {
    private String email;
    private String password;
}
