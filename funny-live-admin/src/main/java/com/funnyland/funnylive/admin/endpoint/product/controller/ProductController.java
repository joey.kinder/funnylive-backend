package com.funnyland.funnylive.admin.endpoint.product.controller;

import java.util.List;

import com.funnyland.funnylive.admin.endpoint.product.request.ProductFilter;
import com.funnyland.funnylive.admin.endpoint.product.request.ProductRequest;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductDetailResponse;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductResponse;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductSimpleResponse;
import com.funnyland.funnylive.admin.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@Tag(name="상품", description = "상품 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/products")
public class ProductController {

    private final ProductService productService;

    @GetMapping("/available")
    @Operation(summary = "사용가능한 상품 리스트 조회", description = "사용한가능한 상품 리스트 조회")
    public List<ProductSimpleResponse> getAvailableProducts() {
        return productService.getAvailable();
    }

    @GetMapping
    @Operation(summary = "상품 리스트 조회", description = "상품 리스트 조회")
    public Page<ProductResponse> getProducts(ProductFilter filter) {
        return productService.getAll(filter);
    }

    @GetMapping("/{productId}")
    @Operation(summary = "상품 상세 조회", description = "상품 상세 조회")
    public ProductDetailResponse getProduct(@PathVariable Long productId) {
        return ProductDetailResponse.of(productService.getProduct(productId));
    }

    @PostMapping
    @Operation(summary = "상품 추가", description = "상품 리스트 조회")
    public ProductResponse create(@RequestPart("product") ProductRequest request, @RequestPart(name = "image", required = false) MultipartFile image) {
        return productService.createProduct(request, image);
    }

    @PutMapping("/{productId}")
    @Operation(summary = "상품 수정", description = "상품 수정")
    public ProductDetailResponse update(@PathVariable Long productId, @RequestPart("product") ProductRequest request, @RequestPart(value = "image", required = false) MultipartFile image) {
        return productService.update(productId, request, image);
    }

    @DeleteMapping("/{productId}")
    @Operation(summary = "상품 삭제", description = "상품 삭제")
    public void delete(@PathVariable Long productId) {
        productService.delete(productId);
    }

}
