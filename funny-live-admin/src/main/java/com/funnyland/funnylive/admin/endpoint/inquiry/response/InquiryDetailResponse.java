package com.funnyland.funnylive.admin.endpoint.inquiry.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryReply;
import com.funnyland.funnylive.domain.member.Member;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InquiryDetailResponse {

    private Long inquiryId;
    private String typeName;
    private String title;
    private String content;
    private String stateName;
    private String filePath;
    private String fileName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    private MemberResponse updater;
    private InquiryReplyResponse reply;

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class MemberResponse {
        private String email;
        private String name;
        private String oAuthProviderName;
        private String gradeName;
    }

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class InquiryReplyResponse {
        private String title;
        private String content;
        private String updater;
        @JsonFormat(pattern = "yyyy-MM-dd")
        private LocalDateTime createdAt;
    }

    public static InquiryDetailResponse of(Inquiry inquiry) {
        Member member = inquiry.getMember();
        MemberResponse memberResponse = null;
        if (member != null) {
            memberResponse = MemberResponse.builder()
                    .email(member.getEmail())
                    .name(member.getName())
                    .oAuthProviderName(member.getOAuthProvider() != null ? member.getOAuthProvider().getName() : null)
                    .gradeName(member.getGrade() != null ? member.getGrade().getName() : null)
                    .build();
        }

        InquiryReply reply = inquiry.getReply();
        InquiryReplyResponse inquiryReplyResponse = null;
        if(reply != null) {
            inquiryReplyResponse = InquiryReplyResponse.builder()
                    .title(reply.getTitle())
                    .content(reply.getContent())
                    .updater(reply.getLastModifiedBy())
                    .createdAt(reply.getCreatedAt())
                    .build();
        }

        return InquiryDetailResponse.builder()
                             .inquiryId(inquiry.getId())
                             .typeName(inquiry.getType() != null? inquiry.getType().getName() : null)
                             .title(inquiry.getTitle())
                             .content(inquiry.getContent())
                             .createdAt(inquiry.getCreatedAt())
                             .stateName(inquiry.getStatus() != null? inquiry.getStatus().getName() : null)
                            .updater(memberResponse)
                            .reply(inquiryReplyResponse)
                             .build();

    }
}
