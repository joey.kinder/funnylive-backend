package com.funnyland.funnylive.admin.endpoint.product.response;

import com.funnyland.funnylive.domain.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductSimpleResponse {
    private Long productId;
    private String name;
    private String stateName;


    public static ProductSimpleResponse of(Product product) {
        return ProductSimpleResponse.builder()
                .productId(product.getId())
                .name(product.getName())
                .stateName(product.getStatus() != null ? product.getStatus().getName() : null)
                .build();
    }

}
