package com.funnyland.funnylive.admin.endpoint.receipt.request;

import static com.funnyland.funnylive.domain.product.QProduct.product;
import static com.funnyland.funnylive.domain.receipt.QReceipt.receipt;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.receipt.PaymentMethod;
import com.funnyland.funnylive.domain.receipt.ReceiptState;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReceiptFilter extends ApiPageRequest {

    private ReceiptSearchType searchType;

    private String searchValue;

    private PaymentMethod paymentMethod;

    private ReceiptState state;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchStartDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchEndDate;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link ReceiptSearchType#EMAIL} 이메일 검색
     *   member.email like '% this.email %'
     *
     * - {@link ReceiptSearchType#NAME} 이름 검색
     *   member.name like '% this.name %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case EMAIL:
            builder.and(receipt.member.email.like("%" + this.searchValue + "%"));
            break;
        case NAME:
            builder.and(
                    receipt.member.name.like("%" + this.searchValue + "%"));
            break;
        default:
            break;
        }
    }

    /**
     * 검색 시작일 조건 추가.
     * - created_at > this.searchStartDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchStartDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchStartDate)) {
            LocalDateTime startDateTime = this.searchStartDate.atStartOfDay();
            builder.and(receipt.createdAt.gt(startDateTime));
        }
    }

    /**
     * 검색 종료일 조건 추가.
     * - created_at < this.searchEndDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchEndDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchEndDate)) {
            LocalDateTime endDateTime = this.searchEndDate.atTime(23, 59, 59);
            builder.and(receipt.createdAt.lt(endDateTime));
        }
    }

    /**
     * 상태 조건 추가.
     * - receipt.state in (this.state)
     *
     * @param builder query dsl boolean builder
     * */
    private void stateBuilder(BooleanBuilder builder) {
        if(this.state != null) {
            builder.and(receipt.state.eq(this.state));
        }
    }

    /**
     * 결제수단 조건 추가.
     * - receipt.paymentMethod in (this.paymentMethod)
     *
     * @param builder query dsl boolean builder
     * */
    private void paymentMethodBuilder(BooleanBuilder builder) {
        if(this.paymentMethod != null) {
            builder.and(receipt.paymentMethod.eq(this.paymentMethod));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        searchStartDateBuilder(builder);
        searchEndDateBuilder(builder);
        searchTypeAndContentBuilder(builder);
        paymentMethodBuilder(builder);
        stateBuilder(builder);

        return builder;
    }
}

