package com.funnyland.funnylive.admin.service;

import java.util.List;
import java.util.stream.Collectors;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.inquiry.request.InquiryFilter;
import com.funnyland.funnylive.admin.endpoint.inquiry.request.InquiryReplyRequest;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.InquiryDetailResponse;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.InquiryResponse;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.InquiryTypeResponse;
import com.funnyland.funnylive.admin.repository.InquiryReadRepository;
import com.funnyland.funnylive.admin.repository.InquiryReplyReadRepository;
import com.funnyland.funnylive.admin.repository.InquiryTypeReadRepository;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryReply;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import com.funnyland.funnylive.domain.receipt.Receipt;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class InquiryService {

    private final InquiryReadRepository inquiryReadRepository;
    private final InquiryTypeReadRepository inquiryTypeReadRepository;
    private final InquiryReplyReadRepository inquiryReplyReadRepository;

    public Page<InquiryResponse> getAll(InquiryFilter filter) {
        Page<Inquiry> inquiries = inquiryReadRepository.getInquiries(filter.generateBooleanBuilder(), filter.getPageRequest());
        return inquiries.map(InquiryResponse::of);
    }

    public List<Inquiry> getLatestInquiries(Long memberId, int limit) {
        List<Inquiry> findInquiries = inquiryReadRepository.getLatestInquiries(memberId, limit);
        return findInquiries;
    }

    public List<InquiryTypeResponse> getTypes() {
        List<InquiryType> types = inquiryTypeReadRepository.getTypes();
        return types.stream()
                .map(InquiryTypeResponse::of)
                .collect(Collectors.toList());
    }

    public InquiryDetailResponse get(Long inquiryId) {
        Inquiry findInquiry = inquiryReadRepository.findById(inquiryId).orElseThrow(() -> new DomainException("없는 문의 입니다"));
        return InquiryDetailResponse.of(findInquiry);
    }

    @Transactional
    public InquiryDetailResponse createReply(Long inquiryId, InquiryReplyRequest request) {
        Inquiry updatedInquiry = inquiryReadRepository.findById(inquiryId)
                .orElseThrow(() -> new DomainException("없는 문의 입니다"))
                .addReply(request.toDomain());

        return InquiryDetailResponse.of(updatedInquiry);
    }

    @Transactional
    public InquiryDetailResponse updateReply(Long inquiryId, InquiryReplyRequest request) {
        Inquiry updatedInquiry = inquiryReadRepository.findById(inquiryId)
                .orElseThrow(() -> new DomainException("없는 문의 입니다"))
                .updateReply(request.toDomain());

        return InquiryDetailResponse.of(updatedInquiry);
    }
}
