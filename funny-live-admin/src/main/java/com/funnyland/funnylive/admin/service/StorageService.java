package com.funnyland.funnylive.admin.service;

import com.funnyland.funnylive.admin.service.dto.StorageType;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import org.springframework.web.multipart.MultipartFile;


public interface StorageService {
    UploadDto upload(MultipartFile file, StorageType type);
}
