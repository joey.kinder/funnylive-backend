package com.funnyland.funnylive.admin.endpoint.login;

import com.funnyland.funnylive.admin.config.JwtTokenProvider;
import com.funnyland.funnylive.admin.repository.AdminMemberRepository;
import com.funnyland.funnylive.admin.service.CustomUserDetailsService;
import com.funnyland.funnylive.common.exception.CustomException;
import com.funnyland.funnylive.common.exception.ErrorCode;
import com.funnyland.funnylive.common.exception.ErrorResponse;
import com.funnyland.funnylive.domain.member.AdminMember;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;


@Tag(name="로그인", description = "로그인 API")
@Controller
@RequiredArgsConstructor
public class LoginController {

    @Value("${app.cookie.secure}")
    private boolean isCookieSecure;
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomUserDetailsService customUserDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final AdminMemberRepository adminMemberRepository;

    @PostMapping("/login")
    @Operation(summary = "로그인", description = "로그인")
    @ResponseBody
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest, HttpServletResponse response) {
        try {
            AdminMember adminMember = adminMemberRepository.findByEmail(loginRequest.getEmail())
                    .orElseThrow(() -> new UsernameNotFoundException("User not found with email: " + loginRequest.getEmail()));

            if (!passwordEncoder.matches(loginRequest.getPassword(), adminMember.getPassword())) {
                throw new CustomException(ErrorCode.USER_PASSWORD_NOT_EQUAL);
            }

            String token = jwtTokenProvider.createToken(adminMember);
            setCookie(response, "auth_token" , token);

            return ResponseEntity.ok("Login successful");
        } catch (UsernameNotFoundException e) {
            throw new CustomException(ErrorCode.USER_NOT_FOUND);
        } catch (CustomException e) {
            return ResponseEntity.ok(new ErrorResponse(e.getErrorCode()));
        }
    }

    private void setCookie(HttpServletResponse response, String name, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        cookie.setSecure(isCookieSecure);
        response.addCookie(cookie);
    }


    @GetMapping("/login")
    public String login() {
        return "login"; // 로그인 성공 후 리다이렉트할 페이지
    }

    @GetMapping("/")
    public String main() {
        return "main"; // 로그인 성공 후 리다이렉트할 페이지
    }
}
