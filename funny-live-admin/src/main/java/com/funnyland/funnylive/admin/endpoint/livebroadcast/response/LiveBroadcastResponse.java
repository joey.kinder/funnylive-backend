package com.funnyland.funnylive.admin.endpoint.livebroadcast.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcast;
import com.funnyland.funnylive.domain.notice.Notice;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LiveBroadcastResponse {

    private Long liveBroadcastId;
    private String title;
    private String updater;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime modifiedAt;

    public static LiveBroadcastResponse of(LiveBroadcast liveBroadcast) {
        return LiveBroadcastResponse.builder()
                .liveBroadcastId(liveBroadcast.getId())
                .title(liveBroadcast.getTitle())
                .createdAt(liveBroadcast.getCreatedAt())
                .modifiedAt(liveBroadcast.getModifiedAt())
                .updater(liveBroadcast.getLastModifiedBy())
                .build();

    }
}
