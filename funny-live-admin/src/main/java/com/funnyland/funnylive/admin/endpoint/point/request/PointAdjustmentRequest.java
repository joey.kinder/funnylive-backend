package com.funnyland.funnylive.admin.endpoint.point.request;

import java.time.LocalDate;
import java.util.List;

import com.funnyland.funnylive.domain.point.PointTransactionType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PointAdjustmentRequest {
    private List<String> memberEmails; // 회원 ID 목록
    private boolean applyToAllMembers; // 모든 회원에 적용 여부
    private int point; // 지급/차감할 포인트 양
    private String reason; // 사유
    private LocalDate expirationStartDate; // 유효기간 시작일
    private LocalDate expirationEndDate; // 유효기간 종료일
    private PointTransactionType transactionType; // 트랜잭션 유형

}
