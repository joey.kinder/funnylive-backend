package com.funnyland.funnylive.admin.service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import com.funnyland.funnylive.admin.service.dto.StorageType;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import lombok.AllArgsConstructor;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Exception;


@AllArgsConstructor
public class S3StorageService implements StorageService {

    private final S3Client s3Client;
    private final String bucketName;
    private final String s3Profile;

    @Override
    public UploadDto upload(MultipartFile file, StorageType type) {
        try {
            String fileName = generateFileName(file.getOriginalFilename());
            String currentMonth = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM"));
            String s3Key;
            if(type.isImage()) {
                s3Key = String.format("%s/images/%s/%s", s3Profile, currentMonth, fileName);
            }
            else {
                s3Key = String.format("%s/attachments/%s/%s", s3Profile, currentMonth, fileName);
            }
            PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                                                                .bucket(bucketName)
                                                                .key(s3Key)
                                                                .build();

            s3Client.putObject(putObjectRequest, RequestBody.fromInputStream(file.getInputStream(), file.getSize()));

            UploadDto uploadDto = UploadDto.builder()
                    .filePath(s3Key)
                    .fileName(fileName)
                    .originalName(file.getOriginalFilename())
                    .build();
            return uploadDto;
        } catch (S3Exception e) {
            throw new RuntimeException("Failed to upload image to S3", e);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read image file", e);
        }
    }

    private String generateFileName(String originalFileName) {
        // 파일 이름을 무작위 UUID로 생성
        String extension = originalFileName.substring(originalFileName.lastIndexOf("."));
        return UUID.randomUUID() + extension;
    }

    private boolean isImage(MultipartFile file) {
        // 파일 확장자를 추출하여 이미지 확장자 목록과 비교
        String[] imageExtensions = {"jpg", "jpeg", "png", "gif", "bmp"};
        String originalFileName = file.getOriginalFilename().toLowerCase();
        for (String extension : imageExtensions) {
            if (originalFileName.endsWith("." + extension)) {
                return true;
            }
        }
        return false;
    }
}
