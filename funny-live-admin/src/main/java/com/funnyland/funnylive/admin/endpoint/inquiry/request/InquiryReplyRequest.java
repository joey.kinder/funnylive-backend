package com.funnyland.funnylive.admin.endpoint.inquiry.request;

import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryReply;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InquiryReplyRequest {
    private String title;
    private String content;

    public InquiryReply toDomain() {
        InquiryReply inquiryReply = InquiryReply.builder()
                .title(this.title)
                .content(this.content)
                .build();
        return inquiryReply;
    }
}