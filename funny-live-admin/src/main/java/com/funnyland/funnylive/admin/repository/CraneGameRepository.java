package com.funnyland.funnylive.admin.repository;

import com.funnyland.funnylive.domain.cranegame.CraneGame;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CraneGameRepository extends JpaRepository<CraneGame, Long> {
}

