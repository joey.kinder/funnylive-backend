package com.funnyland.funnylive.admin.endpoint.notice.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.notice.Notice;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NoticeDetailResponse {

    private Long noticeId;
    private String type;
    private String title;
    private String content;
    private String updater;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime modifiedAt;
    private Long viewCount;

    public static NoticeDetailResponse of(Notice notice) {
        return NoticeDetailResponse.builder()
                                   .noticeId(notice.getId())
                                   .type(notice.getNoticeType() != null? notice.getNoticeType().name() : null)
                                   .title(notice.getTitle())
                                    .content(notice.getContent())
                                   .createdAt(notice.getCreatedAt())
                                   .modifiedAt(notice.getModifiedAt())
                                   .viewCount(notice.getViewCount())
                                   .updater(notice.getLastModifiedBy())
                                   .build();

    }
}
