package com.funnyland.funnylive.admin.endpoint.delivery.request;

import com.funnyland.funnylive.domain.delivery.Delivery;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryUpdateRequest {
    private String invoiceCompanyName;
    private String invoiceNumber;
    private Delivery.DeliveryStatus state;
    private String memo;

    public Delivery toDomain() {
        Delivery delivery = Delivery.builder()
                .invoiceCompanyName(this.invoiceCompanyName)
                .invoiceNumber(this.invoiceNumber)
                .status(this.state)
                .memo(this.memo)
                .build();
        return delivery;
    }
}
