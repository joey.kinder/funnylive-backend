package com.funnyland.funnylive.admin.endpoint.delivery.request;

public enum DeliverySearchType {
    ORDER_NUMBER,
    UPDATER,
    NICKNAME,
    EMAIL,
    PHONE,
    RECIPIENT,
    PRODUCT_NAME;
}
