package com.funnyland.funnylive.admin.endpoint.member.response;

import com.funnyland.funnylive.domain.member.AdminMember;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdminMemberDetailResponse {
    private Long memberId;
    private String email;
    private String role;
    private String password;
    private String passwordConfirm;

    private String association;
    private String rank;
    private String name;
    private String phone;
    private String ip;
    private String state;
    private String memo;

    public static AdminMemberDetailResponse of(AdminMember adminMember) {
        return AdminMemberDetailResponse.builder()
                .memberId(adminMember.getId())
                .email(adminMember.getEmail())
                .role(adminMember.getRole() != null ? adminMember.getRole().name() : null)
                .password(adminMember.getPassword())
                .passwordConfirm(adminMember.getPasswordConfirm())
                .association(adminMember.getAssociation())
                .rank(adminMember.getRank())
                .name(adminMember.getName())
                .phone(adminMember.getPhone())
                .ip(adminMember.getIp())
                .state(adminMember.getStatus() != null ? adminMember.getStatus().name() : null)
                .memo(adminMember.getMemo())
                .build();
    }

}
