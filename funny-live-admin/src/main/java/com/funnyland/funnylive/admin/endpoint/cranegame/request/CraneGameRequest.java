package com.funnyland.funnylive.admin.endpoint.cranegame.request;

import java.util.List;

import com.funnyland.funnylive.domain.cranegame.CraneGame;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "크레인 기기 정보")
@ToString
public class CraneGameRequest {

    @Schema(title = "고유번호", example = "300-120", requiredMode = Schema.RequiredMode.REQUIRED)
    private String uuid;
    @Schema(title = "게임기 설명", example = "2023 신상 상품 등록! - 귀멸의 칼날", requiredMode = Schema.RequiredMode.REQUIRED)
    private String description;
    @Schema(title = "플레이 요금", example = "500", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer playFee;
    @Schema(title = "타이머 시간", example = "60", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer timer;
    @Schema(title = "ip", example = "192.168.1.11", requiredMode = Schema.RequiredMode.REQUIRED)
    private String ip;
    @Schema(title = "state", example = "READY", requiredMode = Schema.RequiredMode.REQUIRED)
    private CraneGame.CraneGameState state;
    @Schema(title = "게임기 상품 id", example = "1", requiredMode = Schema.RequiredMode.REQUIRED)
    private List<Long> productIds;

    public CraneGame toDomain() {
        CraneGame craneGame = CraneGame.builder()
                                       .uuid(this.uuid)
                                       .description(this.description)
                                       .playFee(this.playFee)
                                       .timer(this.timer)
                                        .ip(this.ip)
                                        .gameState(this.state)
                                       .build();
        return craneGame;
    }
}