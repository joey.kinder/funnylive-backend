package com.funnyland.funnylive.admin.endpoint.product.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {
    private Long productId;
    private String name;
    private double price;
    private String imageUrl;
    private String stateName;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;

    public static ProductResponse of(Product product) {
        return ProductResponse.builder()
                .productId(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .imageUrl(UrlUtil.convertToCdnUrl(product.getImageURL()))
                .stateName(product.getStatus() != null ? product.getStatus().getName() : null)
                .createdAt(product.getCreatedAt())
                .build();
    }

}
