package com.funnyland.funnylive.admin.endpoint.storagebox.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.storagebox.StorageBox;
import com.funnyland.funnylive.domain.storagebox.StorageBoxHistory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StorageBoxDetailResponse {
    private Long storageBoxHistoryId;

    private ProductResponse product;
    private Integer acquisitionPoint;
    private MemberResponse updater;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;


    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class MemberResponse {
        private String email;
        private String name;
        private String oAuthProviderName;
        private String gradeName;
    }

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class ProductResponse {
        private Long id;
        private String name;
        private String imageUrl;
    }


    public static StorageBoxDetailResponse of(StorageBoxHistory storageBoxHistory) {
        Product product = storageBoxHistory.getOriginProduct();
        ProductResponse productResponse = null;
        if (product != null) {
            productResponse = ProductResponse.builder()
                    .id(product.getId())
                    .name(product.getName())
                    .imageUrl(UrlUtil.convertToCdnUrl(storageBoxHistory.getOriginProduct().getImageURL()))
                    .build();
        }

        Member member = storageBoxHistory.getStorageBox()
                                         .getMember();
        MemberResponse memberResponse = null;
        if (member != null) {
            memberResponse = MemberResponse.builder()
                    .email(member.getEmail())
                    .name(member.getName())
                    .oAuthProviderName(member.getOAuthProvider() != null ? member.getOAuthProvider().getName() : null)
                    .gradeName(member.getGrade() != null ? member.getGrade().getName() : null)
                    .build();
        }

        return StorageBoxDetailResponse.builder()
                .storageBoxHistoryId(storageBoxHistory.getId())
                .product(productResponse)
                .updater(memberResponse)
                .acquisitionPoint(storageBoxHistory.getProductPrice())
                .createdAt(storageBoxHistory.getCreatedAt())
                .build();
    }

}
