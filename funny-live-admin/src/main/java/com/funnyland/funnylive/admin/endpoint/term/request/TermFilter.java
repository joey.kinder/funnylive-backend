package com.funnyland.funnylive.admin.endpoint.term.request;

import static com.funnyland.funnylive.domain.term.QTerm.term;

import com.funnyland.funnylive.admin.endpoint.notice.request.NoticeSearchType;
import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.term.Term;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TermFilter extends ApiPageRequest {
    private TermSearchType searchType;

    private String searchValue;

    private Term.TermType type;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link NoticeSearchType#TITLE} 제목 검색
     *   term.title like '% this.title %'
     *
     * - {@link NoticeSearchType#CONTENT} 내용 검색
     *   term.content like '% this.content %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case TITLE:
            builder.and(term.title.like("%" + this.searchValue + "%"));
            break;
        case CONTENT:
            builder.and(
                    term.content.like("%" + this.searchValue + "%"));
            break;
        default:
            break;
        }
    }

    /**
     * type 조건 추가.
     * - term.type in (this.type)
     *
     * @param builder query dsl boolean builder
     * */
    private void typeBuilder(BooleanBuilder builder) {
        if(this.type != null) {
            builder.and(term.type.eq(this.type));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        typeBuilder(builder);
        searchTypeAndContentBuilder(builder);

        return builder;
    }
}
