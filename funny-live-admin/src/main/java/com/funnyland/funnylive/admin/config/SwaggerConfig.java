package com.funnyland.funnylive.admin.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.IntegerSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.StringSchema;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SwaggerConfig {

    @Value("${swagger.server.url}")
    private String swaggerServerUrl;

    @Bean
    public OpenAPI customOpenAPI() {
        // Define the security scheme for JSESSIONID cookie
        SecurityScheme apiKeyScheme = new SecurityScheme()
                .type(SecurityScheme.Type.APIKEY)
                .in(SecurityScheme.In.HEADER)
                .name("Authorization")
                .description("[token]");

        // Define the custom schema for Pageable
        Schema<?> pageableSchema = new Schema<>()
                .addProperty("page", new IntegerSchema().example(0))
                .addProperty("size", new IntegerSchema().example(20))
                .addProperty("sort", new StringSchema().example(""))
                .description("Pageable object");

        Components components = new Components()
                .addSecuritySchemes("BearerAuth", apiKeyScheme)
                .addSchemas("Pageable", pageableSchema);

        // Define the security requirement for the API
        SecurityRequirement securityRequirement = new SecurityRequirement().addList("BearerAuth");


        // Combine everything into the OpenAPI configuration
        return new OpenAPI().addServersItem(new Server().url(swaggerServerUrl))
                .components(components)
                .addSecurityItem(securityRequirement);
    }

}
