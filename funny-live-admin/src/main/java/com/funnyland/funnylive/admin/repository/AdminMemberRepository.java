package com.funnyland.funnylive.admin.repository;

import java.util.Optional;

import com.funnyland.funnylive.domain.member.AdminMember;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AdminMemberRepository extends JpaRepository<AdminMember, Long> {
    Optional<AdminMember> findByEmail(String email);
}

