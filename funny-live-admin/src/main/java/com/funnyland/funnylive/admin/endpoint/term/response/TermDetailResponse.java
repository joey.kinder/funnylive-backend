package com.funnyland.funnylive.admin.endpoint.term.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.term.Term;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class TermDetailResponse {
    private Long termId;
    private String type;
    private String title;
    private String content;
    private String updater;
    private Long viewCount;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;


    public static TermDetailResponse of(Term term) {
        return TermDetailResponse.builder()
                            .termId(term.getId())
                            .type(term.getType() != null ? term.getType().name() : null)
                            .title(term.getTitle())
                            .content(term.getContent())
                            .viewCount(term.getViewCount())
                            .createdAt(term.getCreatedAt())
                            .updater(term.getLastModifiedBy())
                            .build();
    }
}
