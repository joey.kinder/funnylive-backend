package com.funnyland.funnylive.admin.endpoint.receipt.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.InquiryDetailResponse;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.receipt.Receipt;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReceiptResponse {
    private Long receiptId;
    private MemberResponse updater;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime purchaseTime;
    private Integer paymentAmount;
    private Integer chargeAmount;
    private String stateName;
    private String paymentMethodName;

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class MemberResponse {
        private String email;
        private String name;
        private String oAuthProviderName;
        private String gradeName;
    }

    public static ReceiptResponse of(Receipt receipt) {
        Member member = receipt.getMember();
        MemberResponse memberResponse = null;
        if (member != null) {
            memberResponse = MemberResponse.builder()
                                         .email(member.getEmail())
                                         .name(member.getName())
                                         .oAuthProviderName(member.getOAuthProvider() != null ? member.getOAuthProvider().getName() : null)
                                         .gradeName(member.getGrade() != null ? member.getGrade().getName() : null)
                                         .build();
        }

        return ReceiptResponse.builder()
                              .receiptId(receipt.getId())
                              .updater(memberResponse)
                            .paymentMethodName(receipt.getPaymentMethod() != null ? receipt.getPaymentMethod().getName(): null)
                              .purchaseTime(receipt.getPurchaseTime())
                              .paymentAmount(receipt.getPaymentAmount())
                              .chargeAmount(receipt.getChargeAmount())
                              .stateName(receipt.getState() != null ? receipt.getState().getName() : null)
                              .build();
    }

}
