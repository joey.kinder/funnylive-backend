package com.funnyland.funnylive.admin.endpoint.receipt.request;

public enum ReceiptSearchType {
    NAME,
    EMAIL
}
