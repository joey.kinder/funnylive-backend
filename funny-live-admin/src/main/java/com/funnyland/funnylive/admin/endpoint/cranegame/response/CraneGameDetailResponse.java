package com.funnyland.funnylive.admin.endpoint.cranegame.response;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductSimpleResponse;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.cranegame.CraneGameProduct;
import com.funnyland.funnylive.domain.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CraneGameDetailResponse {

    private Long craneGameId;
    private String description;
    private String imageUrl;
    private String uuid;
    private String ip;
    private String state;
    private Integer playFee;
    private Integer timer;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastHeartbeatTime;
    private ProductSimpleResponse product;


    public static CraneGameDetailResponse of(CraneGame craneGame, LocalDateTime lastHeartbeatTime) {
        List<ProductSimpleResponse> products = craneGame.getCraneGameProducts()
                                          .stream()
                                          .map(CraneGameProduct::getProduct)
                                            .map(ProductSimpleResponse::of)
                                          .collect(Collectors.toList());



        return CraneGameDetailResponse.builder()
                                      .craneGameId(craneGame.getId())
                                      .uuid(craneGame.getUuid())
                                      .imageUrl(UrlUtil.convertToCdnUrl(craneGame.getImageUrl()))
                                      .ip(craneGame.getIp())
                                      .playFee(craneGame.getPlayFee())
                                      .timer(craneGame.getTimer())
                                      .state(craneGame.getGameState() != null ? craneGame.getGameState().name() : null)
                                      .description(craneGame.getDescription())
                                      .product(products.isEmpty() ? null : products.get(0))
                                     .lastHeartbeatTime(lastHeartbeatTime)
                                      .build();

    }

    public static CraneGameDetailResponse of(CraneGame craneGame) {
        List<ProductSimpleResponse> products = craneGame.getCraneGameProducts()
                .stream()
                .map(CraneGameProduct::getProduct)
                .map(ProductSimpleResponse::of)
                .collect(Collectors.toList());

        return CraneGameDetailResponse.builder()
                .craneGameId(craneGame.getId())
                .uuid(craneGame.getUuid())
                .imageUrl(UrlUtil.convertToCdnUrl(craneGame.getImageUrl()))
                .ip(craneGame.getIp())
                .playFee(craneGame.getPlayFee())
                .timer(craneGame.getTimer())
                .state(craneGame.getGameState() != null ? craneGame.getGameState().name() : null)
                .description(craneGame.getDescription())
                .product(products.isEmpty() ? null: products.get(0))
                .build();

    }
}
