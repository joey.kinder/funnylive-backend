package com.funnyland.funnylive.admin.endpoint.inquiry.request;

public enum InquirySearchType {
    TITLE,
    CONTENT,
    NAME,
    EMAIL;
}
