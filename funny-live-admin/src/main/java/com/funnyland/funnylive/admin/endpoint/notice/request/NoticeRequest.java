package com.funnyland.funnylive.admin.endpoint.notice.request;

import com.funnyland.funnylive.domain.notice.Notice;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class NoticeRequest {

    private String title;
    private String content;
    private Notice.NoticeType type;

    public Notice toDomain() {
        Notice notice = Notice.builder()
                .title(this.title)
                .content(this.content)
                .noticeType(this.type)
                .build();
        return notice;
    }

}