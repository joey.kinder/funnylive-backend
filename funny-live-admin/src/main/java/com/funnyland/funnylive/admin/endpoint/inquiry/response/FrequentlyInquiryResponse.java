package com.funnyland.funnylive.admin.endpoint.inquiry.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FrequentlyInquiryResponse {

    private Long frequentlyInquiryId;
    private String typeName;
    private String title;
    private String updater;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime modifiedAt;
    private Long viewCount;

    public static FrequentlyInquiryResponse of(FrequentlyInquiry frequentlyInquiry) {
        return FrequentlyInquiryResponse.builder()
                .frequentlyInquiryId(frequentlyInquiry.getId())
                .typeName(frequentlyInquiry.getType() != null? frequentlyInquiry.getType().getName() : null)
                .title(frequentlyInquiry.getQuestion())
                .updater(frequentlyInquiry.getLastModifiedBy())
                .createdAt(frequentlyInquiry.getCreatedAt())
                .modifiedAt(frequentlyInquiry.getModifiedAt())
                .viewCount(frequentlyInquiry.getViewCount())
                .build();

    }
}
