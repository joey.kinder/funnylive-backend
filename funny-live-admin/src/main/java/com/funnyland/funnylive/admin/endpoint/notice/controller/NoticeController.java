package com.funnyland.funnylive.admin.endpoint.notice.controller;

import com.funnyland.funnylive.admin.endpoint.notice.request.NoticeFilter;
import com.funnyland.funnylive.admin.endpoint.notice.request.NoticeRequest;
import com.funnyland.funnylive.admin.endpoint.notice.response.NoticeDetailResponse;
import com.funnyland.funnylive.admin.endpoint.notice.response.NoticeResponse;
import com.funnyland.funnylive.admin.service.NoticeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="공지사항", description = "공지사항 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/notices")
public class NoticeController {

    private final NoticeService noticeService;

    @GetMapping
    @Operation(summary = "공지사항 리스트 조회", description = "공지사항 리스트 조회")
    public Page<NoticeResponse> getAll(NoticeFilter filter) {
        return noticeService.getAll(filter);
    }

    @GetMapping("/{noticeId}")
    @Operation(summary = "공지사항 상세 조회", description = "공지사항 상세 조회")
    public NoticeDetailResponse get(@PathVariable Long noticeId) {
        return noticeService.get(noticeId);
    }

    @PostMapping
    @Operation(summary = "공지사항 추가", description = "공지사항 추가")
    public NoticeDetailResponse create(@RequestBody NoticeRequest request) {
        return noticeService.create(request);
    }

    @PutMapping("/{noticeId}")
    @Operation(summary = "공지사항 수정", description = "공지사항 수정")
    public NoticeDetailResponse update(@PathVariable Long noticeId, @RequestBody NoticeRequest request) {
        return noticeService.update(noticeId, request);
    }


}
