package com.funnyland.funnylive.admin.service;

import java.time.LocalDate;
import java.util.List;

import com.funnyland.funnylive.domain.member.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class MemberVisitService {

    private final StringRedisTemplate stringRedisTemplate;
    private final MemberService memberService;


    @Transactional
    public void writeMemberVisit() {
        List<Member> visitedMembers = memberService.getVisitedMembers();
        String key = "member_visits:" + LocalDate.now();

        // 회원 ID를 String 배열로 변환
        String[] memberIds = visitedMembers.stream()
                .map(member -> String.valueOf(member.getId()))
                .toArray(String[]::new);

        // Redis Set에 모든 회원 ID 추가
        stringRedisTemplate.opsForSet().add(key, memberIds);
    }
}
