package com.funnyland.funnylive.admin.endpoint.product.request;

import com.funnyland.funnylive.domain.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {
    private String name;
    private Integer price;
    private Product.ProductStatus state;

    public Product toDomain() {
        Product product = Product.builder()
                .name(this.name)
                .price(this.price)
                .status(this.state)
                .build();
        return product;
    }

}