package com.funnyland.funnylive.admin.config;

import com.funnyland.funnylive.admin.service.S3StorageService;
import com.funnyland.funnylive.admin.service.StorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;


@Configuration
public class StorageConfig {

    @Value("${upload.image-path}")
    private String localImageUploadPath;

    @Value("${aws.s3.bucket-name}")
    private String s3BucketName;

    @Value("${aws.s3.profile}")
    private String s3Profile;

    @Value("${aws.access-key}")
    private String accessKey;

    @Value("${aws.secret-key}")
    private String secretKey;

    @Bean
    public StorageService s3ImageStorageService() {
        S3Client s3Client = S3Client.builder().region(Region.AP_NORTHEAST_2)
                .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKey, secretKey))).build();
        return new S3StorageService(s3Client, s3BucketName, s3Profile);
    }

}
