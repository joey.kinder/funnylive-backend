package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.member.QAdminMember.adminMember;

import java.util.List;

import com.funnyland.funnylive.domain.member.AdminMember;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class AdminMemberReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public AdminMemberReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(AdminMember.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<AdminMember> getAdminMembers(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<AdminMember> query = jpaQueryFactory.selectFrom(adminMember)
                              .where(builder);

        List<AdminMember> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(adminMember.count())
                .from(adminMember)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }
}

