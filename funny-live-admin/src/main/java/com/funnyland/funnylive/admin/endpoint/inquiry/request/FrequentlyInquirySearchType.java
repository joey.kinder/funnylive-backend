package com.funnyland.funnylive.admin.endpoint.inquiry.request;

public enum FrequentlyInquirySearchType {
    TITLE,
    CONTENT,
    UPDATER;
}
