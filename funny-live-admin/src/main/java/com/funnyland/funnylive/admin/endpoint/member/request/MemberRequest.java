package com.funnyland.funnylive.admin.endpoint.member.request;

import com.funnyland.funnylive.domain.member.Member;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MemberRequest {

    private Member.MemberStatus status;

    public Member toDomain() {
        return Member.builder()
                .status(this.status)
                .build();
    }
}
