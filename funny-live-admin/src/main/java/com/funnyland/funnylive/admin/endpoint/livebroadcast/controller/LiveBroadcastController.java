package com.funnyland.funnylive.admin.endpoint.livebroadcast.controller;

import com.funnyland.funnylive.admin.endpoint.livebroadcast.request.LiveBroadcastFilter;
import com.funnyland.funnylive.admin.endpoint.livebroadcast.request.LiveBroadcastRequest;
import com.funnyland.funnylive.admin.endpoint.livebroadcast.response.LiveBroadcastDetailResponse;
import com.funnyland.funnylive.admin.endpoint.livebroadcast.response.LiveBroadcastResponse;
import com.funnyland.funnylive.admin.endpoint.notice.request.NoticeFilter;
import com.funnyland.funnylive.admin.endpoint.notice.response.NoticeResponse;
import com.funnyland.funnylive.admin.service.LiveBroadcastService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="라이브방송", description = "라이브방송 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/live-broadcast")
public class LiveBroadcastController {

    private final LiveBroadcastService liveBroadcastService;

    @GetMapping
    @Operation(summary = "라이브방송 리스트 조회", description = "라이브방송 리스트 조회")
    public Page<LiveBroadcastResponse> getAll(LiveBroadcastFilter filter) {
        return liveBroadcastService.getAll(filter);
    }

    @GetMapping("/{liveBroadcastId}")
    @Operation(summary = "라이브방송 리스트 조회", description = "라이브방송 리스트 조회")
    public LiveBroadcastDetailResponse get(@PathVariable Long liveBroadcastId) {
        return liveBroadcastService.get(liveBroadcastId);
    }

    @PutMapping("/{liveBroadcastId}")
    @Operation(summary = "라이브방송 수정", description = "라이브방송 수정")
    public LiveBroadcastDetailResponse update(@PathVariable Long liveBroadcastId, @RequestBody LiveBroadcastRequest request) {
        return liveBroadcastService.update(liveBroadcastId, request);
    }

    @PostMapping
    @Operation(summary = "라이브방송 추가", description = "라이브방송 추가")
    public LiveBroadcastDetailResponse create(@RequestBody LiveBroadcastRequest request) {
        return liveBroadcastService.create(request);
    }

}
