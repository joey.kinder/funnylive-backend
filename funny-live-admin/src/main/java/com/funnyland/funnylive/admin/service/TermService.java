package com.funnyland.funnylive.admin.service;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.notice.response.NoticeResponse;
import com.funnyland.funnylive.admin.endpoint.term.request.TermFilter;
import com.funnyland.funnylive.admin.endpoint.term.request.TermRequest;
import com.funnyland.funnylive.admin.endpoint.term.response.TermDetailResponse;
import com.funnyland.funnylive.admin.endpoint.term.response.TermResponse;
import com.funnyland.funnylive.admin.repository.TermReadRepository;
import com.funnyland.funnylive.domain.notice.Notice;
import com.funnyland.funnylive.domain.term.Term;
import com.funnyland.funnylive.domain.term.TermDomainService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class TermService {

    private final TermReadRepository termReadRepository;
    private final TermDomainService termDomainService;

    public Page<TermResponse> getAll(TermFilter filter) {
        Page<Term> terms = termReadRepository.getTerms(filter.generateBooleanBuilder(), filter.getPageRequest());
        return terms.map(TermResponse::of);
    }

    public TermDetailResponse get(Long termId) {
        Term findTerm = termReadRepository.findById(termId)
                                                .orElseThrow(() -> new DomainException("없는 약관입니다"));
        return TermDetailResponse.of(findTerm);
    }

    public TermDetailResponse create(TermRequest request) {
        Term savedTerm = termDomainService.save(request.toDomain());
        return TermDetailResponse.of(savedTerm);
    }

    @Transactional
    public TermDetailResponse update(Long termId, TermRequest request) {
        Term updateTerm = termDomainService.update(termId, request.toDomain());
        return TermDetailResponse.of(updateTerm);
    }
}
