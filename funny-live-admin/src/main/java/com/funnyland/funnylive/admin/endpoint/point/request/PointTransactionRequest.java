package com.funnyland.funnylive.admin.endpoint.point.request;

import java.time.LocalDate;
import java.util.List;

import com.funnyland.funnylive.domain.point.PointTransactionType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PointTransactionRequest {
    private List<PointTransactionDetail> transactions;

    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PointTransactionDetail {
        private Long memberId;
        private PointTransactionType transactionType;
        private String reason;
        private Integer amount;
        private LocalDate expirationStartDate;
        private LocalDate expirationEndDate;
    }
}
