package com.funnyland.funnylive.admin.endpoint.inquiry.request;

import static com.funnyland.funnylive.domain.inquiry.QInquiry.inquiry;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.funnyland.funnylive.admin.endpoint.member.request.SearchType;
import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryReply;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import com.funnyland.funnylive.domain.member.Member;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InquiryFilter extends ApiPageRequest {

    private InquirySearchType searchType;

    private String searchValue;

    private InquiryType type;

    private Inquiry.InquiryStatus state;

    private Long inquiryTypeId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchStartDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchEndDate;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link InquirySearchType#TITLE} 타이틀 검색
     *   inquiry.title like '% this.title %'
     *
     * - {@link InquirySearchType#CONTENT} 내용 검색
     *   inquiry.content like '% this.content %'
     *
     * - {@link InquirySearchType#NAME} 이름 검색
     *   inquiry.name like '% this.name %'
     *
     * - {@link InquirySearchType#EMAIL} 이메일 검색
     *   inquiry.email like '% this.email %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case TITLE:
            builder.and(inquiry.title.like("%" + this.searchValue + "%"));
            break;
        case CONTENT:
            builder.and(
                    inquiry.content.like("%" + this.searchValue + "%"));
            break;
        case NAME:
            builder.and(
                    inquiry.member.name.like("%" + this.searchValue + "%"));
            break;
        case EMAIL:
            builder.and(
                    inquiry.member.email.like("%" + this.searchValue + "%"));
            break;
        default:
            break;
        }
    }

    /**
     * 타입 조건 추가.
     * - inquiry.type in (this.type)
     *
     * @param builder query dsl boolean builder
     * */
    private void typeBuilder(BooleanBuilder builder) {
        if(this.type != null) {
            builder.and(inquiry.type.eq(this.type));
        }
    }

    /**
     * 문의 타입 아이디 조건 추가.
     * - inquiry.type.id in (this.inquiryTypeId)
     *
     * @param builder query dsl boolean builder
     * */
    private void typeIdBuilder(BooleanBuilder builder) {
        if(this.inquiryTypeId != null) {
            builder.and(inquiry.type.id.eq(this.inquiryTypeId));
        }
    }

    /**
     * 답변상태 조건 추가.
     * - inquiry.status in (this.status)
     *
     * @param builder query dsl boolean builder
     * */
    private void stateBuilder(BooleanBuilder builder) {
        if(this.state != null) {
            builder.and(inquiry.status.eq(this.state));
        }
    }

    /**
     * 검색 시작일 조건 추가.
     * - created_at > this.searchStartDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchStartDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchStartDate)) {
            LocalDateTime startDateTime = this.searchStartDate.atStartOfDay();
            builder.and(inquiry.createdAt.gt(startDateTime));
        }
    }

    /**
     * 검색 종료일 조건 추가.
     * - created_at < this.searchEndDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchEndDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchEndDate)) {
            LocalDateTime endDateTime = this.searchEndDate.atTime(23, 59, 59);
            builder.and(inquiry.createdAt.lt(endDateTime));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        typeBuilder(builder);
        typeIdBuilder(builder);
        stateBuilder(builder);
        searchStartDateBuilder(builder);
        searchEndDateBuilder(builder);
        searchTypeAndContentBuilder(builder);

        return builder;
    }
}
