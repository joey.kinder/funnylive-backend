package com.funnyland.funnylive.admin.endpoint.main.response;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.admin.endpoint.delivery.response.DeliveryResponse;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.receipt.Receipt;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MainResponse {

    private List<MemberResponse> members;
    private List<MemberResponse> newMembers;
    private List<ReceiptResponse> newReceipts;
    private Map<CraneGame.CraneGameState, Long> craneGameState;
    private InquiryResponse inquiry;
    private MemberVisitStatisticsResponse memberVisitStatistics;
    private List<DeliveryResponse> deliveries;


    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MemberVisitStatisticsResponse {
        private long todayVisitCount;
        private int averageDailyVisitCount;
        private long totalVisitCount;

        public static MemberVisitStatisticsResponse of(long todayVisitCount, int averageDailyVisitCount, long totalVisitCount) {
            return MemberVisitStatisticsResponse.builder()
                    .todayVisitCount(todayVisitCount)
                    .averageDailyVisitCount(averageDailyVisitCount)
                    .totalVisitCount(totalVisitCount)
                    .build();
        }
    }

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class InquiryResponse {
        private long todayCount;
        private long pendingCount;
        private long answeredCount;

        public static InquiryResponse of(long todayInquiriesCount, long pendingInquiriesCount, long answeredInquiriesCount) {
            return InquiryResponse.builder()
                           .todayCount(todayInquiriesCount)
                           .pendingCount(pendingInquiriesCount)
                           .answeredCount(answeredInquiriesCount)
                           .build();
        }
    }

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class DeliveryResponse {
        private Long deliveryId;
        private String orderNumber;
        private String recipientName;
        private String memberName;
        private String productName;
        private String stateName;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime createdAt;

        public static DeliveryResponse of(Delivery delivery) {
            String productName = null;
            if (delivery.getItems() != null && !delivery.getItems().isEmpty()) {
                String firstProductName = delivery.getItems()
                        .get(0)
                        .getProductName();
                int additionalItemsCount = delivery.getItems()
                        .size() - 1;

                if (additionalItemsCount > 0) {
                    productName = firstProductName + " 외 " + additionalItemsCount + "개";
                } else {
                    productName =  firstProductName;
                }
            }

            return DeliveryResponse.builder()
                    .deliveryId(delivery.getId())
                    .orderNumber(delivery.getOrderNumber())
                    .recipientName(delivery.getRecipientName())
                    .memberName(delivery.getMember().getName())
                    .productName(productName)
                    .stateName(delivery.getStatus() != null ? delivery.getStatus().getName(): null)
                    .createdAt(delivery.getCreatedAt())
                    .build();
        }
    }

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class ReceiptResponse {
        private Long receiptId;
        private MemberResponse member;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime purchaseTime;
        private Integer paymentAmount;
        private Integer bonusAmount;

        public static ReceiptResponse of(Receipt receipt) {
            Member member = receipt.getMember();
            MemberResponse memberResponse = null;
            if (member != null) {
                memberResponse = MemberResponse.builder()
                        .email(member.getEmail())
                        .oAuthProviderName(member.getOAuthProvider() != null ? member.getOAuthProvider().getName() : null)
                        .gradeName(member.getGrade() != null ? member.getGrade().getName() : null)
                        .nickName(member.getNickName())
                        .visitedTime(member.getVisitedTime())
                        .createdAt(member.getCreatedAt())
                        .phone(member.getPhone())
                        .build();
            }

            return ReceiptResponse
                    .builder()
                    .receiptId(receipt.getId())
                    .member(memberResponse)
                    .purchaseTime(receipt.getPurchaseTime())
                    .paymentAmount(receipt.getPaymentAmount())
                    .bonusAmount(receipt.getChargeAmount() - receipt.getPaymentAmount())
                    .build();
        }
    }

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class MemberResponse {
        private Long memberId;
        private String email;
        private String name;
        private String oAuthProviderName;
        private String gradeName;
        private String nickName;
        private String phone;
        @JsonFormat(pattern = "yyyy-MM-dd")
        private LocalDateTime createdAt;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime visitedTime;


        public static MemberResponse of(Member member) {
            return MemberResponse
                    .builder()
                    .memberId(member.getId())
                    .name(member.getName())
                    .email(member.getEmail())
                    .oAuthProviderName(member.getOAuthProvider() != null ? member.getOAuthProvider().getName() : null)
                    .gradeName(member.getGrade() != null ? member.getGrade().getName() : null)
                    .nickName(member.getNickName())
                    .visitedTime(member.getVisitedTime())
                    .createdAt(member.getCreatedAt())
                    .phone(member.getPhone())
                    .build();
        }
    }

    public static MainResponse of(List<Member> members, List<Member> newMembers, List<Receipt> newReceipts, Map<CraneGame.CraneGameState, Long> craneGameStateMap, List<Delivery> deliveries, long todayInquiries,
                                  long pendingInquiries, long answeredInquiries, long todayVisitCount, int averageDailyVisitCount, long totalVisitCount) {
        List<MemberResponse> memberResponses = new ArrayList<>();
        if (!members.isEmpty()) {
            memberResponses = members.stream()
                    .map(MemberResponse::of)
                    .collect(Collectors.toList());
        }

        List<MemberResponse> newMemberResponses = new ArrayList<>();
        if (!newMembers.isEmpty()) {
            newMemberResponses = newMembers.stream()
                    .map(MemberResponse::of)
                    .collect(Collectors.toList());
        }

        List<ReceiptResponse> newReceiptResponses = new ArrayList<>();
        if (!newReceipts.isEmpty()) {
            newReceiptResponses = newReceipts.stream()
                    .map(ReceiptResponse::of)
                    .collect(Collectors.toList());
        }

        List<DeliveryResponse> deliveryResponses = new ArrayList<>();
        if (!deliveries.isEmpty()) {
            deliveryResponses = deliveries.stream()
                    .map(DeliveryResponse::of)
                    .collect(Collectors.toList());
        }

        InquiryResponse inquiryResponse = InquiryResponse.of(todayInquiries, pendingInquiries, answeredInquiries);
        MemberVisitStatisticsResponse memberVisitStatisticsResponse = MemberVisitStatisticsResponse.of(todayVisitCount, averageDailyVisitCount, totalVisitCount);

        return MainResponse.builder()
                .members(memberResponses)
                .newMembers(newMemberResponses)
                .newReceipts(newReceiptResponses)
                .craneGameState(craneGameStateMap)
               .inquiry(inquiryResponse)
               .deliveries(deliveryResponses)
               .memberVisitStatistics(memberVisitStatisticsResponse)
                .build();

    }
}
