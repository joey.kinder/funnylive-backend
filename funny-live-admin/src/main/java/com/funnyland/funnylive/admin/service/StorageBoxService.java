package com.funnyland.funnylive.admin.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.notice.response.NoticeDetailResponse;
import com.funnyland.funnylive.admin.endpoint.product.request.ProductFilter;
import com.funnyland.funnylive.admin.endpoint.product.request.ProductRequest;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductDetailResponse;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductResponse;
import com.funnyland.funnylive.admin.endpoint.product.response.ProductSimpleResponse;
import com.funnyland.funnylive.admin.endpoint.storagebox.request.StorageBoxFilter;
import com.funnyland.funnylive.admin.endpoint.storagebox.response.StorageBoxDetailResponse;
import com.funnyland.funnylive.admin.endpoint.storagebox.response.StorageBoxResponse;
import com.funnyland.funnylive.admin.repository.ProductReadRepository;
import com.funnyland.funnylive.admin.repository.StorageBoxHistoryReadRepository;
import com.funnyland.funnylive.admin.repository.StorageBoxReadRepository;
import com.funnyland.funnylive.admin.service.dto.StorageType;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import com.funnyland.funnylive.domain.notice.Notice;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.product.ProductDomainService;
import com.funnyland.funnylive.domain.storagebox.StorageBox;
import com.funnyland.funnylive.domain.storagebox.StorageBoxHistory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
public class StorageBoxService {

    private final StorageBoxHistoryReadRepository storageBoxHistoryReadRepository;
    private final StorageBoxReadRepository storageBoxReadRepository;


    public Page<StorageBoxResponse> getAll(StorageBoxFilter filter) {
        Page<StorageBoxHistory> findStorageBoxes = storageBoxHistoryReadRepository.getStorageBoxes(filter.generateBooleanBuilder(), filter.getPageRequest());
        return findStorageBoxes.map(StorageBoxResponse::of);
    }

    public StorageBoxDetailResponse get(Long storageBoxHistoryId) {
        StorageBoxHistory findStorageBoxHistory = storageBoxHistoryReadRepository.findById(storageBoxHistoryId).orElseThrow(() -> new DomainException("없는 보관함 입니다"));
        return StorageBoxDetailResponse.of(findStorageBoxHistory);

    }
}
