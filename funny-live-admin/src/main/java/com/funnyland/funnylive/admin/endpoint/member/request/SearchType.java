package com.funnyland.funnylive.admin.endpoint.member.request;

public enum SearchType {
    EMAIL,
    NAME,
    NICKNAME,
    PHONE;
}
