package com.funnyland.funnylive.admin.service;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.livebroadcast.request.LiveBroadcastFilter;
import com.funnyland.funnylive.admin.endpoint.livebroadcast.request.LiveBroadcastRequest;
import com.funnyland.funnylive.admin.endpoint.livebroadcast.response.LiveBroadcastDetailResponse;
import com.funnyland.funnylive.admin.endpoint.livebroadcast.response.LiveBroadcastResponse;
import com.funnyland.funnylive.admin.endpoint.notice.response.NoticeDetailResponse;
import com.funnyland.funnylive.admin.repository.LiveBroadcastReadRepository;
import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcast;
import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcastDomainService;
import com.funnyland.funnylive.domain.notice.Notice;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class LiveBroadcastService {

    private final LiveBroadcastReadRepository liveBroadcastReadRepository;
    private final LiveBroadcastDomainService liveBroadcastDomainService;

    public Page<LiveBroadcastResponse> getAll(LiveBroadcastFilter filter) {
        Page<LiveBroadcast> findLiveBroadcasts = liveBroadcastReadRepository.getLiveBroadcasts(filter.generateBooleanBuilder(), filter.getPageRequest());
        return findLiveBroadcasts.map(LiveBroadcastResponse::of);

    }

    public LiveBroadcastDetailResponse get(Long liveBroadcastId) {
        LiveBroadcast findLiveBroadcast = liveBroadcastReadRepository.findById(liveBroadcastId)
                .orElseThrow(() -> new DomainException("없는 라이브 방송입니다"));
        return LiveBroadcastDetailResponse.of(findLiveBroadcast);
    }

    @Transactional
    public LiveBroadcastDetailResponse update(Long liveBroadcastId, LiveBroadcastRequest request) {
        LiveBroadcast updateLiveBroadcast = liveBroadcastDomainService.update(liveBroadcastId, request.toDomain());
        return LiveBroadcastDetailResponse.of(updateLiveBroadcast);
    }

    public LiveBroadcastDetailResponse create(LiveBroadcastRequest request) {
        LiveBroadcast savedLiveBroadcast = liveBroadcastDomainService.save(request.toDomain());
        return LiveBroadcastDetailResponse.of(savedLiveBroadcast);
    }
}
