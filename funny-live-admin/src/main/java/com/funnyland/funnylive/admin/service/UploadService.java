package com.funnyland.funnylive.admin.service;

import java.util.Optional;

import com.funnyland.funnylive.admin.service.dto.StorageType;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
@Slf4j
public class UploadService {

    private final StorageService storageService;

    public Optional<UploadDto> upload(MultipartFile file, StorageType type) {
        log.info("upload() file : {}, type: {}", file.getName(), type.name());
        if (!ObjectUtils.isEmpty(file)) {
            // 파일 크기 제한 검사 (30MB)
            if (file.getSize() > 10 * 1024 * 1024) {
                throw new RuntimeException("File size exceeds the limit (30MB)");
            }
            UploadDto upload = storageService.upload(file, type);
            UploadDto uploadDto = upload;
            log.info("upload() success file : {}, type: {}", file.getName(), type.name());
            return Optional.of(uploadDto);
        }
        return Optional.empty();
    }

}
