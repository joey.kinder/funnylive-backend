package com.funnyland.funnylive.admin.endpoint.point.response;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PointTransactionResponse {
    private Long pointTransactionId;
    private MemberResponse updater;
    private Integer totalAmount;
    private String transactionTypeName;
    private PointTransactionType.TransactionChange change;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate expirationStartDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate expirationEndDate;

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MemberResponse {
        private Long id;
        private String email;
        private String name;
        private String oAuthProviderName;
        private String gradeName;
    }

    public static PointTransactionResponse of(PointTransaction pointTransaction) {
        Member member = pointTransaction.getMember();
        MemberResponse memberResponse = null;
        if (member != null) {
            memberResponse = MemberResponse.builder()
                    .id(member.getId())
                    .email(member.getEmail())
                    .name(member.getName())
                    .oAuthProviderName(member.getOAuthProvider() != null ? member.getOAuthProvider().getName() : null)
                    .gradeName(member.getGrade() != null ? member.getGrade().getName() : null)
                    .build();
        }

        int bonusAmount = pointTransaction.getBonusAmount() != null ? pointTransaction.getBonusAmount() : 0;
        int totalAmount = pointTransaction.getAmount() + bonusAmount;

        return PointTransactionResponse.builder()
                .pointTransactionId(pointTransaction.getId())
                .updater(memberResponse)
                .totalAmount(totalAmount)
                .transactionTypeName(pointTransaction.getTransactionType() != null ? pointTransaction.getTransactionType().getDescription() : null)
                .change(pointTransaction.getTransactionType() != null ? pointTransaction.getTransactionType()
                        .getChange() : null)
                .createdAt(pointTransaction.getCreatedAt())
                .expirationStartDate(pointTransaction.getExpirationStartDate())
                .expirationEndDate(pointTransaction.getExpirationEndDate())
                .build();
    }

}
