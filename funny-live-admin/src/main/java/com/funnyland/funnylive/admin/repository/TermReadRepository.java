package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.term.QTerm.term;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.notice.Notice;
import com.funnyland.funnylive.domain.term.Term;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class TermReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public TermReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(Term.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<Term> getTerms(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<Term> query = jpaQueryFactory.selectFrom(term)
                                              .where(builder);

        List<Term> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(term.count())
                .from(term)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Optional<Term> findById(Long noticeId) {
        JPAQuery<Term> query = jpaQueryFactory.selectFrom(term)
                .where(term.id.eq(noticeId));

        return Optional.ofNullable(query.fetchOne());
    }
}
