package com.funnyland.funnylive.admin.endpoint.delivery.response;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.delivery.DeliveryItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryDetailResponse {

    private Long deliveryId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;
    private String orderNumber;
    private String productName;
    private Long memberId;
    private String memberName;
    private String memberEmail;
    private String recipientName;
    private String stateName;
    private String state;

    //상세
    private List<DeliveryItemResponse> items;
    private int deliveryFee;
    private String recipientPhone;
    private String roadNameAddress;
    private String inputDetail;
    private String postalCode;
    private String requirement;

    private String invoiceCompanyName;
    private String invoiceNumber;

    private String memo;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedAt;

    public static DeliveryDetailResponse of(Delivery delivery) {
        String displayProductName = null;

        if (delivery.getItems() != null && !delivery.getItems().isEmpty()) {
            String firstProductName = delivery.getItems()
                                              .get(0)
                                              .getProductName();
            int additionalItemsCount = delivery.getItems()
                                               .size() - 1;

            if (additionalItemsCount > 0) {
                displayProductName = firstProductName + " 외 " + additionalItemsCount + "개";
            } else {
                displayProductName =  firstProductName;
            }
        }

        Map<Long, Integer> groupedItems = delivery.getItems().stream()
                                                  .collect(Collectors.groupingBy(
                                                          DeliveryItem::getProductId,
                                                          Collectors.summingInt(DeliveryItem::getQuantity)
                                                  ));

        // 각 productId에 대한 첫 번째 상품명을 가져오기
        Map<Long, String> productNames = delivery.getItems().stream()
                                                 .collect(Collectors.toMap(
                                                         DeliveryItem::getProductId,
                                                         DeliveryItem::getProductName,
                                                         (existing, replacement) -> existing
                                                 ));

        List<DeliveryItemResponse> itemResponses = new ArrayList<>();
        for (Map.Entry<Long, Integer> entry : groupedItems.entrySet()) {
            Long productId = entry.getKey();
            Integer quantity = entry.getValue();
            String productName = productNames.get(productId);

            itemResponses.add(DeliveryItemResponse.builder()
                                                  .productName(productName)
                                                  .quantity(quantity)
                                                  .build());
        }

        return DeliveryDetailResponse.builder()
                                     .deliveryId(delivery.getId())
                                     .createdAt(delivery.getCreatedAt())
                                     .orderNumber(delivery.getOrderNumber())
                                     .productName(displayProductName)
                                     .memberId(delivery.getMember().getId())
                                     .memberName(delivery.getMember().getName())
                                     .memberEmail(delivery.getMember().getEmail())
                                     .recipientName(delivery.getRecipientName())
                                     .stateName(delivery.getStatus() != null? delivery.getStatus().getName() : null)
                                     .state(delivery.getStatus() != null? delivery.getStatus().name() : null)
                .deliveryFee(delivery.getDeliveryFee())
                .recipientPhone(delivery.getRecipientPhone())
                .roadNameAddress(delivery.getRoadNameAddress())
                .inputDetail(delivery.getInputDetail())
                .postalCode(delivery.getPostalCode())
                .requirement(delivery.getRequirement())
                .invoiceCompanyName(delivery.getInvoiceCompanyName())
                .invoiceNumber(delivery.getInvoiceNumber())
                .memo(delivery.getMemo())
                .updatedAt(delivery.getModifiedAt())
                                     .items(itemResponses)
                                     .build();

    }

    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class DeliveryItemResponse {
        private String productName;
        private int quantity;
    }
}
