package com.funnyland.funnylive.admin.config;

import static com.funnyland.funnylive.common.exception.BaseResponseStatus.EXPIRED_JWT;
import static com.funnyland.funnylive.common.exception.BaseResponseStatus.INVALID_JWT;
import static com.funnyland.funnylive.common.exception.BaseResponseStatus.NOT_EXIST_REFRESH_JWT;

import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.crypto.SecretKey;

import com.funnyland.funnylive.common.exception.BaseException;
import com.funnyland.funnylive.domain.member.AdminMember;
import com.funnyland.funnylive.domain.member.Member;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class JwtTokenProvider {
    private SecretKey secretKey;
    private long validityInMilliseconds = 3600000; // 1시간

    @PostConstruct
    protected void init() {
        byte[] keyBytes = Base64.getDecoder()
                .decode("secretsecretsecretsecretsecretsecretsecretse");
        this.secretKey = Keys.hmacShaKeyFor(keyBytes);
    }

    public String createToken(AdminMember adminMember) {
        Claims claims = Jwts.claims().setSubject(adminMember.getEmail());
        claims.put("adminMemberId", adminMember.getId());
        claims.put("email", adminMember.getEmail());
        claims.put("name", adminMember.getName());
        claims.put("roles", Collections.singletonList("ROLE_" + adminMember.getRole().name()));

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(secretKey, SignatureAlgorithm.HS256)
                .compact();
    }

    public String createToken(String username, List<String> roles) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("roles", roles);

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(secretKey, SignatureAlgorithm.HS256)
                .compact();
    }

    public boolean validateToken(String token){
        try{
            Jwts.parserBuilder()
                    .setSigningKey(this.secretKey)// 비밀키를 설정하여 파싱한다.
                    .build()
                    .parseClaimsJws(token); // 주어진 토큰을 파싱하여 Claims 객체를 얻는다.
            return true;
        } catch(ExpiredJwtException e) {
            log.error(EXPIRED_JWT.getMessage());
            throw e;
        } catch(JwtException e) {
            log.error(INVALID_JWT.getMessage());
            throw new BaseException(INVALID_JWT);
        }
    }

    public String getUsername(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .build()
                .parseClaimsJws(token).getBody().getSubject();
    }

    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(secretKey).build()
                .parseClaimsJws(token).getBody();

        String username = claims.getSubject();
        List<String> roles = claims.get("roles", List.class);

        List<SimpleGrantedAuthority> authorities = roles.stream()
                .map(role -> new SimpleGrantedAuthority(role))
                .collect(Collectors.toList());

        User principal = new User(username, "", authorities);

        return new UsernamePasswordAuthenticationToken(principal, "", authorities);
    }
}
