package com.funnyland.funnylive.admin.endpoint.cranegame.response;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.cranegame.CraneGameProduct;
import com.funnyland.funnylive.domain.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CraneGameResponse {

    private Long craneGameId;
    private String imageUrl;
    private String uuid;
    private String gameStateName;
    private Integer playFee;
    private Integer timer;
    private String productName;
    private String communicationStatus; // 통신 상태

    public static CraneGameResponse of(CraneGame craneGame, LocalDateTime lastHeartbeatTime) {
        List<Product> products = craneGame.getCraneGameProducts()
                                          .stream()
                                          .map(CraneGameProduct::getProduct)
                                          .collect(Collectors.toList());

        String communicationStatus = determineCommunicationStatus(lastHeartbeatTime);



        return CraneGameResponse.builder()
                         .craneGameId(craneGame.getId())
                         .uuid(craneGame.getUuid())
                         .imageUrl(UrlUtil.convertToCdnUrl(craneGame.getImageUrl()))
                         .playFee(craneGame.getPlayFee())
                         .timer(craneGame.getTimer())
                         .gameStateName(craneGame.getGameState().getName())
                         .productName(products.isEmpty()?null:products.get(0).getName())
                .communicationStatus(communicationStatus)
                         .build();

    }

    private static String determineCommunicationStatus(LocalDateTime lastHeartbeatTime) {
        if (lastHeartbeatTime == null) {
            return "끊김"; // 통신 정보가 없는 경우
        }

        LocalDateTime currentTime = LocalDateTime.now();
        Duration duration = Duration.between(lastHeartbeatTime, currentTime);
        return duration.getSeconds() > 10 ? "끊김" : "연동중";
    }
}
