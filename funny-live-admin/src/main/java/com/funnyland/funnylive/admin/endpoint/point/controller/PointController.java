package com.funnyland.funnylive.admin.endpoint.point.controller;

import java.util.List;

import com.funnyland.funnylive.admin.endpoint.point.request.PointAdjustmentRequest;
import com.funnyland.funnylive.admin.endpoint.point.request.PointFilter;
import com.funnyland.funnylive.admin.endpoint.point.request.PointTransactionRequest;
import com.funnyland.funnylive.admin.endpoint.point.response.PointTransactionPreviewResponse;
import com.funnyland.funnylive.admin.endpoint.point.response.PointTransactionResponse;
import com.funnyland.funnylive.admin.service.PointService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="포인트", description = "포인트 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/points")
public class PointController {

    private final PointService pointService;

    @GetMapping
    @Operation(summary = "포인트 리스트 조회", description = "포인트 리스트 조회")
    public Page<PointTransactionResponse> getPoints(PointFilter filter) {
        return pointService.getAll(filter);
    }


    @PostMapping
    @Operation(summary = "포인트 지급/차감 목록 추가", description = "포인트 지급/차감 목록 추가")
    public List<PointTransactionPreviewResponse> previewPoints(@RequestBody PointAdjustmentRequest request) {
        return pointService.preview(request);
    }

    @PostMapping("/apply")
    @Operation(summary = "포인트 적용", description = "포인트 적용")
    public void applyPoints(@RequestBody PointTransactionRequest request) {
        pointService.apply(request.getTransactions());
    }

}
