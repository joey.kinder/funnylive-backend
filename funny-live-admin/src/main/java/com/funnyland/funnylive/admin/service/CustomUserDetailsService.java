package com.funnyland.funnylive.admin.service;

import java.util.Collection;
import java.util.Collections;

import com.funnyland.funnylive.admin.repository.AdminMemberRepository;
import com.funnyland.funnylive.domain.member.AdminMember;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final AdminMemberRepository adminMemberRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        AdminMember adminMember = adminMemberRepository.findByEmail(email)
                                                       .orElseThrow(() -> new UsernameNotFoundException("User not found with email: " + email));

        return new User(adminMember.getEmail(), adminMember.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + adminMember.getRole().name())));
    }

}
