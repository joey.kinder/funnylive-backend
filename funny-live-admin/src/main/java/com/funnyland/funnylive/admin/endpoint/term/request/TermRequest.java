package com.funnyland.funnylive.admin.endpoint.term.request;

import com.funnyland.funnylive.domain.term.Term;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TermRequest {
    private String title;
    private String content;
    private Term.TermType type;

    public Term toDomain() {
        Term term = Term.builder()
                        .title(this.title)
                        .content(this.content)
                        .type(this.type)
                        .build();
        return term;
    }
}
