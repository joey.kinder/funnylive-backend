package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.cart.QCartItem.cartItem;
import static com.funnyland.funnylive.domain.delivery.QDelivery.delivery;
import static com.funnyland.funnylive.domain.inquiry.QInquiry.inquiry;
import static com.funnyland.funnylive.domain.member.QMember.member;
import static com.funnyland.funnylive.domain.point.QPointTransaction.pointTransaction;
import static com.funnyland.funnylive.domain.receipt.QReceipt.receipt;
import static com.funnyland.funnylive.domain.storagebox.QStorageBox.storageBox;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.receipt.Receipt;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class MemberReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public MemberReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(Member.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<Member> getMembers(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<Member> query = jpaQueryFactory.selectFrom(member)
                              .where(builder);

        List<Member> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(member.count())
                .from(member)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public List<Member> getAll() {
        JPAQuery<Member> query = jpaQueryFactory
                .selectFrom(member);

        return query.fetch();
    }

    public List<Member> getMembersByDate(LocalDate date) {
        LocalDateTime startOfToday = date.atStartOfDay();
        LocalDateTime startOfTomorrow = date.plusDays(1).atStartOfDay();
        JPAQuery<Member> query = jpaQueryFactory
                .selectFrom(member)
                .where(member.createdAt.goe(startOfToday)
                               .and(member.createdAt.lt(startOfTomorrow)));

        return query.fetch();
    }

    public List<Member> getMembersByVisitedDate(LocalDate today) {
        LocalDateTime startOfToday = today.atStartOfDay();
        LocalDateTime startOfTomorrow = today.plusDays(1).atStartOfDay();

        JPAQuery<Member> query = jpaQueryFactory
                .selectFrom(member)
                .where(member.visitedTime.goe(startOfToday)
                                         .and(member.visitedTime.lt(startOfTomorrow)));

        return query.fetch();
    }

    // 모든 활동 중인 회원 조회
    public List<Member> findAllActive() {
        JPAQuery<Member> query = jpaQueryFactory
                .selectFrom(member)
                .where(member.status.ne(Member.MemberStatus.WITHDRAW));

        return query.fetch();
    }

    // 특정 ID의 활동 중인 회원 조회
    public List<Member> findAllActiveByEmail(List<String> memberEmails) {
        JPAQuery<Member> query = jpaQueryFactory
                .selectFrom(member)
                .where(member.email.in(memberEmails)
                               .and(member.status.ne(Member.MemberStatus.WITHDRAW)));

        return query.fetch();
    }

    public Optional<Member> findById(Long memberId) {
        Member findMember = jpaQueryFactory
                .selectFrom(member)
                .leftJoin(member.deliveries, delivery).fetchJoin()
                .where(member.id.eq(memberId))
                .fetchOne();

        return Optional.ofNullable(findMember);
    }

}

