package com.funnyland.funnylive.admin.scheduler;

import java.time.LocalDate;
import java.util.List;

import com.funnyland.funnylive.admin.service.MemberService;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.scheduler.DailyMember;
import com.funnyland.funnylive.domain.scheduler.DailyMemberDomainRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
@Slf4j
public class MemberScheduler {

    private final MemberService memberService;
    private final DailyMemberDomainRepository dailyMemberDomainRepository;
    private final StringRedisTemplate stringRedisTemplate;

    @Scheduled(cron = "0 5/5 * * * *")
    public void recordUserMembers() {
        List<Member> visitedMembers = memberService.getVisitedMembers();
        String key = "member_visits:" + LocalDate.now();

        // 회원 ID를 String 배열로 변환
        String[] memberIds = visitedMembers.stream()
                .map(member -> String.valueOf(member.getId()))
                .toArray(String[]::new);

        // Redis Set에 회원 ID 추가, 단 배열이 비어있지 않은 경우에만
        if (memberIds.length > 0) {
            stringRedisTemplate.opsForSet().add(key, memberIds);
        }
    }

    @Scheduled(cron = "0 0 0 * * ?") // 매일 자정에 실행
//    @Scheduled(fixedDelay = 30000)
    @Transactional
    public void saveDailyMemberToDB() {
        LocalDate yesterday = LocalDate.now().minusDays(1);
        String key = "member_visits:" + yesterday;

        boolean isAlreadyRecorded = dailyMemberDomainRepository.existsByDate(yesterday);
        if (isAlreadyRecorded) {
            log.info("DailyMember {} is already recorded", yesterday);
            return;
        }

        try {
            Long uniqueUserCount = stringRedisTemplate.opsForSet().size(key);
            DailyMember dailyMembers = new DailyMember(yesterday, uniqueUserCount);
            dailyMemberDomainRepository.save(dailyMembers);
        } catch (Exception e) {
            log.error("Error processing daily member", e);
            return;
        }

        // Redis 데이터 정리
        stringRedisTemplate.delete(key);
    }
}
