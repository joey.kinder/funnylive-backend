package com.funnyland.funnylive.admin.service;

import java.util.ArrayList;
import java.util.List;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.point.request.PointAdjustmentRequest;
import com.funnyland.funnylive.admin.endpoint.point.request.PointFilter;
import com.funnyland.funnylive.admin.endpoint.point.request.PointTransactionRequest;
import com.funnyland.funnylive.admin.endpoint.point.response.PointTransactionPreviewResponse;
import com.funnyland.funnylive.admin.endpoint.point.response.PointTransactionResponse;
import com.funnyland.funnylive.admin.repository.MemberReadRepository;
import com.funnyland.funnylive.admin.repository.PointTransactionReadRepository;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.point.PointTransactionDomainRepository;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class PointService {

    private final PointTransactionReadRepository pointTransactionReadRepository;
    private final PointTransactionDomainRepository pointTransactionDomainRepository;
    private final MemberReadRepository memberReadRepository;
    private final MemberDomainService memberDomainService;

    public Page<PointTransactionResponse> getAll(PointFilter filter) {
        Page<PointTransaction> findPoints = pointTransactionReadRepository.getPoints(filter.generateBooleanBuilder(), filter.getPageRequest());
        return findPoints.map(PointTransactionResponse::of);

    }

    public List<PointTransaction> getLatestPointTransactions(Long memberId, int limit) {
        List<PointTransaction> findPointTransactions = pointTransactionReadRepository.getLatestPointTransactions(memberId, limit);
        return findPointTransactions;
    }

    public List<PointTransactionPreviewResponse> preview(PointAdjustmentRequest request) {
        List<PointTransactionPreviewResponse> responses = new ArrayList<>();

        // 요청에 따른 회원 목록 조회
        List<Member> members;
        if (request.isApplyToAllMembers()) {
            members = memberReadRepository.findAllActive();
        } else {
            members = memberReadRepository.findAllActiveByEmail(request.getMemberEmails());
        }

        // 각 회원에 대한 포인트 트랜잭션 미리보기 생성
        for (Member member : members) {
            PointTransaction transaction = PointTransaction.builder()
                    .member(member)
                    .transactionType(request.getTransactionType())
                    .amount(request.getPoint())
                    .expirationStartDate(request.getExpirationStartDate())
                    .expirationEndDate(request.getExpirationEndDate())
                    .reason(request.getReason())
                    .build();

            responses.add(PointTransactionPreviewResponse.of(transaction));
        }

        return responses;
    }

    public int getPoint(Long memberId) {
        return pointTransactionReadRepository.getSumPoint(memberId);
    }

    @Transactional
    public void apply(List<PointTransactionRequest.PointTransactionDetail> request) {
        for (PointTransactionRequest.PointTransactionDetail transactionResponse : request) {
            Member member = memberDomainService.find(transactionResponse.getMemberId());
            // amount 값에 - 부호를 붙일지 결정
            int adjustedAmount = transactionResponse.getAmount();
            if (PointTransactionType.TransactionChange.DECREASE.equals(transactionResponse.getTransactionType().getChange())) {
                adjustedAmount = -adjustedAmount;
            }
            PointTransaction transaction = PointTransaction.builder()
                    .member(member)
                    .transactionType(transactionResponse.getTransactionType())
                    .transactionChange(transactionResponse.getTransactionType()
                                                          .getChange())
                    .amount(adjustedAmount)
                    .bonusAmount(0)
                    .expirationStartDate(transactionResponse.getExpirationStartDate())
                    .expirationEndDate(transactionResponse.getExpirationEndDate())
                    .build();
            transaction.createDetail(transaction.getAmount(), member, transaction, transactionResponse.getTransactionType());
            pointTransactionDomainRepository.save(transaction);
        }
    }

    @Transactional
    public void refundPoint(int totalPoint, Member member) {
        PointTransactionType pointTransactionType = PointTransactionType.REFUND;
        PointTransaction transaction = PointTransaction.builder()
                .amount(totalPoint)
                .bonusAmount(0)
                .member(member)
                .transactionType(pointTransactionType)
                .transactionChange(pointTransactionType.getChange())
                .build();
        transaction.createDetail(totalPoint, member, transaction, pointTransactionType);
        pointTransactionDomainRepository.save(transaction);
    }
}
