package com.funnyland.funnylive.admin.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.funnyland.funnylive.admin.endpoint.main.response.MainResponse;
import com.funnyland.funnylive.admin.repository.CraneGameReadRepository;
import com.funnyland.funnylive.admin.repository.DailyMemberReadRepository;
import com.funnyland.funnylive.admin.repository.DeliveryReadRepository;
import com.funnyland.funnylive.admin.repository.InquiryReadRepository;
import com.funnyland.funnylive.admin.repository.MemberReadRepository;
import com.funnyland.funnylive.admin.repository.ReceiptReadRepository;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.receipt.Receipt;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class MainService {

    private final MemberReadRepository memberReadRepository;
    private final ReceiptReadRepository receiptReadRepository;
    private final CraneGameReadRepository craneGameReadRepository;
    private final InquiryReadRepository inquiryReadRepository;
    private final DailyMemberReadRepository dailyMemberReadRepository;
    private final DeliveryReadRepository deliveryReadRepository;

    private final static int DELIVERY_LIMIT = 10;


    public MainResponse getMain() {
        LocalDate today = LocalDate.now();
        List<Member> membersByVisitedDate = memberReadRepository.getMembersByVisitedDate(today);
        List<Member> newMembers = memberReadRepository.getMembersByDate(today);
        List<Receipt> newReceipts = receiptReadRepository.getReceiptsByDate(today);
        List<Inquiry> todayInquiries = inquiryReadRepository.findInquiriesCreatedToday();
        Map<Inquiry.InquiryStatus, Long> inquiryStatusCounts = inquiryReadRepository.countInquiriesByStatus();

        Map<CraneGame.CraneGameState, Long> craneGameStateMap = craneGameReadRepository.countByGameState();

        List<Delivery> deliveries = deliveryReadRepository.findOrderByCreatedAt(DELIVERY_LIMIT);

        //오늘 방문자는 레디스에서 조회
        Long todayVisitCount = dailyMemberReadRepository.findVisitCountByDate(today).orElse(0L);
        int averageDailyVisitCount = dailyMemberReadRepository.getAverageDailyVisits();
        Long totalVisitCount = dailyMemberReadRepository.getTotalVisits();



        return MainResponse.of(membersByVisitedDate, newMembers, newReceipts, craneGameStateMap, deliveries,
                todayInquiries.size(),
                inquiryStatusCounts.get(Inquiry.InquiryStatus.PENDING),
                inquiryStatusCounts.get(Inquiry.InquiryStatus.ANSWERED),
                               todayVisitCount,
                               averageDailyVisitCount,
                               totalVisitCount);
    }
}
