package com.funnyland.funnylive.admin.endpoint.receipt.request;

import com.funnyland.funnylive.domain.receipt.Receipt;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ReceiptRequest {
    private String memo;

    public Receipt toDomain() {
        Receipt receipt = Receipt.builder()
                .memo(this.memo)
                .build();
        return receipt;
    }

}