package com.funnyland.funnylive.admin.service;

import java.util.List;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.delivery.request.DeliveryFilter;
import com.funnyland.funnylive.admin.endpoint.delivery.request.DeliveryUpdateRequest;
import com.funnyland.funnylive.admin.endpoint.delivery.response.DeliveryDetailResponse;
import com.funnyland.funnylive.admin.endpoint.delivery.response.DeliveryResponse;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.InquiryDetailResponse;
import com.funnyland.funnylive.admin.repository.DeliveryReadRepository;
import com.funnyland.funnylive.admin.repository.StorageBoxHistoryReadRepository;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.delivery.DeliveryItem;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.storagebox.StorageBoxHistory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class DeliveryService {

    private final DeliveryReadRepository deliveryReadRepository;
    private final PointService pointService;

    public Page<DeliveryResponse> getAll(DeliveryFilter filter) {
        Page<Delivery> deliveries = deliveryReadRepository.getDeliveries(filter, filter.getPageRequest());
        return deliveries.map(DeliveryResponse::of);
    }

    public DeliveryDetailResponse get(Long deliveryId) {
        Delivery findDelivery = deliveryReadRepository.findById(deliveryId).orElseThrow(() -> new DomainException("없는 배송 입니다"));
        return DeliveryDetailResponse.of(findDelivery);
    }

    @Transactional
    public DeliveryDetailResponse update(Long deliveryId, DeliveryUpdateRequest request) {
        Delivery updatedDelivery = deliveryReadRepository.findById(deliveryId)
                .orElseThrow(() -> new DomainException("없는 배송 입니다"))
                .edit(request.toDomain());

        if(updatedDelivery.getStatus() == Delivery.DeliveryStatus.CANCELED) {

            List<DeliveryItem> items = updatedDelivery.getItems();
            for(DeliveryItem item : items) {
                StorageBoxHistory findStorageBoxHistory = item.getStorageBoxHistory();
                findStorageBoxHistory.setDeliveryRequested(false);
            }
            pointService.refundPoint(updatedDelivery.getDeliveryFee(), updatedDelivery.getMember());
        }

        return DeliveryDetailResponse.of(updatedDelivery);
    }

    public List<Delivery> getLatestDeliveries(Long memberId, int limit) {
        List<Delivery> findDeliveries = deliveryReadRepository.getLatestDeliveries(memberId, limit);
        return findDeliveries;
    }
}
