package com.funnyland.funnylive.admin.service;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.inquiry.request.FrequentlyInquiryFilter;
import com.funnyland.funnylive.admin.endpoint.inquiry.request.FrequentlyInquiryRequest;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.FrequentlyInquiryDetailResponse;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.FrequentlyInquiryResponse;
import com.funnyland.funnylive.admin.repository.FrequentlyInquiryReadRepository;
import com.funnyland.funnylive.admin.repository.InquiryTypeReadRepository;
import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiryDomainService;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class FrequentlyInquiryService {

    private final FrequentlyInquiryReadRepository frequentlyInquiryReadRepository;
    private final InquiryTypeReadRepository inquiryTypeReadRepository;
    private final FrequentlyInquiryDomainService frequentlyInquiryDomainService;

    public Page<FrequentlyInquiryResponse> getAll(FrequentlyInquiryFilter filter) {
        Page<FrequentlyInquiry> frequentlyInquiries = frequentlyInquiryReadRepository.getFrequentlyInquiries(filter.generateBooleanBuilder(), filter.getPageRequest());
        return frequentlyInquiries.map(FrequentlyInquiryResponse::of);
    }

    public FrequentlyInquiryDetailResponse get(Long frequentlyInquiryId) {
        FrequentlyInquiry findFrequentlyInquiry = frequentlyInquiryReadRepository.findById(frequentlyInquiryId)
                                                                         .orElseThrow(() -> new DomainException("없는 자주묻는 질문입니다"));
        return FrequentlyInquiryDetailResponse.of(findFrequentlyInquiry);
    }

    public FrequentlyInquiryDetailResponse create(FrequentlyInquiryRequest request) {
        InquiryType findInquiryType = inquiryTypeReadRepository.findById(request.getInquiryTypeId())
                                                           .orElseThrow(() -> new DomainException("없는 문의 타입입니다"));
        FrequentlyInquiry savedFrequentlyInquiry = frequentlyInquiryDomainService.save(request.toDomain(findInquiryType));
        return FrequentlyInquiryDetailResponse.of(savedFrequentlyInquiry);
    }

    @Transactional
    public FrequentlyInquiryDetailResponse update(Long frequentlyInquiryId, FrequentlyInquiryRequest request) {
        InquiryType findInquiryType = inquiryTypeReadRepository.findById(request.getInquiryTypeId())
                .orElseThrow(() -> new DomainException("없는 문의 타입입니다"));
        FrequentlyInquiry updatedFrequentlyInquiry = frequentlyInquiryReadRepository.findById(frequentlyInquiryId).orElseThrow(() -> new DomainException("없는 문의 타입입니다"))
                .edit(request.toDomain(findInquiryType));
        return FrequentlyInquiryDetailResponse.of(updatedFrequentlyInquiry);
    }
}
