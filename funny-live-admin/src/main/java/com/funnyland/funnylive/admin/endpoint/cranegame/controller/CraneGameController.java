package com.funnyland.funnylive.admin.endpoint.cranegame.controller;

import com.funnyland.funnylive.admin.endpoint.cranegame.request.CraneGameFilter;
import com.funnyland.funnylive.admin.endpoint.cranegame.request.CraneGameRequest;
import com.funnyland.funnylive.admin.endpoint.cranegame.response.CraneGameDetailResponse;
import com.funnyland.funnylive.admin.endpoint.cranegame.response.CraneGameResponse;
import com.funnyland.funnylive.admin.service.CraneGameService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@Tag(name="크레인 게임", description = "크레인 게임 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/crane-games")
public class CraneGameController {

    private final CraneGameService craneGameService;

    @GetMapping
    @Operation(summary = "크레인 리스트 조회", description = "크레원 리스트 조회")
    public Page<CraneGameResponse> getCraneGames(CraneGameFilter filter) {
        return craneGameService.getAll(filter);
    }

    @GetMapping("/{craneGameId}")
    @Operation(summary = "크레인 상세 조회", description = "크레원 상세 조회")
    public CraneGameDetailResponse getCraneGame(@PathVariable Long craneGameId) {
        return craneGameService.get(craneGameId);
    }

    @PostMapping
    @Operation(summary = "크레인 게임 추가", description = "크레인 게임 추가")
    public CraneGameDetailResponse create(@RequestPart("craneGame") CraneGameRequest request, @RequestPart(value = "image", required = false) MultipartFile image) {
        return craneGameService.create(request, image);
    }

    @GetMapping("/check")
    @Operation(summary = "크레인 게임 고유번호 체크", description = "크레인 게임 고유번호 체크")
    public boolean check(@RequestParam String uuid) {
        return craneGameService.check(uuid);
    }

    @PutMapping("/{craneGameId}")
    @Operation(summary = "크레인 게임 수정", description = "크레인 게임 수정")
    public CraneGameDetailResponse update(@PathVariable Long craneGameId, @RequestBody CraneGameRequest request) {
        return craneGameService.update(craneGameId, request);
    }

    @DeleteMapping("/{craneGameId}")
    @Operation(summary = "크레인 게임 삭제", description = "크레인 게임 삭제")
    public void delete(@PathVariable Long craneGameId) {
        craneGameService.delete(craneGameId);
    }

}
