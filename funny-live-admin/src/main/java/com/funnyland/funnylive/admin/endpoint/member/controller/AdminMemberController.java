package com.funnyland.funnylive.admin.endpoint.member.controller;

import com.funnyland.funnylive.admin.endpoint.member.request.AdminMemberFilter;
import com.funnyland.funnylive.admin.endpoint.member.request.AdminMemberRequest;
import com.funnyland.funnylive.admin.endpoint.member.response.AdminMemberDetailResponse;
import com.funnyland.funnylive.admin.endpoint.member.response.AdminMemberResponse;
import com.funnyland.funnylive.admin.service.AdminMemberService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="어드민 멤버", description = "어드민 멤버 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/admin-members")
public class AdminMemberController {

    private final AdminMemberService adminMemberService;

    @GetMapping
    @Operation(summary = "어드민 멤버 리스트 조회", description = "어드민 멤버 리스트 조회")
    public Page<AdminMemberResponse> getMembers(AdminMemberFilter filter) {
        return adminMemberService.getAll(filter);
    }

    @GetMapping("/{adminMemberId}")
    @Operation(summary = "어드민 멤버 상세 조회", description = "어드민 멤버 상세 조회")
    public AdminMemberDetailResponse getMember(@PathVariable Long adminMemberId) {
        return adminMemberService.get(adminMemberId);
    }

    @PostMapping
    @Operation(summary = "어드민 멤버 추가", description = "어드민 멤버 추가")
    public AdminMemberDetailResponse create(@RequestBody AdminMemberRequest request) {
        return adminMemberService.create(request);
    }

    @PutMapping("/{adminMemberId}")
    @Operation(summary = "어드민 멤버 수정", description = "어드민 멤버 수정")
    public AdminMemberDetailResponse update(@PathVariable Long adminMemberId, @RequestBody AdminMemberRequest request) {
        return adminMemberService.update(adminMemberId, request);
    }
}
