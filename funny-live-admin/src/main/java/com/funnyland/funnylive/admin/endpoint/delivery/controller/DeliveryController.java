package com.funnyland.funnylive.admin.endpoint.delivery.controller;

import com.funnyland.funnylive.admin.endpoint.delivery.request.DeliveryFilter;
import com.funnyland.funnylive.admin.endpoint.delivery.request.DeliveryUpdateRequest;
import com.funnyland.funnylive.admin.endpoint.delivery.response.DeliveryDetailResponse;
import com.funnyland.funnylive.admin.endpoint.delivery.response.DeliveryResponse;
import com.funnyland.funnylive.admin.service.DeliveryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="배송요청", description = "배송요청 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/deliveries")
public class DeliveryController {

    private final DeliveryService deliveryService;

    @GetMapping
    @Operation(summary = "배송요청 리스트 조회", description = "배송요청 리스트 조회")
    public Page<DeliveryResponse> getAll(DeliveryFilter filter) {
        return deliveryService.getAll(filter);
    }

    @GetMapping("/{deliveryId}")
    @Operation(summary = "배송요청 상세 조회", description = "배송요청 상세 조회")
    public DeliveryDetailResponse get(@PathVariable Long deliveryId) {
        return deliveryService.get(deliveryId);
    }

    @PutMapping("/{deliveryId}")
    @Operation(summary = "배송요청 수정", description = "배송요청 수정")
    public DeliveryDetailResponse update(@PathVariable Long deliveryId, @RequestBody DeliveryUpdateRequest request) {
        return deliveryService.update(deliveryId, request);
    }

}
