package com.funnyland.funnylive.admin.endpoint.notice.request;

public enum NoticeSearchType {
    TITLE,
    CONTENT,
    UPDATER;
}
