package com.funnyland.funnylive.admin.endpoint.delivery.request;

import static com.funnyland.funnylive.domain.delivery.QDelivery.delivery;
import static com.funnyland.funnylive.domain.delivery.QDeliveryItem.deliveryItem;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DeliveryFilter extends ApiPageRequest {

    private DeliverySearchType searchType;

    private String searchValue;

    private Delivery.DeliveryStatus state;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchStartDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchEndDate;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link DeliverySearchType#ORDER_NUMBER} 주분번호 검색
     *   delivery.orderNumer like '% this.orderNumer %'
     *
     * - {@link DeliverySearchType#NICKNAME} 회원명 검색
     *   delivery.member.nickName like '% this.nickName %'
     *
     * - {@link DeliverySearchType#UPDATER} 작성자 검색
     *   delivery.member.name like '% this.name %'
     *
     * - {@link DeliverySearchType#EMAIL} 제목 검색
     *   delivery.title like '% this.title %'
     *
     * - {@link DeliverySearchType#PHONE} 내용 검색
     *   delivery.content like '% this.content %'
     *
     * - {@link DeliverySearchType#UPDATER} 작성자 검색
     *   delivery.writer like '% this.writer %'
     *
     * - {@link DeliverySearchType#RECIPIENT} 내용 검색
     *   delivery.content like '% this.content %'
     *
     * - {@link DeliverySearchType#PRODUCT_NAME} 작성자 검색
     *   delivery.writer like '% this.writer %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(JPAQueryFactory queryFactory, BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case ORDER_NUMBER:
            builder.and(delivery.orderNumber.like("%" + this.searchValue + "%"));
            break;
        case NICKNAME:
            builder.and(
                    delivery.member.nickName.like("%" + this.searchValue + "%"));
            break;
        case UPDATER:
            builder.and(
                    delivery.lastModifiedBy.like("%" + this.searchValue + "%"));
            break;
        case EMAIL:
            builder.and(delivery.member.email.like("%" + this.searchValue + "%"));
            break;
        case PHONE:
            builder.and(
                    delivery.member.phone.like("%" + this.searchValue + "%"));
            break;
        case RECIPIENT:
            builder.and(
                    delivery.recipientName.like("%" + this.searchValue + "%"));
            break;
        case PRODUCT_NAME:
            JPQLQuery<Long> deliveryItemSubQuery = queryFactory
                    .select(deliveryItem.delivery.id)
                    .from(deliveryItem)
                    .where(deliveryItem.productName.like("%" + this.searchValue + "%"));
            builder.and(delivery.id.in(deliveryItemSubQuery));
            break;

        default:
            break;
        }
    }

    /**
     * state 조건 추가.
     * - delivery.status in (this.state)
     *
     * @param builder query dsl boolean builder
     * */
    private void stateBuilder(BooleanBuilder builder) {
        if(this.state != null) {
            builder.and(delivery.status.eq(this.state));
        }
    }

    /**
     * 검색 시작일 조건 추가.
     * - created_at > this.searchStartDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchStartDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchStartDate)) {
            LocalDateTime startDateTime = this.searchStartDate.atStartOfDay();
            builder.and(delivery.createdAt.gt(startDateTime));
        }
    }

    /**
     * 검색 종료일 조건 추가.
     * - created_at < this.searchEndDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchEndDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchEndDate)) {
            LocalDateTime endDateTime = this.searchEndDate.atTime(23, 59, 59);
            builder.and(delivery.createdAt.lt(endDateTime));
        }
    }

    public BooleanBuilder generateBooleanBuilder(JPAQueryFactory queryFactory) {
        BooleanBuilder builder = new BooleanBuilder();
        stateBuilder(builder);
        searchStartDateBuilder(builder);
        searchEndDateBuilder(builder);
        searchTypeAndContentBuilder(queryFactory, builder);

        return builder;
    }
}
