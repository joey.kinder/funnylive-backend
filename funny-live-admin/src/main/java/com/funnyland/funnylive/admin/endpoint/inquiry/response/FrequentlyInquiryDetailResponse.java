package com.funnyland.funnylive.admin.endpoint.inquiry.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FrequentlyInquiryDetailResponse {

    private Long frequentlyInquiryId;
    private InquiryTypeResponse inquiryType;
    private String title;
    private String content;
    private String updater;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime modifiedAt;
    private Long viewCount;

    public static FrequentlyInquiryDetailResponse of(FrequentlyInquiry frequentlyInquiry) {
        InquiryTypeResponse inquiryTypeResponse = InquiryTypeResponse.builder()
                                       .inquiryTypeId(frequentlyInquiry.getType() != null? frequentlyInquiry.getType().getId() : null)
                                       .name(frequentlyInquiry.getType() != null? frequentlyInquiry.getType().getName() : null)
                                       .build();
        return FrequentlyInquiryDetailResponse.builder()
                                              .frequentlyInquiryId(frequentlyInquiry.getId())
                                              .inquiryType(inquiryTypeResponse)
                                              .title(frequentlyInquiry.getQuestion())
                                            .content(frequentlyInquiry.getAnswer())
                                              .updater(frequentlyInquiry.getLastModifiedBy())
                                              .createdAt(frequentlyInquiry.getCreatedAt())
                                              .modifiedAt(frequentlyInquiry.getModifiedAt())
                                              .viewCount(frequentlyInquiry.getViewCount())
                                              .build();

    }
}
