package com.funnyland.funnylive.admin.endpoint.member.response;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.deliveryaddress.DeliveryAddress;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.point.PointTransactionType;
import com.funnyland.funnylive.domain.receipt.Receipt;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MemberDetailResponse {
    private Long memberId;
    private String oauthId;
    private String name;
    private String nickName;
    private String grade;
    private String email;
    private String provider;
    private String phone;
    private String roadNameAddress;
    private String inputDetail;
    private String postalCode;

    private boolean agreeReceiveEmail;
    private boolean agreeReceivePhone;

    private Integer point;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime visitedTime;
    private String status;

    private List<ReceiptResponse> receipts;
    private List<PointTransactionResponse> pointTransactions;
    private List<InquiryResponse> inquiries;
    private List<DeliveryResponse> deliveries;

    public static MemberDetailResponse of(Member member, List<PointTransaction> pointTransactions, List<Receipt> receipts, List<Inquiry> inquiries, List<Delivery> deliveries, int point) {
        DeliveryAddress defaultAddress = member.getDeliveryAddresses().stream()
                                               .filter(DeliveryAddress::isDefault)
                                               .findFirst()
                                               .orElse(null);

        String roadNameAddress = defaultAddress != null ? defaultAddress.getRoadNameAddress() : null;
        String inputDetail = defaultAddress != null ? defaultAddress.getInputDetail() : null;
        String postalCode = defaultAddress != null ? defaultAddress.getPostalCode() : null;

        return MemberDetailResponse.builder()
                                   .memberId(member.getId())
                                   .oauthId(member.getOauthId())
                                   .provider(member.getOAuthProvider() != null ? member.getOAuthProvider().getName() : null)
                                   .grade(member.getGrade() !=null ? member.getGrade().getName() : null)
                                   .name(member.getName())
                                   .email(member.getEmail())
                                   .nickName(member.getNickName())
                                   .phone(member.getPhone())
                                   .status(member.getStatus() != null ? member.getStatus().name() : null)
                                   .roadNameAddress(roadNameAddress)
                                   .inputDetail(inputDetail)
                                   .postalCode(postalCode)
                .agreeReceiveEmail(member.isAgreeReceiveEmail())
                .agreeReceivePhone(member.isAgreeReceivePhone())
                .point(point)
                .createdAt(member.getCreatedAt())
                .visitedTime(member.getVisitedTime())
                .receipts(receipts.stream()
                                  .map(ReceiptResponse::of)
                                  .collect(Collectors.toList()))
                .pointTransactions(pointTransactions.stream()
                                           .map(PointTransactionResponse::of)
                                           .collect(Collectors.toList()))
                .inquiries(inquiries.stream().map(InquiryResponse::of).collect(Collectors.toList()))
                .deliveries(deliveries.stream().map(DeliveryResponse::of).collect(Collectors.toList()))
                                   .build();
    }

    @Builder
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReceiptResponse {
        private Long receiptId;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime purchaseTime;
        private Integer amount;
        public static ReceiptResponse of(Receipt receipt) {
            return ReceiptResponse.builder()
                    .receiptId(receipt.getId())
                    .amount(receipt.getPaymentAmount())
                    .purchaseTime(receipt.getCreatedAt())
                    .build();
        }

    }

    @Builder
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PointTransactionResponse {
        private Long pointTransactionId;
        private String transactionType;
        private PointTransactionType.TransactionChange change;
        private int amount;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime createdAt;
        public static PointTransactionResponse of(PointTransaction pointTransaction) {
            return PointTransactionResponse.builder()
                    .pointTransactionId(pointTransaction.getId())
                    .transactionType(pointTransaction.getTransactionType() != null ? pointTransaction.getTransactionType().getDescription() : null)
                    .change(pointTransaction.getTransactionType() != null ? pointTransaction.getTransactionType()
                            .getChange() : null)
                    .amount(pointTransaction.getAmount())
                    .createdAt(pointTransaction.getCreatedAt())
                    .build();
        }

    }

    @Builder
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InquiryResponse {
        private Long inquiryId;
        private String title;
        private String status;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime createdAt;
        public static InquiryResponse of(Inquiry inquiry) {
            return InquiryResponse.builder()
                    .inquiryId(inquiry.getId())
                    .title(inquiry.getTitle())
                    .status(inquiry.getStatus() != null ? inquiry.getStatus().getName() : null)
                    .createdAt(inquiry.getCreatedAt())
                    .build();
        }

    }

    @Builder
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeliveryResponse {
        private Long deliveryId;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime createdAt;
        private Integer count;
        private String stateName;
        public static DeliveryResponse of(Delivery delivery) {
            return DeliveryResponse.builder()
                    .deliveryId(delivery.getId())
                    .createdAt(delivery.getCreatedAt())
                    .count(delivery.getItems() != null ? delivery.getItems().size() : 0)
                    .stateName(delivery.getStatus() != null ? delivery.getStatus().getName() : null)
                    .build();
        }

    }
}
