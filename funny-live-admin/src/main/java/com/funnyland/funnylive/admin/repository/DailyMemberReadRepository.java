package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.scheduler.QDailyMember.dailyMember;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Optional;

import com.funnyland.funnylive.domain.scheduler.DailyMember;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;


@Repository
public class DailyMemberReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public DailyMemberReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(DailyMember.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Optional<Long> findVisitCountByDate(LocalDate date) {
        Long visitCount = jpaQueryFactory.select(dailyMember.uniqueUserCount)
                .from(dailyMember)
                .where(dailyMember.date.eq(date))
                .fetchOne();
        return Optional.ofNullable(visitCount);
    }

    public int getAverageDailyVisits() {
        Long totalVisitCount = jpaQueryFactory
                .select(dailyMember.uniqueUserCount.sum())
                .from(dailyMember)
                .fetchOne();
        Long countOfDays = jpaQueryFactory
                .select(dailyMember.count())
                .from(dailyMember)
                .fetchOne();

        if (totalVisitCount == null || countOfDays == null || countOfDays == 0) {
            return 0;
        }

        BigDecimal average = BigDecimal.valueOf(totalVisitCount)
                .divide(BigDecimal.valueOf(countOfDays + 1), 0, RoundingMode.HALF_UP);
        return average.intValue();
    }

    public Long getTotalVisits() {
        Long totalVisits = jpaQueryFactory.select(dailyMember.uniqueUserCount.sum())
                .from(dailyMember)
                .fetchOne();
        return totalVisits != null ? totalVisits : 0;
    }

}

