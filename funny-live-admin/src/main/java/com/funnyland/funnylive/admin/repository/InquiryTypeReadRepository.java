package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.inquiry.QInquiryType.inquiryType;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class InquiryTypeReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public InquiryTypeReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(InquiryType.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<InquiryType> getTypes(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<InquiryType> query = jpaQueryFactory.selectFrom(inquiryType)
                .where(builder);

        List<InquiryType> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(inquiryType.count())
                .from(inquiryType)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Optional<InquiryType> findById(Long inquiryTypeId) {
        JPAQuery<InquiryType> query = jpaQueryFactory.selectFrom(inquiryType)
                                                     .where(inquiryType.id.eq(inquiryTypeId));
        return Optional.ofNullable(query.fetchOne());
    }

    public List<InquiryType> getTypes() {
        return jpaQueryFactory.selectFrom(inquiryType).fetch();
    }
}

