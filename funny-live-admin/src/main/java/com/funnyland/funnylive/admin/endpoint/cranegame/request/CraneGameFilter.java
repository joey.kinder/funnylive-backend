package com.funnyland.funnylive.admin.endpoint.cranegame.request;

import static com.funnyland.funnylive.domain.cranegame.QCraneGame.craneGame;
import static com.funnyland.funnylive.domain.member.QMember.member;

import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CraneGameFilter extends ApiPageRequest {

    private CraneGameSearchType searchType;

    private String searchValue;

    private Integer playFee;

    private CraneGame.CraneGameState state;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link CraneGameSearchType#UUID} 고유번호 검색
     *   craneGame.uuid like '% this.uuid %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case UUID:
            builder.and(craneGame.uuid.like("%" + this.searchValue + "%"));
            break;
        default:
            break;
        }
    }

    /**
     * 상태 조건 추가.
     * - craneGame.gameState in (this.state)
     *
     * @param builder query dsl boolean builder
     * */
    private void stateBuilder(BooleanBuilder builder) {
        if(this.state != null) {
            builder.and(craneGame.gameState.eq(this.state));
        }
    }

    /**
     * 포인트 조건트추가.
     * - craneGame.playFee in (this.state)
     *
     * @param builder query dsl boolean builder
     * */
    private void playFeeBuilder(BooleanBuilder builder) {
        if(this.playFee != null) {
            builder.and(craneGame.playFee.eq(this.playFee));
        }
    }



    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        searchTypeAndContentBuilder(builder);
        stateBuilder(builder);
        playFeeBuilder(builder);
        return builder;
    }
}
