package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.inquiry.QInquiry.inquiry;
import static com.funnyland.funnylive.domain.receipt.QReceipt.receipt;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.member.Member;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class InquiryReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public InquiryReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(Inquiry.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<Inquiry> getInquiries(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<Inquiry> query = jpaQueryFactory.selectFrom(inquiry)
                                                .where(builder);

        List<Inquiry> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(inquiry.count())
                .from(inquiry)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Optional<Inquiry> findById(Long inquiryId) {
        JPAQuery<Inquiry> query = jpaQueryFactory
                .selectFrom(inquiry)
                .where(inquiry.id.eq(inquiryId));

        return Optional.ofNullable(query.fetchOne());
    }

    public List<Inquiry> findInquiriesCreatedToday() {
        LocalDate today = LocalDate.now();
        return jpaQueryFactory.selectFrom(inquiry)
                              .where(inquiry.createdAt.between(today.atStartOfDay(), today.plusDays(1).atStartOfDay()))
                              .fetch();
    }

    public Map<Inquiry.InquiryStatus, Long> countInquiriesByStatus() {
        List<Tuple> results = jpaQueryFactory
                .select(inquiry.status, inquiry.count())
                .from(inquiry)
                .groupBy(inquiry.status)
                .fetch();

        Map<Inquiry.InquiryStatus, Long> statusCounts = new HashMap<>();
        for (Tuple result : results) {
            statusCounts.put(result.get(0, Inquiry.InquiryStatus.class), result.get(1, Long.class));
        }

        // 모든 상태에 대해 값이 있도록 보장
        Arrays.stream(Inquiry.InquiryStatus.values())
              .forEach(status -> statusCounts.putIfAbsent(status, 0L));

        return statusCounts;
    }

    public List<Inquiry> getLatestInquiries(Long memberId, int limit) {
        return jpaQueryFactory
                .selectFrom(inquiry)
                .where(inquiry.member.id.eq(memberId))
                .orderBy(inquiry.createdAt.desc())
                .limit(limit)
                .fetch();
    }
}
