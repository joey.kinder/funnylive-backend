package com.funnyland.funnylive.admin.service.dto;

public enum StorageType {
    IMAGE,
    ETC;

    public boolean isImage() {
        return this.equals(IMAGE);
    }
}
