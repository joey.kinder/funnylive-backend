package com.funnyland.funnylive.admin.endpoint.notice.request;

import static com.funnyland.funnylive.domain.inquiry.QInquiry.inquiry;
import static com.funnyland.funnylive.domain.notice.QNotice.notice;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.notice.Notice;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NoticeFilter extends ApiPageRequest {

    private NoticeSearchType searchType;

    private String searchValue;

    private Notice.NoticeType type;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchStartDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate searchEndDate;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link NoticeSearchType#TITLE} 제목 검색
     *   notice.title like '% this.title %'
     *
     * - {@link NoticeSearchType#CONTENT} 내용 검색
     *   notice.content like '% this.content %'
     *
     * - {@link NoticeSearchType#UPDATER} 작성자 검색
     *   notice.writer like '% this.writer %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case TITLE:
            builder.and(notice.title.like("%" + this.searchValue + "%"));
            break;
        case CONTENT:
            builder.and(
                    notice.content.like("%" + this.searchValue + "%"));
            break;
        case UPDATER:
            builder.and(
                    notice.lastModifiedBy.like("%" + this.searchValue + "%"));
            break;
        default:
            break;
        }
    }

    /**
     * type 조건 추가.
     * - notice.type in (this.type)
     *
     * @param builder query dsl boolean builder
     * */
    private void typeBuilder(BooleanBuilder builder) {
        if(this.type != null) {
            builder.and(notice.noticeType.eq(this.type));
        }
    }

    /**
     * 검색 시작일 조건 추가.
     * - created_at > this.searchStartDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchStartDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchStartDate)) {
            LocalDateTime startDateTime = this.searchStartDate.atStartOfDay();
            builder.and(notice.createdAt.gt(startDateTime));
        }
    }

    /**
     * 검색 종료일 조건 추가.
     * - created_at < this.searchEndDate
     *
     * @param builder query dsl boolean builder
     * */
    private void searchEndDateBuilder(BooleanBuilder builder) {
        if(!ObjectUtils.isEmpty(this.searchEndDate)) {
            LocalDateTime endDateTime = this.searchEndDate.atTime(23, 59, 59);
            builder.and(notice.createdAt.lt(endDateTime));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        typeBuilder(builder);
        searchStartDateBuilder(builder);
        searchEndDateBuilder(builder);
        searchTypeAndContentBuilder(builder);

        return builder;
    }
}
