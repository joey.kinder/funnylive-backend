package com.funnyland.funnylive.admin.endpoint.member.request;

import static com.funnyland.funnylive.domain.member.QMember.member;

import com.funnyland.funnylive.domain.ApiPageRequest;
import com.funnyland.funnylive.domain.member.Member;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MemberFilter extends ApiPageRequest {

    private SearchType searchType;

    private String searchValue;

    private Member.Grade grade;

    private Member.MemberStatus status;

    /**
     * 검색 타입, 내용 모두 있어야만 조건 추가.
     *
     * - {@link SearchType#EMAIL} 이메일 검색
     *   member.email like '% this.email %'
     *
     * - {@link SearchType#NAME} 이름 검색
     *   member.name like '% this.name %'
     *
     * - {@link SearchType#NICKNAME} 닉네임 검색
     *   member.nickname like '% this.nickname %'
     *
     * - {@link SearchType#PHONE} 핸드폰 검색
     *   member.phone like '% this.phone %'
     *
     * @param builder query dsl boolean builder
     * */
    private void searchTypeAndContentBuilder(BooleanBuilder builder) {
        if(this.searchType == null || !StringUtils.hasText(this.searchValue)) {
            return;
        }

        switch (this.searchType) {
        case EMAIL:
            builder.and(member.email.like("%" + this.searchValue + "%"));
            break;
        case NAME:
            builder.and(
                    member.name.like("%" + this.searchValue + "%"));
            break;
        case NICKNAME:
            builder.and(
                    member.nickName.like("%" + this.searchValue + "%"));
            break;
        case PHONE:
            builder.and(
                    member.phone.like("%" + this.searchValue + "%"));
            break;
        default:
            break;
        }
    }

    /**
     * 상태 조건 추가.
     * - member.status in (this.status)
     *
     * @param builder query dsl boolean builder
     * */
    private void statusBuilder(BooleanBuilder builder) {
        if(this.status != null) {
            builder.and(member.status.eq(this.status));
        }
    }

    /**
     * 등급 조건 추가.
     * - member.grade in (this.grade)
     *
     * @param builder query dsl boolean builder
     * */
    private void gradeBuilder(BooleanBuilder builder) {
        if(this.grade != null) {
            builder.and(member.grade.eq(this.grade));
        }
    }

    public BooleanBuilder generateBooleanBuilder() {
        BooleanBuilder builder = new BooleanBuilder();
        statusBuilder(builder);
        gradeBuilder(builder);
        searchTypeAndContentBuilder(builder);

        return builder;
    }
}
