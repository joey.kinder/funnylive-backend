package com.funnyland.funnylive.admin.endpoint.product.request;

public enum ProductSearchType {
    NAME,
    PRICE;
}
