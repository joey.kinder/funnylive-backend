package com.funnyland.funnylive.admin.service;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.notice.request.NoticeFilter;
import com.funnyland.funnylive.admin.endpoint.notice.request.NoticeRequest;
import com.funnyland.funnylive.admin.endpoint.notice.response.NoticeDetailResponse;
import com.funnyland.funnylive.admin.endpoint.notice.response.NoticeResponse;
import com.funnyland.funnylive.admin.repository.NoticeReadRepository;
import com.funnyland.funnylive.domain.notice.Notice;
import com.funnyland.funnylive.domain.notice.NoticeDomainService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class NoticeService {

    private final NoticeReadRepository noticeReadRepository;
    private final NoticeDomainService noticeDomainService;

    public Page<NoticeResponse> getAll(NoticeFilter filter) {
        Page<Notice> notices = noticeReadRepository.getNotices(filter.generateBooleanBuilder(), filter.getPageRequest());
        return notices.map(NoticeResponse::of);
    }

    public NoticeDetailResponse get(Long noticeId) {
        Notice findNotice = noticeReadRepository.findById(noticeId)
                .orElseThrow(() -> new DomainException("없는 공지사항입니다"));
        return NoticeDetailResponse.of(findNotice);
    }

    public NoticeDetailResponse create(NoticeRequest request) {
        Notice savedNotice = noticeDomainService.save(request.toDomain());
        return NoticeDetailResponse.of(savedNotice);
    }

    @Transactional
    public NoticeDetailResponse update(Long noticeId, NoticeRequest request) {
        Notice updatedNotice = noticeDomainService.update(noticeId, request.toDomain());
        return NoticeDetailResponse.of(updatedNotice);
    }
}
