package com.funnyland.funnylive.admin.scheduler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import com.funnyland.funnylive.admin.repository.CraneGameReadRepository;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.cranegame.CraneGameDomainRepository;
import com.funnyland.funnylive.domain.cranegame.CraneGameDomainService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
public class CraneGameScheduler {

    private final StringRedisTemplate stringRedisTemplate;
    private final CraneGameDomainRepository craneGameDomainRepository;
    private final CraneGameReadRepository craneGameReadRepository;

    @Scheduled(cron = "0 */1 * * * *")
    public void checkGameTimeouts() {
        List<CraneGame> findCraneGames = craneGameReadRepository.findAllByState(CraneGame.CraneGameState.GAME_NORMAL);

        for(CraneGame craneGame : findCraneGames) {
            String key = "CraneGamePlayTimeout:" + craneGame.getId();
            if (!stringRedisTemplate.hasKey(key)) {
                // 해당 게임의 타임아웃이 없으면 상태를 대기로 변경
                craneGame.end();
                craneGameDomainRepository.save(craneGame);
                log.info("CraneGameScheduler.checkGameTimeouts : force quit craneGameId={}", craneGame.getId());
            }
        }
        log.info("CraneGameScheduler.checkGameTimeouts finished");
    }
}
