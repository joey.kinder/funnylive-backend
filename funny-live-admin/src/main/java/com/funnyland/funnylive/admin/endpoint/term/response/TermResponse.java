package com.funnyland.funnylive.admin.endpoint.term.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.term.Term;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class TermResponse {
    private Long termId;
    private String typeName;
    private String title;
    private String content;
    private String updater;
    private Long viewCount;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifiedAt;

    public static TermResponse of(Term term) {
        return TermResponse.builder()
                            .termId(term.getId())
                            .typeName(term.getType() != null ? term.getType().getName() : null)
                            .title(term.getTitle())
                            .content(term.getContent())
                            .viewCount(term.getViewCount())
                            .createdAt(term.getCreatedAt())
                            .modifiedAt(term.getModifiedAt())
                            .updater(term.getLastModifiedBy())
                            .build();
    }
}
