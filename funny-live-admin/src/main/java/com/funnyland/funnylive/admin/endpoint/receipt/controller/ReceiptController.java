package com.funnyland.funnylive.admin.endpoint.receipt.controller;

import com.funnyland.funnylive.admin.endpoint.receipt.request.ReceiptFilter;
import com.funnyland.funnylive.admin.endpoint.receipt.request.ReceiptRequest;
import com.funnyland.funnylive.admin.endpoint.receipt.response.ReceiptDetailResponse;
import com.funnyland.funnylive.admin.endpoint.receipt.response.ReceiptResponse;
import com.funnyland.funnylive.admin.service.ReceiptService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="결제", description = "결제 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/receipts")
public class ReceiptController {

    private final ReceiptService receiptService;

    @GetMapping
    @Operation(summary = "결제 리스트 조회", description = "결제 리스트 조회")
    public Page<ReceiptResponse> getReceipts(ReceiptFilter filter) {
        return receiptService.getAll(filter);
    }

    @GetMapping("/{receiptId}")
    @Operation(summary = "결제 상세 조회", description = "결제 상세 조회")
    public ReceiptDetailResponse getReceipt(@PathVariable Long receiptId) {
        return receiptService.get(receiptId);
    }

    @PutMapping("/{receiptId}")
    @Operation(summary = "결제 수정", description = "결제 수정")
    public ReceiptDetailResponse update(@PathVariable Long receiptId, @RequestBody ReceiptRequest request) {
        return receiptService.update(receiptId, request);
    }

}
