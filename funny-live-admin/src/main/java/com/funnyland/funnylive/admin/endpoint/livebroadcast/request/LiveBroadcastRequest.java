package com.funnyland.funnylive.admin.endpoint.livebroadcast.request;

import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcast;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LiveBroadcastRequest {
    private String title;
    private String url;

    public LiveBroadcast toDomain() {
        return LiveBroadcast.builder()
                .title(this.title)
                .url(this.url)
                .build();
    }

}
