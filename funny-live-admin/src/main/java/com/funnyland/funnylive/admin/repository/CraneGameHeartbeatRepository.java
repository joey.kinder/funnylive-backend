package com.funnyland.funnylive.admin.repository;

import com.funnyland.funnylive.domain.cranegame.CraneGameHeartbeat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CraneGameHeartbeatRepository extends CrudRepository<CraneGameHeartbeat, Long> {
}
