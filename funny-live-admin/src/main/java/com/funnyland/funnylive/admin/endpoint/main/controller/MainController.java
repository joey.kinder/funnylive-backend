package com.funnyland.funnylive.admin.endpoint.main.controller;

import com.funnyland.funnylive.admin.endpoint.main.response.MainResponse;
import com.funnyland.funnylive.admin.service.MainService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="메인", description = "메인 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/main")
public class MainController {

    private final MainService mainService;


    @GetMapping
    @Operation(summary = "메인화면 조회", description = "메인화면 조회")
    public MainResponse getMain() {
        return mainService.getMain();
    }


}
