package com.funnyland.funnylive.admin.endpoint.member.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.member.AdminMember;
import com.funnyland.funnylive.domain.member.Member;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MemberResponse {
    private Long memberId;
    private String provider;
    private String email;
    private String grade;
    private String name;
    private String nickName;
    private String phone;
    private String role;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime visitedTime;
    private String status;

    public static MemberResponse of(Member member) {
        return MemberResponse.builder()
                .memberId(member.getId())
                .provider(member.getOAuthProvider() != null ? member.getOAuthProvider().getName() : null)
                .role(member.getRole().name())
                .grade(member.getGrade() != null? member.getGrade().getName() : null)
                .name(member.getName())
                .email(member.getEmail())
                .nickName(member.getNickName())
                .phone(member.getPhone())
                .createdAt(member.getCreatedAt())
                .visitedTime(member.getVisitedTime())
                .status(member.getStatus() != null? member.getStatus().name() : null)
                .build();
    }

}
