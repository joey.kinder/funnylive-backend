package com.funnyland.funnylive.admin.service;

import java.util.List;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.receipt.request.ReceiptFilter;
import com.funnyland.funnylive.admin.endpoint.receipt.request.ReceiptRequest;
import com.funnyland.funnylive.admin.endpoint.receipt.response.ReceiptDetailResponse;
import com.funnyland.funnylive.admin.endpoint.receipt.response.ReceiptResponse;
import com.funnyland.funnylive.admin.repository.ReceiptReadRepository;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.receipt.Receipt;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class ReceiptService {

    private final ReceiptReadRepository receiptReadRepository;

    public Page<ReceiptResponse> getAll(ReceiptFilter filter) {
        Page<Receipt> findProducts = receiptReadRepository.getReceipts(filter.generateBooleanBuilder(), filter.getPageRequest());
        return findProducts.map(ReceiptResponse::of);
    }

    public List<Receipt> getLatestReceipts(Long memberId, int limit) {
        List<Receipt> findReceipts = receiptReadRepository.getLatestReceipts(memberId, limit);
        return findReceipts;
    }

    public ReceiptDetailResponse get(Long receiptId) {
        Receipt findReceipt = receiptReadRepository.getReceipt(receiptId).orElseThrow(() -> new DomainException("없는 결제 아이디입니다"));
        return ReceiptDetailResponse.of(findReceipt);
    }

    @Transactional
    public ReceiptDetailResponse update(Long receiptId, ReceiptRequest request) {
        Receipt updateReceipt = receiptReadRepository.getReceipt(receiptId).orElseThrow(() -> new DomainException("없는 결제 아이디입니다"))
                .edit(request.toDomain());

        return ReceiptDetailResponse.of(updateReceipt);
    }
}
