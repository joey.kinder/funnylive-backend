package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.livebroadcast.QLiveBroadcast.liveBroadcast;
import static com.funnyland.funnylive.domain.notice.QNotice.notice;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcast;
import com.funnyland.funnylive.domain.notice.Notice;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class LiveBroadcastReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public LiveBroadcastReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(LiveBroadcast.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<LiveBroadcast> getLiveBroadcasts(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<LiveBroadcast> query = jpaQueryFactory.selectFrom(liveBroadcast)
                .where(builder);

        List<LiveBroadcast> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(liveBroadcast.count())
                .from(liveBroadcast)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Optional<LiveBroadcast> findById(Long id) {
        JPAQuery<LiveBroadcast> query = jpaQueryFactory.selectFrom(liveBroadcast)
                .where(liveBroadcast.id.eq(id));

        return Optional.ofNullable(query.fetchOne());
    }
}

