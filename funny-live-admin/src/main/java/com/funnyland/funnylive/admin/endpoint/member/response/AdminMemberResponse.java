package com.funnyland.funnylive.admin.endpoint.member.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.domain.member.AdminMember;
import com.funnyland.funnylive.domain.member.Role;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdminMemberResponse {
    private Long memberId;
    private String email;
    private String roleName;
    private String association;
    private String rank;
    private String name;
    private String phone;
    private String ip;
    private String stateName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;

    public static AdminMemberResponse of(AdminMember adminMember) {
        return AdminMemberResponse.builder()
                .memberId(adminMember.getId())
                .email(adminMember.getEmail())
                .roleName(adminMember.getRole() != null ? adminMember.getRole().getName() : null)
                .association(adminMember.getAssociation())
                .rank(adminMember.getRank())
                .name(adminMember.getName())
                .phone(adminMember.getPhone())
                .ip(adminMember.getIp())
                .stateName(adminMember.getStatus() != null ? adminMember.getStatus().getName() : null)
                .createdAt(adminMember.getCreatedAt())
                .build();
    }

}
