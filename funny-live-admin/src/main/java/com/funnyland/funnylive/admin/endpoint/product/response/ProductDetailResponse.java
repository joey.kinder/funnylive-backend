package com.funnyland.funnylive.admin.endpoint.product.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.funnyland.funnylive.common.util.UrlUtil;
import com.funnyland.funnylive.domain.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetailResponse {
    private Long productId;
    private String name;
    private double price;
    private String imageUrl;
    private String state;


    public static ProductDetailResponse of(Product product) {
        return ProductDetailResponse.builder()
                                    .productId(product.getId())
                                    .name(product.getName())
                                    .price(product.getPrice())
                                    .imageUrl(UrlUtil.convertToCdnUrl(product.getImageURL()))
                                    .state(product.getStatus() != null ? product.getStatus().name() : null)
                                    .build();
    }

}
