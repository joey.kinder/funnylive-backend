package com.funnyland.funnylive.admin.endpoint.term.request;

public enum TermSearchType {
    TITLE,
    CONTENT;
}
