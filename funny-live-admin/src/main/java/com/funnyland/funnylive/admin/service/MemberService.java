package com.funnyland.funnylive.admin.service;

import java.time.LocalDate;
import java.util.List;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.admin.endpoint.member.request.MemberFilter;
import com.funnyland.funnylive.admin.endpoint.member.request.MemberRequest;
import com.funnyland.funnylive.admin.endpoint.member.response.MemberDetailResponse;
import com.funnyland.funnylive.admin.endpoint.member.response.MemberResponse;
import com.funnyland.funnylive.admin.repository.MemberReadRepository;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.member.MemberDomainService;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.receipt.Receipt;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberDomainService memberDomainService;
    private final MemberReadRepository memberReadRepository;
    private final PointService pointService;
    private final ReceiptService receiptService;
    private final InquiryService inquiryService;
    private final DeliveryService deliveryService;

    public Page<MemberResponse> getAll(MemberFilter filter) {
        Page<Member> findMembers = memberReadRepository.getMembers(filter.generateBooleanBuilder(), filter.getPageRequest());
        return findMembers.map(MemberResponse::of);

    }

    @Transactional
    public MemberResponse update(Long memberId, MemberRequest request) {
        Member findMember = memberDomainService.update(memberId, request.toDomain());
        return MemberResponse.of(findMember);
    }

    public MemberDetailResponse get(Long memberId) {
        Member findMember = memberReadRepository.findById(memberId).orElseThrow(() -> new DomainException("없는 유저입니다"));
        List<PointTransaction> latestPointTransactions = pointService.getLatestPointTransactions(findMember.getId(), 5);
        List<Receipt> latestReceipts = receiptService.getLatestReceipts(findMember.getId(), 5);
        List<Inquiry> latestInquiries = inquiryService.getLatestInquiries(findMember.getId(), 5);
        List<Delivery> latestDeliveries = deliveryService.getLatestDeliveries(findMember.getId(), 5);
        int point = pointService.getPoint(findMember.getId());
        return MemberDetailResponse.of(findMember, latestPointTransactions, latestReceipts, latestInquiries, latestDeliveries, point);
    }

    public List<Member> getVisitedMembers() {
        LocalDate today = LocalDate.now();
        return memberReadRepository.getMembersByVisitedDate(today);
    }




}
