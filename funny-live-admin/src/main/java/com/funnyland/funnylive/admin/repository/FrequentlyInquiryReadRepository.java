package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.inquiry.QFrequentlyInquiry.frequentlyInquiry;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.inquiry.FrequentlyInquiry;
import com.funnyland.funnylive.domain.inquiry.QFrequentlyInquiry;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class FrequentlyInquiryReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public FrequentlyInquiryReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(FrequentlyInquiry.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<FrequentlyInquiry> getFrequentlyInquiries(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<FrequentlyInquiry> query = jpaQueryFactory.selectFrom(frequentlyInquiry)
                                                .where(builder);

        List<FrequentlyInquiry> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(frequentlyInquiry.count())
                .from(frequentlyInquiry)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Optional<FrequentlyInquiry> findById(Long frequentlyInquiryId) {

        FrequentlyInquiry frequentlyInquiry = jpaQueryFactory.selectFrom(QFrequentlyInquiry.frequentlyInquiry)
                                                             .where(QFrequentlyInquiry.frequentlyInquiry.id.eq(frequentlyInquiryId))
                                                             .fetchOne();
        return Optional.ofNullable(frequentlyInquiry);
    }
}
