package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.member.QMember.member;
import static com.funnyland.funnylive.domain.point.QPointTransaction.pointTransaction;
import static com.funnyland.funnylive.domain.receipt.QReceipt.receipt;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.admin.endpoint.receipt.response.ReceiptResponse;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.receipt.Receipt;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class ReceiptReadRepository extends QuerydslRepositorySupport {
    private final JPAQueryFactory jpaQueryFactory;

    public ReceiptReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(Receipt.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Page<Receipt> getReceipts(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<Receipt> query = jpaQueryFactory.selectFrom(receipt)
                                                 .where(builder);

        List<Receipt> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(receipt.count())
                .from(receipt)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Optional<Receipt> getReceipt(Long receiptId) {
        JPAQuery<Receipt> query = jpaQueryFactory.selectFrom(receipt)
                .where(receipt.id.eq(receiptId));

        return Optional.ofNullable(query.fetchOne());
    }

    public List<Receipt> getReceiptsByDate(LocalDate date) {
        LocalDateTime startOfToday = date.atStartOfDay();
        LocalDateTime startOfTomorrow = date.plusDays(1).atStartOfDay();
        JPAQuery<Receipt> query = jpaQueryFactory
                .selectFrom(receipt)
                .where(receipt.createdAt.goe(startOfToday)
                               .and(receipt.createdAt.lt(startOfTomorrow)));

        return query.fetch();
    }

    public List<Receipt> getLatestReceipts(Long memberId, int limit) {
        return jpaQueryFactory
                .selectFrom(receipt)
                .where(receipt.member.id.eq(memberId))
                .orderBy(receipt.createdAt.desc())
                .limit(limit)
                .fetch();
    }
}
