package com.funnyland.funnylive.admin.endpoint.storagebox.request;

public enum StorageBoxSearchType {
    NAME,
    EMAIL,
    PRODUCT_NAME;
}
