package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.storagebox.QStorageBox.storageBox;
import static com.funnyland.funnylive.domain.storagebox.QStorageBoxHistory.storageBoxHistory;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.storagebox.StorageBox;
import com.funnyland.funnylive.domain.storagebox.StorageBoxHistory;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class StorageBoxHistoryReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public StorageBoxHistoryReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(StorageBoxHistory.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Optional<StorageBoxHistory> findById(Long id) {
        JPAQuery<StorageBoxHistory> query = jpaQueryFactory.selectFrom(storageBoxHistory)
                .where(storageBoxHistory.id.eq(id));

        return Optional.ofNullable(query.fetchOne());
    }

    public Page<StorageBoxHistory> getStorageBoxes(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<StorageBoxHistory> query = jpaQueryFactory.selectFrom(storageBoxHistory)
                                                    .where(builder.and(storageBoxHistory.deliveryRequested.eq(false)));

        List<StorageBoxHistory> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(storageBoxHistory.count())
                .from(storageBoxHistory)
                .where(builder.and(storageBoxHistory.deliveryRequested.eq(false)));

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }
}

