package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.cranegame.QCraneGame.craneGame;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class CraneGameReadRepository extends QuerydslRepositorySupport {
    private final JPAQueryFactory jpaQueryFactory;

    public CraneGameReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(CraneGame.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }


    public boolean check(String uuid) {
        List<CraneGame> results = jpaQueryFactory.selectFrom(craneGame)
                                                 .where(craneGame.uuid.eq(uuid))
                                                 .fetch();
        return !results.isEmpty();
    }

    public Page<CraneGame> findAll(BooleanBuilder builder, PageRequest pageRequest) {
        JPAQuery<CraneGame> query = jpaQueryFactory.selectFrom(craneGame)
                .where(builder);

        List<CraneGame> content = getQuerydsl().applyPagination(pageRequest, query).fetch();

        JPAQuery<Long> countQuery = jpaQueryFactory
                .select(craneGame.count())
                .from(craneGame)
                .where(builder);

        return PageableExecutionUtils.getPage(content, pageRequest, countQuery::fetchOne);
    }

    public Map<CraneGame.CraneGameState, Long> countByGameState() {
        List<Tuple> results = jpaQueryFactory
                .select(craneGame.gameState, craneGame.count())
                .from(craneGame)
                .groupBy(craneGame.gameState)
                .fetch();

        Map<CraneGame.CraneGameState, Long> gameStateCounts = new HashMap<>();

        // 결과에서 조회된 상태값을 맵에 추가
        for (Tuple result : results) {
            gameStateCounts.put(result.get(0, CraneGame.CraneGameState.class), result.get(1, Long.class));
        }

        // 모든 가능한 상태에 대해 맵을 확인하고, 없는 경우 0으로 설정
        for (CraneGame.CraneGameState state : CraneGame.CraneGameState.values()) {
            gameStateCounts.putIfAbsent(state, 0L);
        }

        return gameStateCounts;
    }

    public List<CraneGame> findAllByState(CraneGame.CraneGameState state) {
        List<CraneGame> results = jpaQueryFactory.selectFrom(craneGame)
                                                 .where(craneGame.gameState.eq(state))
                                                 .fetch();
        return results;
    }
}
