package com.funnyland.funnylive.admin.endpoint.member.request;

import com.funnyland.funnylive.domain.member.AdminMember;
import com.funnyland.funnylive.domain.member.AdminRole;
import com.funnyland.funnylive.domain.member.AdminStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdminMemberRequest {
    private String email;
    private AdminRole role;
    private String password;
    private String passwordConfirm;

    private String association;
    private String rank;
    private String name;
    private String phone;
    private String ip;
    private AdminStatus state;
    private String memo;

    public AdminMember toDomain() {
        return AdminMember.builder()
                .email(this.email)
                .role(this.role)
                .password(this.password)
                .passwordConfirm(this.passwordConfirm)
                .association(this.association)
                .rank(this.rank)
                .name(this.name)
                .phone(this.phone)
                .ip(this.ip)
                .status(this.state)
                .memo(this.memo)
                .build();
    }

}
