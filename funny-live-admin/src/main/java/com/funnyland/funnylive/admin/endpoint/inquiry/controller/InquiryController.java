package com.funnyland.funnylive.admin.endpoint.inquiry.controller;

import java.util.List;

import com.funnyland.funnylive.admin.endpoint.inquiry.request.InquiryFilter;
import com.funnyland.funnylive.admin.endpoint.inquiry.request.InquiryReplyRequest;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.InquiryDetailResponse;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.InquiryResponse;
import com.funnyland.funnylive.admin.endpoint.inquiry.response.InquiryTypeResponse;
import com.funnyland.funnylive.admin.service.InquiryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name="온라인 문의", description = "온라인 문의 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/inquiries")
public class InquiryController {

    private final InquiryService inquiryService;

    @GetMapping
    @Operation(summary = "온라인 문의 리스트 조회", description = "온라인 문의 리스트 조회")
    public Page<InquiryResponse> getInquiries(InquiryFilter filter) {
        return inquiryService.getAll(filter);
    }

    @GetMapping("/types")
    @Operation(summary = "문의 타입 리스트 조회", description = "문의 타입 리스트 조회")
    public List<InquiryTypeResponse> getTypes() {
        return inquiryService.getTypes();
    }

    @Operation(summary = "문의 상세 조회", description = "문의 상세 조회")
    @GetMapping("/{inquiryId}")
    public InquiryDetailResponse getInquiry(@PathVariable Long inquiryId) {
        return inquiryService.get(inquiryId);
    }

    @Operation(summary = "문의 답변 추가", description = "문의 답변추가")
    @PostMapping("/{inquiryId}/reply")
    public InquiryDetailResponse createReply(@PathVariable Long inquiryId, @RequestBody InquiryReplyRequest request) {
        return inquiryService.createReply(inquiryId, request);
    }

    @Operation(summary = "문의 답변 수정", description = "문의 답변 수정")
    @PutMapping("/{inquiryId}/reply")
    public InquiryDetailResponse updateReply(@PathVariable Long inquiryId, @RequestBody InquiryReplyRequest request) {
        return inquiryService.updateReply(inquiryId, request);
    }
}
