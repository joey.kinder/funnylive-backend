package com.funnyland.funnylive.admin.repository;

import static com.funnyland.funnylive.domain.inquiry.QInquiryReply.inquiryReply;

import java.util.List;
import java.util.Optional;

import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryReply;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;


@Repository
public class InquiryReplyReadRepository extends QuerydslRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public InquiryReplyReadRepository(JPAQueryFactory jpaQueryFactory) {
        super(InquiryReply.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    public Optional<InquiryReply> findById(Long inquiryReplyId) {
        JPAQuery<InquiryReply> query = jpaQueryFactory
                .selectFrom(inquiryReply)
                .where(inquiryReply.id.eq(inquiryReplyId));

        return Optional.ofNullable(query.fetchOne());
    }
}
