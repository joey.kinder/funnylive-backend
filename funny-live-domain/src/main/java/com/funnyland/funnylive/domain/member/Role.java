package com.funnyland.funnylive.domain.member;

import lombok.Getter;


@Getter
public enum Role {
    ADMIN,
    USER
}
