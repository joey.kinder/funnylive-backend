package com.funnyland.funnylive.domain.cranegame;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryDomainRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class CraneGameDomainService {

    private final CraneGameDomainRepository craneGameDomainRepository;

    public CraneGame update(Long id, CraneGame updateCraneGame) {

        CraneGame result = craneGameDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("크레인을 찾을 수 없습니다. id : " + id))
                .edit(updateCraneGame);

        log.info("크레인 수정. id : {}, uuid: {}", result.getId(), result.getUuid());
        return result;
    }

    public CraneGame find(Long id) {
        CraneGame result = craneGameDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("크레인을 찾을 수 없습니다. id : " + id));

        log.info("크레인 조회. id : {}, uuid: {}", result.getId(), result.getUuid());
        return result;
    }

    public void delete(Long id) {
        CraneGame result = craneGameDomainRepository.findById(id)
                                                    .orElseThrow(() -> new DomainException("크레인을 찾을 수 없습니다. id : " + id));
        craneGameDomainRepository.delete(result);
        log.info("크레인 삭제. id : {}, uuid: {}", result.getId(), result.getUuid());
    }
}
