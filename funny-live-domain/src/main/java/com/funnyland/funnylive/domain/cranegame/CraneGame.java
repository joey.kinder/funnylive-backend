package com.funnyland.funnylive.domain.cranegame;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.cranegame.dto.PositionDto;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import com.funnyland.funnylive.domain.order.Order;
import com.funnyland.funnylive.domain.product.Product;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "crane_game")
@ToString
public class CraneGame {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false, unique = true)
    private String uuid;

    @Column(name = "description", length = 300)
    private String description;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "ip")
    private String ip;

    @OneToMany(mappedBy = "craneGame", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CraneGameProduct> craneGameProducts = new HashSet<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "game_state")
    private CraneGameState gameState;

    @Column(name = "play_fee")
    private Integer playFee;

    @Column(name = "timer")
    private Integer timer;


    @PrePersist
    public void prePersist() {
        if (this.gameState == null) {
            this.gameState = CraneGameState.READY;
        }
    }

    public void add(Set<Product> products) {
        if (this.craneGameProducts == null) {
            this.craneGameProducts = new HashSet<>();
        }

        Set<Long> newProductIds = products.stream()
                .map(Product::getId)
                .collect(Collectors.toSet());

        // 기존 제품 중 새로운 제품 ID 세트에 없는 제품 제거
        this.craneGameProducts.removeIf(craneGameProduct ->
                                                !newProductIds.contains(craneGameProduct.getProduct().getId()));

        // 새로운 제품 추가
        for (Product product : products) {
            if (this.craneGameProducts.stream()
                    .noneMatch(craneGameProduct -> craneGameProduct.getProduct().getId().equals(product.getId()))) {
                CraneGameProduct craneGameProduct = CraneGameProduct.builder()
                        .craneGame(this)
                        .product(product)
                        .build();
                this.craneGameProducts.add(craneGameProduct);
            }
        }
    }

    public CraneGame edit(CraneGame craneGame) {
        this.uuid = DomainUtil.update(this.uuid, craneGame.getUuid());
        this.description = DomainUtil.update(this.description, craneGame.getDescription());
        this.imageUrl = DomainUtil.update(this.imageUrl, craneGame.getImageUrl());
        this.craneGameProducts = DomainUtil.update(this.craneGameProducts, craneGame.getCraneGameProducts());
        this.gameState = DomainUtil.update(this.gameState, craneGame.getGameState());
        this.playFee = DomainUtil.update(this.playFee, craneGame.getPlayFee());
        this.timer = DomainUtil.update(this.timer, craneGame.getTimer());
        this.ip = DomainUtil.update(this.ip, craneGame.getIp());
        return this;
    }

    public void addImage(UploadDto uploadDto) {
        if (uploadDto != null) {
            this.imageUrl = uploadDto.getFilePath();
        }
    }

    public boolean start() {
        if(this.gameState == CraneGameState.READY) {
            this.gameState = CraneGameState.GAME_NORMAL;
            return true;
        }
        return false;
    }

    public boolean restart() {
        if(this.gameState == CraneGameState.GAME_NORMAL) {
            return true;
        }
        return false;
    }

    public boolean end() {
        if(this.gameState == CraneGameState.GAME_NORMAL) {
            this.gameState = CraneGameState.READY;

            return true;
        }
        return false;
    }

    @AllArgsConstructor
    @Getter
    public enum CraneGameState {
        READY("정상"),
        GAME_NORMAL("게임중"),    // 게임 중 (정상)
        GAME_ERROR("오류"),     // 오류 상태
        UNDER_INSPECTION("점검 중"); // 점검 중

        private String name;
    }
}
