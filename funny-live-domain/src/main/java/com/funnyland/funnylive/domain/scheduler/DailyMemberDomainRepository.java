package com.funnyland.funnylive.domain.scheduler;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;


public interface DailyMemberDomainRepository extends JpaRepository<DailyMember, Long> {
    boolean existsByDate(LocalDate date);
}
