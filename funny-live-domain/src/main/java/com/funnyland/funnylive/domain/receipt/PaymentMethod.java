package com.funnyland.funnylive.domain.receipt;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum PaymentMethod {
    CARD("카드결제");

    private String name;
}
