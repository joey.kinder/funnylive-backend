package com.funnyland.funnylive.domain.member;

import org.springframework.data.jpa.repository.JpaRepository;


public interface AdminMemberDomainRepository extends JpaRepository<AdminMember, Long> {
}
