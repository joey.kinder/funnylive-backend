package com.funnyland.funnylive.domain.point;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum PointTransactionType {
    CHARGE("포인트 충전", TransactionChange.INCREASE),
    REFUND("포인트 환불", TransactionChange.DECREASE),
    OPEN_MARKET_SALE("오픈마켓 판매", TransactionChange.INCREASE),
    ADMIN_GRANT("관리자 포인트 충전", TransactionChange.INCREASE),
    GAME_PLAY("게임 플레이", TransactionChange.DECREASE),
    SHIPPING_FEE("배송비", TransactionChange.DECREASE),
    OPEN_MARKET_PURCHASE("오픈마켓 구매", TransactionChange.DECREASE),
    DIRECT_PURCHASE("인기상품 직구 구매", TransactionChange.DECREASE),
    ADMIN_DEDUCTION("관리자 포인트 차감", TransactionChange.DECREASE);

    private final String description;
    private final TransactionChange change;

    public enum TransactionChange {
        INCREASE, DECREASE
    }
}
