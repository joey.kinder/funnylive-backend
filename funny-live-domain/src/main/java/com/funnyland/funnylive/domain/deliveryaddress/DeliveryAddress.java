package com.funnyland.funnylive.domain.deliveryaddress;

import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.member.Member;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "delivery_address")
public class DeliveryAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private Member member;

    private String name;
    private String phoneNumber;
    private String recipientName;
    private String roadNameAddress;
    private String inputDetail;
    private String referenceAddress;
    private String lotNumberAddress;
    private String postalCode;
    private boolean isDefault;

    public DeliveryAddress setMember(Member member) {
        this.member = member;
        return this;
    }

    public void update(String roadNameAddress, String inputDetail, String postalCode) {
        this.roadNameAddress = DomainUtil.update(this.roadNameAddress, roadNameAddress);
        this.inputDetail = DomainUtil.update(this.inputDetail, inputDetail);
        this.postalCode = DomainUtil.update(this.postalCode, postalCode);
    }

}
