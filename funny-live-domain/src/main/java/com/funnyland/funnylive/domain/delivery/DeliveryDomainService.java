package com.funnyland.funnylive.domain.delivery;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.InquiryDomainRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class DeliveryDomainService {

    private final DeliveryDomainRepository deliveryDomainRepository;

    public Delivery save(Delivery delivery) {
        Delivery result = deliveryDomainRepository.save(delivery);
        log.info("배송 저장. id : {}, name: {}", result.getId(), result.getRecipientName());
        return result;
    }

    public Delivery find(Long deliveryId) {
        Delivery result = deliveryDomainRepository.findById(deliveryId)
                                              .orElseThrow(() -> new DomainException("배송을 찾을 수 없습니다. id : " + deliveryId));
        return result;
    }

    public Delivery update(Long id, Delivery updateDelivery) {

        Delivery result = deliveryDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("배송을 찾을 수 없습니다. id : " + id))
                .edit(updateDelivery);

        log.info("배송 수정. id : {}, name: {}", result.getId(), result.getRecipientName());
        return result;
    }
}
