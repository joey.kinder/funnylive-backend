package com.funnyland.funnylive.domain.notice;

import com.funnyland.funnylive.domain.livebroadcast.LiveBroadcast;
import org.springframework.data.jpa.repository.JpaRepository;


public interface NoticeDomainRepository extends JpaRepository<Notice, Long> {
}
