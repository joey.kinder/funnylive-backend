package com.funnyland.funnylive.domain.point;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PointTransactionDomainRepository extends JpaRepository<PointTransaction, Long> {

    Page<PointTransaction> findAllById(Long id, Pageable pagable);
}
