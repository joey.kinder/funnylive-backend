package com.funnyland.funnylive.domain.member;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public enum OAuthProvider {
    kakao("카카오"), naver("네이버"), google("구글");

    private String name;

}
