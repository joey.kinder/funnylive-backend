package com.funnyland.funnylive.domain.member;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.cart.CartItem;
import com.funnyland.funnylive.domain.delivery.Delivery;
import com.funnyland.funnylive.domain.deliveryaddress.DeliveryAddress;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.point.PointTransaction;
import com.funnyland.funnylive.domain.receipt.Receipt;
import com.funnyland.funnylive.domain.storagebox.StorageBox;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "member")
public class Member extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String oauthId;
    @Enumerated(EnumType.STRING)
    private Role role;
    @Enumerated(EnumType.STRING)
    private Grade grade;
    private String name;
    @Column(unique = true)
    private String email;
    @Column(unique = true)
    private String nickName;
    private String phone;

    @Enumerated(EnumType.STRING)
    private MemberStatus status;

    private String withdrawReason;

    private LocalDateTime visitedTime;

    @Enumerated(EnumType.STRING)
    private OAuthProvider oAuthProvider;

    private boolean agreeReceiveEmail;
    private boolean agreeReceivePhone;

    @Builder.Default
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<DeliveryAddress> deliveryAddresses = new HashSet<>();

    @OneToOne(mappedBy = "member", cascade = CascadeType.ALL)
    private StorageBox storageBox;

    @Builder.Default
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Inquiry> inquiries = new HashSet<>();

    @Builder.Default
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Delivery> deliveries = new HashSet<>();


    @Builder.Default
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Receipt> receipts = new HashSet<>();

    @Builder.Default
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PointTransaction> pointTransactions = new HashSet<>();

    @PrePersist
    public void prePersist() {
        if (this.status == null) {
            this.status = MemberStatus.ACTIVE;
        }
        if (this.grade == null) {
            this.grade = Grade.BRONZE;
        }
    }

    public Member update(String userId, String nickname) {
        this.email = userId;
        this.nickName = nickname;
        return this;
    }

    public Member edit(Member member) {
        this.name = DomainUtil.update(this.name, member.getName());
        this.phone = DomainUtil.update(this.phone, member.getPhone());
        this.email = DomainUtil.update(this.email, member.getEmail());
        this.nickName = DomainUtil.update(this.nickName, member.getNickName());
        this.agreeReceiveEmail = DomainUtil.update(this.agreeReceiveEmail, member.isAgreeReceiveEmail());
        this.agreeReceivePhone = DomainUtil.update(this.agreeReceivePhone, member.isAgreeReceivePhone());
        this.status = DomainUtil.update(this.status, member.getStatus());
        updateDeliveryAddress(member.getDeliveryAddresses());
        return this;
    }

    public void addDeliveryAddress(DeliveryAddress deliveryAddress) {
        deliveryAddress.setMember(this);
        this.deliveryAddresses.add(deliveryAddress);
    }

    private void updateDeliveryAddress(Set<DeliveryAddress> newAddresses) {
        if (newAddresses == null || newAddresses.isEmpty()) {
            return; // 새 주소가 없는 경우 업데이트하지 않음
        }

        DeliveryAddress newAddress = newAddresses.iterator().next(); // 새로운 기본 주소

        // 기본 주소 찾기
        Optional<DeliveryAddress> defaultAddressOpt = this.deliveryAddresses.stream()
                .filter(DeliveryAddress::isDefault)
                .findFirst();

        if (defaultAddressOpt.isPresent()) {
            // 기본 주소 업데이트
            DeliveryAddress defaultAddress = defaultAddressOpt.get();
            defaultAddress.update(newAddress.getRoadNameAddress(), newAddress.getInputDetail(), newAddress.getPostalCode());
        } else {
            // 새 기본 주소 추가
            newAddress.setMember(this);
            this.deliveryAddresses.add(newAddress);
        }
    }


    public Member delete(String withdrawReason) {
        this.name = null;
        this.phone = null;
        this.email = null;
        this.nickName = null;
        this.status = MemberStatus.WITHDRAW;
        this.deliveryAddresses.clear();
        this.withdrawReason = withdrawReason;
        return this;
    }

    public void saveLoginTime() {
        this.visitedTime = LocalDateTime.now();
    }

    @Getter
    @AllArgsConstructor
    public enum MemberStatus {
        ACTIVE("정상"),
        INACTIVE("차단"),
        WITHDRAW("탈퇴");

        private String name;
    }

    @Getter
    @AllArgsConstructor
    public enum Grade {
        BRONZE("브론즈"),
        SILVER("실버"),
        GOLD("골드");

        private String name;
    }
}
