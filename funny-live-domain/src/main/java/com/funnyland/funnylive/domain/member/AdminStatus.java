package com.funnyland.funnylive.domain.member;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public enum AdminStatus {
    NORMAL("정상"),
    SUSPENDED("중지");

    private String name;

}
