package com.funnyland.funnylive.domain.receipt;

import java.time.LocalDateTime;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.member.Member;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "receipt")
public class Receipt extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String orderId;
    private String packageName;
    private String productId;
    private String purchaseToken;
    private LocalDateTime purchaseTime;
    private Integer quantity;
    private Integer chargeAmount; //충전금액
    private Integer paymentAmount; //결제금액

    @Setter
    private boolean acknowledged;

    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @Setter
    @Enumerated(EnumType.STRING)
    private ReceiptState state;

    private String memo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private Member member;

    public Receipt edit(Receipt receipt) {
        this.memo = DomainUtil.update(this.memo, receipt.getMemo());
        return this;
    }
}
