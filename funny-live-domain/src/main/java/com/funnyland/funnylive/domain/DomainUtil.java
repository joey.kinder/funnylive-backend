package com.funnyland.funnylive.domain;

import lombok.NonNull;
import org.springframework.util.StringUtils;


public final class DomainUtil {

    private DomainUtil() {
        throw new AssertionError();
    }

    /**
     * updateDomainColumn 컬럼이 null 아 아니면 updateDomainColumn 제공
     *
     * @param <T> entity column type
     * @param currentEntityColumn 현재 entity 컬럼
     * @param updateEntityColumn  업데이트할 entity 컬럼
     * */
    public static <T> T update(T currentEntityColumn, T updateEntityColumn) {
        if(updateEntityColumn == null) {
            return currentEntityColumn;
        }

        return updateEntityColumn;
    }

    /**
     * currentDomainColumn 값이 null 이면 defaultValue 제공
     *
     * @param <T> entity column type
     * @param currentEntityColumn 현재 entity 컬럼
     * @param defaultValue        기본값 (필수)
     * */
    public static <T> T defaultValue(T currentEntityColumn, @NonNull T defaultValue) {
        if(currentEntityColumn == null) {
            return defaultValue;
        }

        return currentEntityColumn;
    }

    /**
     * @param str str 내용
     * @param size 자를 사이즈
     * @return msg 길이를 size 만큼 substring
     * */
    public static String cutStr(final String str, int size) {
        if(StringUtils.hasText(str)) {
            return null;
        }
        if(str.length() < size) {
            return str;
        }
        return str.substring(0, size);
    }
}
