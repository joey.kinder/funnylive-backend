package com.funnyland.funnylive.domain.inquiry;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InquiryDomainRepository extends JpaRepository<Inquiry, Long> {

    Page<Inquiry> findAllById(Long id, Pageable pagable);
}
