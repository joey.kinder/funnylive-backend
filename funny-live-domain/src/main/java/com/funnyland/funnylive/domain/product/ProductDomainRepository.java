package com.funnyland.funnylive.domain.product;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductDomainRepository extends JpaRepository<Product, Long> {
}
