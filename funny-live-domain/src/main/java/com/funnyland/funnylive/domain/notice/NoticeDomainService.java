package com.funnyland.funnylive.domain.notice;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.domain.inquiry.InquiryType;
import com.funnyland.funnylive.domain.member.Member;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class NoticeDomainService {

    private final NoticeDomainRepository noticeDomainRepository;

    public Notice find(Long noticeId) {
        Notice result = noticeDomainRepository.findById(noticeId)
                                              .orElseThrow(() -> new DomainException("공지사항을 찾을 수 없습니다. id : " + noticeId));
        return result;
    }

    public Notice save(Notice notice) {
        Notice result = noticeDomainRepository.save(notice);
        log.info("공지사항 저장. id : {}, name: {}", result.getId(), result.getTitle());
        return result;
    }

    public Notice update(Long id, Notice updateNotice) {

        Notice result = noticeDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("공지사항을 찾을 수 없습니다. id : " + id))
                .edit(updateNotice);

        log.info("공지사항 수정. id : {}, name: {}", result.getId(), result.getTitle());
        return result;
    }

    public void delete(Long noticeId) {
        Notice result = noticeDomainRepository.findById(noticeId)
                .orElseThrow(() -> new DomainException("공지사항을 찾을 수 없습니다. id : " + noticeId));
        noticeDomainRepository.delete(result);
        log.info("공지사항 삭제. id : {}, name: {}", result.getId(), result.getTitle());
    }
}
