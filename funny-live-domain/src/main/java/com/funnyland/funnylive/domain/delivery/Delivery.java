package com.funnyland.funnylive.domain.delivery;

import java.util.ArrayList;
import java.util.List;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.member.Member;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "delivery")
public class Delivery extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "member_id")
    private Member member;

    private String orderNumber;
    private String roadNameAddress;
    private String inputDetail;
    private String postalCode;
    private String recipientName;
    private String recipientPhone;

    private String invoiceCompanyName;
    private String invoiceNumber;

    @Enumerated(EnumType.STRING)
    private DeliveryStatus status;
    private String requirement;

    private int deliveryFee;
    private String memo;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "delivery_id")
    private List<DeliveryItem> items = new ArrayList<>();

    public void addDeliveryItem(DeliveryItem item) {
        if (this.items == null) {
            this.items = new ArrayList<>();
        }
        this.items.add(item);
        item.setDelivery(this);
    }

    public Delivery edit(Delivery delivery) {
        if(status != DeliveryStatus.CANCELED) {
            this.status = DomainUtil.update(this.status, delivery.getStatus());
        }
        this.invoiceCompanyName = DomainUtil.update(this.invoiceCompanyName, delivery.getInvoiceCompanyName());
        this.invoiceNumber = DomainUtil.update(this.invoiceNumber, delivery.getInvoiceNumber());
        this.memo = DomainUtil.update(this.memo, delivery.getMemo());
        return this;
    }

    @AllArgsConstructor
    @Getter
    public enum DeliveryStatus {
        PENDING("배송대기"),
        SHIPPED("배송중"),
        DELIVERED("배달완료"),
        CANCELED("배송취소");

        private String name;
    }
}
