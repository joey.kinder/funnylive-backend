package com.funnyland.funnylive.domain.term;

import com.funnyland.funnylive.DomainException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class TermDomainService {

    private final TermDomainRepository termDomainRepository;

    public Term save(Term term) {
        Term result = termDomainRepository.save(term);
        log.info("약관 저장. id : {}, name: {}", result.getId(), result.getTitle());
        return result;
    }

    public Term update(Long id, Term updateTerm) {

        Term result = termDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("약관를 찾을 수 없습니다. id : " + id))
                .edit(updateTerm);

        log.info("약관 수정. id : {}, name: {}", result.getId(), result.getTitle());
        return result;
    }
}
