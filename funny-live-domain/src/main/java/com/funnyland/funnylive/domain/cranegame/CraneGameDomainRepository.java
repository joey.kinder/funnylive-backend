package com.funnyland.funnylive.domain.cranegame;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CraneGameDomainRepository extends JpaRepository<CraneGame, Long> {

    Page<CraneGame> findAllById(Long id, Pageable pagable);
}
