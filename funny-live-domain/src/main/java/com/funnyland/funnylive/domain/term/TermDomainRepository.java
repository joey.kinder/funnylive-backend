package com.funnyland.funnylive.domain.term;

import org.springframework.data.jpa.repository.JpaRepository;


public interface TermDomainRepository extends JpaRepository<Term, Long> {
}
