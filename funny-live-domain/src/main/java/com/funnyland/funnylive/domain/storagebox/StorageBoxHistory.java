package com.funnyland.funnylive.domain.storagebox;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.product.Product;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "storage_box_history")
public class StorageBoxHistory extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "origin_product_id")
    private Product originProduct;

    private Long productId;
    private String productName;
    private Integer productPrice;
    private String productImageURL;

    @ManyToOne
    @JoinColumn(name = "storage_box_id")
    private StorageBox storageBox;

    @Enumerated(EnumType.STRING)
    private AcquisitionMethod acquisitionMethod;

    @Setter
    private boolean deliveryRequested = false;
}
