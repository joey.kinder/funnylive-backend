package com.funnyland.funnylive.domain.livebroadcast;

import com.funnyland.funnylive.DomainException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class LiveBroadcastDomainService {

    private final LiveBroadcastDomainRepository liveBroadCastDomainRepository;

    public LiveBroadcast save(LiveBroadcast liveBroadcast) {
        LiveBroadcast result = liveBroadCastDomainRepository.save(liveBroadcast);
        log.info("라이브방송 저장. id : {}, name: {}", result.getId(), result.getTitle());
        return result;
    }

    public LiveBroadcast update(Long id, LiveBroadcast updateLiveBroadcast) {

        LiveBroadcast result = liveBroadCastDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("라이브방송을 찾을 수 없습니다. id : " + id))
                .edit(updateLiveBroadcast);

        log.info("라이브방송 수정. id : {}, name: {}", result.getId(), result.getTitle());
        return result;
    }
}
