package com.funnyland.funnylive.domain.cart;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.storagebox.StorageBox;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cart_item")
public class CartItem extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "member_id")
    @Setter
    private Member member;

    @ManyToOne
    @JoinColumn(name = "origin_product_id")
    private Product originProduct;

    private Long productId;
    private String productName;
    private Integer productPrice;
    private String productImageURL;

    @Setter
    private int quantity;

    public CartItem(Member member) {
        this.member = member;
    }

    public CartItem edit(Integer quantity) {
        this.quantity = DomainUtil.update(this.quantity, quantity);
        return this;
    }

//    // CartItem 생성시 StorageBox의 상품 데이터를 복사하여 저장
//    public static CartItem createFromStorageBox(StorageBox storageBox, int quantity) {
//        return CartItem.builder()
//                .member(storageBox.getMember())
//                .originProduct(storageBox.getOriginProduct())
//                .quantity(quantity)
//                .productId(storageBox.getOriginProduct().getId())
//                .productName(storageBox.getOriginProduct().getName())
//                .productPrice(storageBox.getOriginProduct().getPrice())
//                .productImageURL(storageBox.getOriginProduct().getImageURL())
//                .build();
//    }
}
