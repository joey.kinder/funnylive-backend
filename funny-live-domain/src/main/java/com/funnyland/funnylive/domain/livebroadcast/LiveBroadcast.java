package com.funnyland.funnylive.domain.livebroadcast;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "live_broadcast")
public class LiveBroadcast extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String url;
    private boolean active;

    public LiveBroadcast edit(LiveBroadcast updateLiveBroadcast) {
        this.title = DomainUtil.update(this.title, updateLiveBroadcast.getTitle());
        this.url = DomainUtil.update(this.url, updateLiveBroadcast.getUrl());
        return this;
    }
}