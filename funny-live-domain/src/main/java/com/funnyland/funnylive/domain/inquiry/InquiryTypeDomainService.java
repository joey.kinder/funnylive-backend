package com.funnyland.funnylive.domain.inquiry;

import com.funnyland.funnylive.DomainException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class InquiryTypeDomainService {

    private final InquiryTypeDomainRepository inquiryTypeDomainRepository;

    public InquiryType save(InquiryType inquiryType) {
        InquiryType result = inquiryTypeDomainRepository.save(inquiryType);
        log.info("문의 타입 저장. id : {}, name: {}", result.getId(), result.getName());
        return result;
    }

    public InquiryType find(Long inquiryTypeId) {
        InquiryType result = inquiryTypeDomainRepository.findById(inquiryTypeId)
                                              .orElseThrow(() -> new DomainException("문의 타입을 찾을 수 없습니다. id : " + inquiryTypeId));
        return result;
    }

    public InquiryType update(Long id, InquiryType updateInquiryType) {

        InquiryType result = inquiryTypeDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("문의 타입을 찾을 수 없습니다. id : " + id))
                .edit(updateInquiryType);

        log.info("문의 타입 수정. id : {}, name: {}", result.getId(), result.getName());
        return result;
    }

    public void delete(Long inquiryTypeId) {
        InquiryType result = inquiryTypeDomainRepository.findById(inquiryTypeId)
                .orElseThrow(() -> new DomainException("문의 타입을 찾을 수 없습니다. id : " + inquiryTypeId));
        inquiryTypeDomainRepository.delete(result);
        log.info("문의 타입 삭제. id : {}, name: {}", result.getId(), result.getName());
    }
}
