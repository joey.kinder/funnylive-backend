package com.funnyland.funnylive.domain.product;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product")
public class Product extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer price;
    private String imageURL;
    @Enumerated(EnumType.STRING)
    private ProductStatus status;
    private boolean active;

    //TODO: 판매자


    private String productType; // "popular" 또는 "open_market" 등

    @PrePersist
    public void prePersist() {
        if (this.status == null) {
            this.status = ProductStatus.AVAILABLE;
        }
    }

    public Product edit(Product product) {
        this.name = DomainUtil.update(this.name, product.getName());
        this.price = DomainUtil.update(this.price, product.getPrice());
        this.imageURL = DomainUtil.update(this.imageURL, product.getImageURL());
        this.status = DomainUtil.update(this.status, product.getStatus());
        return this;
    }

    public Product active() {
        this.active = true;
        return this;
    }

    public Product inActive() {
        this.active = false;
        return this;
    }

    public void addImage(UploadDto uploadDto) {
        this.imageURL = uploadDto.getFilePath();
    }

    @AllArgsConstructor
    @Getter
    public enum ProductStatus {
        AVAILABLE("정상"),       // 정상, 사용 가능
        SUSPENDED("중지"),       // 중지, 일시 중지
        OUT_OF_STOCK("품절");     // 품절

        private String name;
    }
}
