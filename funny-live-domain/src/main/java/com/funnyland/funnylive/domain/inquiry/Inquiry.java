package com.funnyland.funnylive.domain.inquiry;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.inquiry.dto.UploadDto;
import com.funnyland.funnylive.domain.member.Member;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "inquiry")
public class Inquiry extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String content;

    @Enumerated(EnumType.STRING)
    private InquiryStatus status = InquiryStatus.PENDING;

    @ManyToOne
    @Setter
    @JoinColumn(name = "inquiry_type_id")
    private InquiryType type;

    @ManyToOne
    private Member member;

    @Column
    private String fileUrl;

    @Column
    private String fileName;

    @OneToOne(mappedBy = "inquiry", cascade = CascadeType.ALL)
    private InquiryReply reply;

    @PrePersist
    public void prePersist() {
        if (this.status == null) {
            this.status = InquiryStatus.PENDING;
        }
    }

    public Inquiry edit(Inquiry inquiry) {
        this.content = DomainUtil.update(this.content, inquiry.getContent());
        this.title = DomainUtil.update(this.title, inquiry.getTitle());
        this.fileUrl = DomainUtil.update(this.fileUrl, inquiry.getFileUrl());
        this.fileName = DomainUtil.update(this.fileName, inquiry.getFileName());
        this.type = DomainUtil.update(this.type, inquiry.getType());
        return this;
    }

    public void addFile(UploadDto uploadDto) {
        if (uploadDto != null) {
            this.fileUrl = uploadDto.getFilePath();
            this.fileName = uploadDto.getFileName();
        }
    }

    public Inquiry addReply(InquiryReply saveInquiryReply) {
        InquiryReply inquiryReply = InquiryReply.builder()
                .title(saveInquiryReply.getTitle())
                .content(saveInquiryReply.getContent())
                .inquiry(this)
                .build();
        this.reply = inquiryReply;
        this.status = InquiryStatus.ANSWERED;
        return this;
    }

    public Inquiry updateReply(InquiryReply newReply) {
        if (this.reply == null) {
            this.reply = newReply;
        } else {
            this.reply.edit(newReply);
        }
        this.status = InquiryStatus.ANSWERED;
        return this;
    }

    public Inquiry addMember(Member member) {
        this.member = member;
        return this;
    }

    @Getter
    @AllArgsConstructor
    public enum InquiryStatus {
        PENDING("답변대기"),
        ANSWERED("답변완료");

        private String name;
    }
}
