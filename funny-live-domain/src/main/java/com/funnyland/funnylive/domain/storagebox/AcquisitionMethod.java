package com.funnyland.funnylive.domain.storagebox;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum AcquisitionMethod {
    DRAW("뽑기"),
    PURCHASE("구매"),
    TRADE("거래"),
    DELIVERY("배송요청");

    private String name;

}
