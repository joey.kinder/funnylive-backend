package com.funnyland.funnylive.domain.storagebox;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.member.Member;
import com.funnyland.funnylive.domain.product.Product;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "storage_box")
public class StorageBox extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "member_id")
    private Member member;

    @OneToMany(mappedBy = "storageBox", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StorageBoxHistory> histories;

    public StorageBox(Member member) {
        this.member = member;
    }

    public void addHistory(Product product, AcquisitionMethod method) {
        StorageBoxHistory history = StorageBoxHistory.builder()
                .storageBox(this)
                .originProduct(product)
                .productId(product.getId())
                .productName(product.getName())
                .productPrice(product.getPrice())
                .productImageURL(product.getImageURL())
                .acquisitionMethod(method)
                .deliveryRequested(false)
                .build();

        if (this.histories == null) {
            this.histories = new ArrayList<>();
        }
        this.histories.add(history);
    }

    // StorageBox - 상품 추가 및 이력 추가 메서드
    public StorageBox addProduct(Member member, Product product, AcquisitionMethod method) {
        this.member = member;
        addHistory(product, method); // 이력 추가
        return this;
    }

//    public void processDelivery(Long productId) {
//        List<StorageBoxHistory> toRemove = new ArrayList<>();
//
//        for (StorageBoxHistory history : this.histories) {
//            if (history.getProductId().equals(productId) && !history.isDeliveryRequested()) {
//                history.setDeliveryRequested(true);
//                toRemove.add(history);
//            }
//        }
//
//        // 여기서 toRemove 리스트에 있는 항목들을 this.histories에서 제외합니다.
//        this.histories.removeAll(toRemove);
//    }

    public List<StorageBoxHistory> processDelivery(Long productId, int quantity) {
        List<StorageBoxHistory> selectedHistories = new ArrayList<>();
        for (StorageBoxHistory history : this.histories) {
            if (history.getProductId().equals(productId) && !history.isDeliveryRequested() && selectedHistories.size() < quantity) {
                history.setDeliveryRequested(true);
                selectedHistories.add(history);
            }
        }
        return selectedHistories;
    }
}




