package com.funnyland.funnylive.domain.member;

import java.util.List;

import com.funnyland.funnylive.DomainException;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class MemberDomainService {


    private final MemberDomainRepository memberDomainRepository;
    private final EntityManager entityManager;

    public Member find(Long memberId) {
        Member result = memberDomainRepository.findById(memberId)
                                              .orElseThrow(() -> new DomainException("사용자를 찾을 수 없습니다. id : " + memberId));
        return result;
    }
    public List<Member> list() {
        List<Member> result = memberDomainRepository.findAll();
        return result;
    }

    public Member update(Long id, Member updateMember) {
        Member result = memberDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("사용자를 찾을 수 없습니다. id : " + id))
                .edit(updateMember);
        log.info("회원 가입 완료. id : {}, name: {}", result.getId(), result.getName());
        return result;
    }

    public Member save(Member member) {
        // 멤버가 영속 상태인지 확인
        boolean isPersistent = entityManager.contains(member);
        log.info("Saving member with ID: {}, IsPersistent: {}", member.getId(), isPersistent);

        Member savedMember = memberDomainRepository.save(member);

        // 저장 후 멤버가 영속 상태인지 확인
        isPersistent = entityManager.contains(savedMember);
        log.info("Saving member with ID: {}, IsPersistent: {}", savedMember.getId(), isPersistent);

        return savedMember;
    }

    public void delete(Long id, String withdrawReason) {
        Member result = memberDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("사용자를 찾을 수 없습니다. id : " + id))
                .delete(withdrawReason);
        log.info("회원 탈퇴 완료. id : {}, name: {}", result.getId(), result.getName());
    }
}
