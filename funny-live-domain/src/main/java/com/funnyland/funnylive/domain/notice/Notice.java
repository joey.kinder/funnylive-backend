package com.funnyland.funnylive.domain.notice;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.member.AdminMember;
import com.funnyland.funnylive.domain.member.Member;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "notice")
public class Notice extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String content;

    @Enumerated(EnumType.STRING)
    private NoticeType noticeType;

    private Long viewCount;

    @Setter
    @ManyToOne
    @JoinColumn(name = "admin_member_id")
    private AdminMember adminMember;

    @PrePersist
    public void prePersist() {
        if (this.viewCount == null) {
            this.viewCount = 0L;
        }
    }

    public Notice edit(Notice notice) {
        this.title = DomainUtil.update(this.title, notice.getTitle());
        this.content = DomainUtil.update(this.content, notice.getContent());
        this.noticeType = DomainUtil.update(this.noticeType, notice.getNoticeType());
        return this;
    }

    @Getter
    @AllArgsConstructor
    public enum NoticeType {
        INFORM("안내"),
        EVENT("이벤트"),
        UPDATE("업데이트");

        private String name;
    }
}
