package com.funnyland.funnylive.domain.delivery;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.product.Product;
import com.funnyland.funnylive.domain.storagebox.StorageBoxHistory;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "delivery_item")
public class DeliveryItem extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "origin_product_id")
    private Product originProduct;

    private Long productId;
    private String productName;
    private Integer productPrice;
    private String productImageURL;

    private int quantity;

    @ManyToOne
    @JoinColumn(name = "delivery_id")
    @Setter
    private Delivery delivery;

    @OneToOne
    @JoinColumn(name = "storage_box_history_id")
    private StorageBoxHistory storageBoxHistory;
}
