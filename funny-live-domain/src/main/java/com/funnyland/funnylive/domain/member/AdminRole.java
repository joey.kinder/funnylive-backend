package com.funnyland.funnylive.domain.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AdminRole {
    NORMAL("일반"),
    MASTER("마스터");

    private String name;
}
