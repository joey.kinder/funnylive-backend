package com.funnyland.funnylive.domain.term;

import com.funnyland.funnylive.domain.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "term")
public class Term extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 65535, nullable = false)
    private String title;

    @Column(length = 65535, nullable = false)
    private String content;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TermType type;

    @Column(nullable = false)
    private Long viewCount;


    @PrePersist
    public void prePersist() {
        if (this.viewCount == null) {
            this.viewCount = 0L;
        }
    }

    public Term edit(Term term) {
        this.title = term.getTitle();
        this.content = term.getContent();
        this.type = term.getType();
        return this;
    }

    public void increaseViewCount() {
        this.viewCount++;
    }

    @Getter
    @AllArgsConstructor
    public enum TermType {
        PRIVACY_POLICY("개인정보처리방침"),
        TERMS_OF_USE("이용약관");

        private String name;
    }

}
