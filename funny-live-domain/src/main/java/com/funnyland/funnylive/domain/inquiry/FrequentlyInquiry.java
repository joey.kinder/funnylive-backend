package com.funnyland.funnylive.domain.inquiry;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.member.AdminMember;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "frequently_inquiry")
public class FrequentlyInquiry extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String question;

    @Column(nullable = false)
    private String answer;

    private Long viewCount;

    @ManyToOne
    @Setter
    @JoinColumn(name = "inquiry_type_id")
    private InquiryType type;

    @Setter
    @ManyToOne
    @JoinColumn(name = "admin_member_id")
    private AdminMember adminMember;


    public FrequentlyInquiry edit(FrequentlyInquiry frequentlyInquiry) {
        this.question = DomainUtil.update(this.question, frequentlyInquiry.getQuestion());
        this.answer = DomainUtil.update(this.answer, frequentlyInquiry.getAnswer());
        this.type = DomainUtil.update(this.type, frequentlyInquiry.getType());

        return this;
    }
}
