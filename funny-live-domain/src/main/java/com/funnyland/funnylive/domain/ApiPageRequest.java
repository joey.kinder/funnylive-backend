package com.funnyland.funnylive.domain;

import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;


@NoArgsConstructor
@Setter
@Getter
public class ApiPageRequest {

    static final int DEFAULT_PAGE_SIZE = 20;

    @Min(1)
    private Integer page = 1;

    @Min(0)
    private Integer size = DEFAULT_PAGE_SIZE;

    public PageRequest getPageRequest() {
        return PageRequest.of(this.page - 1, this.size);
    }
}
