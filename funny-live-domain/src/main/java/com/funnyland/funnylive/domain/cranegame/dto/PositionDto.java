package com.funnyland.funnylive.domain.cranegame.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PositionDto {
    private Integer x;
    private Integer y;
}
