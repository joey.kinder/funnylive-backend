package com.funnyland.funnylive.domain.member;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "admin_member")
public class AdminMember extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;

    @Enumerated(EnumType.STRING)
    private AdminRole role;
    private String password;
    private String passwordConfirm;

    private String association;

    @Column(name = "`rank`") // 백틱 사용
    private String rank;
    private String name;
    private String phone;
    private String ip;
    @Enumerated(EnumType.STRING)
    private AdminStatus status;
    private String memo;


    public AdminMember edit(AdminMember updateAdminMember) {
        this.role = DomainUtil.update(this.role, updateAdminMember.getRole());
        this.password = DomainUtil.update(this.password, updateAdminMember.getPassword());
        this.passwordConfirm = DomainUtil.update(this.passwordConfirm, updateAdminMember.getPasswordConfirm());
        this.association = DomainUtil.update(this.association, updateAdminMember.getAssociation());
        this.rank = DomainUtil.update(this.rank, updateAdminMember.getRank());
        this.name = DomainUtil.update(this.name, updateAdminMember.getName());
        this.phone = DomainUtil.update(this.phone, updateAdminMember.getPhone());
        this.ip = DomainUtil.update(this.ip, updateAdminMember.getIp());
        this.status = DomainUtil.update(this.status, updateAdminMember.getStatus());
        this.memo = DomainUtil.update(this.memo, updateAdminMember.getMemo());
        return this;
    }
}
