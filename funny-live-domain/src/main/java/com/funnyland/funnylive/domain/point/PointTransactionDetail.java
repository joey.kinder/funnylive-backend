package com.funnyland.funnylive.domain.point;

import java.time.LocalDate;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.member.Member;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "point_transaction_detail")
public class PointTransactionDetail extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private PointTransactionType transactionType;

    @Enumerated(EnumType.STRING)
    private PointTransactionType.TransactionChange transactionChange;

    private Integer amount = 0;

    @ManyToOne
    @JoinColumn(name = "point_transaction_id")
    private PointTransaction pointTransaction;

    @ManyToOne
    @JoinColumn(name = "save_point_transaction_id")
    private PointTransaction savePointTransaction;

    @ManyToOne
    @JoinColumn(name = "member_id")
    private Member member;

}
