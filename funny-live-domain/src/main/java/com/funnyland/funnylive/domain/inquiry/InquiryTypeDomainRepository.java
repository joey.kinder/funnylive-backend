package com.funnyland.funnylive.domain.inquiry;

import org.springframework.data.jpa.repository.JpaRepository;


public interface InquiryTypeDomainRepository extends JpaRepository<InquiryType, Long> {

}
