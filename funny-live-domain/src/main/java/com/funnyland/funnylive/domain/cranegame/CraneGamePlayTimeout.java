package com.funnyland.funnylive.domain.cranegame;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("CraneGamePlayTimeout")
@Builder
public class CraneGamePlayTimeout implements Serializable {

    @Serial
    private static final long serialVersionUID = 1234567891357924L;

    @Id
    private Long id; // 게임 기기의 고유 ID

    private LocalDateTime timeoutTime; // 게임 타임아웃 시간

    @TimeToLive
    private Long expiration;


    public static CraneGamePlayTimeout of(Long id, LocalDateTime timeoutTime, Long timeoutSecond) {
        return CraneGamePlayTimeout.builder()
                               .id(id)
                               .timeoutTime(timeoutTime)
                                .expiration(timeoutSecond)
                               .build();
    }
}
