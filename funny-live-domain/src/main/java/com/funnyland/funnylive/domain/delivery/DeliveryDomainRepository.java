package com.funnyland.funnylive.domain.delivery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DeliveryDomainRepository extends JpaRepository<Delivery, Long> {

    Page<Delivery> findAllById(Long id, Pageable pagable);
}
