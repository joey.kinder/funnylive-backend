package com.funnyland.funnylive.domain.inquiry;

import org.springframework.data.jpa.repository.JpaRepository;


public interface FrequentlyInquiryDomainRepository extends JpaRepository<FrequentlyInquiry, Long> {
}
