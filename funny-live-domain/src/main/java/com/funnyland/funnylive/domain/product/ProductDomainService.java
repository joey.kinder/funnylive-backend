package com.funnyland.funnylive.domain.product;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.domain.inquiry.Inquiry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class ProductDomainService {

    private final ProductDomainRepository productDomainRepository;

    public Product save(Product product) {
        Product result = productDomainRepository.save(product);
        log.info("상품 저장. id : {}, name: {}", result.getId(), result.getName());
        return result;
    }

    public Product update(Long id, Product updateProduct) {

        Product result = productDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("상품을 찾을 수 없습니다. id : " + id))
                .edit(updateProduct);

        log.info("상품 수정. id : {}, name: {}", result.getId(), result.getName());
        return result;
    }

    public Product find(Long productId) {
        Product result = productDomainRepository.findById(productId)
                .orElseThrow(() -> new DomainException("상품을 찾을 수 없습니다. id : " + productId));
        return result;
    }

    public void delete(Long productId) {
        Product result = productDomainRepository.findById(productId)
                                                .orElseThrow(() -> new DomainException("상품을 찾을 수 없습니다. id : " + productId));
        productDomainRepository.delete(result);
        log.info("상품 삭제. id : {}, name: {}", result.getId(), result.getName());
    }
}
