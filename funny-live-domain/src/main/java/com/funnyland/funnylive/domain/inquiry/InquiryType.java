package com.funnyland.funnylive.domain.inquiry;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.DomainUtil;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "inquiry_type")
public class InquiryType extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    public InquiryType edit(InquiryType inquiryType) {
        this.name = DomainUtil.update(this.name, inquiryType.getName());
        return this;
    }
}
