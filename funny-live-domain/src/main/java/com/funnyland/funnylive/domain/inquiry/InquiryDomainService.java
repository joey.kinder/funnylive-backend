package com.funnyland.funnylive.domain.inquiry;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.domain.member.Member;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class InquiryDomainService {

    private final InquiryDomainRepository inquiryDomainRepository;

    public Inquiry save(Inquiry inquiry) {
        Inquiry result = inquiryDomainRepository.save(inquiry);
        log.info("문의 저장. id : {}, name: {}", result.getId(), result.getTitle());
        return result;
    }

    public Inquiry find(Long inquiryId) {
        Inquiry result = inquiryDomainRepository.findById(inquiryId)
                                              .orElseThrow(() -> new DomainException("문의를 찾을 수 없습니다. id : " + inquiryId));
        return result;
    }

    public Inquiry update(Long id, Inquiry updateInquiry) {

        Inquiry result = inquiryDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("문의를 찾을 수 없습니다. id : " + id))
                .edit(updateInquiry);

        log.info("문의 수정. id : {}, name: {}", result.getId(), result.getTitle());
        return result;
    }
}
