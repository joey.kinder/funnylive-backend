package com.funnyland.funnylive.domain.point;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.funnyland.funnylive.domain.BaseEntity;
import com.funnyland.funnylive.domain.member.Member;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "point_transaction")
public class PointTransaction extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private PointTransactionType transactionType;

    @Enumerated(EnumType.STRING)
    private PointTransactionType.TransactionChange transactionChange;

    private Integer amount = 0;
    private Integer bonusAmount = 0;
    private LocalDate expirationStartDate;
    private LocalDate expirationEndDate;
    private String reason;

    @ManyToOne
    @JoinColumn(name = "member_id")
    private Member member;


    @OneToMany(mappedBy = "savePointTransaction", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PointTransactionDetail> details = new ArrayList<>();


    public PointTransaction(int amount, int bonusAmount, Member member, PointTransactionType transactionType) {
        this.transactionType = transactionType;
        this.transactionChange = transactionType.getChange();
        this.amount = amount;
        this.bonusAmount = bonusAmount;
        this.member = member;
    }

    public PointTransaction(int amount, Member member, PointTransactionType transactionType) {
        this.transactionType = transactionType;
        this.transactionChange = transactionType.getChange();
        this.amount = amount;
        this.member = member;
    }

    public PointTransaction(int amount, int bonusAmount, LocalDate expirationStartDate, LocalDate expirationEndDate, Member member, PointTransactionType transactionType) {
        this.transactionType = transactionType;
        this.transactionChange = transactionType.getChange();
        this.amount = amount;
        this.bonusAmount = bonusAmount;
        this.expirationStartDate = expirationStartDate;
        this.expirationEndDate = expirationEndDate;
        this.member = member;
    }

    public void createDetail(int amount, Member member, PointTransaction pointTransaction, PointTransactionType transactionType) {
        if(this.details == null) {
            this.details = new ArrayList<>();
        }

        PointTransactionDetail detail = PointTransactionDetail.builder()
                                                              .amount(amount)
                                                              .pointTransaction(pointTransaction)
                                                              .savePointTransaction(this)
                                                              .member(member)
                                                              .transactionType(transactionType)
                                                              .transactionChange(transactionType.getChange())
                                                              .build();
        this.details.add(detail);
    }

    /**
     * 포인트 사용 처리
     *
     * @param useAmount 사용할 포인트 수
     */
    @Deprecated
    public void usePoints(int useAmount) {
//        int totalPoints = this.amount + this.bonusAmount;
//        if (this.usedAmount + useAmount > totalPoints) {
//            throw new IllegalArgumentException("Insufficient points in transaction");
//        }
//        this.usedAmount += useAmount;
    }

    /**
     * 전체 포인트 사용 처리
     */
    @Deprecated
    public void useAll() {
//        this.usedAmount = this.amount + this.bonusAmount;
    }
}
