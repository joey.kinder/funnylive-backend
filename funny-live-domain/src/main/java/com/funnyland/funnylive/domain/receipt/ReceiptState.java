package com.funnyland.funnylive.domain.receipt;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public enum ReceiptState {
    COMPLETE("결제완료"),
    PENDING("결제대기"),
    FAILED("결제실패");

    private String name;
}
