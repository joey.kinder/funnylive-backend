package com.funnyland.funnylive.domain.inquiry;

import java.util.List;

import com.funnyland.funnylive.DomainException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class FrequentlyInquiryDomainService {

    private final FrequentlyInquiryDomainRepository frequentlyInquiryDomainRepository;

    public FrequentlyInquiry save(FrequentlyInquiry frequentlyInquiry) {
        FrequentlyInquiry result = frequentlyInquiryDomainRepository.save(frequentlyInquiry);
        log.info("자주 묻는 질문 저장. id : {}, question: {}", result.getId(), result.getQuestion());
        return result;
    }

    public List<FrequentlyInquiry> findAll() {
        List<FrequentlyInquiry> findFrequentlyInquiries = frequentlyInquiryDomainRepository.findAll();
        return findFrequentlyInquiries;
    }

    public FrequentlyInquiry update(Long frequentlyInquiryId, FrequentlyInquiry updateFrequentlyInquiry) {
        FrequentlyInquiry result = frequentlyInquiryDomainRepository.findById(frequentlyInquiryId)
                                                                                .orElseThrow(() -> new DomainException("자주묻는 질문을 찾을 수 없습니다. id : " + frequentlyInquiryId)).edit(updateFrequentlyInquiry);
        log.info("자주 묻는 질문 수정. id : {}, question: {}", result.getId(), result.getQuestion());

        return result;
    }


    public void delete(Long frequentlyInquiryId) {
        frequentlyInquiryDomainRepository.deleteById(frequentlyInquiryId);

        log.info("자주 묻는 질문 삭제. id : {}, question: {}", frequentlyInquiryId);
    }
}
