package com.funnyland.funnylive.domain.inquiry.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;


@Getter
@Builder
@AllArgsConstructor
public class UploadDto {
    private String originalName;
    private String fileName;
    private String filePath;
    private String extension;
}
