package com.funnyland.funnylive.domain.member;

import java.util.List;

import com.funnyland.funnylive.DomainException;
import com.funnyland.funnylive.domain.cranegame.CraneGame;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@RequiredArgsConstructor
@Service
public class AdminMemberDomainService {


    private final AdminMemberDomainRepository adminMemberDomainRepository;

    public AdminMember find(Long memberId) {
        AdminMember result = adminMemberDomainRepository.findById(memberId)
                                              .orElseThrow(() -> new DomainException("어드민 사용자를 찾을 수 없습니다. id : " + memberId));
        return result;
    }
    public List<AdminMember> list() {
        List<AdminMember> result = adminMemberDomainRepository.findAll();
        return result;
    }

    public AdminMember save(AdminMember member) {
        return adminMemberDomainRepository.save(member);
    }

    public AdminMember update(Long id, AdminMember updateAdminMember) {

        AdminMember result = adminMemberDomainRepository.findById(id)
                .orElseThrow(() -> new DomainException("어드민 사용자를 찾을 수 없습니다. id : " + id))
                .edit(updateAdminMember);

        log.info("어드민 사용자 수정. id : {}, email: {}", result.getId(), result.getEmail());
        return result;
    }
}
