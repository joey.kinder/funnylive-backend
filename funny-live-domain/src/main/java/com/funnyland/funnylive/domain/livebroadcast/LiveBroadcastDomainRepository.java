package com.funnyland.funnylive.domain.livebroadcast;

import org.springframework.data.jpa.repository.JpaRepository;


public interface LiveBroadcastDomainRepository extends JpaRepository<LiveBroadcast, Long> {
}
