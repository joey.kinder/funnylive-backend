package com.funnyland.funnylive.domain.member;

import lombok.Getter;


@Getter
public class UserProfile {

    private final String oauthId;
    private final String userId;
    private final String name;
    private final String profileImageUrl;
    private final OAuthProvider provider;

    public UserProfile(String oauthId, String name, String userId, String profileImageUrl, OAuthProvider provider) {
        this.oauthId = oauthId;
        this.name = name;
        this.userId = userId;
        this.profileImageUrl = profileImageUrl;
        this.provider = provider;
    }

    public Member toMember() {
        return Member.builder()
                .oauthId(oauthId)
                .nickName(name)
                .email(userId)
                .role(Role.USER)
                .oAuthProvider(provider)
                .build();
    }

}