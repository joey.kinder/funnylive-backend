package com.funnyland.funnylive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Profile("test")
@SpringBootApplication
@EnableJpaAuditing
public class FunnyLiveDomainApplication {

    public static void main(String[] args) {
        SpringApplication.run(FunnyLiveDomainApplication.class, args);
    }

}
