package com.funnyland.funnylive;

import lombok.Getter;


public class DomainException extends RuntimeException {

    @Getter
    private final String msg;

    public DomainException(String msg) {
        super(msg);
        this.msg = msg;
    }

    /**
     * @param <T>     the object type
     * @param obj     the object
     * @param message error 메세지
     *
     * @throws DomainException obj 가 null 일경우 발생
     */
    public static <T> void requireNonNull(T obj, String message) {
        if (obj == null) {
            throw new DomainException(message);
        }
    }
}
